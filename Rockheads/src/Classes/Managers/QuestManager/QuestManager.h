#include "../../../PrecompiledHeaders/stdafx.h"
#ifndef QUEST_MANAGER_H
#define QUEST_MANAGER_H

#include "../../Questing/Quest/Quest.h"

class QuestManager
{
private:
  // Factory stuff.
  typedef std::map<string,Quest*> QuestMap;
  
  // Singleton stuff.
  static  QuestManager* instance;
  QuestManager();
  ~QuestManager();

  // Quest creation and storage stuff.
  int     numQuests;
  Quest** quests;
  QuestMap questMap;

public:
  static  QuestManager* getInstance();


  void    initialise();
  Quest*  getQuest(int _quest);
  void    release();

  vector<Quest*>* loadQuestData();

  bool registerQuest(const char *_type, Quest* _quest);

  Quest* getQuest(string _name);
  Quest* createQuest(string _name);
}; // end class QuestManager

#endif