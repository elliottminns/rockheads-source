#include "stdafx.h"
#include "QuestManager.h"
#include "../../XMLParser/XMLParser.h"
#include "boost/algorithm/string.hpp"

QuestManager* QuestManager::instance = 0;

QuestManager* QuestManager::getInstance()
{
  if(instance == NULL)
  {
    instance = new QuestManager();
  } // end if

  return instance;
} // QuestManager::getInstance()

QuestManager::QuestManager()
{
  this->initialise();
} // QuestManager::QuestManager()

QuestManager::~QuestManager()
{
  this->release();
  delete instance;
} // Quest::~Quest()

void QuestManager::initialise()
{
  vector<Quest*>* questData = this->loadQuestData();

  this->numQuests = questData->size();
  this->quests = new Quest*[numQuests];
  
  int i=0;
  for(vector<Quest*>::iterator it = questData->begin(); it != questData->end(); ++it)
  {
    this->quests[i] = *it;
    this->registerQuest(this->quests[i]->getName().c_str(),this->quests[i]);
    ++i;
  } // end for

} // QuestManager::init()


Quest* QuestManager::getQuest(int _quest)
{
  return quests[_quest];
} // QuestManager::getQuest(int _quest)


void QuestManager::release()
{
  delete[] quests;
} // QuestManager::release


vector<Quest*>* QuestManager::loadQuestData()
{
  string fileName = "../../data/quest.xml";
  return XMLParser::getInstance()->parseQuestFile(fileName);
} // vector<Quest> QuestManager::loadQuestData()


bool QuestManager::registerQuest(const char *_type, Quest* _quest)
{
  string str = string(_type);
  boost::to_lower(str);
  return this->questMap.insert(QuestMap::value_type(str, _quest)).second;
} // bool QuestManager::registerItem(const char *_type, Quest* _quest)


Quest* QuestManager::getQuest(string _name)
{
  string str = string(_name);
  boost::to_lower(str);
  QuestMap::iterator it = this->questMap.find(string(str));
  if(it != this->questMap.end())
  {
    Quest* quest = (*it).second;
    return quest;
  } // end if
  return NULL;
} // Quest* QuestManager::getQuest(string _name)


Quest* QuestManager::createQuest(string _name)
{
    string str = string(_name);
  boost::to_lower(str);
  QuestMap::iterator it = this->questMap.find(string(str));
  if(it != this->questMap.end())
  {
    Quest* quest = (*it).second;
    return quest->clone();
  } // end if
  return NULL;
} // Quest* QuestManager::createQuest(string _name)