#include "stdafx.h"
#include "EffectManager.h"
#include "../../XMLParser/XMLParser.h"
#include "../../Effects/AbstractEffect/AbstractEffect.h"

EffectManager* EffectManager::instance = NULL;
bool EffectManager::initialisedData = false;


EffectManager::EffectManager()
{
  this->numEffects = 0;
  this->effects = NULL;
} // EffectManager::EffectManager()


EffectManager::~EffectManager()
{
  if(this->effects)
  {
    delete[] this->effects;
  } // end if
} // EffectManager::~EffectManager()


EffectManager& EffectManager::getSingleton()
{
  if(!instance)
  {
    instance = new EffectManager;
  } // end if
  if(!initialisedData)
  {
    instance->initialise();
    initialisedData = true;
  } // end if

  return *instance;
} // EffectManager& EffectManager::getSingleton()


EffectManager* EffectManager::getSingletonPtr()
{
  if(!instance)
  {
    instance = new EffectManager;
  } // end if
  if(!initialisedData)
  {
    instance->initialise();
    initialisedData = true;
  } // end if

  return instance;
} // static EffectManager* EffectManager::getSingletonPtr()


void EffectManager::initialise()
{
  vector<AbstractEffect*>* effectsData = this->loadEffectData();

  this->numEffects = effectsData->size();
  this->effects = new AbstractEffect*[this->numEffects];
  
  int i=0;
  for(vector<AbstractEffect*>::iterator it = effectsData->begin(); it != effectsData->end(); ++it)
  {
    this->effects[i] = *it;
    this->registerEffect(this->effects[i]->getName().c_str(), this->effects[i]);
  } // end for

} // void EffectManager::initialise()


vector<AbstractEffect*>* EffectManager::loadEffectData()
{
  string dataFile = "../../data/effects.xml";
  return XMLParser::getInstance()->parseEffectFile(dataFile);

} // void EffectManager::loadEffectData()


bool EffectManager::registerEffect(const char *_type, AbstractEffect* _effect)
{
  string str = string(_type);
  boost::to_lower(str);
  return this->effectMap.insert(EffectMap::value_type(str, _effect)).second;
} // bool EffectManager::registerEffect(const char *_type, AbstractEffect* _effect)


AbstractEffect* EffectManager::getEffect(string _name)
{
  EffectMap::iterator it = this->effectMap.find(string(_name));
  if(it != this->effectMap.end())
  {
    AbstractEffect* effect = (*it).second;
    return effect;
  } // end if

  return NULL;
} // AbstractEffect* EffectManager::createSkill(string _name)


AbstractEffect* EffectManager::createEffect(string _name)
{
  EffectMap::iterator it = this->effectMap.find(string(_name));
  if(it != this->effectMap.end())
  {
    AbstractEffect* effect = (*it).second;
    return effect->clone();
  } // end if

  return NULL;
} // AbstractEffect* EffectManager::createSkill(string _name)