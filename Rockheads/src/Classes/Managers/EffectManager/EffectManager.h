#include "../../../PrecompiledHeaders/stdafx.h"
#ifndef EFFECT_MANAGER_H
#define EFFECT_MANAGER_H

class AbstractEffect;

class EffectManager
{
private:
  static EffectManager* instance;
  static bool initialisedData;

  EffectManager();
  ~EffectManager();

  int numEffects;
  AbstractEffect** effects;

  // Factory stuff.
  typedef map<string,AbstractEffect*> EffectMap;
  EffectMap effectMap;

public:
  static EffectManager& getSingleton();
  static EffectManager* getSingletonPtr();

  void initialise();
  vector<AbstractEffect*>* loadEffectData();

  // Factory details.
  bool registerEffect(const char *_type, AbstractEffect* _effect);
  AbstractEffect* getEffect(string _name);
  AbstractEffect* createEffect(string _name);

};

#endif