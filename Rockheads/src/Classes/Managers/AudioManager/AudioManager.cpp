#include "stdafx.h"
#include "AudioManager.h"
#include "../../XMLParser/XMLParser.h"

AudioManager* AudioManager::instance = NULL;
bool AudioManager::initialised = false;


AudioManager& AudioManager::getSingleton()
{
  if(!instance)
  {
    instance = new AudioManager;
  } // end if
  if(!initialised)
  {
    instance->initialise();
    initialised = true;
  } // end if

  return *instance;
} // AudioManager& AudioManager::getSingleton()


AudioManager* AudioManager::getSingletonPtr()
{
  if(!instance)
  {
    instance = new AudioManager;
  } // end if
  if(!initialised)
  {
    instance->initialise();
    initialised = true;
  } // end if

  return instance;
} // AudioManager* AudioManager::getSingletonPtr()


AudioManager::AudioManager()
{
  this->sounds = NULL;
  this->numSounds = 0;
} // AudioManager::AudioManager()


AudioManager::~AudioManager()
{

} // AudioManager::~AudioManager()


void AudioManager::initialise()
{
  vector<OgreAL::Sound*>* dataSounds = this->loadSounds();

  this->numSounds = dataSounds->size();
  this->sounds = new OgreAL::Sound*[this->numSounds];

  int i = 0;
  for(vector<OgreAL::Sound*>::iterator it = dataSounds->begin(); 
    it != dataSounds->end(); ++it)
  {
    this->sounds[i] = *it;
    ++i;
  } // end for

} // void AudioManager::initialise()


void AudioManager::release()
{
  for(int i=0;i<this->numSounds;i++)
  {
    if(this->sounds[i])
    {
      delete this->sounds[i];
      this->sounds[i] = NULL;
    } // end if
  } // end for

  if(this->sounds)
  {
    delete this->sounds;
    this->sounds = NULL;
  } // end if
} // void AudioManager::release()


void AudioManager::playSound(string _name)
{
  if(OgreAL::SoundManager::getSingleton().hasSound(_name))
  {
    if(!OgreAL::SoundManager::getSingleton().getSound(_name)->isLooping())
    {
      OgreAL::SoundManager::getSingleton().getSound(_name)->pause();
      OgreAL::SoundManager::getSingleton().getSound(_name)->play();
    } // end if
    else if(!OgreAL::SoundManager::getSingleton().getSound(_name)->isPlaying())
    {
      OgreAL::SoundManager::getSingleton().getSound(_name)->play();
    } // end else if
  } // end if
} // void AudioManager::playSound(string _name)


void AudioManager::stopSound(string _name)
{
  if(OgreAL::SoundManager::getSingleton().hasSound(_name))
  {
    OgreAL::SoundManager::getSingleton().getSound(_name)->stop();
  } // end if
} // void AudioManager::stopSound(string _name)


vector<OgreAL::Sound*>* AudioManager::loadSounds()
{
  string dataFile = "../../data/sounds.xml";
  return XMLParser::getInstance()->parseSoundFile(dataFile);
} // vector<OgreAL::Sound*>* AudioManager::loadSounds()