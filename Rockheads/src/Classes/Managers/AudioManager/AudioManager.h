#include "../../../PrecompiledHeaders/stdafx.h"
#ifndef AUDIO_MANAGER_H
#define AUDIO_MANAGER_H


class AudioManager
{
private:
  static AudioManager* instance;
  static bool initialised;

  int numSounds;
  OgreAL::Sound** sounds;


public:
  static AudioManager& getSingleton();
  static AudioManager* getSingletonPtr();

  AudioManager();
  ~AudioManager();

  void initialise();
  void release();

  void playSound(string _name);
  void stopSound(string _name);

  vector<OgreAL::Sound*>* loadSounds();


};

#endif
