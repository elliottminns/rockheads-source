#include "../../../PrecompiledHeaders/stdafx.h"
#ifndef STATIC_ENTITY_MANAGER_H
#define STATIC_ENTITY_MANAGER_H

#include <iostream>
#include <vector>
#include "../../GameEntities/StaticEntities/StaticEntity/StaticEntity.h"

class DroppedObject;
class Vendor;
class NPC;

using namespace std;

class StaticEntityManager
{
private:
  StaticEntityManager();
  ~StaticEntityManager();

  static StaticEntityManager* instance;
  static bool initialisedData;

  int                   numEntities;
  vector<StaticEntity*>*  entities;
  vector<DroppedObject*>* droppedObjects;

  // NPC.
  int numNPCS;
  NPC** npcs;
  // NPC factory.
  typedef std::map<string,NPC*> NPCMap;
  NPCMap npcMap;

  bool initialised;

public:

  static StaticEntityManager* getInstance();

  void          initialise();
  void          release();
  void          releaseEntities();
  void          initialiseEntities();
  void          calculateEntitiesY();
  void          update(double _timeSinceLastFrame);
  vector<NPC*>* loadNPCFile();


  void          createDroppedObject(DropTable* _table, Ogre::SceneManager* _sceneManager, AbstractEntity* _dropper);
  DroppedObject* findClosestDrop(Ogre::Vector3 _position);
  StaticEntity*  findClosestEntity(Ogre::Vector3 _positon);

  bool registerNPC(const char *_type, NPC* _npc);
  NPC* getNPC(string _name);
  NPC* createNPC(string _name);
  vector<StaticEntity*>* getEntities() const;
  void pushNPCOntoVector(NPC* _npc);

  void registerDropObjectIDs();
}; // end class StaticEntityManager

#endif