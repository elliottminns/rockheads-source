#include "stdafx.h"
#include "StaticEntityManager.h"
#include "../../GameEntities/StaticEntities/DroppedObject/DroppedObject.h"
#include "../../GameEntities/StaticEntities/NPC/NPC.h"
#include "../../GameEntities/StaticEntities/NPC/Vendor/Vendor.h"
#include "../../XMLParser/XMLParser.h"
#include "../CollisionManager/QueryFlags.h"
#include "../QuestManager/QuestManager.h"

StaticEntityManager* StaticEntityManager::instance = 0;
bool StaticEntityManager::initialisedData = false;


StaticEntityManager* StaticEntityManager::getInstance()
{
  if(!instance)
  {
    instance = new StaticEntityManager();
  } // end if
  if(!initialisedData)
  {
    initialisedData = true;
    instance->initialise();
  } // end if

  return instance;
} // StaticEntityManager::getInstance()


StaticEntityManager::StaticEntityManager()
{

} // StaticEntityManager::StaticEntityManager()


StaticEntityManager::~StaticEntityManager()
{
  this->release();
} // StaticEntityManager::~StaticEntityManager()


void StaticEntityManager::initialise()
{
  this->droppedObjects = new vector<DroppedObject*>;
  for(int i=0;i<15;i++)
  {
    this->droppedObjects->push_back(new DroppedObject());
  } // end for
  
  this->entities = new vector<StaticEntity*>;

  // Put the npc data into the NPC array.
  vector<NPC*>* npcData = this->loadNPCFile();
  this->numNPCS = npcData->size();
  this->npcs = new NPC*[this->numNPCS];

  int i=0;
  for(vector<NPC*>::iterator it = npcData->begin(); it!= npcData->end(); ++it)
  {
    this->npcs[i] = *it;
    this->npcs[i]->setQuest(QuestManager::getInstance()->getQuest(this->npcs[i]->getQuestName()));
    this->registerNPC(this->npcs[i]->getName().c_str(), this->npcs[i]);
    ++i;
  } // end for


} // StaticEntityManager::init()


void StaticEntityManager::release()
{
  for(vector<DroppedObject*>::iterator it = this->droppedObjects->begin(); it != this->droppedObjects->end(); ++it)
  {
    DroppedObject* object = *it;
    delete object;
    object = NULL;
  } // end for

  delete this->droppedObjects;
  delete[] this->npcs;
  this->npcs = NULL;

} // StaticEntityManager::release()


void StaticEntityManager::releaseEntities()
{
  for(vector<StaticEntity*>::iterator it = this->entities->begin(); it != this->entities->end(); ++it)
  {
    StaticEntity* staticEntity = *it;
    staticEntity->release();
    delete staticEntity;
  } // end for

  entities->clear();

} // void StaticEntityManager::releaseEntities()


void StaticEntityManager::initialiseEntities()
{
  for(vector<StaticEntity*>::iterator it = this->entities->begin(); it != this->entities->end(); ++it)
  {
    StaticEntity* entity = *it;
    entity->init();
  } // for(vector<StaticEntity*>::iterator it = this->droppedObjects->begin(); it != this->droppedObjects->end(); ++it)
} // void  StaticEntityManager::initialiseEntities()


void StaticEntityManager::update(double _timeSinceLastFrame)
{
  for(vector<StaticEntity*>::iterator it = this->entities->begin(); it != this->entities->end(); ++it)
  {
    StaticEntity* entity = *it;
    if(entity->getActive())
    {
      entity->update(_timeSinceLastFrame);
    } // end if
  } // for(vector<StaticEntity*>::iterator it = this->droppedObjects->begin(); it != this->droppedObjects->end(); ++it)
} // StaticEntityManager::update(double _timeSinceLastFrame)


void StaticEntityManager::calculateEntitiesY()
{
  for(vector<StaticEntity*>::iterator it = this->entities->begin(); it != this->entities->end(); ++it)
  {
    StaticEntity* entity = *it;
    if(entity->getActive())
    {
      entity->calculateYPosition();
    } // end if
  } // for(vector<StaticEntity*>::iterator it = this->droppedObjects->begin(); it != this->droppedObjects->end(); ++it)
} // void StaticEntityManager::calculateEntiriesY()


void StaticEntityManager::createDroppedObject(DropTable* _table, Ogre::SceneManager* _sceneManager, AbstractEntity* _dropper)
{
  for(vector<DroppedObject*>::iterator it = this->droppedObjects->begin(); it != this->droppedObjects->end(); ++it)
  {
    DroppedObject* obj = *it;
    if(!obj->getActive())
    {
      obj->setDropTable(_table);
      obj->loadEntity(_sceneManager->createEntity(_dropper->getEntity()->getName() + "Drop", "dropped_item.mesh"));
      obj->setNode(_sceneManager->getRootSceneNode()->createChildSceneNode(_dropper->getNode()->getName() + "Drop"));
      obj->attachEntityToNode();
  
      int radius = 8;
      int angle = rand()%360;
      int x = (radius * sin(angle * 180/M_PI)) + _dropper->getPosition().x;
      int z = (radius * cos(angle * 180/M_PI)) + _dropper->getPosition().z;
      int y = _dropper->getPosition().y + 8; // Make sure the drop falls above the floor.
      obj->setPosition(Ogre::Vector3(x, _dropper->getPosition().y, z));
      CollisionManager::getSingletonPtr()->setHeightAdjust((obj->getDimensions().y/2)*4);
      CollisionManager::getSingletonPtr()->calculateY(obj->getNode(),false,false,1.0f,STATIC_FLOOR);
      obj->setPosition(obj->getNode()->getPosition());
      obj->getNode()->scale(2.5,2.5,2.5);
      obj->setActive(true);
      return;
    } // end if
  } // end for

  // When no inactive was found.
  DroppedObject* object = new DroppedObject(_table,_sceneManager,_dropper);
  this->droppedObjects->push_back(object);
  
} // void StaticEntityManager::createDroppedObject(DropTable* _table, Ogre::SceneManager* _sceneManager, AbstractEntity* _dropper)


DroppedObject* StaticEntityManager::findClosestDrop(Ogre::Vector3 _position)
{
  int closestDistance = INT_MAX;
  DroppedObject* object = NULL;

  for(vector<DroppedObject*>::iterator it = this->droppedObjects->begin(); it != this->droppedObjects->end(); ++it)
  {
    DroppedObject* currentObj = *it;

    if(currentObj->getActive())
    {
      int distance = currentObj->getPosition().squaredDistance(_position);
      if(distance < closestDistance)
      {
        closestDistance = distance;
        object = currentObj;
      } // end if
    } // end if
  } // end for

  return object;

} // DroppedObject* StaticEntityManager::findClosestDrop(Ogre::Vector3 _position)


StaticEntity* StaticEntityManager::findClosestEntity(Ogre::Vector3 _position)
{
  int closestDistance = INT_MAX;
  StaticEntity* entity = NULL;

  for(vector<StaticEntity*>::iterator it = this->entities->begin(); it != this->entities->end(); ++it)
  {
    StaticEntity* currentEntity = *it;

    if(currentEntity->getActive())
    {
      int distance = currentEntity->getPosition().squaredDistance(_position);
      if(distance < closestDistance)
      {
        closestDistance = distance;
        entity = currentEntity;
      } // end if
    } // end if
  } // end for

  StaticEntity* droppedObject = this->findClosestDrop(_position);

  if(droppedObject)
  {
    if(droppedObject->getPosition().squaredDistance(_position) < closestDistance)
    {
      entity = droppedObject;
    } // end if
  } // end if

  return entity;
} // StaticEntity* StaticEntityManager::findClosestEntity(Ogre::Vector3 _positon)


vector<NPC*>* StaticEntityManager::loadNPCFile()
{
  string dataFile = "../../data/npc.xml";
  return XMLParser::getInstance()->parseNPCFile(dataFile);
} // vector<StaticEntity*>* StaticEntityManager::loadNPCFile()


bool StaticEntityManager::registerNPC(const char *_type, NPC* _npc)
{
  string str = string(_type);
  boost::to_lower(str);
  return this->npcMap.insert(NPCMap::value_type(str, _npc)).second;
} // bool StaticEntityManager::registerNPC(const char *_type, NPC* _npc)


NPC* StaticEntityManager::getNPC(string _name)
{
  string str = string(_name);
  boost::to_lower(str);
  NPCMap::iterator it = this->npcMap.find(string(str));
  if(it != this->npcMap.end())
  {
    NPC* npc = (*it).second;
    return npc;
  } // end if
  return NULL;
} // NPC* StaticEntityManager::getNPC(string _name)


NPC* StaticEntityManager::createNPC(string _name)
{
  string str = string(_name);
  boost::to_lower(str);
  NPCMap::iterator it = this->npcMap.find(string(str));
  if(it != this->npcMap.end())
  {
    NPC* npc = (*it).second;
    NPC* newNPC = npc->clone();
    this->entities->push_back(newNPC);
    return newNPC;
  } // end if
  return NULL;
} // NPC* StaticEntityManager::createNPC(string _name)


vector<StaticEntity*>* StaticEntityManager::getEntities() const
{
  return this->entities;
} // vector<StaticEntity*>* StaticEntityManager::getEntities() const


void StaticEntityManager::pushNPCOntoVector(NPC* _npc)
{
  this->entities->push_back(_npc);
} // void StaticEntityManager::pushNPCOntoVector(NPC* _npc)


void StaticEntityManager::registerDropObjectIDs()
{
  for(vector<DroppedObject*>::iterator it = this->droppedObjects->begin(); it != this->droppedObjects->end(); ++it)
  {
    DroppedObject* currentObj = *it;
    currentObj->setEntityIDNumber(MessageDispatcher::getInstance()->assignIDNumber(currentObj));
  } // end for
} // void StaticEntityManager::registerDropObjectIDs()