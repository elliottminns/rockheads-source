#include "stdafx.h"
#include "DynamicEntityManager.h"


DynamicEntityManager* DynamicEntityManager::instance = NULL;


DynamicEntityManager* DynamicEntityManager::getInstance()
{
  if(instance == NULL)
  {
    instance = new DynamicEntityManager();
  } // end if

  return instance;
} // DynamicEntityManager::getInstance()


void DynamicEntityManager::init()
{

} // DynamicEntityManager::init()


void DynamicEntityManager::updateEntities()
{

} // DynamicEntityManager::updateEntities()


void DynamicEntityManager::render()
{

} // DynamicEntityManager::render()


void DynamicEntityManager::release()
{

} // DynamicEntityManager::release()


void DynamicEntityManager::createEntities()
{

} // DynamicEntityManager::createEntities()


DynamicEntity* DynamicEntityManager::getEntity(int _entity)
{
  return this->activeEntities[_entity];
} // DynamicEntityManager::getEntity(int _entity)