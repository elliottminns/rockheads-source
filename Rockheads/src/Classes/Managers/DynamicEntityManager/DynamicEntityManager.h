#include "../../../PrecompiledHeaders/stdafx.h"
#ifndef DYNAMIC_ENTITY_MANAGER_H
#define DYNAMIC_ENTITY_MANAGER_H

#include "../../GameEntities/DynamicEntities/DynamicEntity/DynamicEntity.h"

class DynamicEntityManager
{
public:
  static          DynamicEntityManager* getInstance();
  void            init();
  void            updateEntities();
  void            render();
  void            release();
  void            createEntities();
  DynamicEntity*  getEntity(int _entity);

private:
  static                  DynamicEntityManager* instance;
  vector<DynamicEntity*>  activeEntities;
  DynamicEntityManager(){};
  ~DynamicEntityManager(){};
}; // end class DynamicEntityManager

#endif