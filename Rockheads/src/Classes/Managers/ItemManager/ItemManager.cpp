#include "stdafx.h"
#include "ItemManager.h"
#include "../../GameObjects/Items/Weapon/Weapon.h"
#include "../../GameObjects/Items/Armour/Armour.h"
#include "../../GameObjects/Items/Item/Item.h"
#include "../../GameObjects/Items/CraftingItem/CraftingItem.h"
#include "../../GameObjects/Items/ConsumableItem/ConsumableItem.h"
#include "../../GameObjects/DropTable/DropTable.h"
#include "../../GameObjects/Items/Ammo/Ammo.h"
#include "../../XMLParser/XMLParser.h"
#include "boost\algorithm\string.hpp"
#include "CEGUI.h"

ItemManager* ItemManager::instance = 0;
bool ItemManager::initialisedData = false;

ItemManager* ItemManager::getInstance()
{
    if(instance == 0)
    {
      instance = new ItemManager();
    } // end if
    if(!initialisedData)
    {
      instance->initialise();
      initialisedData = true;
    } // end if

    return instance;
} // ItemManager::getInstance()


ItemManager::ItemManager()
{
  this->initialised = false;
} // ItemManager::ItemManager()


ItemManager::~ItemManager()
{
  this->release();
} // ItemManager::ItemManager()


void ItemManager::initialise()
{
  if(!this->initialised)
  {
    this->initialised = true;

    // Load the items.
    vector<Weapon*>* weaponData = this->loadWeapons();
    vector<Armour*>* armourData = this->loadArmour();
    vector<Item*>* itemData     = this->loadItems();
    vector<CraftingItem*>* craftingItemData = this->loadCraftingItems();
    vector<ConsumableItem*>* consumableItemData = this->loadConsumableItems();
    vector<Ammo*>* ammoData = this->loadAmmo();

    // Get the count of the number of items.
    this->numItems = weaponData->size() + armourData->size() + 
      itemData->size() + craftingItemData->size() + consumableItemData->size() + ammoData->size();

    this->items = new AbstractItem*[this->numItems];

    int i=0;
    // Now load the items into the array.
    for(vector<Weapon*>::iterator it = weaponData->begin(); it != weaponData->end(); ++it)
    {
      this->items[i] = *it;
      this->registerItem(this->items[i]->getName().c_str(),this->items[i]);
      CEGUI::ImagesetManager::getSingleton().createFromImageFile(this->items[i]->getImageName().c_str(), 
        this->items[i]->getImageName().c_str());
      ++i;
      
    } // end for
    for(vector<Armour*>::iterator it = armourData->begin(); it != armourData->end(); ++it)
    {
      this->items[i] = *it;
      this->registerItem(this->items[i]->getName().c_str(),this->items[i]);
      CEGUI::ImagesetManager::getSingleton().createFromImageFile(this->items[i]->getImageName().c_str(), 
        this->items[i]->getImageName().c_str());
      ++i;
    } // end for
    for(vector<Item*>::iterator it = itemData->begin(); it != itemData->end(); ++it)
    {
      this->items[i] = *it;
      this->registerItem(this->items[i]->getName().c_str(),this->items[i]);
      CEGUI::ImagesetManager::getSingleton().createFromImageFile(this->items[i]->getImageName().c_str(), 
        this->items[i]->getImageName().c_str());
      ++i;
    } // end for
    for(vector<CraftingItem*>::iterator it = craftingItemData->begin(); it != craftingItemData->end(); ++it)
    {
      this->items[i] = *it;
      this->registerItem(this->items[i]->getName().c_str(),this->items[i]);
      CEGUI::ImagesetManager::getSingleton().createFromImageFile(this->items[i]->getImageName().c_str(), 
        this->items[i]->getImageName().c_str());
      ++i;
    } // end for
    for(vector<ConsumableItem*>::iterator it = consumableItemData->begin(); it != consumableItemData->end(); ++it)
    {
      this->items[i] = *it;
      this->registerItem(this->items[i]->getName().c_str(),this->items[i]);
      CEGUI::ImagesetManager::getSingleton().createFromImageFile(this->items[i]->getImageName().c_str(), 
        this->items[i]->getImageName().c_str());
      ++i;
    } // end for
    for(vector<Ammo*>::iterator it = ammoData->begin(); it != ammoData->end(); ++it)
    {
      this->items[i] = *it;
      this->registerItem(this->items[i]->getName().c_str(),this->items[i]);
      CEGUI::ImagesetManager::getSingleton().createFromImageFile(this->items[i]->getImageName().c_str(), 
        this->items[i]->getImageName().c_str());
      ++i;
    } // for(vector<Ammo*>::iterator it = ammoData->begin(); it != ammoData->end(); ++it)

    // Load the drop tables.
    vector<DropTable*>* dropTablesData = this->loadDropTables();
    this->numDropTables = dropTablesData->size();
    this->dropTables = new DropTable*[numDropTables];
    i=0;

    // Register the drop tables by mob name.
    for(vector<DropTable*>::iterator it = dropTablesData->begin(); it != dropTablesData->end(); ++it)
    {
      this->dropTables[i] = *it;
      vector<string>* mobs = dropTables[i]->getMobs();
      for(vector<string>::iterator it = mobs->begin(); it!= mobs->end(); ++it)
      {
        string mobName = *it;
        this->registerDropTable(mobName.c_str(), this->dropTables[i]);
      } // end for
      ++i;
    } // end for
  } // end if(!initialised)

} // ItemManager::init()


vector<Weapon*>* ItemManager::loadWeapons()
{
  string weaponFile = "../../data/weapons.xml";
  return XMLParser::getInstance()->parseWeaponFile(weaponFile);
} // void ItemManager::loadWeapons()


vector<Armour*>* ItemManager::loadArmour()
{
  string armourFile = "../../data/armour.xml";
  return XMLParser::getInstance()->parseArmourFile(armourFile);
} // vector<Armour*>* ItemManager::loadArmour()


vector<Item*>* ItemManager::loadItems()
{
  string itemFile = "../../data/misc_items.xml";
  return XMLParser::getInstance()->parseItemFile(itemFile);
} // vector<Items*>* ItemManager::loadItems()


vector<CraftingItem*>* ItemManager::loadCraftingItems()
{
  string craftingItemFile = "../../data/crafting_items.xml";
  return XMLParser::getInstance()->parseCraftingFile(craftingItemFile);
} // vector<CraftingItem*>* ItemManager::loadCraftingItems()


vector<ConsumableItem*>* ItemManager::loadConsumableItems()
{
  string consumableItemFile = "../../data/consumable_items.xml";
  return XMLParser::getInstance()->parseConsumableFile(consumableItemFile);
} // vector<ConsumableItem*>* ItemManager::loadConsumableItems()


std::vector<DropTable*>* ItemManager::loadDropTables()
{
  string dropTableItemFile = "../../data/droptable.xml";
  return XMLParser::getInstance()->parseDropTableFile(dropTableItemFile);
} // std::vector<DropTable*>* ItemManager::loadDropTables()


std::vector<Ammo*>* ItemManager::loadAmmo()
{
  string ammoItemFile = "../../data/ammo.xml";
  return XMLParser::getInstance()->parseAmmoFile(ammoItemFile);
} // std::vector<Ammo*>* ItemManager::loadAmmo()


void ItemManager::release()
{

} // ItemManager::release()


bool ItemManager::registerItem(const char *_type, AbstractItem* _item)
{
  string str = string(_type);
  boost::to_lower(str);
  return this->itemMap.insert(ItemMap::value_type(str, _item)).second;
} // bool ItemManager::registerItem(const char *_type, AbstractItem* _item)


bool ItemManager::registerDropTable(const char* _type, DropTable *_dropTable)
{
  string str = string(_type);
  boost::to_lower(str);
  return this->dropTableMap.insert(DropTableMap::value_type(str, _dropTable)).second;
} // bool ItemManager::registerDropTable(const char* _type, DropTable *_dropTable)


AbstractItem* ItemManager::getItem(string _name)
{
  string str = string(_name);
  boost::to_lower(str);
  ItemMap::iterator it = this->itemMap.find(string(str));
  if(it != this->itemMap.end())
  {
    AbstractItem* item = (*it).second;
    return item;/*->clone();*/
  } // end if
  return NULL;
} // AbstractItem* ItemManager::createItem(string _name)


AbstractItem* ItemManager::createItem(string _name)
{
  string str = string(_name);
  boost::to_lower(str);
  ItemMap::iterator it = this->itemMap.find(string(str));
  if(it != this->itemMap.end())
  {
    AbstractItem* item = (*it).second;
    return item->clone();
  } // end if
  return NULL;
} // AbstractItem* ItemManager::createItem(string _name)



DropTable* ItemManager::getDropTable(string _name)
{
  string str = string(_name);
  boost::to_lower(str);
  DropTableMap::iterator it = this->dropTableMap.find(string(str));
  if(it != this->dropTableMap.end())
  {
    DropTable* dropTable = (*it).second;
    return dropTable;
  } // end if
  
  return NULL;
} // AbstractItem* ItemManager::createDropTable(String _dropTable)


int ItemManager::getNumItems()
{
  return this->numItems;
} // int ItemManager::getNumItems()


AbstractItem** ItemManager::getItems()
{
  return this->items;
} // AbstractItem** ItemManager::getItems()
