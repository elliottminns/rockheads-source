#include "../../../PrecompiledHeaders/stdafx.h"
#ifndef ITEM_MANAGER_H
#define ITEM_MANAGER_H

#include "../../GameObjects/Items/AbstractItem/AbstractItem.h"
#include <iostream>
#include <map>

class Weapon;
class Armour;
class Item;
class CraftingItem;
class ConsumableItem;
class Ammo;
class DropTable;

using namespace std;

class ItemManager
{
private:
  // Factory stuff.
  typedef std::map<string,AbstractItem*> ItemMap;
  typedef std::map<string,DropTable*> DropTableMap;

  // Singleton methods.
  static  ItemManager* instance;
  static bool initialisedData;

  ItemManager();
  ~ItemManager();

  int numItems;
  int numDropTables;
  bool initialised;

  AbstractItem** items;
  DropTable** dropTables;

  ItemMap itemMap;
  DropTableMap dropTableMap;

public:

  bool registerItem(const char *_type, AbstractItem* _item);
  bool registerDropTable(const char *_type, DropTable* _dropTable);

  static ItemManager* getInstance();

  void initialise();

  std::vector<Weapon*>* loadWeapons();
  std::vector<Armour*>* loadArmour();
  std::vector<Item*>*  loadItems();
  std::vector<CraftingItem*>* loadCraftingItems();
  std::vector<ConsumableItem*>* loadConsumableItems();
  std::vector<DropTable*>* loadDropTables();
  std::vector<Ammo*>* loadAmmo();
  
  void release();

  AbstractItem* getItem(string _name);
  AbstractItem* createItem(string _name);
  DropTable*    getDropTable(string _name);  
  int getNumItems();
  AbstractItem** getItems();

}; // end class ItemManager

#endif