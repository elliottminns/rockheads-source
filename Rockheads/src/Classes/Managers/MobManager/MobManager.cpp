#include "stdafx.h"
#include "MobManager.h"
#include "../../XMLParser/XMLParser.h"
#include "boost\algorithm\string.hpp"
#include "../../Managers/CollisionManager/QueryFlags.h"

MobManager* MobManager::instance = 0;
bool MobManager::initialisedData = false;

MobManager* MobManager::getInstance()
{
  if(instance == 0)
  {
    instance = new MobManager();
  } // end if
  if(!initialisedData)
  {
    initialisedData = true;
    instance->initialise();
  } // end if

  return instance;
} // MobManager::getInstance()


MobManager::MobManager()
{
  this->initialised = false;
} // MobManager::MobManager()


MobManager::~MobManager()
{
  this->release();
} // MobManager::~MobManager()


void MobManager::initialise()
{
  if(!this->initialised)
  { // Load the items.
    vector<Mob*>* mobData  = this->createMobs();
    vector<Boss*>* bossData = this->createBosses();

    // Get the number of mobs that are in the vector.
    int mobCount  = mobData->size();
    int bossCount = bossData->size(); 

    this->numMob = mobCount+ bossCount;
    
    // Create the dynamic array.
    this->mobTemplates = new Mob*[numMob];

    int i=0;
    // Now load the items into the array.
    for(vector<Mob*>::iterator it = mobData->begin(); it != mobData->end(); ++it)
    {
      this->mobTemplates[i] = *it;
      this->registerMob(this->mobTemplates[i]->getName().c_str(),this->mobTemplates[i]);
      ++i;
    } // end for
    for(vector<Boss*>::iterator it = bossData->begin(); it != bossData->end(); ++it)
    {
      this->mobTemplates[i] = *it;
      this->registerMob(this->mobTemplates[i]->getName().c_str(),this->mobTemplates[i]);
      ++i;
    } // end for

    // Create the mob vector.
    this->mobs = new vector<Mob*>;

    this->initialised = true;

  } // end if(!this->initialised)
} // MobManager::initialise()

void MobManager::initialiseMobs()
{
  for(vector<Mob*>::iterator it = this->mobs->begin(); it!=this->mobs->end(); ++it)
  {
    Mob* mob = *it;
    mob->init();
  } // for(vector<Mob*>::iterator it = this->mobs->begin(); it!=this->mobs->end(); ++it)
} // void MobManager::initialiseMobs()


void MobManager::updateMobs(double _timeSinceLastFrame)
{
  for(vector<Mob*>::iterator it = this->mobs->begin(); it!=this->mobs->end(); ++it)
  {
    Mob* mob = *it;
    if(mob->getPosition().squaredDistance(Player::getInstance()->getPosition()) < 250*250)
    {
      mob->update(_timeSinceLastFrame);
    } // end if
  } // for(vector<Mob*>::iterator it = this->mobs->begin(); it!=this->mobs->end(); ++it)
} // MobManager::updateMobs()


void MobManager::calculateInitialY()
{
  for(vector<Mob*>::iterator it = this->mobs->begin(); it!=this->mobs->end(); ++it)
  {
    Mob* mob = *it;
    mob->setPosition(mob->getSpawnPoint());
    mob->calculateYPosition();
  } // for(vector<Mob*>::iterator it = this->mobs->begin(); it!=this->mobs->end(); ++it)
} // void MobManager::calculateInitialY()


vector<Mob*>* MobManager::createMobs()
{
  string mobFile = "../../data/mobs.xml";
  return XMLParser::getInstance()->parseMobFile(mobFile);
} // MobManager::createMobs()


vector<Boss*>* MobManager::createBosses()
{
  string bossFile = "../../data/bosses.xml";
  return XMLParser::getInstance()->parseBossFile(bossFile);
} // MobManager::createBosses()


Mob* MobManager::createMob(string _name)
{
  string str = string(_name);
  boost::to_lower(str);
  MobMap::iterator it = this->mobMap.find(string(str));
  if(it != this->mobMap.end())
  {
    Mob* mob = (*it).second;
    Mob* returnMob = mob->clone();
    return returnMob;
  } // end if
  return NULL;
} // MobManager::createMob(int _mobType)


void MobManager::release()
{
  
} // MobManager::release()


void MobManager::releaseVector()
{
  for(vector<Mob*>::iterator it = this->mobs->begin(); it!=this->mobs->end(); ++it)
  {
    Mob* mob = *it;
    if(mob)
    {
      mob->release();
      delete mob;
      mob = NULL;
    } // end if
  } // for(vector<Mob*>::iterator it = this->mobs->begin(); it!=this->mobs->end(); ++it)
  this->mobs->clear();
} // void MobManager::releaseVector()


bool MobManager::registerMob(const char *_type, Mob* _mob)
{
  string str = string(_type);
  boost::to_lower(str);
  return this->mobMap.insert(MobMap::value_type(str, _mob)).second;
} // bool MobManager::registerMob(const char *_type, Mob* _mob)


void MobManager::addMobToVector(Mob* _mob)
{
  this->mobs->push_back(_mob);
} // void MobManager::addMobToVector(Mob* _mob)


Mob* MobManager::getMobFromVector(int _id)
{
  return NULL;
} // Mob* MobManager::getMobFromVector(int _id)


Mob* MobManager::getClosestMob(Ogre::Vector3 _position)
{
  Mob* closestMob = NULL;
  int distance = INT_MAX;
  for(vector<Mob*>::iterator it = this->mobs->begin(); it != this->mobs->end(); ++it)
  {
    Mob* currentMob = *it;
    if(currentMob->getAlive())
    {
      Ogre::Vector3 mobPosition = currentMob->getPosition();
      int squareDistance = _position.squaredDistance(mobPosition);

      if(squareDistance < 80*80)
      {
        if(CollisionManager::getSingleton().isEntityInLineOfSight(_position, currentMob, MOB))
        {
          
          if(squareDistance < distance)
          {
            closestMob = currentMob;
            distance = squareDistance;
          } // end if
        } // end if
        else
        {
          int x=0;
        } // end if
      } // end if
    } // end if

  } // end for

  return closestMob;
} // Mob* MobManager::getClosestMob(Ogre::Vector3 _position)


int MobManager::getVectorSize()
{
  return this->mobs->size();
} // int MobManager::getVectorSize()


vector<Mob*>* MobManager::getMobVector()
{
  return this->mobs;
} // vector<Mob*>* MobManager::getMobVector()


Mob* MobManager::getMobByID(int _id)
{
  for(vector<Mob*>::iterator it = this->mobs->begin(); it != this->mobs->end(); ++it)
  {
    Mob* mob = *it;

    if(mob->getEntityIDNumber() == _id)
    {
      return mob;
    } // end if
  } // end for

  return NULL;
} // Mob* MobManager::getMobByID(int _id)