#include "../../../PrecompiledHeaders/stdafx.h"
#ifndef MOB_MANAGER_H
#define MOB_MANAGER_H

#include "../../GameEntities/DynamicEntities/Mob/Mob.h"
#include "../../GameEntities/DynamicEntities/Mob/Boss/Boss.h"

class MobManager
{
private:
  typedef std::map<string,Mob*> MobMap;
  static MobManager* instance;
  static bool initialisedData;

  MobManager();
  ~MobManager();
  int           numMob;
  Mob**         mobTemplates;
  vector<Mob*>* mobs;
  bool          initialised;
  MobMap        mobMap;

public:
  // Factory stuff.
  bool registerMob(const char *_type, Mob* _mob);

  static MobManager* getInstance();
  void   initialise();

  void   initialiseMobs();
  void   updateMobs(double _timeSinceLastFrame);
  void   calculateInitialY();

  vector<Mob*>*  createMobs();
  vector<Boss*>* createBosses();

  Mob*   createMob(string _name);
  void   release();
  void   releaseVector();
  void   addMobToVector(Mob* _mob);
  Mob*   getMobFromVector(int _id);
  Mob*   getClosestMob(Ogre::Vector3 _position);
  int    getVectorSize();
  vector<Mob*>* getMobVector();
  Mob* getMobByID(int _id);

}; // end class MobManager

#endif