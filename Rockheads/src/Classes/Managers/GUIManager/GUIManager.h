#include "../../../PrecompiledHeaders/stdafx.h"

#ifndef GUI_MANAGER_H
#define GUI_MANAGER_H

#include "Ogre.h"

class NPC;
class Quest;
class InventorySlot;

class GUIManager : public Ogre::Singleton<GUIManager>
{
private:
  bool dragDropStarted;

public:
  GUIManager();
	~GUIManager();

  string buildInventoryWindow(CEGUI::FrameWindow* _window, CEGUI::UVector2 _size, CEGUI::UVector2 _pos, bool _open);
  string buildCharacterWindow(CEGUI::FrameWindow* _window, CEGUI::UVector2 _size, CEGUI::UVector2 _pos, bool _open);
  string buildJournalWindow(CEGUI::FrameWindow* _window, CEGUI::UVector2 _size, CEGUI::UVector2 _pos, bool _open);
  string buildSkillsWindow(CEGUI::UVector2 _size, CEGUI::UVector2 _pos, bool _open);
  void updateSkillPoints();
  void buildActiveEffectsWindow();
  void buildTargettedEnemyWindow();
  void updateTargettedEnemyWindow(Mob* _mob);
  void updateTargettedEnemyHealth(double _percentage);
  void buildWindowButtons();

  string buildHotBar();
  void addQuestToJournalWindow(Quest* _quest);
  void updateJournal(Quest* _quest);
  void clearJournal();

  string buildActiveQuestWindow(Quest* _quest, NPC* _npc);
  string buildAvailableQuestWindow(Quest* _quest, NPC* _npc);
  string buildCompletedQuestWindow(Quest* _quest, NPC* _npc);
  string buildNoQuestDialogueBox(NPC* _npc);

  string buildActiveVendorQuestWindow(Quest* _quest, NPC* _npc);
  string buildAvailableVendorQuestWindow(Quest* _quest, NPC* _npc);
  string buildNoQuestVendorWindow(NPC* _npc);

  void buildSavePointWindow();
  void renameInventorySlots(int _previous, int _new);

  string buildCharacterDisplay();
  CEGUI::Window* getWindowWithName(string _windowName);
  void updateHealthDisplay();
  void updateStaminaDisplay();
  void updateExperienceDisplay();
  void updateCurrencyAmount();

  string buildShopWindow();
  void destroyShopWindow();
  void clearShopDetails();
  void destroyNPCWindows();

  void addActiveEffect(AbstractEffect* _effect);
  void flashOnActiveEffect(AbstractEffect* _effect);
  void flashOffActiveEffect(AbstractEffect* _effect);
  void removeActiveEffect(AbstractEffect* _effect);
  void showSkillButtons();

  void addEmptySlotToPlayerInventory(InventorySlot* _emptySlot);
  void addItemToPlayerInventory(int _index, AbstractItem* _item, int _count);
  void removeItemFromSlot(InventorySlot* _slot, bool deleteChild = false);
  void removeEmptySlotFromPlayerInventory(InventorySlot* _slot);
  void updateItemCount(InventorySlot* _slot);
  void updateHotSlotCount(InventorySlot* _slot, int _index);
  void updateHotSlotCooldown(int _index, double _percentage);
  void removeFromHotSlot(int _index);

  string createInteractFrameWindow(CEGUI::FrameWindow* _window, CEGUI::UVector2 _size, CEGUI::UVector2 _pos, string _title);
  void createRewardInspector(AbstractItem* _item);
  void createShopInspector(int slotIDNumber);

  // Events designated for the builder.
  bool closeQuestBoxButtonPressed(const CEGUI::EventArgs& _e);
  bool acceptQuestButtonPressed(const CEGUI::EventArgs& _e);
  bool handleRewardItemClicked(const CEGUI::EventArgs& _e);
  bool frameCloseButtonHit(const CEGUI::EventArgs& _e);
  bool vendorOptionButtonHit(const CEGUI::EventArgs& _e);
  bool mouseClickOnShopSlot(const CEGUI::EventArgs& _e);
  bool mouseClickOnInvForShop(InventorySlot* slot);
  bool saleButtonHit(const CEGUI::EventArgs& _e);
  bool itemDroppedOnHotSlot(const CEGUI::EventArgs& _e);
  bool hotBarSlotClicked(const CEGUI::EventArgs& _e);
  bool skillPlusButtonClicked(const CEGUI::EventArgs& _e);
  bool handleSkillSlotClicked(const CEGUI::EventArgs& _e);
  bool handleMouseOver(const CEGUI::EventArgs& _e);
  bool handleMouseLeaves(const CEGUI::EventArgs& _e);
  bool handleSaveButtonClicked(const CEGUI::EventArgs& _e);

  int getSlotEndNumber(CEGUI::Window* _slot);
  int findFreeContainerNumber();

  void setDragDropStarted(bool _started);

  bool getDragDropStarted();

}; 

#endif