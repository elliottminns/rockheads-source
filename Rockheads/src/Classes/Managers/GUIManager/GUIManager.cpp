#include "stdafx.h"
#include "GUIManager.h"
#include "../../GameScenes/GameScene/GameScene.h"
#include "../../GameEntities/StaticEntities/NPC/NPC.h"
#include "../../GameEntities/StaticEntities/NPC/Vendor/Vendor.h"
#include "../../Questing/Quest/Quest.h"
#include "../../Managers/QuestManager/QuestManager.h"
#include "../../GameObjects/InventorySlot/InventorySlot.h"
#include "../../HotBar/HotBar.h"
#include "../../GameEntities/DynamicEntities/EntityClass/Skill/Skill.h"
#include "../../Effects/AbstractEffect/AbstractEffect.h"
#include "../../HotBar/AbstractHotSlot/AbstractHotSlot.h"
#include "../../HotBar/ItemHotSlot/ItemHotSlot.h"
#include "../../Managers/GameManager/GameManager.h"
#include "../../Managers/AudioManager/AudioManager.h"
#include "../../Managers/StaticEntityManager/StaticEntityManager.h"

#include <Ogre.h>

template<> GUIManager* Ogre::Singleton<GUIManager>::ms_Singleton = 0;


GUIManager::GUIManager()
{

} // GUIManager::GUIManager()


GUIManager::~GUIManager()
{

} // GUIManager::~GUIManager()


string GUIManager::buildInventoryWindow(CEGUI::FrameWindow* _window, CEGUI::UVector2 _size, CEGUI::UVector2 _pos, bool _open)
{
  // Create the initial details.
  string GUILook = OgreFramework::getSingleton().GUILook;
  CEGUI::WindowManager& winManager = CEGUI::WindowManager::getSingleton();
  CEGUI::Window* sheet = winManager.getWindow("Sheet");

  // Create a frame window to serve as our inventory.
  string windowName = "PlayerInventory";
  _window = (CEGUI::FrameWindow*)winManager.createWindow(GUILook + "/FrameWindow", windowName);
  _window->setSize(_size);
  _window->setPosition(_pos);
  _window->setVisible(_open);
  _window->setSizingEnabled(false);
  _window->setCloseButtonEnabled(true);
  _window->setText("Inventory");
  _window->setFont("DejaVuSans-10");
  _window->setVerticalAlignment(CEGUI::VA_BOTTOM);
  _window->subscribeEvent(CEGUI::FrameWindow::EventCloseClicked, 
    CEGUI::Event::Subscriber(&GameScene::frameCloseButtonHit, 
    (GameScene*)GameManager::getInstance()->getCurrentScene()));
  _window->setMaxSize(CEGUI::UVector2(CEGUI::UDim(0,180),CEGUI::UDim(0,400)));
  _window->setMinSize(CEGUI::UVector2(CEGUI::UDim(0,180),CEGUI::UDim(0,100)));
  _window->setEWSizingCursorImage("GlossySerpentCursors", "MouseArrow");
  _window->setRollupEnabled(false);
  sheet->addChildWindow(_window);

  // Add a scrollable pane to the window to allow us to scroll up and down only.
  CEGUI::ScrollablePane* inventoryScroll = (CEGUI::ScrollablePane*)winManager.createWindow(GUILook+"/ScrollablePane", "PlayerInventory/Scroll");
  CEGUI::UVector2 scrollSize = CEGUI::UVector2(CEGUI::UDim(1.0,0), CEGUI::UDim(1.0,0));
  inventoryScroll->setSize(scrollSize);
  inventoryScroll->setClippedByParent(false);
  inventoryScroll->setMouseInputPropagationEnabled(true);
  inventoryScroll->setShowVertScrollbar(true);
  inventoryScroll->setShowHorzScrollbar(false);
  _window->addChildWindow(inventoryScroll);

  // Get the number of inventory slots available to us.
  int numSlots = Player::getInstance()->getInventory()->getItems()->size();

  // Pre define the size of the inventory slots.
  const int xSizeSlot  = 50;
  const int ySizeSlot  = 50;
  const int xImageSlot = 40;
  const int yImageSlot = 40;
  CEGUI::UVector2 slotSize  = CEGUI::UVector2(CEGUI::UDim(0,xSizeSlot),CEGUI::UDim(0,ySizeSlot));
  CEGUI::UVector2 imageSize = CEGUI::UVector2(CEGUI::UDim(0,xImageSlot),CEGUI::UDim(0,yImageSlot));


  // Define how many slots appear in a row.
  const int numSlotsX = 3;

  // Loop through the slots and make the inventory slot outline.
  for(int i=0;i<numSlots;i++)
  {
    // Create an individual slot.
    if(winManager.isWindowPresent("InvSlot-"  + CEGUI::PropertyHelper::intToString(i)))
    {
      exit(101);
    } // end if

    CEGUI::Window* invSlot = winManager.createWindow(GUILook + "/StaticImage", 
      "InvSlot-" + CEGUI::PropertyHelper::intToString(i));
    invSlot->setSize(slotSize);
    invSlot->setMouseInputPropagationEnabled(true);

    // Set the xPositon of the slot.
    int xPos = xSizeSlot*(i%numSlotsX);
    int yPos = ySizeSlot*(i/numSlotsX);

    // Place the position of the slot and add it to the window.
    invSlot->setPosition(CEGUI::UVector2(CEGUI::UDim(0,xPos),CEGUI::UDim(0,yPos)));
    inventoryScroll->addChildWindow(invSlot);

    // Subscribe to the slot events.
    invSlot->subscribeEvent(CEGUI::Window::EventDragDropItemDropped, 
      CEGUI::Event::Subscriber(&GameScene::handleItemDroppedEmptyInventory, 
      (GameScene*)GameManager::getInstance()->getCurrentScene()));

  } // end for

  int j=0;
  // Now loop through and add images to the slot if an image resides in it.
  for(int i=0;i<numSlots;i++)
  {
    // Now get the item details.
    InventorySlot* itemSlot = Player::getInstance()->getInventory()->getItems()->at(i);
    AbstractItem*  item     = itemSlot->getItem();

    // Now add the container depending on whether or not there is an item there.
    if(item)
    {
      // Create a drag conatiner.
      CEGUI::DragContainer* container = (CEGUI::DragContainer*)winManager.createWindow("DragContainer", 
        "invContainer-"+ CEGUI::PropertyHelper::intToString(j));
      // Set the properties of the drag container.
      container->setSize(imageSize);
      container->setHorizontalAlignment(CEGUI::HA_CENTRE);
      container->setVerticalAlignment(CEGUI::VA_CENTRE);
      container->setMouseInputPropagationEnabled(true);
      container->setStickyModeEnabled(true);
      container->setTooltipText(item->getName());

      // Get the parent slot and add the container to it.
      CEGUI::Window* parentSlot = winManager.getWindow("InvSlot-" + CEGUI::PropertyHelper::intToString(i));
      parentSlot->addChildWindow(container);

      // Subscribe to the container events.
      container->subscribeEvent(CEGUI::Window::EventDragDropItemDropped, 
        CEGUI::Event::Subscriber(&GameScene::handleItemDroppedContainer, 
        (GameScene*)GameManager::getInstance()->getCurrentScene()));
      container->subscribeEvent(CEGUI::DragContainer::EventDragStarted,
        CEGUI::Event::Subscriber(&GameScene::handleDragBegan, 
        (GameScene*)GameManager::getInstance()->getCurrentScene()));
      container->subscribeEvent(CEGUI::DragContainer::EventDragEnded, 
        CEGUI::Event::Subscriber(&GameScene::handleDragEnded, 
        (GameScene*)GameManager::getInstance()->getCurrentScene()));
      container->subscribeEvent(CEGUI::Window::EventMouseButtonDown,
        CEGUI::Event::Subscriber(&GameScene::handleInvSlotClicked, 
        (GameScene*)GameManager::getInstance()->getCurrentScene()));
      container->subscribeEvent(CEGUI::Window::EventMouseEntersArea,
        CEGUI::Event::Subscriber(&GameScene::handleMouseOver, 
        (GameScene*)GameManager::getInstance()->getCurrentScene()));
      container->subscribeEvent(CEGUI::Window::EventMouseLeavesArea,
        CEGUI::Event::Subscriber(&GameScene::handleMouseLeave, 
        (GameScene*)GameManager::getInstance()->getCurrentScene()));

      // Create the image inventory slot.
      CEGUI::Window* invSlot  = winManager.createWindow(GUILook + "/StaticImage", 
        "InvItem-" + CEGUI::PropertyHelper::intToString(j));
      invSlot->setSize(imageSize);
      
      // disable frame and standard background
      invSlot->setProperty("FrameEnabled", "False");
      invSlot->setProperty("BackgroundEnabled", "False");
 
      // Set the positioning properties.
      invSlot->setHorizontalAlignment(CEGUI::HA_CENTRE);
      invSlot->setVerticalAlignment(CEGUI::VA_CENTRE);
     
      // Allow mouse to pass through the inventory slot & set the image.
      invSlot->setMousePassThroughEnabled(true);
      invSlot->setProperty("Image","set:"+item->getImageName()+" image:full_image");

      // Add the slot to the container.
      container->addChildWindow(invSlot);

      // Finally add a count if the item is stackable.
      if(item->getStackable())
      {
        CEGUI::Window* itemCount = winManager.createWindow(GUILook + "/StaticText", 
          "InvItem-" + CEGUI::PropertyHelper::intToString(j) + "/Count");
        itemCount->setMousePassThroughEnabled(true);
        itemCount->setText(CEGUI::PropertyHelper::intToString(itemSlot->getCount()));
        itemCount->setHorizontalAlignment(CEGUI::HA_LEFT);
        itemCount->setVerticalAlignment(CEGUI::VA_BOTTOM);
        itemCount->setProperty("HorzFormatting", "HorzLeft");
        itemCount->setProperty("VertFormatting", "BottomAligned");
        itemCount->setProperty("FrameEnabled", "False");
        itemCount->setProperty("BackgroundEnabled", "False");
        itemCount->setFont("DejaVuSans-8");
        itemCount->setSize(CEGUI::UVector2(CEGUI::UDim(0.6,0),CEGUI::UDim(0.35,0)));
        container->addChildWindow(itemCount);

      } // end if
      ++j;
    } // end if(item)
    
  } // end for

  return windowName;

} // string GUIManager::buildInventoryWindow(CEGUI::Window* _inventoryWindow)


string GUIManager::buildCharacterWindow(CEGUI::FrameWindow* _window, CEGUI::UVector2 _size, CEGUI::UVector2 _pos, bool _open)
{
  string GUILook = OgreFramework::getSingleton().GUILook;

  // Create the default window.
  CEGUI::WindowManager& winManager = CEGUI::WindowManager::getSingleton();
  CEGUI::Window* sheet = winManager.getWindow("Sheet");

  // Create a frame window to serve as our inventory.
  string windowName = "CharacterWindow";


  // Create a frame window to serve as our inventory.
  _window = (CEGUI::FrameWindow*)winManager.createWindow(GUILook + "/FrameWindow", windowName);
  _window->setSize(_size);
  _window->setPosition(_pos);
  _window->setVisible(_open);
  _window->setSizingEnabled(false);
  _window->setCloseButtonEnabled(true);
  _window->setRollupEnabled(false);
  _window->setHorizontalAlignment(CEGUI::HA_LEFT);
  _window->setVerticalAlignment(CEGUI::VA_BOTTOM);

  // Work out where the rough center of the title bar is (Hacky but no other way).
  string windowText = Player::getInstance()->getName();
  _window->setFont("NorthwoodHigh-12");
  _window->setText(windowText.c_str());
  _window->subscribeEvent(CEGUI::FrameWindow::EventCloseClicked, 
    CEGUI::Event::Subscriber(&GameScene::frameCloseButtonHit, 
    (GameScene*)GameManager::getInstance()->getCurrentScene()));
  sheet->addChildWindow(_window);

  // Create a static text saying the level and class.
  CEGUI::Window* levelAndClass = winManager.createWindow(GUILook + "/StaticText", "LevelAndClass");
  string levelAndClassText = "Level ";
  levelAndClassText += CEGUI::PropertyHelper::intToString(Player::getInstance()->getLevel()).c_str();
  levelAndClassText += " ";
  levelAndClassText += Player::getInstance()->getSkillClass()->getName();
  levelAndClass->setText(levelAndClassText.c_str());
  levelAndClass->setHorizontalAlignment(CEGUI::HA_CENTRE);
  levelAndClass->setVerticalAlignment(CEGUI::VA_CENTRE);
  levelAndClass->setProperty("HorzFormatting", "HorzCentred");
  levelAndClass->setProperty("VertFormatting", "TopAligned");
  levelAndClass->setSize(CEGUI::UVector2(CEGUI::UDim(1,0),CEGUI::UDim(0,20)));
  levelAndClass->setProperty("BackgroundEnabled", "False");
  levelAndClass->setProperty("FrameEnabled", "False");
  levelAndClass->setFont("DejaVuSerif-HD-10");
  _window->addChildWindow(levelAndClass);

  // Create a stats display.
  CEGUI::Window* statsBox = winManager.createWindow(GUILook + "/StaticText", "StatsBox");
  statsBox->setProperty("BackgroundEnabled", "True");
  statsBox->setProperty("FrameEnabled", "True");
  statsBox->setHorizontalAlignment(CEGUI::HA_CENTRE);
  statsBox->setVerticalAlignment(CEGUI::VA_BOTTOM);
  statsBox->setSize(CEGUI::UVector2(CEGUI::UDim(1.0,0),CEGUI::UDim(0,110)));
  statsBox->setProperty("FrameEnabled", "False");
  statsBox->setProperty("BackgroundEnabled", "False");
  _window->addChildWindow(statsBox);

  // Create the stat names.
  CEGUI::Window* statNames = winManager.createWindow(GUILook + "/StaticText", "StatNames");
  statNames->setProperty("BackgroundEnabled", "False");
  statNames->setProperty("FrameEnabled", "False");
  statNames->setProperty("HorzFormatting", "LeftAligned");
  statNames->setProperty("VertFormatting", "TopAligned");
  statNames->setHorizontalAlignment(CEGUI::HA_CENTRE);
  statNames->setVerticalAlignment(CEGUI::VA_BOTTOM);
  statNames->setSize(CEGUI::UVector2(CEGUI::UDim(0.95,0),CEGUI::UDim(0.95,0)));
  string statNamesText = "Strength:\nAgility:\nDexterity:\nConstitution:\nIntelligence:\nWisdom:\nCharisma:";
  statNames->setText(statNamesText.c_str());
  statNames->setFont("DejaVuSerif-HD-10");
  statsBox->addChildWindow(statNames);

  // Create the stat values.
  CEGUI::Window* statValues = winManager.createWindow(GUILook + "/StaticText", "StatValues");
  statValues->setProperty("BackgroundEnabled", "False");
  statValues->setProperty("FrameEnabled", "False");
  statValues->setProperty("HorzFormatting", "RightAligned");
  statValues->setProperty("VertFormatting", "BottomAligned");
  statValues->setHorizontalAlignment(CEGUI::HA_CENTRE);
  statValues->setVerticalAlignment(CEGUI::VA_BOTTOM);
  statValues->setSize(CEGUI::UVector2(CEGUI::UDim(0.95,0),CEGUI::UDim(0.95,0)));
  string statValuesText = Player::getInstance()->getStatValueString();
  statValues->setText(statValuesText.c_str());
  statValues->setFont("DejaVuSerif-HD-10");
  statsBox->addChildWindow(statValues);

  // Pre define the size of the equip slots.
  const int xSizeSlot  = 50;
  const int ySizeSlot  = 50;
  const int xImageSlot = 40;
  const int yImageSlot = 40;
  CEGUI::UVector2 slotSize  = CEGUI::UVector2(CEGUI::UDim(0,xSizeSlot),CEGUI::UDim(0,ySizeSlot));
  CEGUI::UVector2 imageSize = CEGUI::UVector2(CEGUI::UDim(0,xImageSlot),CEGUI::UDim(0,yImageSlot));

  // Load the image @Todo, add this in to a loading screen.
  CEGUI::ImagesetManager::getSingleton().createFromImageFile("head_doll.tga", "head_doll.tga");

  // Create the head equip slot.
  CEGUI::Window* headSlot = winManager.createWindow(GUILook + "/StaticImage", "Slot-Head");
  headSlot->setSize(slotSize);
  headSlot->setHorizontalAlignment(CEGUI::HA_LEFT);
  headSlot->setVerticalAlignment(CEGUI::VA_TOP);
  headSlot->setRiseOnClickEnabled(false);
  headSlot->subscribeEvent(CEGUI::Window::EventDragDropItemDropped, CEGUI::Event::Subscriber(&GameScene::handleItemDroppedEmptyEquipment, 
    (GameScene*)GameManager::getInstance()->getCurrentScene()));
  headSlot->setProperty("Image", "set:head_doll.tga image:full_image");
  _window->addChildWindow(headSlot);

  // Create the label.
  CEGUI::Window* headLabel = winManager.createWindow(GUILook + "/StaticText", "Label-Head");
  headLabel->setSize(slotSize);
  headLabel->setHorizontalAlignment(CEGUI::HA_LEFT);
  headLabel->setVerticalAlignment(CEGUI::VA_TOP);
  headLabel->setText("Head");
  headLabel->setProperty("FrameEnabled","False");
  headLabel->setProperty("BackgroundEnabled","False");
  headLabel->setProperty("HorzFormatting", "HorzCentred");
  headLabel->setProperty("VertFormatting", "VertCentred");
  headLabel->setFont("NorthwoodHigh-14");
  headLabel->setAlpha(0.7f);
  headLabel->setMousePassThroughEnabled(true);
  headSlot->addChildWindow(headLabel);
  
  if(Player::getInstance()->getHead())
  {
    // Find a free container number.
    int containerNumber = this->findFreeContainerNumber();

    // Create a drag conatiner.
    CEGUI::DragContainer* container = (CEGUI::DragContainer*)winManager.createWindow("DragContainer", "invContainer-" + 
      CEGUI::PropertyHelper::intToString(containerNumber));

    // Set the propeties of the drag container.
    container->setSize(imageSize);
    container->setHorizontalAlignment(CEGUI::HA_CENTRE);
    container->setVerticalAlignment(CEGUI::VA_CENTRE);
    container->setMouseInputPropagationEnabled(true);
    container->setStickyModeEnabled(true);
    headSlot->addChildWindow(container);

    // Subscribe to a click event so drag is not required.
    container->subscribeEvent(CEGUI::Window::EventDragDropItemDropped, 
      CEGUI::Event::Subscriber(&GameScene::handleItemDroppedContainer, 
      (GameScene*)GameManager::getInstance()->getCurrentScene()));
    container->subscribeEvent(CEGUI::Window::EventMouseButtonDown,
      CEGUI::Event::Subscriber(&GameScene::handleInvSlotClicked, 
      (GameScene*)GameManager::getInstance()->getCurrentScene()));
    container->subscribeEvent(CEGUI::Window::EventMouseEntersArea,
      CEGUI::Event::Subscriber(&GameScene::handleMouseOver, 
      (GameScene*)GameManager::getInstance()->getCurrentScene()));
    container->subscribeEvent(CEGUI::Window::EventMouseLeavesArea,
      CEGUI::Event::Subscriber(&GameScene::handleMouseLeave, 
      (GameScene*)GameManager::getInstance()->getCurrentScene()));
    container->subscribeEvent(CEGUI::DragContainer::EventDragEnded,
      CEGUI::Event::Subscriber(&GameScene::handleDragEnded, 
      (GameScene*)GameManager::getInstance()->getCurrentScene()));

    // Create the image inventory slot.
    CEGUI::Window* equipSlot  = winManager.createWindow(GUILook + "/StaticImage", "EquipSlot-Head");
    equipSlot->setSize(imageSize);
    //invSlot->setMouseInputPropagationEnabled(true);

    // disable frame and standard background
    equipSlot->setProperty("FrameEnabled", "False");
    equipSlot->setProperty("BackgroundEnabled", "False");
 
    // Set the positioning properties.
    equipSlot->setHorizontalAlignment(CEGUI::HA_CENTRE);
    equipSlot->setVerticalAlignment(CEGUI::VA_CENTRE);

    AbstractItem* item = Player::getInstance()->getHead();
    // Allow mouse to pass through the inventory slot & set the image.
    equipSlot->setMousePassThroughEnabled(true);
    equipSlot->setProperty("Image","set:" + item->getImageName() + " image:full_image");

    // Finally add the slot to the container.
    container->addChildWindow(equipSlot);
  } // end if

  // Load the image @Todo, add this in to a loading screen.
  CEGUI::ImagesetManager::getSingleton().createFromImageFile("chest_doll.tga", "chest_doll.tga");

  // Create the body equip slot.
  CEGUI::Window* chestSlot = winManager.createWindow(GUILook + "/StaticImage", "Slot-Chest");
  chestSlot->setSize(slotSize);
  chestSlot->setHorizontalAlignment(CEGUI::HA_RIGHT);
  chestSlot->setVerticalAlignment(CEGUI::VA_TOP);
  chestSlot->setRiseOnClickEnabled(false);
  chestSlot->subscribeEvent(CEGUI::Window::EventDragDropItemDropped, 
    CEGUI::Event::Subscriber(&GameScene::handleItemDroppedEmptyEquipment, 
    (GameScene*)GameManager::getInstance()->getCurrentScene()));
  chestSlot->setProperty("Image", "set:chest_doll.tga image:full_image");
  _window->addChildWindow(chestSlot);

  // Create the label.
  CEGUI::Window* chestLabel = winManager.createWindow(GUILook + "/StaticText", "Label-Chest");
  chestLabel->setSize(slotSize);
  chestLabel->setHorizontalAlignment(CEGUI::HA_RIGHT);
  chestLabel->setVerticalAlignment(CEGUI::VA_TOP);
  chestLabel->setText("Chest");
  chestLabel->setProperty("FrameEnabled","False");
  chestLabel->setProperty("BackgroundEnabled","False");
  chestLabel->setProperty("HorzFormatting", "HorzCentred");
  chestLabel->setProperty("VertFormatting", "VertCentred");
  chestLabel->setFont("NorthwoodHigh-14");
  chestLabel->setMousePassThroughEnabled(true);
  chestLabel->setAlpha(0.7f);
  chestSlot->addChildWindow(chestLabel);


  if(Player::getInstance()->getChest())
  {
    // Find a free container number.
    int containerNumber = this->findFreeContainerNumber();

    // Create a drag conatiner.
    CEGUI::DragContainer* container = (CEGUI::DragContainer*)winManager.createWindow("DragContainer", "invContainer-" + 
      CEGUI::PropertyHelper::intToString(containerNumber));

    // Set the propeties of the drag container.
    container->setSize(imageSize);
    container->setHorizontalAlignment(CEGUI::HA_CENTRE);
    container->setVerticalAlignment(CEGUI::VA_CENTRE);
    container->setMouseInputPropagationEnabled(true);
    container->setStickyModeEnabled(true);
    chestSlot->addChildWindow(container);

    // Subscribe to a click event so drag is not required.
    container->subscribeEvent(CEGUI::Window::EventDragDropItemDropped, 
      CEGUI::Event::Subscriber(&GameScene::handleItemDroppedContainer, 
      (GameScene*)GameManager::getInstance()->getCurrentScene()));
    container->subscribeEvent(CEGUI::Window::EventMouseButtonDown,
      CEGUI::Event::Subscriber(&GameScene::handleInvSlotClicked, 
      (GameScene*)GameManager::getInstance()->getCurrentScene()));
    container->subscribeEvent(CEGUI::Window::EventMouseEntersArea,
      CEGUI::Event::Subscriber(&GameScene::handleMouseOver, 
      (GameScene*)GameManager::getInstance()->getCurrentScene()));
    container->subscribeEvent(CEGUI::Window::EventMouseLeavesArea,
      CEGUI::Event::Subscriber(&GameScene::handleMouseLeave, 
      (GameScene*)GameManager::getInstance()->getCurrentScene()));
    container->subscribeEvent(CEGUI::DragContainer::EventDragEnded,
      CEGUI::Event::Subscriber(&GameScene::handleDragEnded, 
      (GameScene*)GameManager::getInstance()->getCurrentScene()));

    // Create the image inventory slot.
    CEGUI::Window* equipSlot  = winManager.createWindow(GUILook + "/StaticImage", "EquipSlot-Chest");
    equipSlot->setSize(imageSize);
    //invSlot->setMouseInputPropagationEnabled(true);

    // disable frame and standard background
    equipSlot->setProperty("FrameEnabled", "False");
    equipSlot->setProperty("BackgroundEnabled", "False");
 
    // Set the positioning properties.
    equipSlot->setHorizontalAlignment(CEGUI::HA_CENTRE);
    equipSlot->setVerticalAlignment(CEGUI::VA_CENTRE);

    AbstractItem* item = Player::getInstance()->getChest();
    // Allow mouse to pass through the inventory slot & set the image.
    equipSlot->setMousePassThroughEnabled(true);
    equipSlot->setProperty("Image","set:" + item->getImageName() + " image:full_image");

    // Finally add the slot to the container.
    container->addChildWindow(equipSlot);
  } // end if


  // Load the image @Todo, add this in to a loading screen.
  CEGUI::ImagesetManager::getSingleton().createFromImageFile("legs_doll.tga", "legs_doll.tga");

  // Create the legs equip slot.
  CEGUI::Window* legsSlot = winManager.createWindow(GUILook + "/StaticImage", "Slot-Legs");
  legsSlot->setSize(slotSize);
  legsSlot->setHorizontalAlignment(CEGUI::HA_LEFT);
  legsSlot->setVerticalAlignment(CEGUI::VA_TOP);
  legsSlot->setYPosition(CEGUI::UDim(0,ySizeSlot));
  legsSlot->setRiseOnClickEnabled(false);
  legsSlot->subscribeEvent(CEGUI::Window::EventDragDropItemDropped, 
    CEGUI::Event::Subscriber(&GameScene::handleItemDroppedEmptyEquipment, 
    (GameScene*)GameManager::getInstance()->getCurrentScene()));
  legsSlot->setProperty("Image", "set:legs_doll.tga image:full_image");
  _window->addChildWindow(legsSlot);


  // Create the label.
  CEGUI::Window* legsLabel = winManager.createWindow(GUILook + "/StaticText", "Label-Legs");
  legsLabel->setSize(slotSize);
  legsLabel->setText("Legs");
  legsLabel->setProperty("FrameEnabled","False");
  legsLabel->setProperty("BackgroundEnabled","False");
  legsLabel->setProperty("HorzFormatting", "HorzCentred");
  legsLabel->setProperty("VertFormatting", "VertCentred");
  legsLabel->setFont("NorthwoodHigh-14");
  legsLabel->setMousePassThroughEnabled(true);
  legsLabel->setAlpha(0.7f);
  legsSlot->addChildWindow(legsLabel);

  if(Player::getInstance()->getLegs())
  {
   // Find a free container number.
    int containerNumber = this->findFreeContainerNumber();

    // Create a drag conatiner.
    CEGUI::DragContainer* container = (CEGUI::DragContainer*)winManager.createWindow("DragContainer", "invContainer-" + 
      CEGUI::PropertyHelper::intToString(containerNumber));

    // Set the propeties of the drag container.
    container->setSize(imageSize);
    container->setHorizontalAlignment(CEGUI::HA_CENTRE);
    container->setVerticalAlignment(CEGUI::VA_CENTRE);
    container->setMouseInputPropagationEnabled(true);
    container->setStickyModeEnabled(true);
    legsSlot->addChildWindow(container);

    // Subscribe to a click event so drag is not required.
    container->subscribeEvent(CEGUI::Window::EventDragDropItemDropped, 
      CEGUI::Event::Subscriber(&GameScene::handleItemDroppedContainer, 
      (GameScene*)GameManager::getInstance()->getCurrentScene()));
    container->subscribeEvent(CEGUI::Window::EventMouseButtonDown,
      CEGUI::Event::Subscriber(&GameScene::handleInvSlotClicked, (GameScene*)GameManager::getInstance()->getCurrentScene()));
    container->subscribeEvent(CEGUI::Window::EventMouseEntersArea,
      CEGUI::Event::Subscriber(&GameScene::handleMouseOver, 
      (GameScene*)GameManager::getInstance()->getCurrentScene()));
    container->subscribeEvent(CEGUI::Window::EventMouseLeavesArea,
      CEGUI::Event::Subscriber(&GameScene::handleMouseLeave, 
      (GameScene*)GameManager::getInstance()->getCurrentScene()));
    container->subscribeEvent(CEGUI::DragContainer::EventDragEnded,
      CEGUI::Event::Subscriber(&GameScene::handleDragEnded, 
      (GameScene*)GameManager::getInstance()->getCurrentScene()));

    // Create the image inventory slot.
    CEGUI::Window* equipSlot = winManager.createWindow(GUILook + "/StaticImage", "EquipSlot-Legs");
    equipSlot->setSize(imageSize);

    // disable frame and standard background
    equipSlot->setProperty("FrameEnabled", "False");
    equipSlot->setProperty("BackgroundEnabled", "False");
 
    // Set the positioning properties.
    equipSlot->setHorizontalAlignment(CEGUI::HA_CENTRE);
    equipSlot->setVerticalAlignment(CEGUI::VA_CENTRE);

    AbstractItem* item = Player::getInstance()->getLegs();
    // Allow mouse to pass through the inventory slot & set the image.
    equipSlot->setMousePassThroughEnabled(true);
    equipSlot->setProperty("Image","set:" + item->getImageName() + " image:full_image");

    // Finally add the slot to the container.
    container->addChildWindow(equipSlot);
  } // end if(Player::getInstance()->getLegs())


  // Load the image @Todo, add this in to a loading screen.
  CEGUI::ImagesetManager::getSingleton().createFromImageFile("feet_doll.tga", "feet_doll.tga");

  // Add the feet slot.
  CEGUI::Window* feetSlot = winManager.createWindow(GUILook + "/StaticImage", "Slot-Feet");
  feetSlot->setSize(slotSize);
  feetSlot->setHorizontalAlignment(CEGUI::HA_RIGHT);
  feetSlot->setVerticalAlignment(CEGUI::VA_TOP);
  feetSlot->setYPosition(CEGUI::UDim(0,ySizeSlot));
  feetSlot->setRiseOnClickEnabled(false);
  feetSlot->subscribeEvent(CEGUI::Window::EventDragDropItemDropped, 
    CEGUI::Event::Subscriber(&GameScene::handleItemDroppedEmptyEquipment, 
    (GameScene*)GameManager::getInstance()->getCurrentScene()));
  feetSlot->setProperty("Image", "set:feet_doll.tga image:full_image");
  _window->addChildWindow(feetSlot);

  // Create the label.
  CEGUI::Window* feetLabel = winManager.createWindow(GUILook + "/StaticText", "Label-Feet");
  feetLabel->setSize(slotSize);
  feetLabel->setHorizontalAlignment(CEGUI::HA_RIGHT);
  feetLabel->setVerticalAlignment(CEGUI::VA_TOP);
  feetLabel->setText("Feet");
  feetLabel->setProperty("FrameEnabled","False");
  feetLabel->setProperty("BackgroundEnabled","False");
  feetLabel->setProperty("HorzFormatting", "HorzCentred");
  feetLabel->setProperty("VertFormatting", "VertCentred");
  feetLabel->setMousePassThroughEnabled(true);
  feetLabel->setFont("NorthwoodHigh-14");
  feetLabel->setAlpha(0.7f);
  feetSlot->addChildWindow(feetLabel);

  if(Player::getInstance()->getFeet())
  {
    // Find a free container number.
    int containerNumber = this->findFreeContainerNumber();

    // Create a drag conatiner.
    CEGUI::DragContainer* container = (CEGUI::DragContainer*)winManager.createWindow("DragContainer", "invContainer-" + 
      CEGUI::PropertyHelper::intToString(containerNumber));

    // Set the propeties of the drag container.
    container->setSize(imageSize);
    container->setHorizontalAlignment(CEGUI::HA_CENTRE);
    container->setVerticalAlignment(CEGUI::VA_CENTRE);
    container->setMouseInputPropagationEnabled(true);
    container->setStickyModeEnabled(true);
    feetSlot->addChildWindow(container);

    // Subscribe to a click event so drag is not required.
    container->subscribeEvent(CEGUI::Window::EventDragDropItemDropped, 
      CEGUI::Event::Subscriber(&GameScene::handleItemDroppedContainer, 
      (GameScene*)GameManager::getInstance()->getCurrentScene()));
    container->subscribeEvent(CEGUI::Window::EventMouseButtonDown,
        CEGUI::Event::Subscriber(&GameScene::handleInvSlotClicked, 
        (GameScene*)GameManager::getInstance()->getCurrentScene()));
    container->subscribeEvent(CEGUI::Window::EventMouseEntersArea,
      CEGUI::Event::Subscriber(&GameScene::handleMouseOver, 
      (GameScene*)GameManager::getInstance()->getCurrentScene()));
    container->subscribeEvent(CEGUI::Window::EventMouseLeavesArea,
      CEGUI::Event::Subscriber(&GameScene::handleMouseLeave, 
      (GameScene*)GameManager::getInstance()->getCurrentScene()));
    container->subscribeEvent(CEGUI::DragContainer::EventDragEnded,
      CEGUI::Event::Subscriber(&GameScene::handleDragEnded, 
      (GameScene*)GameManager::getInstance()->getCurrentScene()));

    // Create the image inventory slot.
    CEGUI::Window* equipSlot  = winManager.createWindow(GUILook + "/StaticImage", "EquipSlot-Feet");
    equipSlot->setSize(imageSize);

    // disable frame and standard background
    equipSlot->setProperty("FrameEnabled", "False");
    equipSlot->setProperty("BackgroundEnabled", "False");
 
    // Set the positioning properties.
    equipSlot->setHorizontalAlignment(CEGUI::HA_CENTRE);
    equipSlot->setVerticalAlignment(CEGUI::VA_CENTRE);

    AbstractItem* item = Player::getInstance()->getFeet();
    // Allow mouse to pass through the inventory slot & set the image.
    equipSlot->setMousePassThroughEnabled(true);
    equipSlot->setProperty("Image","set:" + item->getImageName() + " image:full_image");

    // Finally add the slot to the container.
    container->addChildWindow(equipSlot);
  } // end if

    // Load the image @Todo, add this in to a loading screen.
  CEGUI::ImagesetManager::getSingleton().createFromImageFile("weapon_doll.tga", "weapon_doll.tga");

  // Add the Weapon slot.
  CEGUI::Window* weaponSlot = winManager.createWindow(GUILook + "/StaticImage", "Slot-Weapon");
  weaponSlot->setSize(slotSize);
  weaponSlot->setHorizontalAlignment(CEGUI::HA_CENTRE);
  weaponSlot->setVerticalAlignment(CEGUI::VA_TOP);
  weaponSlot->setRiseOnClickEnabled(false);
  weaponSlot->subscribeEvent(CEGUI::Window::EventDragDropItemDropped, 
    CEGUI::Event::Subscriber(&GameScene::handleItemDroppedEmptyEquipment, 
    (GameScene*)GameManager::getInstance()->getCurrentScene()));
  weaponSlot->setProperty("Image", "set:weapon_doll.tga image:full_image");
  _window->addChildWindow(weaponSlot);

  // Create the label.
  CEGUI::Window* weaponLabel = winManager.createWindow(GUILook + "/StaticText", "Label-Weapon");
  weaponLabel->setSize(slotSize);
  weaponLabel->setHorizontalAlignment(CEGUI::HA_CENTRE);
  weaponLabel->setText("Weapon");
  weaponLabel->setProperty("FrameEnabled","False");
  weaponLabel->setProperty("BackgroundEnabled","False");
  weaponLabel->setProperty("HorzFormatting", "HorzCentred");
  weaponLabel->setProperty("VertFormatting", "VertCentred");
  weaponLabel->setMousePassThroughEnabled(true);
  weaponLabel->setFont("NorthwoodHigh-12");
  weaponLabel->setAlpha(0.7f);
  weaponSlot->addChildWindow(weaponLabel);

  if(Player::getInstance()->getWeapon())
  {
    // Find a free container number.
    int containerNumber = this->findFreeContainerNumber();

    // Create a drag conatiner.
    CEGUI::DragContainer* container = (CEGUI::DragContainer*)winManager.createWindow("DragContainer", "invContainer-" + 
      CEGUI::PropertyHelper::intToString(containerNumber));

    // Set the propeties of the drag container.
    container->setSize(imageSize);
    container->setHorizontalAlignment(CEGUI::HA_CENTRE);
    container->setVerticalAlignment(CEGUI::VA_CENTRE);
    container->setMouseInputPropagationEnabled(true);
    container->setStickyModeEnabled(true);
    weaponSlot->addChildWindow(container);

    // Subscribe to a click event so drag is not required.
    container->subscribeEvent(CEGUI::Window::EventDragDropItemDropped, 
      CEGUI::Event::Subscriber(&GameScene::handleItemDroppedContainer, 
      (GameScene*)GameManager::getInstance()->getCurrentScene()));
    container->subscribeEvent(CEGUI::Window::EventMouseButtonDown,
      CEGUI::Event::Subscriber(&GameScene::handleInvSlotClicked, 
      (GameScene*)GameManager::getInstance()->getCurrentScene()));
    container->subscribeEvent(CEGUI::Window::EventMouseEntersArea,
      CEGUI::Event::Subscriber(&GameScene::handleMouseOver, 
      (GameScene*)GameManager::getInstance()->getCurrentScene()));
    container->subscribeEvent(CEGUI::Window::EventMouseLeavesArea,
      CEGUI::Event::Subscriber(&GameScene::handleMouseLeave, 
      (GameScene*)GameManager::getInstance()->getCurrentScene()));
    container->subscribeEvent(CEGUI::DragContainer::EventDragEnded,
      CEGUI::Event::Subscriber(&GameScene::handleDragEnded, 
      (GameScene*)GameManager::getInstance()->getCurrentScene()));

    // Create the image inventory slot.
    CEGUI::Window* equipSlot  = winManager.createWindow(GUILook + "/StaticImage", "EquipSlot-Weapon");
    equipSlot->setSize(imageSize);

    // disable frame and standard background
    equipSlot->setProperty("FrameEnabled", "False");
    equipSlot->setProperty("BackgroundEnabled", "False");
 
    // Set the positioning properties.
    equipSlot->setHorizontalAlignment(CEGUI::HA_CENTRE);
    equipSlot->setVerticalAlignment(CEGUI::VA_CENTRE);

    AbstractItem* item = Player::getInstance()->getWeapon();
    // Allow mouse to pass through the inventory slot & set the image.
    equipSlot->setMousePassThroughEnabled(true);
    equipSlot->setProperty("Image","set:" + item->getImageName() + " image:full_image");

    // Finally add the slot to the container.
    container->addChildWindow(equipSlot);
  } // end if

  return windowName;

} // string GUIManager::buildCharacterWindow(CEGUI::FrameWindow* _window, CEGUI::UVector2 _size, CEGUI::UVector2 _pos, bool _open)


string GUIManager::buildJournalWindow(CEGUI::FrameWindow* _window, CEGUI::UVector2 _size, CEGUI::UVector2 _pos, bool _open)
{
  // Create the initial details.
  string GUILook = OgreFramework::getSingleton().GUILook;
  CEGUI::WindowManager& winManager = CEGUI::WindowManager::getSingleton();
  CEGUI::Window* sheet = winManager.getWindow("Sheet");

  // Create a frame window to serve as our journal.
  string windowName = "Journal";
  _window = (CEGUI::FrameWindow*)winManager.createWindow(GUILook + "/FrameWindow", windowName);
  _window->setSize(_size);
  _window->setHorizontalAlignment(CEGUI::HA_RIGHT);
  _window->setVerticalAlignment(CEGUI::VA_CENTRE);
  _window->setPosition(_pos);
  _window->setVisible(_open);
  _window->setSizingEnabled(false);
  _window->setCloseButtonEnabled(true);
  _window->setText("Journal");
  _window->subscribeEvent(CEGUI::FrameWindow::EventCloseClicked, 
    CEGUI::Event::Subscriber(&GameScene::frameCloseButtonHit, 
    (GameScene*)GameManager::getInstance()->getCurrentScene()));
  _window->setRollupEnabled(false);
  sheet->addChildWindow(_window);


  // Add the quest Title to the window.
  CEGUI::Window* questTitle = winManager.createWindow(GUILook + "/StaticText", windowName + "/TitleText");
  questTitle->setHorizontalAlignment(CEGUI::HA_LEFT);
  questTitle->setVerticalAlignment(CEGUI::VA_TOP);
  questTitle->setYPosition(CEGUI::UDim(0,5));
  questTitle->setText("No Current Quest :(");
  questTitle->setProperty("VertFormatting", "TopAligned");
  questTitle->setProperty("HorzFormatting", "WordWrapLeftAligned");
  questTitle->setProperty("FrameEnabled", "False");
  questTitle->setProperty("BackgroundEnabled", "False");
  questTitle->setFont("DejaVuSans-12");
  _window->addChildWindow(questTitle);

  // Add the quest text to the window.
  CEGUI::Window* questDescription = winManager.createWindow(
    GUILook + "/StaticText", windowName + "/DescriptionText");
  questDescription->setText("");
  questDescription->setHorizontalAlignment(CEGUI::HA_CENTRE);
  questDescription->setVerticalAlignment(CEGUI::VA_TOP);  
  questDescription->setYPosition(CEGUI::UDim(0,27));
  questDescription->setSize(CEGUI::UVector2(CEGUI::UDim(0.9,0), CEGUI::UDim(0.40,0)));
  questDescription->setFont("DejaVuSans-10");
  questDescription->setProperty("VertFormatting", "TopAligned");
  questDescription->setClippedByParent(true);
  //questDescription->setReadOnly(true);
  _window->addChildWindow(questDescription);

   // Add the rewards title.
  CEGUI::Window* rewardsText = winManager.createWindow(GUILook + "/StaticText", windowName + "/RewardsText");
  rewardsText->setHorizontalAlignment(CEGUI::HA_CENTRE);
  rewardsText->setYPosition(CEGUI::UDim(0.55,0));
  rewardsText->setHeight(CEGUI::UDim(0,25));
  rewardsText->setProperty("VertFormatting", "TopAligned");
  rewardsText->setProperty("HorzFormatting", "LeftAligned");
  rewardsText->setProperty("FrameEnabled", "False");
  rewardsText->setProperty("BackgroundEnabled", "False");
  rewardsText->setFont("DejaVuSans-10");
  _window->addChildWindow(rewardsText);

  // Add the currency amount.
  CEGUI::Window* currencyText = winManager.createWindow(GUILook + "/StaticText", windowName + "/CurrencyText");
  currencyText->setHorizontalAlignment(CEGUI::HA_RIGHT);
  currencyText->setYPosition(CEGUI::UDim(0.55,0));
  currencyText->setHeight(CEGUI::UDim(0,25));
  currencyText->setProperty("VertFormatting", "TopAligned");
  currencyText->setProperty("HorzFormatting", "RightAligned");
  currencyText->setProperty("FrameEnabled", "False");
  currencyText->setProperty("BackgroundEnabled", "False");
  currencyText->setFont("DejaVuSans-10");
  _window->addChildWindow(currencyText);

  if(Player::getInstance()->getCurrentQuest())
  {
    this->addQuestToJournalWindow(Player::getInstance()->getCurrentQuest());
  } // end if

  return windowName;
} // string GUIManager::buildJournalWindow(CEGUI::FrameWindow* _window, CEGUI::UVector2 _size, CEGUI::UVector2 _pos, bool _open)


string GUIManager::buildSkillsWindow(CEGUI::UVector2 _size, CEGUI::UVector2 _pos, bool _open)
{
  CEGUI::System::getSingleton().getDefaultTooltip()->setFont("DejaVuSans-10");
  // Create the initial details.
  string GUILook = OgreFramework::getSingleton().GUILook;
  CEGUI::WindowManager& winManager = CEGUI::WindowManager::getSingleton();
  CEGUI::Window* sheet = winManager.getWindow("Sheet");

  // Create a frame window to serve as our journal.
  string windowName = "Skills";
  CEGUI::FrameWindow* window = (CEGUI::FrameWindow*)winManager.createWindow(GUILook + "/FrameWindow", windowName);
  window->setSize(_size);
  window->setHorizontalAlignment(CEGUI::HA_RIGHT);
  window->setVerticalAlignment(CEGUI::VA_BOTTOM);
  window->setPosition(_pos);
  window->setVisible(_open);
  window->setSizingEnabled(false);
  window->setCloseButtonEnabled(true);
  window->setText("Skills");
  window->subscribeEvent(CEGUI::FrameWindow::EventCloseClicked, 
    CEGUI::Event::Subscriber(&GUIManager::frameCloseButtonHit,this));
  window->setRollupEnabled(false);
  sheet->addChildWindow(window);

  // Create a number of skill slots based on the number of skills the class has.
  int numSkills = Player::getInstance()->getSkillClass()->getNumSkills();
  Skill** skills = Player::getInstance()->getSkillClass()->getSkills();

  // Set the dimensions.
  int slotWidth   = 50;
  int slotHeight  = 50;
  int imageWidth  = 40;
  int imageHeight = 40;
  CEGUI::UVector2 slotSize  = CEGUI::UVector2(CEGUI::UDim(0,slotWidth), CEGUI::UDim(0,slotHeight));
  CEGUI::UVector2 imageSize = CEGUI::UVector2(CEGUI::UDim(0,imageWidth), CEGUI::UDim(0,imageHeight));

  // Loop through the skills and place them.
  for(int i=0;i<numSkills;i++)
  {
    // @Todo - Consider that there might be more than 4 skills in a skillset.

    // First create the slot image and define the skill number to the slot.
    CEGUI::Window* skillSlot = winManager.createWindow(GUILook + "/StaticImage", "SkillSlot-" + 
      CEGUI::PropertyHelper::intToString(i));
    // Set the properties.
    skillSlot->setSize(slotSize);
    skillSlot->setHorizontalAlignment(CEGUI::HA_CENTRE);
    skillSlot->setVerticalAlignment(CEGUI::VA_TOP);
    // Set the position.
    double posX = (i+1)%(numSkills+1) * 0.25 - 0.25/2 - 0.5;
    double posY = (i/numSkills) * slotHeight + 5;
    skillSlot->setXPosition(CEGUI::UDim(posX, 0));
    skillSlot->setYPosition(CEGUI::UDim(0, posY));
    skillSlot->setTooltipText(skills[i]->getName());
  
    // Add the slot to the window.
    window->addChildWindow(skillSlot);

    // Create a drag drop container. 
    CEGUI::DragContainer* dragContainer = 
      static_cast<CEGUI::DragContainer*>(winManager.createWindow("DragContainer", 
      "SkillSlot/DragContainer-" + CEGUI::PropertyHelper::intToString(i)));
    dragContainer->setSize(imageSize);
    dragContainer->setHorizontalAlignment(CEGUI::HA_CENTRE);
    dragContainer->setVerticalAlignment(CEGUI::VA_CENTRE);
    
    dragContainer->setMouseInputPropagationEnabled(true);
    dragContainer->setStickyModeEnabled(true);
    dragContainer->setDraggingEnabled(true);
    dragContainer->setAlwaysOnTop(true);
    dragContainer->subscribeEvent(CEGUI::Window::EventMouseClick, 
      CEGUI::Event::Subscriber(&GUIManager::handleSkillSlotClicked,this));

    // Next, create the image that will appear in the slot.
    CEGUI::Window* skillImage = winManager.createWindow(GUILook + "/StaticImage", "SkillSlot/Image-" +
      CEGUI::PropertyHelper::intToString(i));
    skillImage->setSize(imageSize);
    skillImage->setHorizontalAlignment(CEGUI::HA_CENTRE);
    skillImage->setVerticalAlignment(CEGUI::VA_CENTRE);
    skillImage->setProperty("FrameEnabled", "False");
    skillImage->setProperty("BackgroundEnabled", "True");
    
    string imageName = skills[i]->getImageName();

    // If the skill level is less than 1, add the b&w image and disable drag and drop.
    if(skills[i]->getLevel() <= 0)
    {
      size_t found;
      // Append -bw on to the image name before the period.
      found = imageName.find('.');
      if(found != string::npos)
      {
        imageName.insert(found,"-bw");
      } // end if

      dragContainer->setDraggingEnabled(false);
    } // end if

    skillImage->setProperty("Image", "set:"  + imageName + " image:full_image");
    skillImage->setClippedByParent(true);
    skillImage->setMousePassThroughEnabled(true);
    skillImage->setEnabled(false);

    // Clone an image onto the drag container.
    CEGUI::Window* skillImageClone = skillImage->clone("SkillSlot/DragContainer/Image-" + 
      CEGUI::PropertyHelper::intToString(i));
    skillImageClone->setMousePassThroughEnabled(true);

    // Add the drag container and clones image on top.
    dragContainer->addChildWindow(skillImageClone);
    skillSlot->addChildWindow(dragContainer);
    skillSlot->addChildWindow(skillImage);

    // Add a text image displaying the skill level.
    CEGUI::Window* skillLevelText = winManager.createWindow(GUILook + "/StaticText", "SkillSlot/LevelText-" + 
      CEGUI::PropertyHelper::intToString(i));
    skillLevelText->setHorizontalAlignment(CEGUI::HA_CENTRE);
    skillLevelText->setVerticalAlignment(CEGUI::VA_TOP);
    skillLevelText->setXPosition(CEGUI::UDim(posX, 0));
    skillLevelText->setYPosition(CEGUI::UDim(0, posY+slotHeight));
    skillLevelText->setSize(CEGUI::UVector2(CEGUI::UDim(0, slotWidth), CEGUI::UDim(0, 25)));
    skillLevelText->setText(CEGUI::PropertyHelper::intToString(skills[i]->getLevel()));
    skillLevelText->setProperty("HorzFormatting", "HorzCentred");
    skillLevelText->setFont("DejaVuSans-10");
    window->addChildWindow(skillLevelText);

    // Create a plus Button underneath.
    CEGUI::PushButton* plusButton = (CEGUI::PushButton*)winManager.createWindow(GUILook + "/Button", 
      "SkillSlot/PlusButton-" + CEGUI::PropertyHelper::intToString(i));
    plusButton->setHorizontalAlignment(CEGUI::HA_CENTRE);
    plusButton->setVerticalAlignment(CEGUI::VA_TOP);
    plusButton->setXPosition(CEGUI::UDim(posX, 0));
    plusButton->setYPosition(CEGUI::UDim(0, posY+slotHeight+22));
    plusButton->setText("+");
    plusButton->setWidth(CEGUI::UDim(0, slotWidth));
    plusButton->setHeight(CEGUI::UDim(0, 30));
    plusButton->subscribeEvent(CEGUI::PushButton::EventMouseClick, 
      CEGUI::Event::Subscriber(&GUIManager::skillPlusButtonClicked, this));
    window->addChildWindow(plusButton);

    if(Player::getInstance()->getSkillPoints() <= 0)
    {
      plusButton->setVisible(false);
    } // end if

  } // end forfor(int i=0;i<numSkills;i++)


  // Add a text display to show the number of skill points the player has.
  CEGUI::Window* skillPointsDisplay = winManager.createWindow(GUILook + "/StaticText", "SkillPointsText");
  skillPointsDisplay->setHorizontalAlignment(CEGUI::HA_CENTRE);
  skillPointsDisplay->setVerticalAlignment(CEGUI::VA_BOTTOM);
  skillPointsDisplay->setSize(CEGUI::UVector2(CEGUI::UDim(0, 120), CEGUI::UDim(0,30)));
  CEGUI::String skillPointsText = "Skill Points: " + 
    CEGUI::PropertyHelper::intToString(Player::getInstance()->getSkillPoints()); 
  skillPointsDisplay->setText(skillPointsText);
  skillPointsDisplay->setProperty("HorzFormatting", "HorzCentred");
  skillPointsDisplay->setFont("DejaVuSans-10");
  window->addChildWindow(skillPointsDisplay);

  return windowName;
} // string GUIManager::buildSkillsWindow(CEGUI::UVector2 _size, CEGUI::UVector2 _pos, bool _open)


void GUIManager::updateSkillPoints()
{
  CEGUI::WindowManager& winManager = CEGUI::WindowManager::getSingleton();

  if(winManager.isWindowPresent("SkillPointsText"))
  {
    CEGUI::String text = "Skill Points: " + 
      CEGUI::PropertyHelper::intToString(Player::getInstance()->getSkillPoints()); 
    winManager.getWindow("SkillPointsText")->setText(text);
  } // end if

  if(winManager.isWindowPresent("SkillPlusImage"))
  {
    CEGUI::Window* skillPlusImage = winManager.getWindow("SkillPlusImage");
    if(Player::getInstance()->getSkillPoints() > 0)
    {
      skillPlusImage->setVisible(true);

      // Remove the buttons.
      for(int i=0;i<Player::getInstance()->getSkillClass()->getNumSkills();i++)
      {
        CEGUI::Window* buttonWindow = winManager.getWindow("SkillSlot/PlusButton-" + 
          CEGUI::PropertyHelper::intToString(i));
        buttonWindow->setVisible(true);
      } // end for

    } // end if
    else
    {
      skillPlusImage->setVisible(false);
      // Remove the buttons.
      for(int i=0;i<Player::getInstance()->getSkillClass()->getNumSkills();i++)
      {
        CEGUI::Window* buttonWindow = winManager.getWindow("SkillSlot/PlusButton-" + 
          CEGUI::PropertyHelper::intToString(i));
        buttonWindow->setVisible(false);
      } // end for
    } // end else
  } // end if

} // void GUIManager::updateSkillPoints()


void GUIManager::buildActiveEffectsWindow()
{

  // Load the image. Cheeky, should be done at loading screen (Copy and paste :]). @Todo
  CEGUI::ImagesetManager::getSingleton().createFromImageFile("heal.tga", "heal.tga");

  // Create the initial details.
  string GUILook = OgreFramework::getSingleton().GUILook;
  CEGUI::WindowManager& winManager = CEGUI::WindowManager::getSingleton();
  CEGUI::Window* sheet = winManager.getWindow("Sheet");

  // Create a outlined window.
  string windowName = "ActiveEffectsWindow";
  CEGUI::Window* window = winManager.createWindow(GUILook + "/SimpleTransparentPanel", windowName);

  const int WINDOW_WIDTH  = 250;
  const int WINDOW_HEIGHT = 140;

  window->setWidth(CEGUI::UDim(0,WINDOW_WIDTH));
  window->setHeight(CEGUI::UDim(0,WINDOW_HEIGHT));
  window->setHorizontalAlignment(CEGUI::HA_RIGHT);
  window->setVerticalAlignment(CEGUI::VA_TOP);
  window->setXPosition(CEGUI::UDim(0,-5));
  window->setYPosition(CEGUI::UDim(0,5));
  sheet->addChildWindow(window);

  const int TEXT_HEIGHT = 30;

  // Add text to the top.
  CEGUI::Window* title = winManager.createWindow(GUILook + "/StaticText", windowName + "/Title");

  title->setWidth(CEGUI::UDim(1.0,0));
  title->setHeight(CEGUI::UDim(0,TEXT_HEIGHT));
  title->setHorizontalAlignment(CEGUI::HA_CENTRE);
  title->setVerticalAlignment(CEGUI::VA_TOP);
  title->setYPosition(CEGUI::UDim(0,0));
  title->setText("Active Effects");
  title->setFont("DejaVuSans-10");
  title->setProperty("HorzFormatting", "HorzCentred");
  window->addChildWindow(title);
  
  // Loop through and check if the player has any active effects and add the images.
  vector<AbstractEffect*>* effects = Player::getInstance()->getActiveEffects();

  for(vector<AbstractEffect*>::iterator it = effects->begin(); it != effects->end(); ++it)
  {
    AbstractEffect* currentEffect = *it;

    this->addActiveEffect(currentEffect);
  } // end for


} // void GUIManager::buildActiveEffectsWindow()


void GUIManager::buildTargettedEnemyWindow()
{
  // Create the initial details.
  string GUILook = OgreFramework::getSingleton().GUILook;
  CEGUI::WindowManager& winManager = CEGUI::WindowManager::getSingleton();
  CEGUI::Window* sheet = winManager.getWindow("Sheet");

  // Create a outlined window.
  string windowName = "TargetWindow";
  CEGUI::ProgressBar* window = (CEGUI::ProgressBar*)winManager.createWindow(GUILook + "/ProgressBar", windowName);
  window->setHorizontalAlignment(CEGUI::HA_CENTRE);
  window->setVerticalAlignment(CEGUI::VA_TOP);
  window->setYPosition(CEGUI::UDim(0,5));
  window->setWidth(CEGUI::UDim(0, 200));
  window->setHeight(CEGUI::UDim(0, 35));
  window->setProgress(1.0);
  window->setVisible(false);
  sheet->addChildWindow(window);

  // Create some text to go on the windows.
  CEGUI::Window* text = winManager.createWindow(GUILook + "/StaticText", windowName + "/Text");
  text->setWidth(CEGUI::UDim(1.0,0));
  text->setHeight(CEGUI::UDim(1.0,0));
  text->setHorizontalAlignment(CEGUI::HA_CENTRE);
  text->setVerticalAlignment(CEGUI::VA_CENTRE);
  text->setFont("DejaVuSans-10");
  text->setProperty("HorzFormatting", "HorzCentred");
  text->setProperty("BackgroundEnabled", "False");
  text->setProperty("FrameEnabled", "False");
  window->addChildWindow(text);

} // void GUIManager::buildTargettedEnemyWindow()


void GUIManager::updateTargettedEnemyWindow(Mob* _mob)
{
  // Get the window and the text.
  CEGUI::WindowManager& winManager = CEGUI::WindowManager::getSingleton();
  CEGUI::ProgressBar* targetHealth = (CEGUI::ProgressBar*)winManager.getWindow("TargetWindow");
  CEGUI::Window* targetName = winManager.getWindow("TargetWindow/Text");

  // Update based on the mob parameter passed in.
  if(_mob)
  {
    targetHealth->setVisible(true);
    targetName->setText(_mob->getName());
    double percentage = (double)_mob->getHP()/(double)_mob->getMaxHP();
    this->updateTargettedEnemyHealth(percentage);
    targetName->setProperty("HorzFormatting", "HorzCentred");
  } // end if
  else
  {
    targetHealth->setVisible(false);
  } // end else
} // void GUIManager::updateTargettedEnemyWindow()


void GUIManager::buildWindowButtons()
{
  // Load in the imageset.
  if(!CEGUI::ImagesetManager::getSingleton().isDefined("windows-buttons.imageset"))
  {
    CEGUI::ImagesetManager::getSingleton().create("window-buttons.imageset");
    CEGUI::ImagesetManager::getSingleton().createFromImageFile("gold-plus","gold-plus.png");
  } // end if
  CEGUI::WindowManager& winManager = CEGUI::WindowManager::getSingleton();
  string GUILook = OgreFramework::getSingleton().GUILook;
  CEGUI::Window* sheet = winManager.getWindow("Sheet");


  CEGUI::UVector2 buttonSize = CEGUI::UVector2(CEGUI::UDim(0,64), CEGUI::UDim(0, 64));
  int xPos = -5;
  int yPos = -5;

  // Add the first button.
  CEGUI::PushButton* inventoryButton = (CEGUI::PushButton*)winManager.createWindow(GUILook + "/ImageButton","InventoryButton");
  inventoryButton->setProperty("NormalImage","set:window-buttons image:inventory-btn-normal");
  inventoryButton->setProperty("HoverImage","set:window-buttons image:inventory-btn-hover");
  inventoryButton->setProperty("PushedImage","set:window-buttons image:inventory-btn-pushed");
  inventoryButton->setProperty("DisabledImage","set:window-buttons image:inventory-btn-disabled");
  inventoryButton->setHorizontalAlignment(CEGUI::HA_RIGHT);
  inventoryButton->setVerticalAlignment(CEGUI::VA_BOTTOM);
  inventoryButton->setSize(buttonSize);
  inventoryButton->setXPosition(CEGUI::UDim(0,xPos));
  inventoryButton->setYPosition(CEGUI::UDim(0,yPos));
  inventoryButton->subscribeEvent(CEGUI::PushButton::EventMouseClick, CEGUI::Event::Subscriber(
    &GameScene::handleWindowButtonClicked, (GameScene*)GameManager::getInstance()->getCurrentScene()));
  sheet->addChildWindow(inventoryButton);

  // Add the second button.
  CEGUI::PushButton* characterButton = (CEGUI::PushButton*)winManager.createWindow(GUILook + "/ImageButton","CharacterButton");
  characterButton->setProperty("NormalImage","set:window-buttons image:character-btn-normal");
  characterButton->setProperty("HoverImage","set:window-buttons image:character-btn-hover");
  characterButton->setProperty("PushedImage","set:window-buttons image:character-btn-pushed");
  characterButton->setProperty("DisabledImage","set:window-buttons image:character-btn-disabled");
  characterButton->setHorizontalAlignment(CEGUI::HA_RIGHT);
  characterButton->setVerticalAlignment(CEGUI::VA_BOTTOM);
  characterButton->setSize(buttonSize);
  characterButton->setXPosition(CEGUI::UDim(0,xPos));
  characterButton->setYPosition(CEGUI::UDim(0,yPos-64));
  sheet->addChildWindow(characterButton);
  characterButton->subscribeEvent(CEGUI::PushButton::EventMouseClick, CEGUI::Event::Subscriber(
    &GameScene::handleWindowButtonClicked, (GameScene*)GameManager::getInstance()->getCurrentScene()));

  // Add the third button.
  CEGUI::PushButton* skillButton = (CEGUI::PushButton*)winManager.createWindow(GUILook + "/ImageButton","SkillButton");
  skillButton->setProperty("NormalImage","set:window-buttons image:skill-btn-normal");
  skillButton->setProperty("HoverImage","set:window-buttons image:skill-btn-hover");
  skillButton->setProperty("PushedImage","set:window-buttons image:skill-btn-pushed");
  skillButton->setProperty("DisabledImage","set:window-buttons image:skill-btn-disabled");
  skillButton->setHorizontalAlignment(CEGUI::HA_RIGHT);
  skillButton->setVerticalAlignment(CEGUI::VA_BOTTOM);
  skillButton->setSize(buttonSize);
  skillButton->setXPosition(CEGUI::UDim(0,xPos));
  skillButton->setYPosition(CEGUI::UDim(0,yPos-128));
  sheet->addChildWindow(skillButton);
  skillButton->subscribeEvent(CEGUI::PushButton::EventMouseClick, CEGUI::Event::Subscriber(
    &GameScene::handleWindowButtonClicked, (GameScene*)GameManager::getInstance()->getCurrentScene()));

  // Add the plus sign on the top right of the skill window.
  CEGUI::Window* plusImage = winManager.createWindow(GUILook + "/StaticImage", "SkillPlusImage");
  plusImage->setSize(CEGUI::UVector2(CEGUI::UDim(0,30),CEGUI::UDim(0,30)));
  plusImage->setHorizontalAlignment(CEGUI::HA_LEFT);
  plusImage->setVerticalAlignment(CEGUI::VA_TOP);
  plusImage->setProperty("FrameEnabled", "False");
  plusImage->setProperty("BackgroundEnabled", "False");
  plusImage->setProperty("Image","set:gold-plus image:full_image");
  plusImage->setMousePassThroughEnabled(true);
  plusImage->setClippedByParent(false);
  skillButton->addChildWindow(plusImage);

  // Check to see if there are any skill points.
  if(Player::getInstance()->getSkillPoints() > 0)
  {
    plusImage->setVisible(true);
  } // end if
  else
  {
    plusImage->setVisible(false);
  } // end else

  // Add the fourth button.
  CEGUI::PushButton* journalButton = (CEGUI::PushButton*)winManager.createWindow(GUILook + "/ImageButton","JournalButton");
  journalButton->setProperty("NormalImage","set:window-buttons image:journal-btn-normal");
  journalButton->setProperty("HoverImage","set:window-buttons image:journal-btn-hover");
  journalButton->setProperty("PushedImage","set:window-buttons image:journal-btn-pushed");
  journalButton->setProperty("DisabledImage","set:window-buttons image:journal-btn-disabled");
  journalButton->setHorizontalAlignment(CEGUI::HA_RIGHT);
  journalButton->setVerticalAlignment(CEGUI::VA_BOTTOM);
  journalButton->setSize(buttonSize);
  journalButton->setXPosition(CEGUI::UDim(0,xPos));
  journalButton->setYPosition(CEGUI::UDim(0,yPos-192));
  sheet->addChildWindow(journalButton);
  journalButton->subscribeEvent(CEGUI::PushButton::EventMouseClick, CEGUI::Event::Subscriber(
    &GameScene::handleWindowButtonClicked, (GameScene*)GameManager::getInstance()->getCurrentScene()));

} // void GUIManager::buildWindowButtons()


void GUIManager::updateTargettedEnemyHealth(double _percentage)
{
  CEGUI::WindowManager& winManager = CEGUI::WindowManager::getSingleton();
  CEGUI::ProgressBar* targetHealth = (CEGUI::ProgressBar*)winManager.getWindow("TargetWindow");
  if(targetHealth->isVisible())
  {
    targetHealth->setProgress(_percentage);
  } // end fi
} // void GUIManager::updateTargettedEnemyHealth()


string GUIManager::buildHotBar()
{
  // Create the initial details.
  string GUILook = OgreFramework::getSingleton().GUILook;
  CEGUI::WindowManager& winManager = CEGUI::WindowManager::getSingleton();
  CEGUI::Window* sheet = winManager.getWindow("Sheet");

  // Create a outlined window to serve as our hotbar.
  string windowName = "HotBar";
  CEGUI::Window* window = winManager.createWindow(GUILook + "/SimplePanel", windowName);
  
  int width  = 411;
  int height = 57;
  window->setWidth(CEGUI::UDim(0,width));
  window->setHeight(CEGUI::UDim(0,height));
  window->setHorizontalAlignment(CEGUI::HA_CENTRE);
  window->setVerticalAlignment(CEGUI::VA_BOTTOM);
  window->setYPosition(CEGUI::UDim(0,-5));
  sheet->addChildWindow(window);

  int slotWidth  = 50;
  int slotHeight = 50;
  CEGUI::UVector2 size = CEGUI::UVector2(CEGUI::UDim(0, slotWidth), CEGUI::UDim(0, slotHeight));

  AbstractHotSlot** hotSlots = HotBar::getSingleton().getSlots();

  // Add the slot placeholders.
  for(int i=0;i<8;i++)
  {
    CEGUI::Window* hotSlot = winManager.createWindow(GUILook + "/SimpleTransparentPanel", 
      "HotBar/Slot-" + CEGUI::PropertyHelper::intToString(i));
    hotSlot->setSize(size);
    hotSlot->setVerticalAlignment(CEGUI::VA_CENTRE);
    hotSlot->setXPosition(CEGUI::UDim(0, slotWidth*i+5));
    hotSlot->subscribeEvent(CEGUI::Window::EventDragDropItemDropped,
      CEGUI::Event::Subscriber(&GUIManager::itemDroppedOnHotSlot,this));
    hotSlot->subscribeEvent(CEGUI::Window::EventMouseClick,
      CEGUI::Event::Subscriber(&GUIManager::hotBarSlotClicked, this));
    window->addChildWindow(hotSlot);

    // Add the image and count to the hotbar.
    CEGUI::Window* imageWindow = winManager.createWindow(OgreFramework::getSingleton().GUILook + "/StaticImage",
      "HotBar/Slot/Image-" + CEGUI::PropertyHelper::intToString(i));
    imageWindow->setHeight(CEGUI::UDim(0,40));
    imageWindow->setWidth(CEGUI::UDim(0,40));
    imageWindow->setHorizontalAlignment(CEGUI::HA_CENTRE);
    imageWindow->setVerticalAlignment(CEGUI::VA_CENTRE);
    imageWindow->setProperty("FrameEnabled", "False");
    imageWindow->setMousePassThroughEnabled(true);
    imageWindow->setVisible(false);
    hotSlot->addChildWindow(imageWindow);

    // Check to see if an image exists.
    if(hotSlots[i])
    {
      imageWindow->setVisible(true);
      imageWindow->setProperty("Image", "set:" + hotSlots[i]->getImageName() + " image:full_image");
    } // end if

    // Add the number count of item.
    CEGUI::Window* countWindow = winManager.createWindow(OgreFramework::getSingleton().GUILook + "/StaticText",
      "HotBar/Slot/Count-" + CEGUI::PropertyHelper::intToString(i));
    countWindow->setMousePassThroughEnabled(true);
    countWindow->setHorizontalAlignment(CEGUI::HA_LEFT);
    countWindow->setVerticalAlignment(CEGUI::VA_BOTTOM);
    countWindow->setProperty("HorzFormatting", "HorzLeft");
    countWindow->setProperty("VertFormatting", "BottomAligned");
    countWindow->setProperty("FrameEnabled", "False");
    countWindow->setProperty("BackgroundEnabled", "False");
    countWindow->setFont("DejaVuSans-8");
    countWindow->setSize(CEGUI::UVector2(CEGUI::UDim(0.6,0),CEGUI::UDim(0.35,0)));
    countWindow->setXPosition(CEGUI::UDim(0,5));
    countWindow->setYPosition(CEGUI::UDim(0,-5));
    countWindow->setVisible(false);
    hotSlot->addChildWindow(countWindow);

    if(hotSlots[i])
    {
      if(hotSlots[i]->getType() == "Item")
      {
        ItemHotSlot* hotSlotItem = (ItemHotSlot*)hotSlots[i];
        countWindow->setVisible(true);
        countWindow->setText(CEGUI::PropertyHelper::intToString(hotSlotItem->getInventorySlot()->getCount()));
      } // end if
    } // end if

    // Add the progress bars.
    CEGUI::ProgressBar* cooldownBar = (CEGUI::ProgressBar*)winManager.createWindow(
      OgreFramework::getSingleton().GUILook + "/ProgressBarAlpha",
      "HotBar/Slot/Cooldown-"  + CEGUI::PropertyHelper::intToString(i));
    cooldownBar->setMousePassThroughEnabled(true);
    cooldownBar->setSize(CEGUI::UVector2(CEGUI::UDim(0.96,0), CEGUI::UDim(0.85,0)));
    cooldownBar->setClippedByParent(true);
    cooldownBar->setHorizontalAlignment(CEGUI::HA_CENTRE);
    cooldownBar->setVerticalAlignment(CEGUI::VA_CENTRE);
    cooldownBar->setProgress(0.0);
    cooldownBar->setAlwaysOnTop(true);
    hotSlot->addChildWindow(cooldownBar);

  } // end for


  return windowName;
} // string GUIManager::buildHotBar()


void GUIManager::addQuestToJournalWindow(Quest* _quest)
{
  // Create the initial details.
  string GUILook = OgreFramework::getSingleton().GUILook;
  CEGUI::WindowManager& winManager = CEGUI::WindowManager::getSingleton();
  

  winManager.getWindow("Journal/TitleText")->setText(_quest->getName());
  winManager.getWindow("Journal/DescriptionText")->setText(_quest->getObjectiveText());
  winManager.getWindow("Journal/RewardsText")->setText("Rewards:");
  winManager.getWindow("Journal/CurrencyText")->setText(
    CEGUI::PropertyHelper::intToString(_quest->getRewardCurrency()) + " Shiny Rocks");

  CEGUI::FrameWindow* window = (CEGUI::FrameWindow*)winManager.getWindow("Journal");

  // Add the reward graphics.
  int numRewards = _quest->getNumRewards();
  RewardItem** rewards = _quest->getRewards();

  // Pre define the size of the reward slots.
  const int xSizeSlot  = 50;
  const int ySizeSlot  = 50;
  const int xImageSlot = 40;
  const int yImageSlot = 40;
  CEGUI::UVector2 slotSize  = CEGUI::UVector2(CEGUI::UDim(0,xSizeSlot),CEGUI::UDim(0,ySizeSlot));
  CEGUI::UVector2 imageSize = CEGUI::UVector2(CEGUI::UDim(0,xImageSlot),CEGUI::UDim(0,yImageSlot));

  for(int i=0;i<numRewards;i++)
  {
    AbstractItem* item = ItemManager::getInstance()->getItem(rewards[i]->getItemName());
    // Create a slot.
    CEGUI::Window* rewardSlot = winManager.createWindow(GUILook + "/StaticImage", 
      "JournalRewardSlot-" + CEGUI::PropertyHelper::intToString(i));
    rewardSlot->setSize(slotSize);
    rewardSlot->setMouseInputPropagationEnabled(true);

    // Set the xPositon of the slot.
    double xPos = ((double)(i+1)/(double)(numRewards+1)) -0.5;
    double yPos = 0.65;
    // Place the position of the slot and add it to the window.
    rewardSlot->setHorizontalAlignment(CEGUI::HA_CENTRE);
    rewardSlot->setPosition(CEGUI::UVector2(CEGUI::UDim(xPos,0),CEGUI::UDim(yPos,0)));
    window->addChildWindow(rewardSlot);

    // Add the image to the slot.
    // Create a slot.
    CEGUI::Window* rewardImage = winManager.createWindow(GUILook + "/StaticImage", 
      "Journal/" + item->getName());
    rewardImage->setProperty("FrameEnabled","False");
    rewardImage->setProperty("Image","set:" + item->getImageName() + " image:full_image");
    rewardImage->setSize(imageSize);
    rewardImage->setHorizontalAlignment(CEGUI::HA_CENTRE);
    rewardImage->setVerticalAlignment(CEGUI::VA_CENTRE);
    rewardImage->subscribeEvent(CEGUI::Window::EventMouseButtonDown, 
      CEGUI::Event::Subscriber(&GUIManager::handleRewardItemClicked, this));
    rewardSlot->addChildWindow(rewardImage);

    if(rewards[i]->getAmount() > 1)
    {
      CEGUI::Window* itemCount = winManager.createWindow(OgreFramework::getSingleton().GUILook + "/StaticText", 
        "Journal/RewardItem-" + CEGUI::PropertyHelper::intToString(i) + "/Count");
      itemCount->setMousePassThroughEnabled(true);
      itemCount->setText(CEGUI::PropertyHelper::intToString(rewards[i]->getAmount()));
      itemCount->setHorizontalAlignment(CEGUI::HA_LEFT);
      itemCount->setVerticalAlignment(CEGUI::VA_BOTTOM);
      itemCount->setProperty("HorzFormatting", "HorzLeft");
      itemCount->setProperty("VertFormatting", "BottomAligned");
      itemCount->setProperty("FrameEnabled", "False");
      itemCount->setProperty("BackgroundEnabled", "False");
      itemCount->setFont("DejaVuSans-8");
      itemCount->setSize(CEGUI::UVector2(CEGUI::UDim(0.6,0),CEGUI::UDim(0.35,0)));
      rewardImage->addChildWindow(itemCount);
    } // end if
  } // end for

} // void GUIManager::addQuestToJournalWindow(Quest* _quest)


void GUIManager::updateJournal(Quest* _quest)
{
  // Create the initial details.
  string GUILook = OgreFramework::getSingleton().GUILook;
  CEGUI::WindowManager& winManager = CEGUI::WindowManager::getSingleton();
  if(winManager.isWindowPresent("Journal"))
  {
    winManager.getWindow("Journal/DescriptionText")->setText(_quest->getObjectiveText());
  } // end if
} // void GUIManager::updateJournal(Quest* _quest)


void GUIManager::clearJournal()
{
  // Create the initial details.
  string GUILook = OgreFramework::getSingleton().GUILook;
  CEGUI::WindowManager& winManager = CEGUI::WindowManager::getSingleton();
  
  winManager.getWindow("Journal/TitleText")->setText("No Current Quest :(");
  winManager.getWindow("Journal/DescriptionText")->setText("");
  winManager.getWindow("Journal/RewardsText")->setText("");
  winManager.getWindow("Journal/CurrencyText")->setText("");

  CEGUI::FrameWindow* window = (CEGUI::FrameWindow*)winManager.getWindow("Journal");

  // Add the reward graphics.
  int numRewards = Player::getInstance()->getCurrentQuest()->getNumRewards();

  for(int i=0;i<numRewards;i++)
  {
    // Destroy the slot.
    winManager.destroyWindow("JournalRewardSlot-" + CEGUI::PropertyHelper::intToString(i));
  } // end for
} // void GUIManager::clearJournal()


string GUIManager::buildActiveQuestWindow(Quest* _quest, NPC* _npc)
{
  // Create the initial details.
  string GUILook = OgreFramework::getSingleton().GUILook;
  CEGUI::WindowManager& winManager = CEGUI::WindowManager::getSingleton();
  CEGUI::Window* sheet = winManager.getWindow("Sheet");

  CEGUI::FrameWindow* window = NULL;
  CEGUI::UVector2 size = CEGUI::UVector2(CEGUI::UDim(0,200), CEGUI::UDim(0,120));
  CEGUI::UVector2 position = CEGUI::UVector2(CEGUI::UDim(0,5), CEGUI::UDim(0,115));

  string windowName = this->createInteractFrameWindow(window, size, position, _npc->getName());
  window = (CEGUI::FrameWindow*)winManager.getWindow(windowName);

  // Create a close button.
  CEGUI::PushButton* closeButton = static_cast<CEGUI::PushButton*>(winManager.createWindow(
    GUILook + "/Button", windowName + "/CloseButton"));

  closeButton->setText("Close");
  closeButton->setFont("DejaVuSans-10");
  closeButton->setSize(CEGUI::UVector2(CEGUI::UDim(0.55,0), CEGUI::UDim(0,30)));
  closeButton->setHorizontalAlignment(CEGUI::HA_CENTRE);
  closeButton->setVerticalAlignment(CEGUI::VA_BOTTOM);
  closeButton->subscribeEvent(CEGUI::Window::EventMouseButtonUp, 
    CEGUI::Event::Subscriber(&GUIManager::closeQuestBoxButtonPressed,this));
  

  // Add the text to the window.
  CEGUI::Window* windowText = winManager.createWindow(GUILook + "/StaticText", windowName + "/DescriptionTest");
  windowText->setText(_quest->getDuringDialogue());
  windowText->setSize(CEGUI::UVector2(CEGUI::UDim(0.95,0),CEGUI::UDim(0.6,0)));
  windowText->setHorizontalAlignment(CEGUI::HA_CENTRE);
  windowText->setVerticalAlignment(CEGUI::VA_TOP);
  windowText->setYPosition(CEGUI::UDim(0,5));
  windowText->setProperty("VertFormatting", "TopAligned");
  windowText->setProperty("HorzFormatting", "WordWrapLeftAligned");
  windowText->setProperty("FrameEnabled", "False");
  windowText->setProperty("BackgroundEnabled", "False");
  windowText->setFont("DejaVuSans-10");
  window->addChildWindow(windowText);
  window->addChildWindow(closeButton);

  return windowName;
  
} // string GUIManager::buildActiveQuestWindow(Quest* _quest, NPC* _npc)


string GUIManager::buildAvailableQuestWindow(Quest* _quest, NPC* _npc)
{
  // Create the initial details.
  string GUILook = OgreFramework::getSingleton().GUILook;
  CEGUI::WindowManager& winManager = CEGUI::WindowManager::getSingleton();
  CEGUI::Window* sheet = winManager.getWindow("Sheet");

  CEGUI::FrameWindow* window = NULL;
  CEGUI::UVector2 size = CEGUI::UVector2(CEGUI::UDim(0,300), CEGUI::UDim(0,450));
  CEGUI::UVector2 position = CEGUI::UVector2(CEGUI::UDim(0,5), CEGUI::UDim(0,115));

  string windowName = this->createInteractFrameWindow(window, size, position, _npc->getName());
  window = (CEGUI::FrameWindow*)winManager.getWindow(windowName);

  // Build a close button.
  if(winManager.isWindowPresent(windowName + "/CloseButton"))
  {
    winManager.destroyWindow(windowName + "/CloseButton");
  } // end if
  CEGUI::PushButton* closeButton = static_cast<CEGUI::PushButton*>(winManager.createWindow(
  GUILook + "/Button", windowName + "/CloseButton"));

  closeButton->setText("Close");
  closeButton->setFont("DejaVuSans-10");
  closeButton->setSize(CEGUI::UVector2(CEGUI::UDim(0.40,0), CEGUI::UDim(0,30)));
  closeButton->setHorizontalAlignment(CEGUI::HA_RIGHT);
  closeButton->setVerticalAlignment(CEGUI::VA_BOTTOM);
  closeButton->setXPosition(CEGUI::UDim(-0.05,0));
  closeButton->subscribeEvent(CEGUI::Window::EventMouseButtonUp, 
    CEGUI::Event::Subscriber(&GUIManager::closeQuestBoxButtonPressed,this));

  // Build an accept button.
  if(winManager.isWindowPresent(_quest->getName()))
  {
    winManager.getWindow(_quest->getName())->destroy();
  } // end if
  CEGUI::PushButton* acceptButton = static_cast<CEGUI::PushButton*>(winManager.createWindow(
    GUILook + "/Button", _quest->getName()));

  acceptButton->setText("Accept");
  acceptButton->setFont("DejaVuSans-10");
  acceptButton->setSize(CEGUI::UVector2(CEGUI::UDim(0.40,0), CEGUI::UDim(0,30)));
  acceptButton->setHorizontalAlignment(CEGUI::HA_LEFT);
  acceptButton->setVerticalAlignment(CEGUI::VA_BOTTOM);
  acceptButton->setXPosition(CEGUI::UDim(0.05,0));
  acceptButton->subscribeEvent(CEGUI::Window::EventMouseButtonUp, 
    CEGUI::Event::Subscriber(&GUIManager::acceptQuestButtonPressed,this));
  
  // Add the quest Title to the window.
  CEGUI::Window* questTitle = winManager.createWindow(GUILook + "/StaticText", windowName + "/TitleText");
  questTitle->setText(_quest->getName());
  questTitle->setHorizontalAlignment(CEGUI::HA_LEFT);
  questTitle->setVerticalAlignment(CEGUI::VA_TOP);
  questTitle->setYPosition(CEGUI::UDim(0,5));
  questTitle->setProperty("VertFormatting", "TopAligned");
  questTitle->setProperty("HorzFormatting", "WordWrapLeftAligned");
  questTitle->setProperty("FrameEnabled", "False");
  questTitle->setProperty("BackgroundEnabled", "False");
  questTitle->setFont("DejaVuSans-12");
  window->addChildWindow(questTitle);
  
  // Add the quest text to the window.
  CEGUI::MultiLineEditbox* questDescription = (CEGUI::MultiLineEditbox*)winManager.createWindow(
    GUILook + "/MultiLineEditbox", windowName + "/DescriptionText");
  questDescription->setText(_quest->getOpeningDialogue());
  questDescription->setHorizontalAlignment(CEGUI::HA_CENTRE);
  questDescription->setVerticalAlignment(CEGUI::VA_TOP);  
  questDescription->setYPosition(CEGUI::UDim(0,27));
  questDescription->setSize(CEGUI::UVector2(CEGUI::UDim(1.0,0), CEGUI::UDim(0.60,0)));
  questDescription->setFont("DejaVuSans-10");
  questDescription->setClippedByParent(true);
  questDescription->setReadOnly(true);
  window->addChildWindow(questDescription);

  // Add the rewards title.
  CEGUI::Window* rewardsText = winManager.createWindow(GUILook + "/StaticText", windowName + "/RewardsText");
  rewardsText->setText("Rewards:");
  rewardsText->setHorizontalAlignment(CEGUI::HA_CENTRE);
  rewardsText->setYPosition(CEGUI::UDim(0.69,0));
  rewardsText->setHeight(CEGUI::UDim(0,25));
  rewardsText->setProperty("VertFormatting", "TopAligned");
  rewardsText->setProperty("HorzFormatting", "LeftAligned");
  rewardsText->setProperty("FrameEnabled", "False");
  rewardsText->setProperty("BackgroundEnabled", "False");
  rewardsText->setFont("DejaVuSans-10");
  window->addChildWindow(rewardsText);

  // Add the currency amount.
  CEGUI::Window* currencyText = winManager.createWindow(GUILook + "/StaticText", windowName + "/CurrencyText");
  currencyText->setText(CEGUI::PropertyHelper::intToString(_quest->getRewardCurrency()) + " Shiny Rocks ");
  currencyText->setHorizontalAlignment(CEGUI::HA_RIGHT);
  currencyText->setYPosition(CEGUI::UDim(0.69,0));
  currencyText->setHeight(CEGUI::UDim(0,25));
  currencyText->setProperty("VertFormatting", "TopAligned");
  currencyText->setProperty("HorzFormatting", "RightAligned");
  currencyText->setProperty("FrameEnabled", "False");
  currencyText->setProperty("BackgroundEnabled", "False");
  currencyText->setFont("DejaVuSans-10");
  window->addChildWindow(currencyText);

  // Add the reward graphics.
  int numRewards = _quest->getNumRewards();
  RewardItem** rewards = _quest->getRewards();

  // Pre define the size of the reward slots.
  const int xSizeSlot  = 50;
  const int ySizeSlot  = 50;
  const int xImageSlot = 40;
  const int yImageSlot = 40;
  CEGUI::UVector2 slotSize  = CEGUI::UVector2(CEGUI::UDim(0,xSizeSlot),CEGUI::UDim(0,ySizeSlot));
  CEGUI::UVector2 imageSize = CEGUI::UVector2(CEGUI::UDim(0,xImageSlot),CEGUI::UDim(0,yImageSlot));
  
  for(int i=0;i<numRewards;i++)
  {
    AbstractItem* item = ItemManager::getInstance()->getItem(rewards[i]->getItemName());
    // Create a slot.
    CEGUI::Window* rewardSlot = winManager.createWindow(GUILook + "/StaticImage", 
      "RewardSlot-" + CEGUI::PropertyHelper::intToString(i));
    rewardSlot->setSize(slotSize);
    rewardSlot->setMouseInputPropagationEnabled(true);

    double xPos;

    // Set the xPositon of the slot.
    xPos = ((double)(i+1)/(double)(numRewards+1)) -0.5;

    // Place the position of the slot and add it to the window.
    rewardSlot->setHorizontalAlignment(CEGUI::HA_CENTRE);
    rewardSlot->setPosition(CEGUI::UVector2(CEGUI::UDim(xPos,0),CEGUI::UDim(0.75,0)));
    window->addChildWindow(rewardSlot);

    // Add the image to the slot.
    // Create a slot.
    CEGUI::Window* rewardImage = winManager.createWindow(GUILook + "/StaticImage", 
      item->getName());
    rewardImage->setProperty("FrameEnabled","False");
    rewardImage->setProperty("Image","set:" + item->getImageName() + " image:full_image");
    rewardImage->setSize(imageSize);
    rewardImage->setHorizontalAlignment(CEGUI::HA_CENTRE);
    rewardImage->setVerticalAlignment(CEGUI::VA_CENTRE);
    rewardImage->subscribeEvent(CEGUI::Window::EventMouseButtonDown, 
      CEGUI::Event::Subscriber(&GUIManager::handleRewardItemClicked, this));
    rewardSlot->addChildWindow(rewardImage);

    if(rewards[i]->getAmount() > 1)
    {
      CEGUI::Window* itemCount = winManager.createWindow(OgreFramework::getSingleton().GUILook + "/StaticText", 
        "RewardItem-" + CEGUI::PropertyHelper::intToString(i) + "/Count");
      itemCount->setMousePassThroughEnabled(true);
      itemCount->setText(CEGUI::PropertyHelper::intToString(rewards[i]->getAmount()));
      itemCount->setHorizontalAlignment(CEGUI::HA_LEFT);
      itemCount->setVerticalAlignment(CEGUI::VA_BOTTOM);
      itemCount->setProperty("HorzFormatting", "HorzLeft");
      itemCount->setProperty("VertFormatting", "BottomAligned");
      itemCount->setProperty("FrameEnabled", "False");
      itemCount->setProperty("BackgroundEnabled", "False");
      itemCount->setFont("DejaVuSans-8");
      itemCount->setSize(CEGUI::UVector2(CEGUI::UDim(0.6,0),CEGUI::UDim(0.35,0)));
      rewardImage->addChildWindow(itemCount);
    } // end if

  } // end for

  
  window->addChildWindow(acceptButton);
  window->addChildWindow(closeButton);
  return windowName;

} // string GUIManager::buildAvailableQuestWindow(Quest* _quest, NPC* _npc)


string GUIManager::buildCompletedQuestWindow(Quest* _quest, NPC* _npc)
{
  // Create the initial details.
  string GUILook = OgreFramework::getSingleton().GUILook;
  CEGUI::WindowManager& winManager = CEGUI::WindowManager::getSingleton();
  CEGUI::Window* sheet = winManager.getWindow("Sheet");

  CEGUI::FrameWindow* window = NULL;
  CEGUI::UVector2 size = CEGUI::UVector2(CEGUI::UDim(0,300), CEGUI::UDim(0,450));
  CEGUI::UVector2 position = CEGUI::UVector2(CEGUI::UDim(0,5), CEGUI::UDim(0,120));

  string windowName = this->createInteractFrameWindow(window, size, position, _npc->getName());
  window = (CEGUI::FrameWindow*)winManager.getWindow(windowName);

  // Build an accept button.
  if(winManager.isWindowPresent(_quest->getName()))
  {
    winManager.getWindow(_quest->getName())->destroy();
  } // end if
  CEGUI::PushButton* closeButton = static_cast<CEGUI::PushButton*>(winManager.createWindow(
    GUILook + "/Button", _quest->getName()));

  closeButton->setText("Done");
  closeButton->setFont("DejaVuSans-10");
  closeButton->setSize(CEGUI::UVector2(CEGUI::UDim(0.40,0), CEGUI::UDim(0,30)));
  closeButton->setHorizontalAlignment(CEGUI::HA_CENTRE);
  closeButton->setVerticalAlignment(CEGUI::VA_BOTTOM);
  closeButton->subscribeEvent(CEGUI::Window::EventMouseButtonUp, 
    CEGUI::Event::Subscriber(&GUIManager::closeQuestBoxButtonPressed,this));
  
  // Add the quest Title to the window.
  CEGUI::Window* questTitle = winManager.createWindow(GUILook + "/StaticText", windowName + "/TitleText");
  questTitle->setText(_quest->getName());
  questTitle->setHorizontalAlignment(CEGUI::HA_LEFT);
  questTitle->setVerticalAlignment(CEGUI::VA_TOP);
  questTitle->setYPosition(CEGUI::UDim(0,5));
  questTitle->setProperty("VertFormatting", "TopAligned");
  questTitle->setProperty("HorzFormatting", "WordWrapLeftAligned");
  questTitle->setProperty("FrameEnabled", "False");
  questTitle->setProperty("BackgroundEnabled", "False");
  questTitle->setFont("DejaVuSans-12");
  window->addChildWindow(questTitle);
  
  // Add the quest text to the window.
  CEGUI::MultiLineEditbox* questDescription = (CEGUI::MultiLineEditbox*)winManager.createWindow(
    GUILook + "/MultiLineEditbox", windowName + "/DescriptionText");
  questDescription->setText(_quest->getClosingDialogue());
  questDescription->setHorizontalAlignment(CEGUI::HA_CENTRE);
  questDescription->setVerticalAlignment(CEGUI::VA_TOP);  
  questDescription->setYPosition(CEGUI::UDim(0,27));
  questDescription->setSize(CEGUI::UVector2(CEGUI::UDim(1.0,0), CEGUI::UDim(0.60,0)));
  questDescription->setFont("DejaVuSans-10");
  questDescription->setClippedByParent(true);
  questDescription->setReadOnly(true);
  window->addChildWindow(questDescription);

  // Add the rewards title.
  CEGUI::Window* rewardsText = winManager.createWindow(GUILook + "/StaticText", windowName + "/RewardsText");
  rewardsText->setText("Rewards:");
  rewardsText->setHorizontalAlignment(CEGUI::HA_CENTRE);
  rewardsText->setYPosition(CEGUI::UDim(0.69,0));
  rewardsText->setHeight(CEGUI::UDim(0,25));
  rewardsText->setProperty("VertFormatting", "TopAligned");
  rewardsText->setProperty("HorzFormatting", "LeftAligned");
  rewardsText->setProperty("FrameEnabled", "False");
  rewardsText->setProperty("BackgroundEnabled", "False");
  rewardsText->setFont("DejaVuSans-10");
  window->addChildWindow(rewardsText);

  // Add the currency amount.
  CEGUI::Window* currencyText = winManager.createWindow(GUILook + "/StaticText", windowName + "/CurrencyText");
  currencyText->setText(CEGUI::PropertyHelper::intToString(_quest->getRewardCurrency()) + " Shiny Rocks ");
  currencyText->setHorizontalAlignment(CEGUI::HA_RIGHT);
  currencyText->setYPosition(CEGUI::UDim(0.69,0));
  currencyText->setHeight(CEGUI::UDim(0,25));
  currencyText->setProperty("VertFormatting", "TopAligned");
  currencyText->setProperty("HorzFormatting", "RightAligned");
  currencyText->setProperty("FrameEnabled", "False");
  currencyText->setProperty("BackgroundEnabled", "False");
  currencyText->setFont("DejaVuSans-10");
  window->addChildWindow(currencyText);

  // Add the reward graphics.
  int numRewards = _quest->getNumRewards();
  RewardItem** rewards = _quest->getRewards();

  // Pre define the size of the reward slots.
  const int xSizeSlot  = 50;
  const int ySizeSlot  = 50;
  const int xImageSlot = 40;
  const int yImageSlot = 40;
  CEGUI::UVector2 slotSize  = CEGUI::UVector2(CEGUI::UDim(0,xSizeSlot),CEGUI::UDim(0,ySizeSlot));
  CEGUI::UVector2 imageSize = CEGUI::UVector2(CEGUI::UDim(0,xImageSlot),CEGUI::UDim(0,yImageSlot));

  for(int i=0;i<numRewards;i++)
  {
    AbstractItem* item = ItemManager::getInstance()->getItem(rewards[i]->getItemName());
    // Create a slot.
    CEGUI::Window* rewardSlot = winManager.createWindow(GUILook + "/StaticImage", 
      "RewardSlot-" + CEGUI::PropertyHelper::intToString(i));
    rewardSlot->setSize(slotSize);
    rewardSlot->setMouseInputPropagationEnabled(true);

    // Set the xPositon of the slot.
    double xPos = ((double)(i+1)/(double)(numRewards+1)) -0.5;

    // Place the position of the slot and add it to the window.
    rewardSlot->setHorizontalAlignment(CEGUI::HA_CENTRE);
    rewardSlot->setPosition(CEGUI::UVector2(CEGUI::UDim(xPos,0),CEGUI::UDim(0.75,0)));
    window->addChildWindow(rewardSlot);

    // Add the image to the slot.
    // Create a slot.
    CEGUI::Window* rewardImage = winManager.createWindow(GUILook + "/StaticImage", 
      item->getName());
    rewardImage->setProperty("FrameEnabled","False");
    rewardImage->setProperty("Image","set:" + item->getImageName() + " image:full_image");
    rewardImage->setSize(imageSize);
    rewardImage->setHorizontalAlignment(CEGUI::HA_CENTRE);
    rewardImage->setVerticalAlignment(CEGUI::VA_CENTRE);
    rewardImage->subscribeEvent(CEGUI::Window::EventMouseButtonDown, 
      CEGUI::Event::Subscriber(&GUIManager::handleRewardItemClicked, this));
    rewardSlot->addChildWindow(rewardImage);

    if(rewards[i]->getAmount() > 1)
    {
      CEGUI::Window* itemCount = winManager.createWindow(OgreFramework::getSingleton().GUILook + "/StaticText", 
        "RewardItem-" + CEGUI::PropertyHelper::intToString(i) + "/Count");
      itemCount->setMousePassThroughEnabled(true);
      itemCount->setText(CEGUI::PropertyHelper::intToString(rewards[i]->getAmount()));
      itemCount->setHorizontalAlignment(CEGUI::HA_LEFT);
      itemCount->setVerticalAlignment(CEGUI::VA_BOTTOM);
      itemCount->setProperty("HorzFormatting", "HorzLeft");
      itemCount->setProperty("VertFormatting", "BottomAligned");
      itemCount->setProperty("FrameEnabled", "False");
      itemCount->setProperty("BackgroundEnabled", "False");
      itemCount->setFont("DejaVuSans-8");
      itemCount->setSize(CEGUI::UVector2(CEGUI::UDim(0.6,0),CEGUI::UDim(0.35,0)));
      rewardImage->addChildWindow(itemCount);
    } // end if

  } // end for

  window->addChildWindow(closeButton);

  // Play the completed sound.
  AudioManager::getSingleton().playSound("Quest Completed");

  return windowName;
} // string GUIManager::buildCompletedQuestWindow(Quest* _quest, NPC* _npc)


string GUIManager::buildNoQuestDialogueBox(NPC* _npc)
{
  // Create the initial details.
  string GUILook = OgreFramework::getSingleton().GUILook;
  CEGUI::WindowManager& winManager = CEGUI::WindowManager::getSingleton();
  CEGUI::Window* sheet = winManager.getWindow("Sheet");

  CEGUI::FrameWindow* window = NULL;
  CEGUI::UVector2 size = CEGUI::UVector2(CEGUI::UDim(0,200), CEGUI::UDim(0,110));
  CEGUI::UVector2 position = CEGUI::UVector2(CEGUI::UDim(0,5), CEGUI::UDim(0,120));

  string windowName = this->createInteractFrameWindow(window, size, position, _npc->getName());
  window = (CEGUI::FrameWindow*)winManager.getWindow(windowName);


  // Create a close button.
  CEGUI::PushButton* closeButton = static_cast<CEGUI::PushButton*>(winManager.createWindow(
    GUILook + "/Button", windowName + "/CloseButton"));

  closeButton->setText("Close");
  closeButton->setFont("DejaVuSans-10");
  closeButton->setSize(CEGUI::UVector2(CEGUI::UDim(0.55,0), CEGUI::UDim(0,30)));
  closeButton->setHorizontalAlignment(CEGUI::HA_CENTRE);
  closeButton->setVerticalAlignment(CEGUI::VA_BOTTOM);
  closeButton->subscribeEvent(CEGUI::Window::EventMouseButtonUp, 
    CEGUI::Event::Subscriber(&GUIManager::closeQuestBoxButtonPressed,this));
  

  // Add the text to the window.
  CEGUI::Window* windowText = winManager.createWindow(GUILook + "/StaticText", windowName + "/DescriptionTest");
  windowText->setText("Nice to meet you friend!");
  windowText->setSize(CEGUI::UVector2(CEGUI::UDim(0.95,0),CEGUI::UDim(0.6,0)));
  windowText->setHorizontalAlignment(CEGUI::HA_CENTRE);
  windowText->setVerticalAlignment(CEGUI::VA_TOP);
  windowText->setYPosition(CEGUI::UDim(0,5));
  windowText->setProperty("VertFormatting", "TopAligned");
  windowText->setProperty("HorzFormatting", "WordWrapLeftAligned");
  windowText->setProperty("FrameEnabled", "False");
  windowText->setProperty("BackgroundEnabled", "False");
  windowText->setFont("DejaVuSans-10");
  window->addChildWindow(windowText);
  window->addChildWindow(closeButton);

  return windowName;
} // string GUIManager::buildNoQuestDialogueBox(NPC* _npc)


string GUIManager::buildActiveVendorQuestWindow(Quest* _quest, NPC* _npc)
{
  // Create the initial details.
  string GUILook = OgreFramework::getSingleton().GUILook;
  CEGUI::WindowManager& winManager = CEGUI::WindowManager::getSingleton();
  CEGUI::Window* sheet = winManager.getWindow("Sheet");

  // Create a frame window to serve as our Quest box.
  string windowName = "VendorOptions";
  if(winManager.isWindowPresent(windowName))
  {
    winManager.destroyWindow(windowName);
  } // end if
  if(winManager.isWindowPresent("InteractWindow"))
  {
    winManager.destroyWindow("InteractWindow");
  } // end if

  CEGUI::UVector2 size = CEGUI::UVector2(CEGUI::UDim(0,200), CEGUI::UDim(0,120));
  CEGUI::UVector2 position = CEGUI::UVector2(CEGUI::UDim(0,5), CEGUI::UDim(0,120));

  CEGUI::FrameWindow* window = static_cast<CEGUI::FrameWindow*>(winManager.createWindow(GUILook + "/FrameWindow", windowName));
  window->setText(_npc->getName());
  window->setSize(size);
  window->setPosition(position);
  window->setRollupEnabled(false);
  window->setSizingEnabled(false);
  window->setCloseButtonEnabled(true);
  window->subscribeEvent(CEGUI::FrameWindow::EventCloseClicked, 
    CEGUI::Event::Subscriber(&GUIManager::frameCloseButtonHit, this));
  window->setDragMovingEnabled(false);
  sheet->addChildWindow(window);

  // Create a shop button.
  CEGUI::PushButton* shopButton = static_cast<CEGUI::PushButton*>(winManager.createWindow(
    GUILook + "/Button","Vendor/ShopButton"));

  shopButton->setText("Shop");
  shopButton->setFont("DejaVuSans-10");
  shopButton->setSize(CEGUI::UVector2(CEGUI::UDim(0.45,0), CEGUI::UDim(0,30)));
  shopButton->setHorizontalAlignment(CEGUI::HA_CENTRE);
  shopButton->setXPosition(CEGUI::UDim(-0.25,0));
  shopButton->setVerticalAlignment(CEGUI::VA_BOTTOM);
  shopButton->subscribeEvent(CEGUI::Window::EventMouseButtonUp, 
    CEGUI::Event::Subscriber(&GUIManager::vendorOptionButtonHit,this));


  // Create a shop button.
  CEGUI::PushButton* closeButton = static_cast<CEGUI::PushButton*>(winManager.createWindow(
    GUILook + "/Button","Vendor/CloseButton"));

  closeButton->setText("Close");
  closeButton->setFont("DejaVuSans-10");
  closeButton->setSize(CEGUI::UVector2(CEGUI::UDim(0.45,0), CEGUI::UDim(0,30)));
  closeButton->setHorizontalAlignment(CEGUI::HA_CENTRE);
  closeButton->setXPosition(CEGUI::UDim(0.25,0));
  closeButton->setVerticalAlignment(CEGUI::VA_BOTTOM);
  closeButton->subscribeEvent(CEGUI::Window::EventMouseButtonUp, 
    CEGUI::Event::Subscriber(&GUIManager::closeQuestBoxButtonPressed,this));


  // Add the text to the window.
  CEGUI::Window* windowText = winManager.createWindow(GUILook + "/StaticText", windowName + "/DescriptionTest");
  windowText->setText(_quest->getDuringDialogue());
  windowText->setSize(CEGUI::UVector2(CEGUI::UDim(0.95,0),CEGUI::UDim(0.6,0)));
  windowText->setHorizontalAlignment(CEGUI::HA_CENTRE);
  windowText->setVerticalAlignment(CEGUI::VA_TOP);
  windowText->setYPosition(CEGUI::UDim(0,5));
  windowText->setProperty("VertFormatting", "TopAligned");
  windowText->setProperty("HorzFormatting", "WordWrapLeftAligned");
  windowText->setProperty("FrameEnabled", "False");
  windowText->setProperty("BackgroundEnabled", "False");
  windowText->setFont("DejaVuSans-10");
  window->addChildWindow(windowText);
  window->addChildWindow(shopButton);
  window->addChildWindow(closeButton);

  return windowName;
} // string GUIManager::buildActiveVendorQuestWindow(Quest* _quest, NPC* _npc)


string GUIManager::buildAvailableVendorQuestWindow(Quest* _quest, NPC* _npc)
{
  // Create the initial details.
  string GUILook = OgreFramework::getSingleton().GUILook;
  CEGUI::WindowManager& winManager = CEGUI::WindowManager::getSingleton();
  CEGUI::Window* sheet = winManager.getWindow("Sheet");

  // Create a frame window to serve as our Quest box.
  string windowName = "VendorOptions";
  if(winManager.isWindowPresent(windowName))
  {
    winManager.destroyWindow(windowName);
  } // end if
  if(winManager.isWindowPresent("InteractWindow"))
  {
    winManager.destroyWindow("InteractWindow");
  } // end if

  CEGUI::UVector2 size = CEGUI::UVector2(CEGUI::UDim(0,200), CEGUI::UDim(0,120));
  CEGUI::UVector2 position = CEGUI::UVector2(CEGUI::UDim(0,5), CEGUI::UDim(0,115));

  CEGUI::FrameWindow* window = static_cast<CEGUI::FrameWindow*>(winManager.createWindow(GUILook + "/FrameWindow", windowName));
  window->setText(_npc->getName());
  window->setSize(size);
  window->setPosition(position);
  window->setRollupEnabled(false);
  window->setSizingEnabled(false);
  window->setCloseButtonEnabled(true);
  window->subscribeEvent(CEGUI::FrameWindow::EventCloseClicked, 
    CEGUI::Event::Subscriber(&GUIManager::frameCloseButtonHit, this));
  window->setDragMovingEnabled(false);
  sheet->addChildWindow(window);

  // Now add the two buttons. 
  // Create a quest button.
  if(winManager.isWindowPresent(_quest->getName()))
  {
    winManager.destroyWindow(_quest->getName());
  } // end if
    CEGUI::PushButton* questButton = static_cast<CEGUI::PushButton*>(winManager.createWindow(
      GUILook + "/Button", _quest->getName()));

  questButton->setText("Quest");
  questButton->setFont("DejaVuSans-10");
  questButton->setSize(CEGUI::UVector2(CEGUI::UDim(0.45,0), CEGUI::UDim(0,30)));
  questButton->setHorizontalAlignment(CEGUI::HA_CENTRE);
  questButton->setXPosition(CEGUI::UDim(-0.25,0));
  questButton->setVerticalAlignment(CEGUI::VA_BOTTOM);
  questButton->subscribeEvent(CEGUI::Window::EventMouseButtonUp, 
    CEGUI::Event::Subscriber(&GUIManager::vendorOptionButtonHit,this));

  // Create a shop button.
  CEGUI::PushButton* shopButton = static_cast<CEGUI::PushButton*>(winManager.createWindow(
    GUILook + "/Button","Vendor/ShopButton"));

  shopButton->setText("Shop");
  shopButton->setFont("DejaVuSans-10");
  shopButton->setSize(CEGUI::UVector2(CEGUI::UDim(0.45,0), CEGUI::UDim(0,30)));
  shopButton->setHorizontalAlignment(CEGUI::HA_CENTRE);
  shopButton->setXPosition(CEGUI::UDim(0.25,0));
  shopButton->setVerticalAlignment(CEGUI::VA_BOTTOM);
  shopButton->subscribeEvent(CEGUI::Window::EventMouseButtonUp, 
    CEGUI::Event::Subscriber(&GUIManager::vendorOptionButtonHit,this));


  // Add the text to the window.
  CEGUI::Window* windowText = winManager.createWindow(GUILook + "/StaticText", windowName + "/DescriptionTest");
  windowText->setText("Please friend, I have a favour to ask of you.");
  windowText->setSize(CEGUI::UVector2(CEGUI::UDim(0.95,0),CEGUI::UDim(0.6,0)));
  windowText->setHorizontalAlignment(CEGUI::HA_CENTRE);
  windowText->setVerticalAlignment(CEGUI::VA_TOP);
  windowText->setYPosition(CEGUI::UDim(0,5));
  windowText->setProperty("VertFormatting", "TopAligned");
  windowText->setProperty("HorzFormatting", "WordWrapLeftAligned");
  windowText->setProperty("FrameEnabled", "False");
  windowText->setProperty("BackgroundEnabled", "False");
  windowText->setFont("DejaVuSans-10");
  window->addChildWindow(windowText);
  window->addChildWindow(questButton);
  window->addChildWindow(shopButton);

  return windowName;
} // string GUIManager::buildVendorOptionWindow(Quest* _quest, NPC* _npc)


string GUIManager::buildNoQuestVendorWindow(NPC* _npc)
{
  // Create the initial details.
  string GUILook = OgreFramework::getSingleton().GUILook;
  CEGUI::WindowManager& winManager = CEGUI::WindowManager::getSingleton();
  CEGUI::Window* sheet = winManager.getWindow("Sheet");

  CEGUI::FrameWindow* window = NULL;
  CEGUI::UVector2 size = CEGUI::UVector2(CEGUI::UDim(0,200), CEGUI::UDim(0,120));
  CEGUI::UVector2 position = CEGUI::UVector2(CEGUI::UDim(0,5), CEGUI::UDim(0,115));

  string windowName = this->createInteractFrameWindow(window, size, position, _npc->getName());
  window = (CEGUI::FrameWindow*)winManager.getWindow(windowName);

  // Create a close button.
  CEGUI::PushButton* shopButton = static_cast<CEGUI::PushButton*>(winManager.createWindow(
    GUILook + "/Button", "Vendor/ShopButton"));

  shopButton->setText("Shop");
  shopButton->setFont("DejaVuSans-10");
  shopButton->setSize(CEGUI::UVector2(CEGUI::UDim(0.45,0), CEGUI::UDim(0,30)));
  shopButton->setHorizontalAlignment(CEGUI::HA_CENTRE);
  shopButton->setXPosition(CEGUI::UDim(-0.25,0));
  shopButton->setVerticalAlignment(CEGUI::VA_BOTTOM);
  shopButton->subscribeEvent(CEGUI::Window::EventMouseButtonUp, 
    CEGUI::Event::Subscriber(&GUIManager::vendorOptionButtonHit,this));


  // Create a close button.
  CEGUI::PushButton* closeButton = static_cast<CEGUI::PushButton*>(winManager.createWindow(
    GUILook + "/Button", windowName + "/CloseButton"));

  closeButton->setText("Close");
  closeButton->setFont("DejaVuSans-10");
  closeButton->setSize(CEGUI::UVector2(CEGUI::UDim(0.45,0), CEGUI::UDim(0,30)));
  closeButton->setHorizontalAlignment(CEGUI::HA_CENTRE);
  closeButton->setXPosition(CEGUI::UDim(0.25,0));
  closeButton->setVerticalAlignment(CEGUI::VA_BOTTOM);
  closeButton->subscribeEvent(CEGUI::Window::EventMouseButtonUp, 
    CEGUI::Event::Subscriber(&GUIManager::closeQuestBoxButtonPressed,this));
  

  // Add the text to the window.
  CEGUI::Window* windowText = winManager.createWindow(GUILook + "/StaticText", windowName + "/DescriptionTest");
  windowText->setText("What can I help you with?");
  windowText->setSize(CEGUI::UVector2(CEGUI::UDim(0.95,0),CEGUI::UDim(0.6,0)));
  windowText->setHorizontalAlignment(CEGUI::HA_CENTRE);
  windowText->setVerticalAlignment(CEGUI::VA_TOP);
  windowText->setYPosition(CEGUI::UDim(0,5));
  windowText->setProperty("VertFormatting", "TopAligned");
  windowText->setProperty("HorzFormatting", "WordWrapLeftAligned");
  windowText->setProperty("FrameEnabled", "False");
  windowText->setProperty("BackgroundEnabled", "False");
  windowText->setFont("DejaVuSans-10");
  window->addChildWindow(windowText);
  window->addChildWindow(closeButton);
  window->addChildWindow(shopButton);

  return windowName;
} // string GUIManager::buildNoQuestVendorWindow(NPC* _npc)


void GUIManager::buildSavePointWindow()
{
  // Create the initial details.
  string GUILook = OgreFramework::getSingleton().GUILook;
  CEGUI::WindowManager& winManager = CEGUI::WindowManager::getSingleton();
  CEGUI::Window* sheet = winManager.getWindow("Sheet");

  CEGUI::FrameWindow* window = NULL;
  CEGUI::UVector2 size = CEGUI::UVector2(CEGUI::UDim(0,200), CEGUI::UDim(0,80));
  CEGUI::UVector2 position = CEGUI::UVector2(CEGUI::UDim(0,5), CEGUI::UDim(0,115));
  window = (CEGUI::FrameWindow*)winManager.getWindow(this->createInteractFrameWindow(window, size, position, "Save Point"));

  // Add two buttons, one for save and one for close.
  if(winManager.isWindowPresent("Save/SaveButton"))
  {
    winManager.destroyWindow("Save/SaveButton");
  } // end if
  CEGUI::PushButton* saveButton = static_cast<CEGUI::PushButton*>(winManager.createWindow(
    GUILook + "/Button", "Save/SaveButton"));

  saveButton->setText("Save");
  saveButton->setFont("DejaVuSans-10");
  saveButton->setSize(CEGUI::UVector2(CEGUI::UDim(0.45,0), CEGUI::UDim(0,30)));
  saveButton->setHorizontalAlignment(CEGUI::HA_CENTRE);
  saveButton->setXPosition(CEGUI::UDim(-0.25,0));
  saveButton->setVerticalAlignment(CEGUI::VA_CENTRE);
  saveButton->subscribeEvent(CEGUI::Window::EventMouseClick, 
    CEGUI::Event::Subscriber(&GUIManager::handleSaveButtonClicked,this));

  // Create a close button.
  if(winManager.isWindowPresent("Save/CloseButton"))
  {
    winManager.destroyWindow("Save/CloseButton");
  } // end if
  CEGUI::PushButton* closeButton = static_cast<CEGUI::PushButton*>(winManager.createWindow(
    GUILook + "/Button", "Save/CloseButton"));

  closeButton->setText("Close");
  closeButton->setFont("DejaVuSans-10");
  closeButton->setSize(CEGUI::UVector2(CEGUI::UDim(0.45,0), CEGUI::UDim(0,30)));
  closeButton->setHorizontalAlignment(CEGUI::HA_CENTRE);
  closeButton->setXPosition(CEGUI::UDim(0.25,0));
  closeButton->setVerticalAlignment(CEGUI::VA_CENTRE);
  closeButton->subscribeEvent(CEGUI::Window::EventMouseClick, 
    CEGUI::Event::Subscriber(&GUIManager::closeQuestBoxButtonPressed,this));

  window->addChildWindow(closeButton);
  window->addChildWindow(saveButton);

} // void GUIManager::buildSavePointWindow()


void GUIManager::renameInventorySlots(int _previous, int _new)
{
  CEGUI::WindowManager& winManager = CEGUI::WindowManager::getSingleton();

  CEGUI::Window* invSlot = winManager.getWindow("InvSlot-"+CEGUI::PropertyHelper::intToString(_previous));
  invSlot->rename("InvSlot-"+CEGUI::PropertyHelper::intToString(_new));
} // void GUIManager::renameInventorySlots(int _previous, int _new)


string GUIManager::buildCharacterDisplay()
{
  // Create the initial details.
  string GUILook = OgreFramework::getSingleton().GUILook;
  CEGUI::WindowManager& winManager = CEGUI::WindowManager::getSingleton();
  CEGUI::Window* sheet = winManager.getWindow("Sheet");

  string windowName = "CharacterDisplay";

  const int OUTLINE_HEIGHT = 107;
  const int OUTLINE_WIDTH  = 296;
  // Create the window outline.
  CEGUI::Window* outline = winManager.createWindow(GUILook + "/SimplePanel", windowName);
  outline->setXPosition(CEGUI::UDim(0,5));
  outline->setYPosition(CEGUI::UDim(0,5));
  outline->setWidth(CEGUI::UDim(0,OUTLINE_WIDTH));
  outline->setHeight(CEGUI::UDim(0,OUTLINE_HEIGHT));
  sheet->addChildWindow(outline);
  outline->setRiseOnClickEnabled(false);
  
  
  // Load the image. Cheeky, should be done at loading screen. @Todo
  string set = "portrait-" + Player::getInstance()->getSkillClass()->getType() + "-" + Player::getInstance()->getGender() +".tga";
  boost::to_lower(set);
  CEGUI::ImagesetManager::getSingleton().createFromImageFile(set, set);

  // Create the character portrait.
  CEGUI::Window* portrait = winManager.createWindow(GUILook + "/StaticImage", windowName + "/Portait");
  portrait->setXPosition(CEGUI::UDim(0,0));
  portrait->setYPosition(CEGUI::UDim(0,0));
  portrait->setWidth(CEGUI::UDim(0,OUTLINE_HEIGHT));
  portrait->setHeight(CEGUI::UDim(1.0,0));
  portrait->setProperty("Image", "set:" + set + " image:full_image");
  outline->addChildWindow(portrait);

  
  const int BAR_X = 108;

  // Player Name
  CEGUI::Window* playerName = winManager.createWindow(GUILook + "/StaticText", windowName + "/Name");
  playerName->setXPosition(CEGUI::UDim(0, BAR_X+3));
  playerName->setYPosition(CEGUI::UDim(0, 10));
  playerName->setWidth(CEGUI::UDim(0, 120));
  playerName->setHeight(CEGUI::UDim(0, 15));
  playerName->setText(Player::getInstance()->getName());
  playerName->setProperty("FrameEnabled", "False");
  playerName->setProperty("BackgroundEnabled", "False");
  playerName->setFont("DejaVuSans-10");
  outline->addChildWindow(playerName);

  // Add a currency count.
  CEGUI::Window* currencyAmount = winManager.createWindow(GUILook + "/StaticText", windowName + "/CurrencyAmount");
  currencyAmount->setXPosition(CEGUI::UDim(0, -26));
  currencyAmount->setYPosition(CEGUI::UDim(0, 11));
  currencyAmount->setWidth(CEGUI::UDim(0, 60));
  currencyAmount->setHeight(CEGUI::UDim(0, 15));
  currencyAmount->setText(Player::getInstance()->getName());
  currencyAmount->setHorizontalAlignment(CEGUI::HA_RIGHT);
  currencyAmount->setProperty("FrameEnabled", "False");
  currencyAmount->setProperty("BackgroundEnabled", "False");
  currencyAmount->setProperty("HorzFormatting", "RightAligned");
  currencyAmount->setFont("DejaVuSans-8");
  outline->addChildWindow(currencyAmount);


  // Load the image. Cheeky, should be done at loading screen (Copy and paste :]). @Todo
  CEGUI::ImagesetManager::getSingleton().createFromImageFile("currency.png", "currency.png");

  // Add a currency icon.
  CEGUI::Window* currencyIcon = winManager.createWindow(GUILook + "/StaticImage", windowName + "/CurrencyIcon");
  currencyIcon->setXPosition(CEGUI::UDim(0, -10));
  currencyIcon->setYPosition(CEGUI::UDim(0, 10));
  currencyIcon->setWidth(CEGUI::UDim(0, 15));
  currencyIcon->setHeight(CEGUI::UDim(0, 15));
  currencyIcon->setHorizontalAlignment(CEGUI::HA_RIGHT);
  currencyIcon->setProperty("FrameEnabled", "False");
  currencyIcon->setProperty("BackgroundEnabled", "False");
  currencyIcon->setProperty("Image", "set:currency.png image:full_image");
  outline->addChildWindow(currencyIcon);

  // Add a progress bar for health.
  CEGUI::ProgressBar* healthBar = 
    (CEGUI::ProgressBar*)winManager.createWindow(GUILook + "/ProgressBar", windowName + "/HealthBar");
  healthBar->setProgress(1.0);
  healthBar->setXPosition(CEGUI::UDim(0, BAR_X));
  healthBar->setYPosition(CEGUI::UDim(0, 27));
  healthBar->setWidth(CEGUI::UDim(0, 180));
  healthBar->setHeight(CEGUI::UDim(0, 25));
  outline->addChildWindow(healthBar);

  // Make some text to display the values.
  CEGUI::Window* healthText = winManager.createWindow(GUILook + "/StaticText", windowName + "/HealthBar/Text");
  healthText->setHorizontalAlignment(CEGUI::HA_CENTRE);
  healthText->setVerticalAlignment(CEGUI::VA_CENTRE);
  healthText->setWidth(CEGUI::UDim(0, 70));
  healthText->setHeight(CEGUI::UDim(0,20));
  healthText->setProperty("FrameEnabled", "False");
  healthText->setProperty("BackgroundEnabled", "False");
  healthText->setFont("DejaVuSans-8");
  healthText->setProperty("HorzFormatting", "HorzCentred");
  healthBar->addChildWindow(healthText);


  // Add a progress bar for stamina.
  CEGUI::ProgressBar* staminaBar = 
    (CEGUI::ProgressBar*)winManager.createWindow(GUILook + "/ProgressBarPurple", windowName + "/StaminaBar");
  staminaBar->setProgress(1.0);
  staminaBar->setXPosition(CEGUI::UDim(0, BAR_X));
  staminaBar->setYPosition(CEGUI::UDim(0, 52));
  staminaBar->setWidth(CEGUI::UDim(0, 180));
  staminaBar->setHeight(CEGUI::UDim(0, 25));
  outline->addChildWindow(staminaBar);

  // Make some text to display the values.
  CEGUI::Window* staminaText = winManager.createWindow(GUILook + "/StaticText", windowName + "/StaminaBar/Text");
  staminaText->setHorizontalAlignment(CEGUI::HA_CENTRE);
  staminaText->setVerticalAlignment(CEGUI::VA_CENTRE);
  staminaText->setWidth(CEGUI::UDim(0, 70));
  staminaText->setHeight(CEGUI::UDim(0,20));
  staminaText->setProperty("FrameEnabled", "False");
  staminaText->setProperty("BackgroundEnabled", "False");
  staminaText->setFont("DejaVuSans-8");
  staminaText->setProperty("HorzFormatting", "HorzCentred");
  staminaBar->addChildWindow(staminaText);

  // Add a progress bar for exp.
  CEGUI::ProgressBar* experienceBar = 
    (CEGUI::ProgressBar*)winManager.createWindow(GUILook + "/ProgressBarGreen", windowName + "/ExperienceBar");
  experienceBar->setProgress(0.0);
  experienceBar->setXPosition(CEGUI::UDim(0, BAR_X));
  experienceBar->setYPosition(CEGUI::UDim(0, 77));
  experienceBar->setWidth(CEGUI::UDim(0, 180));
  experienceBar->setHeight(CEGUI::UDim(0, 25));
  outline->addChildWindow(experienceBar);

  // Make some text to display the values.
  CEGUI::Window* experienceText = winManager.createWindow(GUILook + "/StaticText", windowName + "/ExperienceBar/Text");
  experienceText->setHorizontalAlignment(CEGUI::HA_CENTRE);
  experienceText->setVerticalAlignment(CEGUI::VA_CENTRE);
  experienceText->setWidth(CEGUI::UDim(0, 70));
  experienceText->setHeight(CEGUI::UDim(0,20));
  experienceText->setProperty("FrameEnabled", "False");
  experienceText->setProperty("BackgroundEnabled", "False");
  experienceText->setFont("DejaVuSans-8");
  experienceText->setProperty("HorzFormatting", "HorzCentred");
  experienceBar->addChildWindow(experienceText);

  // Finally add a nice frame - :)
  CEGUI::Window* frame = winManager.createWindow(GUILook + "/SimpleFrame", windowName + "/Frame");
  frame->setXPosition(CEGUI::UDim(0,5));
  frame->setYPosition(CEGUI::UDim(0,5));
  frame->setWidth(CEGUI::UDim(0,OUTLINE_WIDTH));
  frame->setHeight(CEGUI::UDim(0,OUTLINE_HEIGHT));
  sheet->addChildWindow(frame);

  return windowName;
} // string GUIManager::buildCharacterDisplay()


CEGUI::Window* GUIManager::getWindowWithName(string _windowName)
{
  // Create the initial details.
  CEGUI::WindowManager& winManager = CEGUI::WindowManager::getSingleton();
  return winManager.getWindow(_windowName);
} // string GUIManager::updateCharacterDisplay()


void GUIManager::updateHealthDisplay()
{
  CEGUI::WindowManager& winManager = CEGUI::WindowManager::getSingleton();
 
  if(winManager.isWindowPresent("CharacterDisplay/HealthBar"))
  {
    CEGUI::ProgressBar* healthBar = (CEGUI::ProgressBar*)winManager.getWindow("CharacterDisplay/HealthBar");
  
    double currentHP = Player::getInstance()->getHP();
    double maxHP = Player::getInstance()->getMaxHP();
    double currentHPPercentage = currentHP / maxHP;

    healthBar->setProgress(currentHPPercentage);

    // Update the text.
    CEGUI::Window* text = winManager.getWindow(healthBar->getName() + "/Text");
    text->setText(CEGUI::PropertyHelper::intToString((int)currentHP) 
      + "/" + CEGUI::PropertyHelper::intToString((int)maxHP));
    text->setHorizontalAlignment(CEGUI::HA_CENTRE);
    text->setProperty("HorzFormatting", "HorzCentred");
  } // end if

} // string GUIManager::updateHealthDisplay()


void GUIManager::updateStaminaDisplay()
{
  CEGUI::WindowManager& winManager = CEGUI::WindowManager::getSingleton();
 
  if(winManager.isWindowPresent("CharacterDisplay/StaminaBar"))
  {
    CEGUI::ProgressBar* staminaBar = (CEGUI::ProgressBar*)winManager.getWindow("CharacterDisplay/StaminaBar");
  
    double currentStamina = Player::getInstance()->getStamina();
    double maxStamina = Player::getInstance()->getMaxStamina();
    double currentStaminaPercentage = currentStamina / maxStamina;

    staminaBar->setProgress(currentStaminaPercentage);

    // Update the text.
    CEGUI::Window* text = winManager.getWindow(staminaBar->getName() + "/Text");
    text->setText(CEGUI::PropertyHelper::intToString((int)currentStamina)
      + "/" + CEGUI::PropertyHelper::intToString((int)maxStamina));
    text->setHorizontalAlignment(CEGUI::HA_CENTRE);
    text->setProperty("HorzFormatting", "HorzCentred");
  } // end if

} // string GUIManager::updateStaminaDisplay()


void GUIManager::updateExperienceDisplay()
{
  CEGUI::WindowManager& winManager = CEGUI::WindowManager::getSingleton();
 
  if(winManager.isWindowPresent("CharacterDisplay/HealthBar"))
  {
    CEGUI::ProgressBar* experienceBar = 
      (CEGUI::ProgressBar*)winManager.getWindow("CharacterDisplay/ExperienceBar");
  
    double currentExperience = Player::getInstance()->getExperience();
    double maxExperience = Player::getInstance()->getExperienceRequired();
    double currentExperiencePercentage = currentExperience / maxExperience;

    experienceBar->setProgress(currentExperiencePercentage);

    // Update the text.
    CEGUI::Window* text = winManager.getWindow(experienceBar->getName() + "/Text");
    text->setText(CEGUI::PropertyHelper::intToString((int)currentExperience)
      + "/" + CEGUI::PropertyHelper::intToString((int)maxExperience));
    text->setHorizontalAlignment(CEGUI::HA_CENTRE);
    text->setProperty("HorzFormatting", "HorzCentred");
  } // end if

} // void GUIManager::updateExperienceDisplay()


void GUIManager::updateCurrencyAmount()
{
  CEGUI::WindowManager& winManager = CEGUI::WindowManager::getSingleton();

  if(winManager.isWindowPresent("CharacterDisplay/HealthBar"))
  {
    CEGUI::Window* currencyAmount = 
      (CEGUI::ProgressBar*)winManager.getWindow("CharacterDisplay/CurrencyAmount");

    currencyAmount->setText(CEGUI::PropertyHelper::intToString(Player::getInstance()->getCurrency()));
  } // end if
} // void GUIManager::updateCurrencyAmount()


string GUIManager::buildShopWindow()
{
  // Set the previous size in order to fix the containers dragging later (Hacky but easy).
  Player::getInstance()->getInventory()->setPreviousInventorySize();

  // Create the initial details.
  string GUILook = OgreFramework::getSingleton().GUILook;
  CEGUI::WindowManager& winManager = CEGUI::WindowManager::getSingleton();
  CEGUI::Window* sheet = winManager.getWindow("Sheet");

  CEGUI::UVector2 size = CEGUI::UVector2(CEGUI::UDim(0,180), CEGUI::UDim(0,355));

  // Create a frame window to serve as our Shop window.
  string windowName = "Shop";
  if(winManager.isWindowPresent(windowName))
  {
    this->clearShopDetails();
    winManager.getWindow(windowName)->setVisible(true);
    //winManager.destroyWindow(windowName);
  } // end if
  else
  {
    CEGUI::FrameWindow* window = (CEGUI::FrameWindow*)winManager.createWindow(GUILook + "/FrameWindow", windowName);

    window->setSize(size);
    window->setHorizontalAlignment(CEGUI::HA_LEFT);
    window->setVerticalAlignment(CEGUI::VA_TOP);
    window->setXPosition(CEGUI::UDim(0,5));
    window->setYPosition(CEGUI::UDim(0,115));
    //window->setPosition(position);
    window->setVisible(true);
    window->setSizingEnabled(false);
    window->setCloseButtonEnabled(true);
    window->setDragMovingEnabled(false);
    window->setText("Shop");
    window->subscribeEvent(CEGUI::FrameWindow::EventCloseClicked, 
      CEGUI::Event::Subscriber(&GUIManager::frameCloseButtonHit, this));
    window->setRollupEnabled(false);
    window->setAlwaysOnTop(true);
    sheet->addChildWindow(window);

    // Add a scrollable pane to the window to allow us to scroll up and down only.
    CEGUI::ScrollablePane* inventoryScroll = (CEGUI::ScrollablePane*)winManager.createWindow(GUILook+"/ScrollablePane", "Shop/Scroll");
    CEGUI::UVector2 scrollSize = CEGUI::UVector2(CEGUI::UDim(0.0,163), CEGUI::UDim(0.7,0));
    inventoryScroll->setHorizontalAlignment(CEGUI::HA_CENTRE);
    inventoryScroll->setSize(scrollSize);
    inventoryScroll->setClippedByParent(true);
    inventoryScroll->setMouseInputPropagationEnabled(true);
    inventoryScroll->setShowVertScrollbar(true);
    inventoryScroll->setShowHorzScrollbar(false);
    window->addChildWindow(inventoryScroll);

    // Pre define the size of the inventory slots.
    const int xSizeSlot  = 50;
    const int ySizeSlot  = 50;
    const int xImageSlot = 40;
    const int yImageSlot = 40;
    CEGUI::UVector2 slotSize  = CEGUI::UVector2(CEGUI::UDim(0,xSizeSlot),CEGUI::UDim(0,ySizeSlot));
    CEGUI::UVector2 imageSize = CEGUI::UVector2(CEGUI::UDim(0,xImageSlot),CEGUI::UDim(0,yImageSlot));
    const int numSlotsX = 3;

    // Loop through the vendors inventory.
    int numItems = Vendor::getInstance()->getInventory()->getItems()->size();
    vector<InventorySlot*>* slots = Vendor::getInstance()->getInventory()->getItems();
    int i = 0;
    for(vector<InventorySlot*>::iterator it = slots->begin(); it != slots->end(); ++it)
    {
      InventorySlot* slot = *it;
      AbstractItem* item = slot->getItem();

      // Build a slot.
      // Create an individual slot.
      CEGUI::Window* invSlot = winManager.createWindow(GUILook + "/StaticImage", 
        "ShopSlot-" + CEGUI::PropertyHelper::intToString(i));
      invSlot->setSize(slotSize);
      invSlot->setMouseInputPropagationEnabled(true);
      invSlot->setClippedByParent(true);

      // Subscribe events.
      invSlot->subscribeEvent(CEGUI::Window::EventMouseButtonDown,CEGUI::Event::Subscriber(&GUIManager::mouseClickOnShopSlot,this));

      // Set the xPositon of the slot.
      int xPos = xSizeSlot*(i%numSlotsX);
      int yPos = ySizeSlot*(i/numSlotsX)+3;

      // Place the position of the slot and add it to the window.
      invSlot->setPosition(CEGUI::UVector2(CEGUI::UDim(0,xPos),CEGUI::UDim(0,yPos)));
      invSlot->setTooltipText(item->getName());
      inventoryScroll->addChildWindow(invSlot);

      // Place in the item.
      // Create the image inventory slot.
      CEGUI::Window* itemImage  = winManager.createWindow(GUILook + "/StaticImage", 
        "Shop/Item-" + CEGUI::PropertyHelper::intToString(i));
      itemImage->setSize(imageSize);

      // disable frame and standard background
      itemImage->setProperty("FrameEnabled", "false");
      itemImage->setProperty("BackgroundEnabled", "true");
      itemImage->setClippedByParent(true);

      // Set the positioning properties.
      itemImage->setHorizontalAlignment(CEGUI::HA_CENTRE);
      itemImage->setVerticalAlignment(CEGUI::VA_CENTRE);
     
      // Allow mouse to pass through the inventory slot & set the image.
      //itemImage->setMousePassThroughEnabled(true);
      itemImage->setMousePassThroughEnabled(true);
      itemImage->setProperty("Image","set:"+item->getImageName()+" image:full_image");

      itemImage->setTooltipText(item->getName());
      /*itemImage->subscribeEvent(CEGUI::Window::EventMouseEntersArea, 
        CEGUI::Event::Subscriber(&GUIManager::handleMouseOver, this));
      itemImage->subscribeEvent(CEGUI::Window::EventMouseLeavesArea,
        CEGUI::Event::Subscriber(&GUIManager::handleMouseLeaves, this));*/

      // Add the slot to the container.
      invSlot->addChildWindow(itemImage);

      ++i;

    } // end for

    // Create a division.
    CEGUI::Window* divWind = winManager.createWindow(GUILook+"/StaticImage","Shop/Holder");
    CEGUI::UVector2 divSize = CEGUI::UVector2(CEGUI::UDim(1.0,0), CEGUI::UDim(0.310,0));
    CEGUI::UVector2 divPosi = CEGUI::UVector2(CEGUI::UDim(0,0), CEGUI::UDim(0.696,0));
    divWind->setPosition(divPosi);
    divWind->setSize(divSize);
    divWind->setProperty("BackgroundEnabled", "False");
    divWind->setAlwaysOnTop(true);
    divWind->setClippedByParent(false);
    window->addChildWindow(divWind);

    // Create a current item slot.
    CEGUI::Window* invSlot = winManager.createWindow(GUILook + "/StaticImage", 
      "CurrentItemSlot");
    invSlot->setSize(slotSize);
    invSlot->setMouseInputPropagationEnabled(true);
    invSlot->setClippedByParent(true);

    // Set the xPositon of the slot.
    int xPos = 10;
    int yPos = 10;

    // Place the position of the slot and add it to the window.
    invSlot->setPosition(CEGUI::UVector2(CEGUI::UDim(0,xPos),CEGUI::UDim(0,yPos)));
    divWind->addChildWindow(invSlot);

    // Create a nametext for the item.
    CEGUI::Window* currentItemName = winManager.createWindow(GUILook + "/StaticText", "CurrentItemName");
    xPos = 63;
    yPos = 10;

    currentItemName->setSize(CEGUI::UVector2(CEGUI::UDim(0,90), CEGUI::UDim(0,50)));
    currentItemName->setPosition(CEGUI::UVector2(CEGUI::UDim(0,xPos),CEGUI::UDim(0,yPos)));
    currentItemName->setProperty("BackgroundEnabled","False");
    currentItemName->setProperty("FrameEnabled", "False");
    currentItemName->setFont("DejaVuSans-10");
    currentItemName->setProperty("HorzFormatting","WordWrapCentred");
    //currentItemName->setProperty("VertFormatting","TopAligned");
    divWind->addChildWindow(currentItemName);
  

    // Set a window for the value of the item.
    CEGUI::Window* valueText = winManager.createWindow(GUILook + "/StaticText", "CurrentItemValue");
    xPos = 10;
    yPos = 60;

    valueText->setSize(CEGUI::UVector2(CEGUI::UDim(0,50), CEGUI::UDim(0,20)));
    valueText->setPosition(CEGUI::UVector2(CEGUI::UDim(0,xPos),CEGUI::UDim(0,yPos)));
    valueText->setProperty("BackgroundEnabled","False");
    valueText->setProperty("FrameEnabled", "False");
    valueText->setFont("DejaVuSans-10");
    valueText->setProperty("HorzFormatting","WordWrapCentred");
    //currentItemName->setProperty("VertFormatting","TopAligned");
    divWind->addChildWindow(valueText);

    // Build a button and set it an event.
    CEGUI::PushButton* button = (CEGUI::PushButton*)winManager.createWindow(GUILook + "/Button", "SaleButton");
    xPos = 80;
    yPos = -15;
    int width  = 60;
    int height = 30;

    button->setSize(CEGUI::UVector2(CEGUI::UDim(0,width), CEGUI::UDim(0, height)));
    button->setPosition(CEGUI::UVector2(CEGUI::UDim(0,xPos), CEGUI::UDim(0,yPos)));
    button->setVerticalAlignment(CEGUI::VA_BOTTOM);
    button->setVisible(false);
    button->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&GUIManager::saleButtonHit, this));
    divWind->addChildWindow(button);
  } // end else

  int invSize = Player::getInstance()->getInventory()->getItems()->size();
  // Loop through and disable all container dragging.
  for(int j=0;j<invSize;j++)
  {
    if(winManager.isWindowPresent("invContainer-"+ CEGUI::PropertyHelper::intToString(j)))
    {
      CEGUI::DragContainer* container = (CEGUI::DragContainer*)winManager.getWindow(
      "invContainer-"+ CEGUI::PropertyHelper::intToString(j));

      container->setDraggingEnabled(false);
      container->setStickyModeEnabled(false);
    } // end if
  } // end for


  return windowName;
} // string GUIManager::buildShopWindow()


void GUIManager::destroyShopWindow()
{
  CEGUI::WindowManager& winManager = CEGUI::WindowManager::getSingleton();
  if(winManager.isWindowPresent("Shop"))
  {
    winManager.getWindow("Shop")->setVisible(false);
    AudioManager::getSingleton().stopSound("Vendor");
    AudioManager::getSingleton().playSound("CaveAmbience");

    //winManager.destroyWindow("Shop");

    // Loop through and enable all container dragging.
    Player::getInstance()->getInventory()->setPreviousInventorySize();
    int invSize = Player::getInstance()->getInventory()->getPreviousInventorySize();
    int lastFound = 0;
    for(int i=0;i<invSize;i++)
    {
      if(winManager.isWindowPresent("invContainer-"+ CEGUI::PropertyHelper::intToString(i)))
      {
        CEGUI::DragContainer* container = (CEGUI::DragContainer*)winManager.getWindow(
        "invContainer-"+ CEGUI::PropertyHelper::intToString(i));

        container->setDraggingEnabled(true);
        container->setStickyModeEnabled(true);
        lastFound = i;
      } // end if
    } // end for

    Player::getInstance()->getInventory()->setPreviousInventorySize(lastFound);
  } // end if
} // void GUIManager::destroyShopWindow()


void GUIManager::clearShopDetails()
{
  CEGUI::WindowManager& winManager = CEGUI::WindowManager::getSingleton();

  if(winManager.isWindowPresent("CurrentItemSlot/Image"))
  {
    winManager.destroyWindow("CurrentItemSlot/Image");
  } // end if

  winManager.getWindow("CurrentItemName")->setText("");
  Vendor::getInstance()->setItemInQuestion(NULL);
  Vendor::getInstance()->setOwnerOfItem(NULL);
  winManager.getWindow("CurrentItemValue")->setText("");
  winManager.getWindow("SaleButton")->setText("Buy");
  winManager.getWindow("SaleButton")->setVisible(false);
  winManager.getWindow("SaleButton")->setEnabled(false);
} // void GUIManager::clearShopDetails()


void GUIManager::destroyNPCWindows()
{
  CEGUI::WindowManager& winManager = CEGUI::WindowManager::getSingleton();
  if(winManager.isWindowPresent("InteractWindow"))
  {
    winManager.getWindow("InteractWindow")->setVisible(false);
  } // end if
  if(winManager.isWindowPresent("VendorOptions"))
  {
    winManager.getWindow("VendorOptions")->setVisible(false);
  } // end if
} // void GUIManager::destroyNPCWindows()


void GUIManager::addActiveEffect(AbstractEffect* _effect)
{
  CEGUI::WindowManager& winManager = CEGUI::WindowManager::getSingleton();
  if(!winManager.isWindowPresent(_effect->getType()))
  {
    string GUILook = OgreFramework::getSingleton().GUILook;
    CEGUI::Window* activeEffectsWindow = winManager.getWindow("ActiveEffectsWindow");
  

    int childAmount = (int)activeEffectsWindow->getChildCount()-1;

    const int SIZE_X = 40;
    const int SIZE_Y = 40;
    const int NUM_EFFECTS_WIDTH = 5;
    const int POS_X = (SIZE_X+1) * (childAmount%NUM_EFFECTS_WIDTH) + 5;
    const int POS_Y = (SIZE_Y+1) * (childAmount/NUM_EFFECTS_WIDTH) + 30;

  
    CEGUI::Window* effectImage = winManager.createWindow(GUILook + "/StaticImage", _effect->getType());
    effectImage->setSize(CEGUI::UVector2(CEGUI::UDim(0, SIZE_X), CEGUI::UDim(0, SIZE_Y)));
    effectImage->setPosition(CEGUI::UVector2(CEGUI::UDim(0, POS_X), CEGUI::UDim(0, POS_Y)));
    effectImage->setProperty("FrameEnabled","False");
    effectImage->setProperty("Image", "set:" + _effect->getImage() + " image:full_image");
    activeEffectsWindow->addChildWindow(effectImage);

  } // end if

} // void GUIManager::addActiveEffect(AbstractEffect* _effect)


void GUIManager::removeActiveEffect(AbstractEffect* _effect)
{
  CEGUI::WindowManager& winManager = CEGUI::WindowManager::getSingleton();
  if(winManager.isWindowPresent(_effect->getType()))
  {
    winManager.destroyWindow(_effect->getType());
  } // end if
} // void GUIManager::removeActiveEffect(AbstractEffect* _effect)


void GUIManager::flashOnActiveEffect(AbstractEffect* _effect)
{
  CEGUI::WindowManager& winManager = CEGUI::WindowManager::getSingleton();
  if(winManager.isWindowPresent(_effect->getType()))
  {
    CEGUI::Window* window = winManager.getWindow(_effect->getType());
    window->setVisible(true);
  } // end if
} // void GUIManager::flashOnActiveEffect(AbstractEffect* _effect)


void GUIManager::flashOffActiveEffect(AbstractEffect* _effect)
{
  CEGUI::WindowManager& winManager = CEGUI::WindowManager::getSingleton();
  if(winManager.isWindowPresent(_effect->getType()))
  {
    CEGUI::Window* window = winManager.getWindow(_effect->getType());
    window->setVisible(false);
  } // end if
} // void GUIManager::flashOffActiveEffect(AbstractEffect* _effect)


void GUIManager::addEmptySlotToPlayerInventory(InventorySlot* _emptySlot)
{
  // Add empty slot to CEGUI.
  CEGUI::WindowManager& winManager = CEGUI::WindowManager::getSingleton();
  if(winManager.isWindowPresent("PlayerInventory"))
  {
    // Get all the details for adding a new slot to the inventory.
    CEGUI::FrameWindow* inventoryWindow = (CEGUI::FrameWindow*)winManager.getWindow("PlayerInventory");
    CEGUI::ScrollablePane* inventoryScroll = (CEGUI::ScrollablePane*)inventoryWindow->getChild("PlayerInventory/Scroll");
  
    const int xSizeSlot  = 50;
    const int ySizeSlot  = 50;
    const int xImageSlot = 40;
    const int yImageSlot = 40;
    CEGUI::UVector2 slotSize  = CEGUI::UVector2(CEGUI::UDim(0,xSizeSlot),CEGUI::UDim(0,ySizeSlot));
    CEGUI::UVector2 imageSize = CEGUI::UVector2(CEGUI::UDim(0,xImageSlot),CEGUI::UDim(0,yImageSlot));

    // Get the slots index.
    int i = _emptySlot->getIndex();

    // Add an empty Slot.
    // Create an individual slot.
    if(winManager.isWindowPresent("InvSlot-"  + CEGUI::PropertyHelper::intToString(i)))
    {
      exit(101);
    } // end if
    CEGUI::Window* invSlot = winManager.createWindow(OgreFramework::getSingleton().GUILook + "/StaticImage", 
      "InvSlot-" + CEGUI::PropertyHelper::intToString(i));

    invSlot->setSize(slotSize);
    invSlot->setMouseInputPropagationEnabled(true);
    const int numSlotsX = 3;

    // Set the xPositon of the slot.
    int xPos = xSizeSlot*(i%numSlotsX);
    int yPos = ySizeSlot*(i/numSlotsX);

    invSlot->setVisible(true);

    // Place the position of the slot and add it to the window.
    invSlot->setPosition(CEGUI::UVector2(CEGUI::UDim(0,xPos),CEGUI::UDim(0,yPos)));
    inventoryScroll->addChildWindow(invSlot);

    // Subscribe to the events.
    invSlot->subscribeEvent(CEGUI::Window::EventDragDropItemDropped, 
      CEGUI::Event::Subscriber(&GameScene::handleItemDroppedEmptyInventory, 
      (GameScene*)GameManager::getInstance()->getCurrentScene()));
    
  } // end if(winManager.isWindowPresent("PlayerInventory"))
} // void GUIManager::addEmptySlotToPlayerInventory(int _index)


void GUIManager::addItemToPlayerInventory(int _index, AbstractItem* _item, int _count)
{
  // Add empty slot to CEGUI.
  CEGUI::WindowManager& winManager = CEGUI::WindowManager::getSingleton();
  if(winManager.isWindowPresent("PlayerInventory"))
  {
    const int xImageSlot = 40;
    const int yImageSlot = 40;
    CEGUI::UVector2 imageSize = CEGUI::UVector2(CEGUI::UDim(0,xImageSlot),CEGUI::UDim(0,yImageSlot));

    // Create a drag container.
    int j=0;

    while(winManager.isWindowPresent("invContainer-" + CEGUI::PropertyHelper::intToString(j)))
    {
      ++j;
    } // end while

    CEGUI::DragContainer* container = (CEGUI::DragContainer*)winManager.createWindow("DragContainer", 
        "invContainer-"+ CEGUI::PropertyHelper::intToString(j));

    // Set the propeties of the drag container.
    container->setSize(imageSize);
    container->setHorizontalAlignment(CEGUI::HA_CENTRE);
    container->setVerticalAlignment(CEGUI::VA_CENTRE);
    container->setMouseInputPropagationEnabled(true);
    container->setStickyModeEnabled(true);
    container->setTooltipText(_item->getName());

    // Get the parent slot and add the container to it.
    CEGUI::Window* parentSlot = winManager.getWindow("InvSlot-" + CEGUI::PropertyHelper::intToString(_index));
    parentSlot->addChildWindow(container);

    // Subscribe to the container events.
    container->subscribeEvent(CEGUI::Window::EventDragDropItemDropped, 
      CEGUI::Event::Subscriber(&GameScene::handleItemDroppedContainer, 
      (GameScene*)GameManager::getInstance()->getCurrentScene()));
    container->subscribeEvent(CEGUI::DragContainer::EventDragStarted,
      CEGUI::Event::Subscriber(&GameScene::handleDragBegan, 
      (GameScene*)GameManager::getInstance()->getCurrentScene()));
    container->subscribeEvent(CEGUI::DragContainer::EventDragEnded, 
      CEGUI::Event::Subscriber(&GameScene::handleDragEnded, 
      (GameScene*)GameManager::getInstance()->getCurrentScene()));
    container->subscribeEvent(CEGUI::Window::EventMouseButtonDown,
      CEGUI::Event::Subscriber(&GameScene::handleInvSlotClicked, 
      (GameScene*)GameManager::getInstance()->getCurrentScene()));
    container->subscribeEvent(CEGUI::Window::EventMouseEntersArea,
      CEGUI::Event::Subscriber(&GameScene::handleMouseOver, 
      (GameScene*)GameManager::getInstance()->getCurrentScene()));
    container->subscribeEvent(CEGUI::Window::EventMouseLeavesArea,
      CEGUI::Event::Subscriber(&GameScene::handleMouseLeave, 
      (GameScene*)GameManager::getInstance()->getCurrentScene()));

    if(winManager.isWindowPresent("Shop"))
    {
      container->setDraggingEnabled(false);
      container->setStickyModeEnabled(false);
    } // end if
    // HERE IS A BUG --- @TODO @TODO
    // Create the image inventory slot.
    CEGUI::Window* invSlot  = winManager.createWindow(OgreFramework::getSingleton().GUILook + "/StaticImage", 
      "InvItem-" + CEGUI::PropertyHelper::intToString(j));
    invSlot->setSize(imageSize);

    // disable frame and standard background
    invSlot->setProperty("FrameEnabled", "false");
    invSlot->setProperty("BackgroundEnabled", "true");
 
    // Set the positioning properties.
    invSlot->setHorizontalAlignment(CEGUI::HA_CENTRE);
    invSlot->setVerticalAlignment(CEGUI::VA_CENTRE);
     
    // Allow mouse to pass through the inventory slot & set the image.
    invSlot->setMousePassThroughEnabled(true);
    invSlot->setProperty("Image","set:" + _item->getImageName() + " image:full_image");

    // Add the slot to the container.
    container->addChildWindow(invSlot);

    // Finally add a count if the item is stackable.
    if(_item->getStackable())
    {
      CEGUI::Window* itemCount = winManager.createWindow(OgreFramework::getSingleton().GUILook + "/StaticText", 
        "InvItem-" + CEGUI::PropertyHelper::intToString(j) + "/Count");
      itemCount->setMousePassThroughEnabled(true);
      itemCount->setText(CEGUI::PropertyHelper::intToString(_count));
      itemCount->setHorizontalAlignment(CEGUI::HA_LEFT);
      itemCount->setVerticalAlignment(CEGUI::VA_BOTTOM);
      itemCount->setProperty("HorzFormatting", "HorzLeft");
      itemCount->setProperty("VertFormatting", "BottomAligned");
      itemCount->setProperty("FrameEnabled", "False");
      itemCount->setProperty("BackgroundEnabled", "False");
      itemCount->setFont("DejaVuSans-8");
      itemCount->setSize(CEGUI::UVector2(CEGUI::UDim(0.6,0),CEGUI::UDim(0.35,0)));
      container->addChildWindow(itemCount);

    } // end if
  } // end if
} // void GUIManager::addEmptySlotToPlayerInventory()


void GUIManager::removeItemFromSlot(InventorySlot* _slot, bool _delete)
{
  int index = _slot->getIndex();

  CEGUI::WindowManager& winManager = CEGUI::WindowManager::getSingleton();
  
  // Find the associated slot.
  CEGUI::Window* invSlot = winManager.getWindow("InvSlot-" + CEGUI::PropertyHelper::intToString(index));
  
  // Destroy all the children.
  for(size_t i=invSlot->getChildCount();i>0;--i)
  {
    if(!_delete)
    {
      invSlot->removeChildWindow(invSlot->getChildAtIdx(i-1));
    } // end if
    else
    {
      invSlot->getChildAtIdx(i-1)->destroy();
    } // end else
  } // end for


} // void GUIManager::removeItemFromSlot(InventorySlot* _slot)


void GUIManager::removeEmptySlotFromPlayerInventory(InventorySlot* _slot)
{
  int index = _slot->getIndex();

  CEGUI::WindowManager& winManager = CEGUI::WindowManager::getSingleton();
  
  int i = index;
  ++i;
  while(winManager.isWindowPresent("InvSlot-" + CEGUI::PropertyHelper::intToString(i)))
  {
    CEGUI::Window* currentWindow = winManager.getWindow("InvSlot-" + CEGUI::PropertyHelper::intToString(i));
    CEGUI::Window* previousWindow = winManager.getWindow("InvSlot-" + CEGUI::PropertyHelper::intToString(i-1));

    // Move all the current window children into the previous one.
    for(size_t j=0;j<currentWindow->getChildCount();j++)
    {
      previousWindow->addChildWindow(currentWindow->getChildAtIdx(j));
      //currentWindow->removeChildWindow(currentWindow->getChildAtIdx(j));
    } // end for
    ++i; 
  } // end while

  // Delete the final window.
  winManager.destroyWindow("InvSlot-" + CEGUI::PropertyHelper::intToString(i-1));

} // void GUIManager::removeEmptySlotFromPlayerInventory()


void GUIManager::updateItemCount(InventorySlot* _slot)
{
  int index = _slot->getIndex();

  CEGUI::WindowManager& winManager = CEGUI::WindowManager::getSingleton();
  CEGUI::Window* slotWindow = winManager.getWindow("InvSlot-" + CEGUI::PropertyHelper::intToString(index));

  CEGUI::Window* container = slotWindow->getChild(0);

  // Now loop through the containers children and find the text child.
  for(size_t i = 0; i<container->getChildCount();i++)
  {
    CEGUI::Window* child = container->getChildAtIdx(i);

    if(child->getName().find("/Count") != string::npos)
    {
      child->setText(CEGUI::PropertyHelper::intToString(_slot->getCount()));
    } // end if

  } // end for


} // void GUIManager::updateItemCount(InventorySlot* _slot)


void GUIManager::updateHotSlotCount(InventorySlot* _slot, int _index)
{
  CEGUI::WindowManager& winManager = CEGUI::WindowManager::getSingleton();

  // Update the count number.
  CEGUI::Window* countWindow = winManager.getWindow(
    "HotBar/Slot/Count-" + CEGUI::PropertyHelper::intToString(_index));
  countWindow->setText(CEGUI::PropertyHelper::intToString(_slot->getCount()));
} // void GUIManager::updateHotSlotCount(InventorySlot* _slot, int _index)


void GUIManager::updateHotSlotCooldown(int _index, double _percentage)
{
  CEGUI::WindowManager& winManager = CEGUI::WindowManager::getSingleton();

  // Update hot slot cooldown.
  CEGUI::ProgressBar* cooldownWindow = (CEGUI::ProgressBar*)winManager.getWindow(
    "HotBar/Slot/Cooldown-" + CEGUI::PropertyHelper::intToString(_index));

  cooldownWindow->setProgress(_percentage);

} // void GUIManager::updateHotSlotCooldown(int _index, double _percentage)
 

void GUIManager::removeFromHotSlot(int _index)
{
  CEGUI::WindowManager& winManager = CEGUI::WindowManager::getSingleton();

  // Update the count number.
  CEGUI::Window* slotWindow = winManager.getWindow(
    "HotBar/Slot-" + CEGUI::PropertyHelper::intToString(_index));

  int numChildren = slotWindow->getChildCount();

  for(size_t i=numChildren;i>0;i--)
  {
    slotWindow->getChildAtIdx(i-1)->setVisible(false);
  } // end for

} // void GUIManager::removeFromHotSlot(int _index)


string GUIManager::createInteractFrameWindow(CEGUI::FrameWindow* _window, CEGUI::UVector2 _size, CEGUI::UVector2 _pos, string _title)
{
  // Create the initial details.
  string GUILook = OgreFramework::getSingleton().GUILook;
  CEGUI::WindowManager& winManager = CEGUI::WindowManager::getSingleton();
  CEGUI::Window* sheet = winManager.getWindow("Sheet");

  // Create a frame window to serve as our Quest box.
  string windowName = "InteractWindow";
  if(winManager.isWindowPresent(windowName))
  {
    winManager.destroyWindow(windowName);
  } // end if
    
  _window = static_cast<CEGUI::FrameWindow*>(winManager.createWindow(GUILook + "/FrameWindow", windowName));

  _window->setText(_title);
  _window->setSize(_size);
  _window->setPosition(CEGUI::UVector2(CEGUI::UDim(0,5), CEGUI::UDim(0,115)));
  _window->setHorizontalAlignment(CEGUI::HA_LEFT);
  _window->setVerticalAlignment(CEGUI::VA_TOP);
  _window->setRollupEnabled(false);
  _window->setSizingEnabled(false);
  _window->setCloseButtonEnabled(true);
  _window->subscribeEvent(CEGUI::FrameWindow::EventCloseClicked, 
    CEGUI::Event::Subscriber(&GUIManager::frameCloseButtonHit, this));
  _window->setDragMovingEnabled(false);

  sheet->addChildWindow(_window);
  return windowName;
} // void GUIManager::createInteractFrameWindow(CEGUI::FrameWindow* _window, CEGUI::UVector2 _size, CEGUI::UVector2 _pos, NPC* _npc)


void GUIManager::createRewardInspector(AbstractItem* _item)
{
  string GUILook = OgreFramework::getSingleton().GUILook;

  // Create the default window.
  CEGUI::WindowManager& winManager = CEGUI::WindowManager::getSingleton();

  CEGUI::Window* sheet = winManager.getWindow("Sheet");

  CEGUI::UVector2 itemInspectorSize = CEGUI::UVector2(CEGUI::UDim(0,190),CEGUI::UDim(0,235));;
  CEGUI::UVector2 itemInspectorPosition = CEGUI::UVector2(CEGUI::UDim(0.3-itemInspectorSize.d_x.d_scale/2,0),
    CEGUI::UDim(0.3-itemInspectorSize.d_y.d_scale/2,0));;

  if(winManager.isWindowPresent("RewardInspector"))
  {
    // Update the details without redrawing the entire window.
    winManager.getWindow("RewardInspector/ImageIcon")->setProperty(
      "Image", "set: " + _item->getImageName() + " image:full_image");
    winManager.getWindow("RewardInspector/NameText")->setText(
      _item->getName());
    winManager.getWindow("RewardInspector/DescriptionText")->setText(
      _item->getDescription());
  } // end if
  else
  {
    // Create a frame window to serve as our inventory.
    CEGUI::FrameWindow* itemInspector = (CEGUI::FrameWindow*)winManager.createWindow(GUILook + "/FrameWindow", "RewardInspector");
    itemInspector->setSize(itemInspectorSize);
    itemInspector->setPosition(itemInspectorPosition);
    itemInspector->setVisible(true);
    itemInspector->setSizingEnabled(false);
    itemInspector->setCloseButtonEnabled(true);
    itemInspector->setText(" ");
    itemInspector->subscribeEvent(CEGUI::FrameWindow::EventCloseClicked, 
      CEGUI::Event::Subscriber(&GUIManager::frameCloseButtonHit, this));
    sheet->addChildWindow(itemInspector);

    // Add item inspector elements - icon image.
    CEGUI::Window* imageIcon = winManager.createWindow(GUILook + "/StaticImage", "RewardInspector/ImageIcon");
    imageIcon->setSize(CEGUI::UVector2(CEGUI::UDim(0,40), CEGUI::UDim(0,40)));
    imageIcon->setProperty("BackgroundEnabled", "True");
    imageIcon->setProperty("FrameEnabled", "False");
    imageIcon->setPosition(CEGUI::UVector2(CEGUI::UDim(0,5), CEGUI::UDim(0,5)));
    imageIcon->setProperty("Image", "set: " + _item->getImageName() + " image:full_image");
    itemInspector->addChildWindow(imageIcon);

    // Add item inspector elements - item name..
    CEGUI::Window* nameText = winManager.createWindow(GUILook + "/StaticText", "RewardInspector/NameText");
    nameText->setSize(CEGUI::UVector2(CEGUI::UDim(0,100), CEGUI::UDim(0,35)));
    nameText->setProperty("BackgroundEnabled", "False");
    nameText->setProperty("FrameEnabled", "False");
    nameText->setPosition(CEGUI::UVector2(CEGUI::UDim(0,60), CEGUI::UDim(0,10)));
    nameText->setProperty("HorzFormatting", "WordWrapCentred");
    nameText->setProperty("VertFormatting", "VertCentred");
    nameText->setFont("DejaVuSans-10");
    nameText->setText(_item->getName());
    itemInspector->addChildWindow(nameText);

    // Add item inspector elements - item description.
    CEGUI::Window* descText = winManager.createWindow(GUILook + "/StaticText", "RewardInspector/DescriptionText");
    descText->setSize(CEGUI::UVector2(CEGUI::UDim(0.85,0), CEGUI::UDim(0,125)));
    descText->setProperty("BackgroundEnabled", "False");
    descText->setProperty("FrameEnabled", "False");
    descText->setHorizontalAlignment(CEGUI::HA_CENTRE);
    descText->setVerticalAlignment(CEGUI::VA_BOTTOM);
    descText->setProperty("HorzFormatting", "WordWrapLeftAligned");
    descText->setProperty("VertFormatting", "TopAligned");
    descText->setFont("DejaVuSans-10");
    descText->setText(_item->getDescription());
    itemInspector->addChildWindow(descText);
  } // end else
} // void GUIManager::createRewardInspector(AbstractItem* _item)


void GUIManager::createShopInspector(int _slotIDNumber)
{
  AbstractItem* item = Vendor::getInstance()->getInventory()->getSlotByIndex(_slotIDNumber)->getItem();
  this->createRewardInspector(item);
} // void GUIManager::createShopInspector(int slotIDNumber)


bool GUIManager::closeQuestBoxButtonPressed(const CEGUI::EventArgs& _e)
{
  const CEGUI::WindowEventArgs* args = static_cast<const CEGUI::WindowEventArgs*>(&_e);

  args->window->getParent()->destroy();
  CEGUI::WindowManager& winManager = CEGUI::WindowManager::getSingleton();
  if(winManager.isWindowPresent("RewardInspector"))
  {
    winManager.getWindow("RewardInspector")->destroy();
  } // end if
  return false;
} // bool GUIManager::closeQuestBoxButtonPressed(const CEGUI::EventArgs& _e)


bool GUIManager::acceptQuestButtonPressed(const CEGUI::EventArgs& _e)
{
  const CEGUI::WindowEventArgs* args = static_cast<const CEGUI::WindowEventArgs*>(&_e);

  CEGUI::WindowManager& winManager = CEGUI::WindowManager::getSingleton();
  if(winManager.isWindowPresent("RewardInspector"))
  {
    winManager.getWindow("RewardInspector")->destroy();
  } // end if

  Player::getInstance()->setCurrentQuest(QuestManager::getInstance()->createQuest(args->window->getName().c_str()));
  Player::getInstance()->getCurrentQuest()->init();
  AudioManager::getSingleton().playSound("Quest Accepted");
  this->addQuestToJournalWindow(Player::getInstance()->getCurrentQuest());
  args->window->getParent()->destroy();

  return true;
} // bool GUIManager::acceptQuestButtonPressed(const CEGUI::EventArgs& _e)


bool GUIManager::handleRewardItemClicked(const CEGUI::EventArgs& _e)
{

  CEGUI::WindowManager& winManager = CEGUI::WindowManager::getSingleton();
  if(winManager.isWindowPresent("RewardInspector"))
  {
    winManager.getWindow("RewardInspector")->destroy();
  } // end if

  // Create inspector for item.
  const CEGUI::WindowEventArgs* args = static_cast<const CEGUI::WindowEventArgs*>(&_e);

  AbstractItem* item;

  if(args->window->getName().substr(0,8) == "Journal/")
  {
    item = ItemManager::getInstance()->getItem(args->window->getName().substr(8).c_str());
  } // end if
  else
  {
    item = ItemManager::getInstance()->getItem(args->window->getName().c_str());
  } // end else
  this->createRewardInspector(item);

  return true;
} // bool GUIManager::handleRewardItemClicked(const CEGUI::EventArgs& _e)


bool GUIManager::frameCloseButtonHit(const CEGUI::EventArgs& _e)
{
  const CEGUI::WindowEventArgs* args = static_cast<const CEGUI::WindowEventArgs*>(&_e);
  CEGUI::WindowManager& winManager = CEGUI::WindowManager::getSingleton();

  AudioManager::getSingleton().playSound("Item Put Down");

  // Get the name of the window.
  string windowName = string(args->window->getName().c_str());

  if(args->window->getName() == "Shop")
  {
    // Loop through and enable all container dragging.
    Player::getInstance()->getInventory()->setPreviousInventorySize();
    int invSize = Player::getInstance()->getInventory()->getPreviousInventorySize();
    int lastFound = 0;

    // Turn off the vendor music.
    AudioManager::getSingleton().stopSound("Vendor");
    AudioManager::getSingleton().playSound("CaveAmbience");

    for(int i=0;i<invSize;i++)
    {
      if(winManager.isWindowPresent("invContainer-"+ CEGUI::PropertyHelper::intToString(i)))
      {
        CEGUI::DragContainer* container = (CEGUI::DragContainer*)winManager.getWindow(
        "invContainer-"+ CEGUI::PropertyHelper::intToString(i));

        container->setDraggingEnabled(true);
        container->setStickyModeEnabled(true);

        lastFound = i;
      } // end if
    } // end for

    Player::getInstance()->getInventory()->setPreviousInventorySize(lastFound);
    args->window->hide();
  } // end if
  else if(windowName == "Skills")
  {
    args->window->hide();
  } // end esle
  else if(windowName == "InteractWindow")
  {
    args->window->destroy();
  } // end else if
  else
  {
    args->window->destroy();
    if(winManager.isWindowPresent("RewardInspector"))
    {
      winManager.getWindow("RewardInspector")->destroy();
    } // end if

  } // end else

  return true;
} // bool GUIManager::frameCloseButtonHit(const CEGUI::EventArgs& _e)


bool GUIManager::vendorOptionButtonHit(const CEGUI::EventArgs& _e)
{
  const CEGUI::WindowEventArgs* args = static_cast<const CEGUI::WindowEventArgs*>(&_e);
  
  CEGUI::WindowManager& winManager = CEGUI::WindowManager::getSingleton();

  AudioManager::getSingleton().playSound("ClickButton");

  if(args->window->getName() == "Vendor/ShopButton")
  {
    args->window->getParent()->destroy();
    this->buildShopWindow();
    AudioManager::getSingleton().stopSound("Volcanic Pit");
    AudioManager::getSingleton().playSound("Vendor");
  } // end if
  else
  {
    Quest* quest = QuestManager::getInstance()->getQuest(args->window->getName().c_str());
    args->window->getParent()->destroy();
    this->buildAvailableQuestWindow(quest, StaticEntityManager::getInstance()->getNPC("Winstone Chalkhill"));
  } // end else

  return true;
} // bool GUIManager::vendorOptionButtonHit(const CEGUI::EventArgs& _e)


bool GUIManager::mouseClickOnShopSlot(const CEGUI::EventArgs& _e)
{
  const CEGUI::MouseEventArgs* args = static_cast<const CEGUI::MouseEventArgs*>(&_e);
  CEGUI::WindowManager& winManager = CEGUI::WindowManager::getSingleton();

  if(args->button == 1)
  {
    string shopSlotName = string(args->window->getName().c_str());
    string shopSlotNumber = shopSlotName.substr(9);
    int slotNumber = CEGUI::PropertyHelper::stringToInt(shopSlotNumber);
    this->createShopInspector(slotNumber);
  } // end if
  if(args->button == 0)
  {
    CEGUI::Window* image = args->window->getChildAtIdx(0);
    CEGUI::Window* currentWindow = winManager.getWindow("CurrentItemSlot");
    if(winManager.isWindowPresent("CurrentItemSlot/Image"))
    {
      image->clonePropertiesTo(*winManager.getWindow("CurrentItemSlot/Image"));
    } // end if
    else
    {
      currentWindow->addChildWindow(image->clone("CurrentItemSlot/Image",true));
    } // end else

    string slotNumberStr = (string)args->window->getName().substr(9).c_str();
    int slotNumber = CEGUI::PropertyHelper::stringToInt(slotNumberStr);

    InventorySlot* slot = Vendor::getInstance()->getInventory()->getSlotByIndex(slotNumber);
    winManager.getWindow("CurrentItemName")->setText(slot->getItem()->getName());

    Vendor::getInstance()->setItemInQuestion(slot);
    Vendor::getInstance()->setOwnerOfItem(StaticEntityManager::getInstance()->getNPC("Winstone Chalkhill"));
    int value = Vendor::getInstance()->workOutValue();
    winManager.getWindow("CurrentItemValue")->setText(
      CEGUI::PropertyHelper::intToString(value));
    winManager.getWindow("SaleButton")->setText("Buy");
    winManager.getWindow("SaleButton")->setVisible(true);
    
    if(value <= Player::getInstance()->getCurrency())
    {
      winManager.getWindow("SaleButton")->setEnabled(true);
    } // end if
    else
    {
      winManager.getWindow("SaleButton")->setEnabled(false);
    } // end else
  } // end if

  return true;
} // bool GUIManager::mouseClickOnShopSlot(const CEGUI::EventArgs& _e)


bool GUIManager::mouseClickOnInvForShop(InventorySlot* _slot)
{
  CEGUI::WindowManager& winManager = CEGUI::WindowManager::getSingleton();

  CEGUI::Window* currentWindow = winManager.getWindow("CurrentItemSlot");
  
  if(winManager.isWindowPresent("CurrentItemSlot/Image"))
  {
    winManager.getWindow("CurrentItemSlot/Image")->setProperty("Image","set:" + 
      _slot->getItem()->getImageName() + " image:full_image");
  } // end if
  else
  {
    CEGUI::Window* image = winManager.createWindow(OgreFramework::getSingleton().GUILook 
      + "/StaticImage", "CurrentItemSlot/Image");
    image->setSize(CEGUI::UVector2(CEGUI::UDim(0,40), CEGUI::UDim(0,40)));
    image->setHorizontalAlignment(CEGUI::HA_CENTRE);
    image->setVerticalAlignment(CEGUI::VA_CENTRE);
    image->setProperty("FrameEnabled", "False");
    image->setProperty("Image","set:" + _slot->getItem()->getImageName() + " image:full_image");
    currentWindow->addChildWindow(image);
  } // end else

  winManager.getWindow("CurrentItemName")->setText(_slot->getItem()->getName());
  Vendor::getInstance()->setItemInQuestion(_slot);
  Vendor::getInstance()->setOwnerOfItem(Player::getInstance());
  winManager.getWindow("CurrentItemValue")->setText(
    CEGUI::PropertyHelper::intToString(Vendor::getInstance()->workOutValue()));

  winManager.getWindow("SaleButton")->setText("Sell");
  winManager.getWindow("SaleButton")->setVisible(true);
  winManager.getWindow("SaleButton")->setEnabled(true);

  return true;
} // bool GUIManager::mouseClickOnInvForShop(const CEGUI::EventArgs& _e)


bool GUIManager::saleButtonHit(const CEGUI::EventArgs& _e)
{
  const CEGUI::MouseEventArgs* args = static_cast<const CEGUI::MouseEventArgs*>(&_e);
  CEGUI::WindowManager& winManager = CEGUI::WindowManager::getSingleton();

  if(args->window->getText() == "Buy")
  {
    Vendor::getInstance()->sellItem();

    // Check to see if the player can afford another instance of the item.
    if(Vendor::getInstance()->workOutValue() > Player::getInstance()->getCurrency())
    {
      args->window->setEnabled(false);
    } // end if

  } // end if
  else if(args->window->getText() == "Sell")
  {
    Vendor::getInstance()->buyItem();
  } // end else if

  

  return true;

} // bool GUIManager::saleButtonhit(const CEGUI::EventArgs& _e)


bool GUIManager::itemDroppedOnHotSlot(const CEGUI::EventArgs& _e)
{
  const CEGUI::DragDropEventArgs* args = static_cast<const CEGUI::DragDropEventArgs*>(&_e);
  CEGUI::WindowManager& winManager = CEGUI::WindowManager::getSingleton();


  // Determine if the drag drop window is a container or a skill.
  if(args->dragDropItem->getName().substr(0,13) == "invContainer-")
  {
    // Get the slot numbers for the container and the item.
    int inventorySlotNumber = this->getSlotEndNumber(args->dragDropItem->getParent());
    int hotSlotNumber = this->getSlotEndNumber(args->window);
    InventorySlot* itemSlot = Player::getInstance()->getInventory()->getSlotByIndex(inventorySlotNumber);
    AbstractItem* item = itemSlot->getItem();

    if(item->getHotSlotAble())
    {

      HotBar::getSingletonPtr()->addToHotBar(itemSlot, hotSlotNumber);

      if(args->window->getChildCount() == 0)
      { // If the window is blank & the imageWindow doesnt exist.
        
      } // end if(args->window->getChildCount() == 0)
      else
      {
        // Update the image.
        CEGUI::Window* imageWindow = winManager.getWindow(
          "HotBar/Slot/Image-" + CEGUI::PropertyHelper::intToString(hotSlotNumber));
        imageWindow->setProperty("Image", "set:" + item->getImageName() + " image:full_image");
        imageWindow->setVisible(true);

        // Update the count number.
        CEGUI::Window* countWindow = winManager.getWindow(
          "HotBar/Slot/Count-" + CEGUI::PropertyHelper::intToString(hotSlotNumber));
        countWindow->setText(CEGUI::PropertyHelper::intToString(itemSlot->getCount()));
        countWindow->setVisible(true);
      } // end else
    } // end if(item->getHotSlotAble)

  } // end if(args->dragDropItem->getName().substr(0,13) == "invContainer-")
  else if(args->dragDropItem->getName().substr(0,24) == "SkillSlot/DragContainer-")
  {
    // Get the skill numbers for the container and the hot slot.
    int skillNumber = this->getSlotEndNumber(args->dragDropItem->getParent());
    int hotSlotNumber = this->getSlotEndNumber(args->window);
    Skill* skill = Player::getInstance()->getSkillClass()->getSkill(skillNumber);
    
    HotBar::getSingleton().addToHotBar(skill, hotSlotNumber);
    
    if(args->window->getChildCount() == 0)
    {

    } // end if
    else
    {
      // Update the image.
      CEGUI::Window* imageWindow = winManager.getWindow(
        "HotBar/Slot/Image-" + CEGUI::PropertyHelper::intToString(hotSlotNumber));
         imageWindow->setProperty("Image", "set:" + skill->getImageName() + " image:full_image");
         imageWindow->setVisible(true);

      // Update the count number.
      CEGUI::Window* countWindow = winManager.getWindow(
        "HotBar/Slot/Count-" + CEGUI::PropertyHelper::intToString(hotSlotNumber));
      countWindow->setVisible(false);

    } // end else

    AudioManager::getSingleton().playSound("Item Put Down");

  } // end else if

  return true;
} // bool GUIManager::itemDroppedOnHotSlot(const CEGUI::EventArgs& _e)


bool GUIManager::hotBarSlotClicked(const CEGUI::EventArgs& _e)
{
  const CEGUI::MouseEventArgs* args = static_cast<const CEGUI::MouseEventArgs*>(&_e);
  CEGUI::WindowManager& winManager = CEGUI::WindowManager::getSingleton();

  if(args->button == CEGUI::LeftButton)
  {
    // Get the hot slot number.
    int index = this->getSlotEndNumber(args->window);
    HotBar::getSingletonPtr()->activateHotSlot(index);
    AudioManager::getSingleton().playSound("ClickButton");
  } // end if

  return true;
} // bool GUIManager::hotBarSlotClicked(const CEGUI::EventArgs& _e)


bool GUIManager::skillPlusButtonClicked(const CEGUI::EventArgs& _e)
{
  const CEGUI::MouseEventArgs* args = static_cast<const CEGUI::MouseEventArgs*>(&_e);
  CEGUI::WindowManager& winManager = CEGUI::WindowManager::getSingleton();

  // Get the skill number.
  int skillNumber = this->getSlotEndNumber(args->window);

  
  Skill** skills = Player::getInstance()->getSkillClass()->getSkills();

  if(skills[skillNumber]->getLevel() >= 3)
  {
    return true;
  } // end if

  // Upgrade the associated skill.
  Player::getInstance()->setSkillPoints(Player::getInstance()->getSkillPoints()-1);
  Player::getInstance()->getSkillClass()->upgradeSkill(skillNumber);
  AudioManager::getSingleton().playSound("ClickButton");
  skills = Player::getInstance()->getSkillClass()->getSkills();
  
  if(skills[skillNumber]->getLevel() == 1)
  {
    CEGUI::DragContainer* dragContainer = 
      (CEGUI::DragContainer*)winManager.getWindow("SkillSlot/DragContainer-" + CEGUI::PropertyHelper::intToString(skillNumber));
    dragContainer->setDraggingEnabled(true);
    
    CEGUI::Window* dragImage = winManager.getWindow("SkillSlot/DragContainer/Image-" +
      CEGUI::PropertyHelper::intToString(skillNumber));
    dragImage->setProperty("Image", "set:" + skills[skillNumber]->getImageName() + " image:full_image");

    CEGUI::Window* skillImage = winManager.getWindow("SkillSlot/Image-" +
      CEGUI::PropertyHelper::intToString(skillNumber));
    skillImage->setProperty("Image", "set:" + skills[skillNumber]->getImageName() + " image:full_image");
  } // end if


  // Now check the player has skill points left.
  if(Player::getInstance()->getSkillPoints() <= 0)
  {
    // Remove the buttons.
    for(int i=0;i<Player::getInstance()->getSkillClass()->getNumSkills();i++)
    {
      CEGUI::Window* buttonWindow = winManager.getWindow("SkillSlot/PlusButton-" + 
        CEGUI::PropertyHelper::intToString(i));
      buttonWindow->setVisible(false);
    } // end for
  } // end if

  // Finally update the count of the clicked slot gui wise.
  CEGUI::Window* levelText = winManager.getWindow("SkillSlot/LevelText-" +
      CEGUI::PropertyHelper::intToString(skillNumber));
  levelText->setText(CEGUI::PropertyHelper::intToString(skills[skillNumber]->getLevel()));

  return true;
} // bool GUIManager::skillPlusButtonClicked(const CEGUI::EventArgs& _e)


bool GUIManager::handleSkillSlotClicked(const CEGUI::EventArgs& _e)
{
  const CEGUI::MouseEventArgs* args = static_cast<const CEGUI::MouseEventArgs*>(&_e);
  CEGUI::WindowManager& winManager = CEGUI::WindowManager::getSingleton();

  if(args->button == CEGUI::RightButton)
  {
    AudioManager::getSingleton().playSound("Item Picked Up");
    if(winManager.isWindowPresent("Inspector"))
    {
      winManager.getWindow("Inspector")->setVisible(true);

      // Get the skill number.
      int skillNumber = this->getSlotEndNumber(args->window);

      // Update the inspector based on this skill on if right click. @Todo - Move inspector code into here.
      GameScene* currentGameScene = (GameScene*)GameManager::getInstance()->getCurrentScene();
      currentGameScene->updateInspector(Player::getInstance()->getSkillClass()->getSkill(skillNumber));
    } // end if
  } // end if

  AudioManager::getSingleton().playSound("Item Picked Up");


  return true;
} // bool GUIManager::handleSkillSlotClicked(const CEGUI::EventArgs& _e)


bool GUIManager::handleMouseOver(const CEGUI::EventArgs& _e)
{
  const CEGUI::WindowEventArgs* args = static_cast<const CEGUI::WindowEventArgs*>(&_e);
  // Set the alpha to 0.5 when mouse is over the window.
  CEGUI::Window* slotWindow = args->window;
  slotWindow->setAlpha(0.5);

  return true;
} // bool GUIManager::handleMouseOver(const CEGUI::EventArgs& _e)


bool GUIManager::handleMouseLeaves(const CEGUI::EventArgs& _e)
{
  const CEGUI::WindowEventArgs* args = static_cast<const CEGUI::WindowEventArgs*>(&_e);

  // Return the alpha value when the mouse pointer is removed from the window.
  CEGUI::Window* slotWindow = args->window;
  slotWindow->setAlpha(1.0);

  return true;
} // bool GUIManager::handleMouseLeave(const CEGUI::EventArgs& _e)


bool GUIManager::handleSaveButtonClicked(const CEGUI::EventArgs& _e)
{
  AudioManager::getSingleton().playSound("ClickButton");
  AudioManager::getSingleton().stopSound("CaveAmbience");
  AudioManager::getSingleton().playSound("SaveTheme");
  GameScene* gameScene = (GameScene*)GameManager::getInstance()->getCurrentScene();
  gameScene->switchToSaveScene();

  return true;
} // bool GUIManager::handleSaveButtonClicked(const CEGUI::EventArgs& _e)


int GUIManager::getSlotEndNumber(CEGUI::Window* _slot)
{ // Gets the end identifier of a number.
  // The number must be at the end of the name and must be prefixed with a '-'.
  // e.g. HotSlot-32 would return 32.

  // Get the names of the window.
  string slotName = _slot->getName().c_str();

  // Extract the string for the number of it.
  string::iterator it = slotName.begin();

  while(*it != '-')
  {
    ++it;
  } // end while
    
  ++it;
  string slotStringNumber;

  while(it != slotName.end())
  {
    slotStringNumber += *it;
    ++it;
  } // end while

  // Now convert the strings to ints.
  return CEGUI::PropertyHelper::stringToInt(slotStringNumber.c_str());

} // int GUIManager::getSlotLocation(CEGUI::Window* _slot)


void GUIManager::setDragDropStarted(bool _started)
{
  this->dragDropStarted = _started;
} // bool GUIManager::setDragDropStarted(bool _started)


bool GUIManager::getDragDropStarted()
{
  return this->dragDropStarted;
} // void GUIManager::getDragDropStarted()


int GUIManager::findFreeContainerNumber()
{ // Obtains a free invContainer- object for use by the equip slots during a redraw.
  CEGUI::WindowManager& winManager = CEGUI::WindowManager::getSingleton();
  int index = 0;

  while(winManager.isWindowPresent("invContainer-" + CEGUI::PropertyHelper::intToString(index)))
  {
    ++index;
  } // end while

  return index;
} // int GUIManager::findFreeContainerNumber()