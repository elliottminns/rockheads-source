#include "stdafx.h"
#include "DamageTextManager.h"


DamageTextManager* DamageTextManager::instance = NULL;
bool DamageTextManager::initialised = false;


DamageTextManager& DamageTextManager::getSingleton()
{
  if(!instance)
  {
    instance = new DamageTextManager;
  } // end if

  return *instance;

} // DamageTextManager DamageTextManager::getSingleton()


DamageTextManager* DamageTextManager::getSingletonPtr()
{
  if(!instance)
  {
    instance = new DamageTextManager;
  } // end if

  return instance;
} // DamageTextManager* DamageTextManager::getSingletonPtr()


DamageTextManager::DamageTextManager()
{
  this->initialise();
} // DamageTextManager::DamageTextManager()


DamageTextManager::~DamageTextManager()
{
  delete[] this->damageTexts;
  this->damageTexts = NULL;
} // DamageTextManager::~DamageTextManager() 


void DamageTextManager::initialise()
{
  this->numDamageTexts = 35;
  this->damageTexts = new DamageText*[this->numDamageTexts];

  for(int i=0;i<this->numDamageTexts;i++)
  {
    this->damageTexts[i] = new DamageText(i);
  } // end for

} // void DamageTextManager::initialise()


void DamageTextManager::update(double _timeSinceLastFrame)
{
  for(int i=0;i<this->numDamageTexts; i++)
  {
    if(this->damageTexts[i]->getActive())
    {
      this->damageTexts[i]->update(_timeSinceLastFrame);
    } // end if
  } // end for
} // void update(double _timeSinceLastFrame)


void DamageTextManager::createDamageText(AbstractEntity* _entity, int _damage)
{
  for(int i=0;i<this->numDamageTexts;i++)
  {
    if(!this->damageTexts[i]->getActive())
    {
      this->damageTexts[i]->createText(_entity, Ogre::StringConverter::toString(_damage), 
        Ogre::ColourValue(1.0f, 1.0, 0.0f, 1.0f));
      break;
    } // end if
  } // end for
} // void DamageTextManager::createDamageText(AbstractEntity* _entity, int _damage)


void DamageTextManager::createHealText(AbstractEntity* _entity, int _healAmount)
{
  for(int i=0;i<this->numDamageTexts;i++)
  {
    if(!this->damageTexts[i]->getActive())
    {
      this->damageTexts[i]->createText(_entity, Ogre::StringConverter::toString(_healAmount), 
        Ogre::ColourValue(0.0f, 1.0, 0.0f, 1.0f));
      break;
    } // end if
  } // end for
} // void DamageTextManager::createHealText(AbstractEntity* _entity, int _healAmount)


void DamageTextManager::createMissText(AbstractEntity* _entity)
{
  for(int i=0;i<this->numDamageTexts;i++)
  {
    if(!this->damageTexts[i]->getActive())
    {
      this->damageTexts[i]->createText(_entity, "Miss");
      break;
    } // end if
  } // end for
} // void DamageTextManager::createMissText(AbstractEntity* _entity)