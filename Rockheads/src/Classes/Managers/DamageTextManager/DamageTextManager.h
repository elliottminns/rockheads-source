#include "../../../PrecompiledHeaders/stdafx.h"
#ifndef DAMAGE_TEXT_MANAGER_H
#define DAMAGE_TEXT_MANAGER_H
#include "../../DamageText/DamageText.h"

class DamageTextManager
{
private:
  int numDamageTexts;
  DamageText** damageTexts;

  static DamageTextManager* instance;
  static bool initialised;

  DamageTextManager();
  ~DamageTextManager();

public:
  static DamageTextManager& getSingleton();
  static DamageTextManager* getSingletonPtr();

  void initialise();
  void update(double _timeSinceLastFrame);

  void createDamageText(AbstractEntity* _entity, int _damage);
  void createHealText(AbstractEntity* _entity, int _healAmount);
  void createMissText(AbstractEntity* _entity);
};

#endif