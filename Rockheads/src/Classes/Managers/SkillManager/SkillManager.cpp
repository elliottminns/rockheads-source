#include "stdafx.h"
#include "SkillManager.h"
#include "../../XMLParser/XMLParser.h"
#include "boost\algorithm\string.hpp"
#include "../../GameEntities/DynamicEntities/EntityClass/SkillSet/SkillSet.h"

SkillManager* SkillManager::instance = NULL;
bool SkillManager::initialisedData = false;


SkillManager* SkillManager::getInstance()
{
  if(!instance)
  {
    instance = new SkillManager;
  } // end if
  if(!initialisedData)
  {
    initialisedData = true;
    instance->initialise();
  } // end if

  return instance;
} // SkillManager* SkillManager::getInstance()

SkillManager::SkillManager()
{
  skillSets = NULL;
  skills = NULL;
} // SkillManager::SkillManager()


SkillManager::~SkillManager()
{
  // Delete the skillSets.
  for(int i=0;i<this->numSkillSets;i++)
  {
    delete this->skillSets[i];
    this->skillSets[i] = NULL;
  } // end for
  delete[] this->skillSets;
  this->skillSets = NULL;

  // Delete the skills.
  for(int i=0;i<this->numSkills;i++)
  {
    delete this->skills[i];
    this->skills[i] = NULL;
  } // end for
  delete skills;
  skills = NULL;
  
} // SkillManager::~SkillManager()


void SkillManager::initialise()
{
  vector<Skill*>* skillsVec = this->loadSkills();

  this->numSkills = skillsVec->size();
  this->skills = new Skill*[this->numSkills];

  int i = 0;
  for(vector<Skill*>::iterator it = skillsVec->begin(); it!=skillsVec->end(); ++it)
  {
    this->skills[i] = *it;
    this->registerSkill(this->skills[i]->getName().c_str(), this->skills[i]);
    string imageName = this->skills[i]->getImageName().c_str();
    string bwImageName = imageName;
    size_t found;
    // Append -bw on to the image name before the period.
    found = bwImageName.find('.');
    if(found != string::npos)
    {
      bwImageName.insert(found,"-bw");
    } // end if

    CEGUI::ImagesetManager::getSingleton().createFromImageFile(this->skills[i]->getImageName().c_str(), 
      this->skills[i]->getImageName().c_str());
    CEGUI::ImagesetManager::getSingleton().createFromImageFile(bwImageName.c_str(), 
      bwImageName.c_str());
    
    ++i;
  } // end for

  vector<SkillSet*>* skillSetData = this->loadSkillSets();
  this->numSkillSets = skillSetData->size();
  this->skillSets = new SkillSet*[this->numSkillSets];

  i = 0;
  for(vector<SkillSet*>::iterator it = skillSetData->begin(); it != skillSetData->end(); ++it)
  {
    this->skillSets[i] = *it;
    this->registerSkillSet(this->skillSets[i]->getName().c_str(), this->skillSets[i]);
    ++i;
  } // end for

  delete skillsVec;
  skillsVec = NULL;
} // void SkillManager::initialise()


vector<Skill*>* SkillManager::loadSkills()
{
  string skillFile = "../../data/skills.xml";
  return XMLParser::getInstance()->parseSkillFile(skillFile);
} // void SkillManager::loadSkills()


vector<SkillSet*>* SkillManager::loadSkillSets()
{
  string skillSetFile = "../../data/skillset.xml";
  return XMLParser::getInstance()->parseSkillSetFile(skillSetFile);
} // vector<SkillSet*>* SkillManager::loadSkillSets()


bool SkillManager::registerSkill(const char *_type, Skill* _skill)
{
  string str = string(_type);
  boost::to_lower(str);
  str += Ogre::StringConverter::toString(_skill->getLevel());
  return this->skillMap.insert(SkillMap::value_type(str, _skill)).second;
} // bool SkillManager::registerSkill(const char *_type, tCreator _creator)


bool SkillManager::registerSkillSet(const char *_type, SkillSet* _skillSet)
{
  string str = string(_type);
  boost::to_lower(str);
  return this->skillSetMap.insert(SkillSetMap::value_type(str, _skillSet)).second;
} // bool SkillManager::registerSkillSet(const char *_type, SkillSet* _skillSet)


Skill* SkillManager::createSkill(string _name, int _level)
{
  string str = string(_name);
  boost::to_lower(str);
  str += Ogre::StringConverter::toString(_level);
  SkillMap::iterator it = this->skillMap.find(string(str));
  if(it != this->skillMap.end())
  {
    Skill* skill = (*it).second;
    return skill->clone();
  } // end if

  return NULL;
} // Skill* SkillMananger::createSkill(string _name)


SkillSet* SkillManager::createSkillSet(string _name)
{
  string str = string(_name);
  boost::to_lower(str);
  SkillSetMap::iterator it = this->skillSetMap.find(string(str));
  if(it != this->skillSetMap.end())
  {
    SkillSet* skillSet = (*it).second;
    return skillSet->clone();
  } // end if

  return NULL;
} // Skill* SkillManager::createSkillSet(string _name)


SkillSet* SkillManager::getSkillSet(string _name)
{ // Method returns the instance of the templated skill set. Should not be used
  // to assign a new instance to an entity.
  string str = string(_name);
  boost::to_lower(str);
  SkillSetMap::iterator it = this->skillSetMap.find(string(str));
  if(it != this->skillSetMap.end())
  {
    SkillSet* skillSet = (*it).second;
    return skillSet;
  } // end if

  return NULL;
} // SkillSet* SkillManager::getSkillSet(string _name) const


SkillSet** SkillManager::getSkillSets() const
{
  return this->skillSets;
} // SkillSet** SkillManager::getSkillSets() const


int SkillManager::getNumSkillSets() const
{
  return this->numSkillSets;
} // int SkillManager::getNumSkillSets() const