#include "../../../PrecompiledHeaders/stdafx.h"
#ifndef SKILL_MANAGER_H
#define SKILL_MANAGER_H

#include "../../GameEntities/DynamicEntities/EntityClass/Skill/Skill.h"
#include <iostream>
#include <vector>
#include <map>
#include <string>

class SkillSet;

using namespace std;

class SkillManager
{
private:
  // Factory stuff.
  typedef map<string,Skill*> SkillMap;
  typedef map<string,SkillSet*> SkillSetMap;

  // Constructor & Destructor for singleton.
  SkillManager();
  ~SkillManager();
  
  static SkillManager* instance;
  static bool initialisedData;

  int numSkills;
  int numSkillSets;

  Skill** skills;
  SkillSet** skillSets;

  SkillMap skillMap;
  SkillSetMap skillSetMap;

public:
  
  static SkillManager* getInstance();

  void initialise();
  vector<Skill*>* loadSkills();
  vector<SkillSet*>* loadSkillSets();

  bool registerSkill(const char *_type, Skill* _skill);
  bool registerSkillSet(const char *_type, SkillSet* _skillSet);

  Skill* createSkill(string _name, int _level);
  SkillSet* createSkillSet(string _name);

  SkillSet* getSkillSet(string _name);

  SkillSet** getSkillSets() const;
  int getNumSkillSets() const;

}; // class SkillManager

#endif