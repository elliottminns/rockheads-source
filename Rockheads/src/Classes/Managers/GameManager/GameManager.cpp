#include "stdafx.h"
#include "GameManager.h"
#include <OgreWindowEventUtilities.h>

GameManager *GameManager::instance = NULL;

GameManager *GameManager::getInstance()
{
  if(instance == NULL)
  {
    instance = new GameManager;
  } // end if

  return instance;


} // GameManager *GameManager::getInstance()

GameManager::GameManager()
{
	this->shutdown = false;
  srand((unsigned)time(NULL));

} // GameManage::AppStateManager()


GameManager::~GameManager()
{
	Scene_Info si;

  while(!this->activeSceneStack.empty())
	{
		this->activeSceneStack.back()->exit();
		this->activeSceneStack.pop_back();
	} // end while

	while(!this->scenes.empty())
	{
		si = this->scenes.back();
    si.scene->destroy();
    this->scenes.pop_back();
	} // end while
} // GameManage::~AppStateManager()


void GameManager::manageScene(Ogre::String _sceneName, AbstractScene* _scene)
{
	try
	{
		Scene_Info newSceneInfo;
		newSceneInfo.name   = _sceneName;
		newSceneInfo.scene  = _scene;
		this->scenes.push_back(newSceneInfo);
	} // end try
	catch(std::exception& e)
	{
		delete _scene;

		throw Ogre::Exception(Ogre::Exception::ERR_INTERNAL_ERROR, "Error while trying to manage a new AppState\n" + Ogre::String(e.what()), "AppStateManager.cpp (39)");
	} // end catch(std::exception& e)
} // void GameManage::manageAppState(Ogre::String _stateName, AbstractScene* _state)


AbstractScene* GameManager::findByName(Ogre::String _sceneName)
{
	std::vector<Scene_Info>::iterator it;

	for(it=this->scenes.begin();it!=this->scenes.end();it++)
	{
		if(it->name == _sceneName)
    {
			return it->scene;
    } // end if
	} // end for

	return 0;

} // AbstractScene* GameManager::findByName(Ogre::String stateName)


void GameManager::start(AbstractScene* _scene)
{
	changeScene(_scene);

  int timeSinceLastFrame = 1;
  this->startTime = 0;

	while(!shutdown)
	{
		if(OgreFramework::getSingletonPtr()->renderWindow->isClosed())
    {
      this->shutdown = true;
    } // end if

		Ogre::WindowEventUtilities::messagePump();

		if(OgreFramework::getSingletonPtr()->renderWindow->isActive())
		{
			startTime = OgreFramework::getSingletonPtr()->timer->getMillisecondsCPU();

			OgreFramework::getSingletonPtr()->keyboard->capture();
			OgreFramework::getSingletonPtr()->mouse->capture();

			this->activeSceneStack.back()->update(timeSinceLastFrame);

			OgreFramework::getSingletonPtr()->updateOgre(timeSinceLastFrame);
			OgreFramework::getSingletonPtr()->root->renderOneFrame();

      //Need to inject timestamps to CEGUI System.
      CEGUI::System::getSingleton().injectTimePulse((float)timeSinceLastFrame/1000);

			timeSinceLastFrame = OgreFramework::getSingletonPtr()->timer->getMillisecondsCPU() - startTime;
      
		} // end if
		else
		{
#if OGRE_PLATFORM == OGRE_PLATFORM_WIN32
            Sleep(1000);
#else
            sleep(1);
#endif
		} // end else
	} // end while(!shutdown);

	OgreFramework::getSingletonPtr()->log->logMessage("Main loop quit");
} // void GameManager::start(AbstractScene* scene)


void GameManager::changeScene(AbstractScene* _scene)
{
	if(!this->activeSceneStack.empty())
	{
		this->activeSceneStack.back()->exit();
		this->activeSceneStack.pop_back();
	} // end if

	this->activeSceneStack.push_back(_scene);
	this->init(_scene);
	this->activeSceneStack.back()->enter();
  this->startTime = 0;
  OgreFramework::getSingletonPtr()->timer->reset();
} // void GameManager::changeScene(AbstractScene* scene)


bool GameManager::pushScene(AbstractScene* _scene)
{
	if(!this->activeSceneStack.empty())
	{
		if(!this->activeSceneStack.back()->pause())
    {
			return false;
    } // end if
	} // end if

	this->activeSceneStack.push_back(_scene);
	this->init(_scene);
	this->activeSceneStack.back()->enter();

	return true;
} // bool GameManager::pushScene(AbstractScene* scene)


void GameManager::popScene()
{
	if(!this->activeSceneStack.empty())
	{
		this->activeSceneStack.back()->exit();
		this->activeSceneStack.pop_back();
	} // end if

	if(!this->activeSceneStack.empty())
	{
		this->init(this->activeSceneStack.back());
		this->activeSceneStack.back()->resume();
	} // end if
  else
  {
		this->beginShutdown();
  } // end else
} // void GameManager::popScene()


void GameManager::popAllAndPushScene(AbstractScene* _scene)
{
  while(!this->activeSceneStack.empty())
  {
    this->activeSceneStack.back()->exit();
    this->activeSceneStack.pop_back();
  } // end while

  this->pushScene(_scene);
} // void GameManager::popAllAndPushScene(AbstractScene* _scene)


void GameManager::pauseScene()
{
	if(!this->activeSceneStack.empty())
	{
		this->activeSceneStack.back()->pause();
	} // end if(!activeSceneStack.empty())

	if(this->activeSceneStack.size() > 2)
	{
		this->init(this->activeSceneStack.at(this->activeSceneStack.size() - 2));
		this->activeSceneStack.at(this->activeSceneStack.size() - 2)->resume();
	} // end if(activeSceneStack.size() > 2)
} // void GameManager::pauseScene()


void GameManager::beginShutdown()
{
	this->shutdown = true;
} // void GameManager::beginShutdown()


void GameManager::init(AbstractScene* _scene)
{
  OgreFramework::getSingletonPtr()->keyboard->setEventCallback(_scene);
	OgreFramework::getSingletonPtr()->mouse->setEventCallback(_scene);
	OgreFramework::getSingletonPtr()->renderWindow->resetStatistics();
} // void GameManager::init(AbstractScene* _scene)


AbstractScene* GameManager::getCurrentScene()
{
  return this->activeSceneStack.back();
} // AbstractScene* GameManager::getCurrentScene()