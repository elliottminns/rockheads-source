#include "../../../PrecompiledHeaders/stdafx.h"
#ifndef GAME_MANAGER_H
#define GAME_MANAGER_H

#include "../../GameScenes/AbstractScene.h"
#include <time.h>
#include "../../GameEntities/DynamicEntities/Player/Player.h"

class GameManager : public SceneListener
{
public:

	typedef struct
	{
		Ogre::String    name;
		AbstractScene*  scene;
	} Scene_Info;

  static GameManager* getInstance();

	void manageScene(Ogre::String _sceneName, AbstractScene* _scene);

	AbstractScene* findByName(Ogre::String _stateName);

	void start(AbstractScene* _scene);
	void changeScene(AbstractScene* _scene);
	bool pushScene(AbstractScene* _scene);
	void popScene();
	void pauseScene();
	void beginShutdown();
  void popAllAndPushScene(AbstractScene* _scene);
  AbstractScene* getCurrentScene();
  int startTime;

protected:
	void init(AbstractScene* _state);

	std::vector<AbstractScene*>		activeSceneStack;
	std::vector<Scene_Info>		    scenes;

	bool shutdown;

private:
  // Singleton functions.
  GameManager();
	~GameManager();
  static GameManager* instance;

}; // end class GameManager : public SceneListener

#endif
