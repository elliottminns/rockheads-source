#include "stdafx.h"

#include "CollisionManager.h"
#include <Ogre.h>
#include "../../GameEntities/AbstractEntity/AbstractEntity.h"
#include "QueryFlags.h"

template<> CollisionManager* Ogre::Singleton<CollisionManager>::ms_Singleton = 0;


CollisionManager::CollisionManager()
{
	this->rsq = NULL;
  this->TSMRsq = NULL;
} // CollisionManager::CollisionManager()
 

CollisionManager::~CollisionManager()
{
  if(this->rsq != NULL)
  {
		delete this->rsq;
  } // end if

  if(this->TSMRsq!= NULL)
  {
    delete this->TSMRsq;
  } // end if
} // CollisionManager::~CollisionManager()


void CollisionManager::initialise(Ogre::SceneManager* _sceneManager)
{
  this->sceneManager = _sceneManager;

	this->rsq = this->sceneManager->createRayQuery(Ogre::Ray());
  if(!this->rsq)
  {
    // LOG_ERROR << "Failed to create Ogre::RaySceneQuery instance" << ENDLOG;
    return;
  }
  this->rsq->setSortByDistance(true);

  this->TSMRsq =  this->sceneManager->createRayQuery(Ogre::Ray());

  this->heightAdjust = 0.0f;
} // void initialise(Ogre::SceneManager* _sceneManager)


bool CollisionManager::raycastFromCamera(Ogre::RenderWindow* _rw, Ogre::Camera* _camera, const Ogre::Vector2& _mousecoords, Ogre::Vector3& _result, Ogre::Entity*& _target,float& _closestDistance, const Ogre::uint32 _queryMask)
{
	return raycastFromCamera(_rw, _camera, _mousecoords, _result, (Ogre::MovableObject*&) _target, _closestDistance, _queryMask);
} // bool CollisionManager::raycastFromCamera(Ogre::RenderWindow* rw, Ogre::Camera* camera, const Ogre::Vector2 &mousecoords, Ogre::Vector3 &result, Ogre::Entity* &target,float &closest_distance, const Ogre::uint32 queryMask)


bool CollisionManager::raycastFromCamera(Ogre::RenderWindow* _rw, Ogre::Camera* _camera, const Ogre::Vector2 &_mousecoords, Ogre::Vector3 &_result, Ogre::MovableObject*& _target,float& _closestDistance, const Ogre::uint32 _queryMask)
{
	// Create the ray to test
	Ogre::Real tx = _mousecoords.x / (Ogre::Real) _rw->getWidth();
	Ogre::Real ty = _mousecoords.y / (Ogre::Real) _rw->getHeight();
	Ogre::Ray ray = _camera->getCameraToViewportRay(tx, ty);

	return raycast(ray, _result, _target, _closestDistance, _queryMask);
} // bool CollisionManager::raycastFromCamera(Ogre::RenderWindow* _rw, Ogre::Camera* _camera, const Ogre::Vector2 &_mousecoords, Ogre::Vector3 &_result, Ogre::MovableObject*& _target,float& _closestDistance, const Ogre::uint32 _queryMask)


bool CollisionManager::collidesWithEntity(const Ogre::Vector3& _fromPoint, const Ogre::Vector3& _toPoint, const float _collisionRadius, const float _rayHeightLevel, const Ogre::uint32 _queryMask)
{
	Ogre::Vector3 fromPointAdj(_fromPoint.x, _fromPoint.y + _rayHeightLevel, _fromPoint.z);
	Ogre::Vector3 toPointAdj(_toPoint.x, _toPoint.y + _rayHeightLevel, _toPoint.z);
	Ogre::Vector3 normal = toPointAdj - fromPointAdj;
	float distToDest = normal.normalise();

	Ogre::Vector3 myResult(0, 0, 0);
	Ogre::MovableObject* myObject = NULL;
	float distToColl = 0.0f;

	if(this->raycastFromPoint(fromPointAdj, normal, myResult, myObject, distToColl, _queryMask))
	{
		distToColl -= _collisionRadius;
		return (distToColl <= distToDest);
	} // end if
	else
	{
		return false;
	} // end else
} // bool CollisionManager::collidesWithEntity(const Ogre::Vector3& _fromPoint, const Ogre::Vector3& _toPoint, const float _collisionRadius, const float _rayHeightLevel, const Ogre::uint32 _queryMask)


float CollisionManager::getTSMHeightAt(const float _x, const float _z) 
{
	float y=0.0f;

  static Ogre::Ray updateRay;

  updateRay.setOrigin(Ogre::Vector3(_x,9999,_z));
  updateRay.setDirection(Ogre::Vector3::NEGATIVE_UNIT_Y);

  TSMRsq->setRay(updateRay);
  Ogre::RaySceneQueryResult& qryResult = TSMRsq->execute();

  Ogre::RaySceneQueryResult::iterator i = qryResult.begin();
  if (i != qryResult.end() && i->worldFragment)
  {
    y=i->worldFragment->singleIntersection.y;
  } // end if
	return y;
} // float CollisionManager::getTSMHeightAt(const float _x, const float _z) 

void CollisionManager::calculateY(Ogre::SceneNode *_n, const bool _doTerrainCheck, const bool _doGridCheck, const float _gridWidth, const Ogre::uint32 _queryMask)
{
	Ogre::Vector3 pos = _n->getPosition();

	float x = pos.x;
	float z = pos.z;
	float y = pos.y;

	Ogre::Vector3 myResult(0,0,0);
	Ogre::MovableObject *myObject=NULL;
	float distToColl = 0.0f;

	float terrY = 0, colY = 0, colY2 = 0;

	if(raycastFromPoint(Ogre::Vector3(x,y,z),Ogre::Vector3::NEGATIVE_UNIT_Y,myResult,myObject, distToColl, _queryMask))
  {
		if(myObject != NULL) 
    {
			colY = myResult.y;
		} // end if
    else 
    {
			colY = -99999;
		} // end else
	} // end if

	//if doGridCheck is on, repeat not to fall through small holes for example when crossing a hangbridge
	if(_doGridCheck) 
  {
		if(raycastFromPoint(Ogre::Vector3(x,y,z)+(_n->getOrientation()*Ogre::Vector3(0,0,_gridWidth)),Ogre::Vector3::NEGATIVE_UNIT_Y,myResult, myObject, distToColl, _queryMask))
    {
			if (myObject != NULL) 
      {
				colY = myResult.y;
			} // end if
      else 
      {
				colY = -99999;
			} // end else
		} // end if
		if (colY<colY2) 
    {
      colY = colY2;
    } // end if
	} // end if(_doGridCheck)

	// set the parameter to false if you are not using ETM or TSM
	if(_doTerrainCheck) 
  {
		// TSM height value
		terrY = getTSMHeightAt(x,z);

		if(terrY < colY ) 
    {
			_n->setPosition(x,colY+this->heightAdjust,z);
		} // end if
    else 
    {
			_n->setPosition(x,terrY+this->heightAdjust,z);
		} // end else
	} // end if(_doTerrainCheck) 
  else 
  {
		if(!_doTerrainCheck && colY == -99999)
    {
      colY = y;
    } // end if

	  _n->setPosition(x,colY+this->heightAdjust,z);

	} // end else
} // void CollisionManager::calculateY(Ogre::SceneNode *_n, const bool _doTerrainCheck, const bool _doGridCheck, const float _gridWidth, const Ogre::uint32 _queryMask)


double CollisionManager::calculateYDouble(Ogre::SceneNode *_n, const bool _doTerrainCheck, const bool _doGridCheck, const float _gridWidth, const Ogre::uint32 _queryMask)
{
  Ogre::Vector3 pos = _n->getPosition();

	float x = pos.x;
	float z = pos.z;
	float y = pos.y;

	Ogre::Vector3 myResult(0,0,0);
	Ogre::MovableObject *myObject=NULL;
	float distToColl = 0.0f;

	float terrY = 0, colY = 0, colY2 = 0;

	if(raycastFromPoint(Ogre::Vector3(x,y,z),Ogre::Vector3::NEGATIVE_UNIT_Y,myResult,myObject, distToColl, _queryMask))
  {
		if(myObject != NULL) 
    {
			colY = myResult.y;
		} // end if
    else 
    {
			colY = -99999;
		} // end else
	} // end if

	//if doGridCheck is on, repeat not to fall through small holes for example when crossing a hangbridge
	if(_doGridCheck) 
  {
		if(raycastFromPoint(Ogre::Vector3(x,y,z)+(_n->getOrientation()*Ogre::Vector3(0,0,_gridWidth)),Ogre::Vector3::NEGATIVE_UNIT_Y,myResult, myObject, distToColl, _queryMask))
    {
			if (myObject != NULL) 
      {
				colY = myResult.y;
			} // end if
      else 
      {
				colY = -99999;
			} // end else
		} // end if
		if (colY<colY2) 
    {
      colY = colY2;
    } // end if
	} // end if(_doGridCheck)

	// set the parameter to false if you are not using ETM or TSM
	if(_doTerrainCheck) 
  {
		// TSM height value
		terrY = getTSMHeightAt(x,z);

		if(terrY < colY ) 
    {
      return (double)colY+this->heightAdjust;
		} // end if
    else 
    {
      return (double)terrY+this->heightAdjust;
		} // end else
	} // end if(_doTerrainCheck) 
  else 
  {
		if(!_doTerrainCheck && colY == -99999)
    {
      colY = y;
    } // end if

    return (double)colY+this->heightAdjust;

	} // end else
} // double CollisionManager::calculateY(Ogre::SceneNode *_n, const bool _doTerrainCheck, const bool _doGridCheck, const float _gridWidth, const Ogre::uint32 _queryMask)


bool CollisionManager::raycastFromPoint(const Ogre::Vector3& _point, const Ogre::Vector3& _normal, Ogre::Vector3& _result,Ogre::Entity*& _target,
float& _closestDistance, const Ogre::uint32 _queryMask) 
{ // raycast from a point in to the scene.
  // returns success or failure.
  // on success the point is returned in the result.
	return raycastFromPoint(_point, _normal, _result,(Ogre::MovableObject*&) _target, _closestDistance, _queryMask);
} // bool CollisionManager::raycastFromPoint(const Ogre::Vector3& _point, const Ogre::Vector3& _normal, Ogre::Vector3& _result,Ogre::Entity*& _target,
  // float& _closest_distance, const Ogre::uint32 _queryMask) 
							

bool CollisionManager::raycastFromPoint(const Ogre::Vector3& _point, const Ogre::Vector3& _normal, Ogre::Vector3 &_result,
Ogre::MovableObject* &_target, float &_closestDistance, const Ogre::uint32 _queryMask)
{
  // create the ray to test
  static Ogre::Ray ray;
	ray.setOrigin(_point);
	ray.setDirection(_normal);

	return raycast(ray, _result, _target, _closestDistance, _queryMask);
} // bool CollisionManager::raycastFromPoint(const Ogre::Vector3& _point, const Ogre::Vector3& _normal, Ogre::Vector3 &_result,
  // Ogre::MovableObject* &_target, float &_closestDistance, const Ogre::uint32 _queryMask)


bool CollisionManager::raycast(const Ogre::Ray &_ray, Ogre::Vector3 &_result, Ogre::Entity* &_target,float &_closestDistance, const Ogre::uint32 _queryMask) 
{
	return raycast(_ray, _result, (Ogre::MovableObject*&)_target, _closestDistance, _queryMask);
} // bool CollisionManager::raycast(const Ogre::Ray &_ray, Ogre::Vector3 &_result, Ogre::Entity* &_target,float &_closestDistance, const Ogre::uint32 _queryMask) 


bool CollisionManager::raycast(const Ogre::Ray &_ray, Ogre::Vector3 &_result,Ogre::MovableObject* &_target,float &_closestDistance, const Ogre::uint32 _queryMask)
{
	_target = NULL;

  // check we are initialised
  if(this->rsq != NULL)
  {
    // create a query object
    this->rsq->setRay(_ray);
	  this->rsq->setSortByDistance(true);
	  this->rsq->setQueryMask(_queryMask);
    // execute the query, returns a vector of hits
    if(this->rsq->execute().size() <= 0)
    {
      // raycast did not hit an objects bounding box
      return (false);
    } // end if
  } // end if(this->rsq != NULL)
  else
  {
    return (false);
  } // end else

  // at this point we have raycast to a series of different objects bounding boxes.
  // we need to test these different objects to see which is the first polygon hit.
  // there are some minor optimizations (distance based) that mean we wont have to
  // check all of the objects most of the time, but the worst case scenario is that
  // we need to test every triangle of every object.
  //Ogre::Ogre::Real closest_distance = -1.0f;
	_closestDistance = -1.0f;
  Ogre::Vector3 closestResult;
  Ogre::RaySceneQueryResult& queryResult = this->rsq->getLastResults();
  for(size_t qr_idx = 0; qr_idx < queryResult.size(); qr_idx++)
  {
    // stop checking if we have found a raycast hit that is closer
    // than all remaining entities
    if((_closestDistance >= 0.0f) && (_closestDistance < queryResult[qr_idx].distance))
    {
       break;
    } // end if

    // only check this result if its a hit against an entity
    if ((queryResult[qr_idx].movable != NULL)  && (queryResult[qr_idx].movable->getMovableType().compare("Entity") == 0))
    {
      // get the entity to check
			Ogre::MovableObject *pentity = static_cast<Ogre::MovableObject*>(queryResult[qr_idx].movable);

      // mesh data to retrieve
      size_t vertex_count;
      size_t index_count;
      Ogre::Vector3 *vertices;
      Ogre::uint32 *indices;

      // get the mesh information
			GetMeshInformation(((Ogre::Entity*)pentity)->getMesh(), vertex_count, vertices, index_count, indices,
                              pentity->getParentNode()->_getDerivedPosition(),
                              pentity->getParentNode()->_getDerivedOrientation(),
                              pentity->getParentNode()->_getDerivedScale());

      // test for hitting individual triangles on the mesh
      bool new_closest_found = false;
      for (size_t i = 0; i < index_count; i += 3)
      {  
        // check for a hit against this triangle
        std::pair<bool, Ogre::Real> hit = Ogre::Math::intersects(_ray, vertices[indices[i]],
            vertices[indices[i+1]], vertices[indices[i+2]], true, false);

        // if it was a hit check if its the closest
        if(hit.first)
        {
          if ((_closestDistance < 0.0f) || (hit.second < _closestDistance))
          {
            // this is the closest so far, save it off
            _closestDistance = hit.second;
            new_closest_found = true;
          } // end if
        } // end if
      } // end for

			// free the verticies and indicies memory
      delete[] vertices;
      delete[] indices;

      // if we found a new closest raycast for this object, update the
      // closest_result before moving on to the next object.
      if (new_closest_found)
      {
				_target = pentity;
        closestResult = _ray.getPoint(_closestDistance);
      } // end if
    } // end if
  } // end for

  // return the result
  if(_closestDistance >= 0.0f)
  {
    // raycast success
	  _result = closestResult;
    return (true);
  } // end if
  else
  {
    // raycast failed
    return (false);
  } // end else
} // bool CollisionManager::raycast(const Ogre::Ray &_ray, Ogre::Vector3 &_result,Ogre::MovableObject* &_target,float &_closestDistance, const Ogre::uint32 _queryMask)


// Get the mesh information for the given mesh.
// Code found on this forum link: http://www.ogre3d.org/wiki/index.php/RetrieveVertexData
void CollisionManager::GetMeshInformation(const Ogre::MeshPtr mesh,
                                size_t &vertex_count,
                                Ogre::Vector3* &vertices,
                                size_t &index_count,
                                Ogre::uint32* &indices,
                                const Ogre::Vector3 &position,
                                const Ogre::Quaternion &orient,
                                const Ogre::Vector3 &scale)
{
    bool added_shared = false;
    size_t current_offset = 0;
    size_t shared_offset = 0;
    size_t next_offset = 0;
    size_t index_offset = 0;

    vertex_count = index_count = 0;

    // Calculate how many vertices and indices we're going to need
    for (unsigned short i = 0; i < mesh->getNumSubMeshes(); ++i)
    {
        Ogre::SubMesh* submesh = mesh->getSubMesh( i );

        // We only need to add the shared vertices once
        if(submesh->useSharedVertices)
        {
            if( !added_shared )
            {
                vertex_count += mesh->sharedVertexData->vertexCount;
                added_shared = true;
            }
        }
        else
        {
            vertex_count += submesh->vertexData->vertexCount;
        }

        // Add the indices
        index_count += submesh->indexData->indexCount;
    }


    // Allocate space for the vertices and indices
    vertices = new Ogre::Vector3[vertex_count];
    indices = new Ogre::uint32[index_count];

    added_shared = false;

    // Run through the submeshes again, adding the data into the arrays
    for ( unsigned short i = 0; i < mesh->getNumSubMeshes(); ++i)
    {
        Ogre::SubMesh* submesh = mesh->getSubMesh(i);

        Ogre::VertexData* vertex_data = submesh->useSharedVertices ? mesh->sharedVertexData : submesh->vertexData;

        if((!submesh->useSharedVertices)||(submesh->useSharedVertices && !added_shared))
        {
            if(submesh->useSharedVertices)
            {
                added_shared = true;
                shared_offset = current_offset;
            }

            const Ogre::VertexElement* posElem =
                vertex_data->vertexDeclaration->findElementBySemantic(Ogre::VES_POSITION);

            Ogre::HardwareVertexBufferSharedPtr vbuf =
                vertex_data->vertexBufferBinding->getBuffer(posElem->getSource());

            unsigned char* vertex =
                static_cast<unsigned char*>(vbuf->lock(Ogre::HardwareBuffer::HBL_READ_ONLY));

            // There is _no_ baseVertexPointerToElement() which takes an Ogre::Ogre::Real or a double
            //  as second argument. So make it float, to avoid trouble when Ogre::Ogre::Real will
            //  be comiled/typedefed as double:
            //      Ogre::Ogre::Real* pOgre::Real;
            float* pReal;

            for( size_t j = 0; j < vertex_data->vertexCount; ++j, vertex += vbuf->getVertexSize())
            {
                posElem->baseVertexPointerToElement(vertex, &pReal);

                Ogre::Vector3 pt(pReal[0], pReal[1], pReal[2]);

                vertices[current_offset + j] = (orient * (pt * scale)) + position;
            }

            vbuf->unlock();
            next_offset += vertex_data->vertexCount;
        }


        Ogre::IndexData* index_data = submesh->indexData;
        size_t numTris = index_data->indexCount / 3;
        Ogre::HardwareIndexBufferSharedPtr ibuf = index_data->indexBuffer;

        bool use32bitindexes = (ibuf->getType() == Ogre::HardwareIndexBuffer::IT_32BIT);

        Ogre::uint32*  pLong = static_cast<Ogre::uint32*>(ibuf->lock(Ogre::HardwareBuffer::HBL_READ_ONLY));
        unsigned short* pShort = reinterpret_cast<unsigned short*>(pLong);


        size_t offset = (submesh->useSharedVertices)? shared_offset : current_offset;

        if ( use32bitindexes )
        {
            for ( size_t k = 0; k < numTris*3; ++k)
            {
                indices[index_offset++] = pLong[k] + static_cast<Ogre::uint32>(offset);
            }
        }
        else
        {
            for ( size_t k = 0; k < numTris*3; ++k)
            {
                indices[index_offset++] = static_cast<Ogre::uint32>(pShort[k]) +
                    static_cast<Ogre::uint32>(offset);
            }
        }

        ibuf->unlock();
        current_offset = next_offset;
    }
}


void CollisionManager::setHeightAdjust(const float _heightadjust) 
{
  this->heightAdjust = _heightadjust;
} // void CollisionManager::setHeightAdjust(const float _heightadjust) 


float CollisionManager::getHeightAdjust(void) 
{
	return this->heightAdjust;
} // float CollisionManager::getHeightAdjust(void)


bool CollisionManager::isPointInLineOfSight(const Ogre::Vector3& _fromPoint, const Ogre::Vector3& _toPoint, const Ogre::uint32 _queryMask)
{
  Ogre::Vector3 result;
  Ogre::Vector3 normal = (_toPoint - _fromPoint) / 1000;
  float collDiss = 0.0f;
  Ogre::Entity* resultObject = NULL;
  
  return !this->raycastFromPoint(_fromPoint, normal, result, (Ogre::MovableObject*&) resultObject, collDiss, _queryMask);
} // bool CollisionManager::isPointInLineOfSight(const Ogre::Vector3& _fromPoint, const Ogre::Vector3& _toPoint)


bool CollisionManager::isEntityInLineOfSight(const Ogre::Vector3& _fromPoint, const AbstractEntity* _target, const Ogre::uint32 _queryMask)
{
  // Check line of sight.
  Ogre::Vector3 result;
  Ogre::Entity* resultObject = NULL;
  Ogre::Vector3 targetsPos = _target->getPosition();
  Ogre::Vector3 normal = (targetsPos - _fromPoint) / 1000;
  float collDiss = 0.0f;

  Ogre::uint32 queryMask = _queryMask | STATIC_WALL;

  this->raycastFromPoint(_fromPoint, normal, result, (Ogre::MovableObject*&) resultObject, collDiss, queryMask);

  bool resultBool =  (resultObject == _target->getEntity());

  if(resultBool == false)
  {
    Ogre::Entity* result = resultObject;
    Ogre::Entity* should = _target->getEntity();
    int x = 0;
  } // end if

  return resultBool;

} // bool CollisionManager::isEntityInLineOfSight(const Ogre::Vector3& fromPoint, const Ogre::Entity* &target)