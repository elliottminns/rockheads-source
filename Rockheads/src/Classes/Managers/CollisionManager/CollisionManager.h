#include "../../../PrecompiledHeaders/stdafx.h"

#ifndef COLLISION_MANAGER_H
#define COLLISION_MANAGER_H

#include "QueryFlags.h"
#include <Ogre.h>

class AbstractEntity;

class CollisionManager : public Ogre::Singleton<CollisionManager>
{
public:

	Ogre::RaySceneQuery *rsq;
	Ogre::RaySceneQuery *TSMRsq;

	Ogre::SceneManager *sceneManager;

	CollisionManager();
	~CollisionManager();

  void initialise(Ogre::SceneManager* _sceneManager);

	bool raycastFromCamera(Ogre::RenderWindow* _rw, Ogre::Camera* _camera, const Ogre::Vector2& _mousecoords, Ogre::Vector3& _result, Ogre::MovableObject*& _target,float& _closest_distance, const Ogre::uint32 _queryMask = 0xFFFFFFFF);
	// convenience wrapper with Ogre::Entity to it:
	bool raycastFromCamera(Ogre::RenderWindow* rw, Ogre::Camera* camera, const Ogre::Vector2 &mousecoords, Ogre::Vector3 &result, Ogre::Entity* &target,float &closest_distance, const Ogre::uint32 queryMask = 0xFFFFFFFF);

	bool collidesWithEntity(const Ogre::Vector3& fromPoint, const Ogre::Vector3& toPoint, const float collisionRadius = 2.5f, const float rayHeightLevel = 0.0f, const Ogre::uint32 queryMask = 0xFFFFFFFF);

	void calculateY(Ogre::SceneNode *n, const bool doTerrainCheck = true, const bool doGridCheck = true, const float gridWidth = 1.0f, const Ogre::uint32 queryMask = 0xFFFFFFFF);
  double calculateYDouble(Ogre::SceneNode *n, const bool doTerrainCheck = true, const bool doGridCheck = true, const float gridWidth = 1.0f, const Ogre::uint32 queryMask = 0xFFFFFFFF);
	float getTSMHeightAt(const float x, const float z);
	
	bool raycastFromPoint(const Ogre::Vector3 &point, const Ogre::Vector3 &normal, Ogre::Vector3 &result,Ogre::MovableObject* &target,float &closest_distance, const Ogre::uint32 queryMask = 0xFFFFFFFF);
	// convenience wrapper with Ogre::Entity to it:
	bool raycastFromPoint(const Ogre::Vector3 &point, const Ogre::Vector3 &normal, Ogre::Vector3 &result,Ogre::Entity* &target,float &closest_distance, const Ogre::uint32 queryMask = 0xFFFFFFFF);
	
	bool raycast(const Ogre::Ray &ray, Ogre::Vector3 &result, Ogre::MovableObject* &target,float &closest_distance, const Ogre::uint32 queryMask = 0xFFFFFFFF);
	// convenience wrapper with Ogre::Entity to it:
	bool raycast(const Ogre::Ray &ray, Ogre::Vector3 &result, Ogre::Entity* &target,float &closest_distance, const Ogre::uint32 queryMask = 0xFFFFFFFF);

  bool isPointInLineOfSight(const Ogre::Vector3& _fromPoint, const Ogre::Vector3& _toPoint, const Ogre::uint32 _queryMask = STATIC_WALL);
  bool isEntityInLineOfSight(const Ogre::Vector3& _fromPoint, const AbstractEntity* _target, const Ogre::uint32 _queryMask);

	void setHeightAdjust(const float _heightadjust);
	float getHeightAdjust();

private:

	float heightAdjust;

	void GetMeshInformation(const Ogre::MeshPtr mesh,
                                size_t &vertex_count,
                                Ogre::Vector3* &vertices,
                                size_t &index_count,
                                Ogre::uint32* &indices,
                                const Ogre::Vector3 &position,
                                const Ogre::Quaternion &orient,
                                const Ogre::Vector3 &scale);

};

#endif