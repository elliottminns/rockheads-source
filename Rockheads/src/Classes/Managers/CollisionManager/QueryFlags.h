#include "../../../PrecompiledHeaders/stdafx.h"

#ifndef QUERY_FLAGS_H
#define QUERY_FLAGS_H

enum QueryFlags
{
  STATIC_FLOOR  = 1<<0,
  STATIC_WALL   = 1<<1,
  MOB           = 1<<2,
  PLAYER        = 1<<3,
  NPC_FLAG      = 1<<4
};

#endif