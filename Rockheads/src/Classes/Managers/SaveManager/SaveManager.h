#include "../../../PrecompiledHeaders/stdafx.h"
#ifndef SAVE_MANAGER_H
#define SAVE_MANAGER_H

#include "../../GameScenes/GameScene/GameScene.h"
#include "../../Saving/Save/Save.h"
#include "../../../Headers/rapidxml.h"

class LoadGameScene;

class SaveManager
{
private:
  static SaveManager* instance;
  static bool initialised;

  SaveManager();
  ~SaveManager();

  vector<Save*>* saves;
  vector<string> saveFileNames;
  string saveDataLocation;
  int numSaves;
  ofstream* writeFile;
  ifstream* readFile;

public:
  static SaveManager& getSingleton();
  static SaveManager* getSingletonPtr();

  void initialise();
  vector<string> getFileNames(string _dataLocation);
  void release();

  int getNumSaves();
  vector<Save*>* getSaves();
  Save* getSaveAt(int _index);

  void saveGame(string _saveName);
  void loadGame(string _saveName, LoadGameScene* _scene);

  bool openWriteFile(string _fileName, rapidxml::xml_document<> *_doc);
  bool openReadFile(string _fileName, rapidxml::xml_document<> *_doc);

  string writeHeader(string _saveName);
  string writePlayer();
  string writeScene();
  string writeHotBar();

  void readPlayer(rapidxml::xml_document<> *_doc);
  void loadPlayerEnvironment(rapidxml::xml_document<> *_doc);
  void readMobs(rapidxml::xml_document<> *_doc);
  void readHotBar(rapidxml::xml_document<> *_doc);

  Ogre::String getAttrib(rapidxml::xml_node<>* XMLNode, const Ogre::String &parameter, const Ogre::String &defaultValue = "");
  Ogre::Real getAttribReal(rapidxml::xml_node<>* XMLNode, const Ogre::String &parameter, Ogre::Real defaultValue = 0);
  bool getAttribBool(rapidxml::xml_node<>* XMLNode, const Ogre::String &parameter, bool defaultValue = false);
  Ogre::Vector3 parseVector3(rapidxml::xml_node<>* XMLNode);
}; // end class SaveManager

#endif