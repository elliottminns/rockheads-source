#include "stdafx.h"
#include "SaveManager.h"
#include "../../../Headers/rapidxml_print.h"
#include "../../GameObjects/Inventory/Inventory.h"
#include "../../GameObjects/InventorySlot/InventorySlot.h"
#include "../../GameObjects/Items/AbstractItem/AbstractItem.h"
#include "../../GameEntities/DynamicEntities/EntityClass/SkillSet/SkillSet.h"
#include "../../GameEntities/DynamicEntities/EntityClass/Skill/Skill.h"
#include "../../GameScenes/LoadGameScene/LoadGameScene.h"
#include "../../Managers/GUIManager/GUIManager.h"
#include "../../Managers/MobManager/MobManager.h"
#include "../../Questing/Quest/Quest.h"
#include "../../Questing/Objectives/CollectObjective/CollectObjective.h"
#include "../../Questing/Objectives/KillObjective/KillObjective.h"
#include "../../Questing/Objectives/TimedObjective/TimedObjective.h"
#include "../../Managers/QuestManager/QuestManager.h"
#include "../../HotBar/AbstractHotSlot/AbstractHotSlot.h"
#include "../../HotBar/ItemHotSlot/ItemHotSlot.h"
#include "../../HotBar/SkillHotSlot/SkillHotSlot.h"
#include "../../Managers/SkillManager/SkillManager.h"

SaveManager* SaveManager::instance = NULL;
bool SaveManager::initialised = false;


SaveManager& SaveManager::getSingleton()
{
  if(!instance)
  {
    instance = new SaveManager();
  } // end if

  if(!initialised)
  {
    initialised = true;
    instance->initialise();
  } // end if

  return *instance;
} // SaveManager::SaveManager& getSingleton()


SaveManager* SaveManager::getSingletonPtr()
{
  if(!instance)
  {
    instance = new SaveManager();
  } // end if

  if(!initialised)
  {
    initialised = true;
    instance->initialise();
  } // end if

  return instance;
} // SaveManager* SaveManager::getSingletonPtr()


SaveManager::SaveManager()
{
  this->saves = NULL;
  this->readFile = NULL;
  this->writeFile = NULL;
} // SaveManager::SaveManager()


SaveManager::~SaveManager()
{

} // SaveManager::~SaveManager()


void SaveManager::initialise()
{
  this->saveDataLocation = "../../saves/";
  int i=0;

  // Populate a list of filenames.
  this->saveFileNames = this->getFileNames(this->saveDataLocation);
  this->numSaves = this->saveFileNames.size();
  this->saves = new vector<Save*>;

  vector<string> saveNames;

  for(vector<string>::iterator it = this->saveFileNames.begin();
    it!=this->saveFileNames.end(); ++it)
  {
    string savename;

    rapidxml::xml_document<> *doc = new rapidxml::xml_document<>;
    string saveFileLocation = this->saveDataLocation + *it;

    if(!this->openReadFile(saveFileLocation, doc))
    {
      exit(401);
    } // end if

    vector<char> buffer((istreambuf_iterator<char>(*this->readFile)), istreambuf_iterator<char>());
    buffer.push_back('\0');
    this->readFile->close();
    delete this->readFile;
    this->readFile = NULL;

    doc->parse<0>(&buffer[0]);

    // Grab the header node
    rapidxml::xml_node<>* XMLRoot;
    XMLRoot = doc->first_node("save");
  
    string name = this->getAttrib(XMLRoot, "name");

    savename = name;

    saveNames.push_back(name);

  } // end for

  for(int i=0;i<this->numSaves;i++)
  {
    Save* save = new Save;
    save->setFileName(this->saveFileNames.at(i));
    save->setSaveName(saveNames.at(i));
    this->saves->push_back(save);
  } // end ford

} // void SaveManager::initialise()


void SaveManager::release()
{
  if(this->saves)
  {
    delete this->saves;
  } // end if
  this->saves = NULL;
  this->saveFileNames.clear();
  
  if(this->readFile)
  {
    delete this->readFile;
    this->readFile = NULL;
  } // end if
  if(this->writeFile)
  {
    delete this->writeFile;
    this->writeFile = NULL;
  } // end if
  
} // void SaveManager::release()


vector<string> SaveManager::getFileNames(string _dataLocation)
{
  vector<string> fileNames;

  UINT counter(0);
  bool working(true);
  string buffer;
  string filesLocation = _dataLocation + "*.save";
  WIN32_FIND_DATA saveFile;
  HANDLE myHandle=FindFirstFile(filesLocation.c_str(),&saveFile);

  if(myHandle!=INVALID_HANDLE_VALUE)
  {
    buffer = saveFile.cFileName;
    fileNames.push_back(buffer);

    while(working)
    {
      FindNextFile(myHandle,&saveFile);
      if(saveFile.cFileName!=buffer)
      {
        buffer = saveFile.cFileName;
        ++counter;
        fileNames.push_back(buffer);
      }
      else
      {
        //end of files reached
        working=false;
      } // end else
    } // end while
  } // end if

  return fileNames;
} // vector<string> SaveManager::getFileNames(string _dataLocation)


int SaveManager::getNumSaves()
{
  return this->numSaves;
} // int SaveManager::getNumSaves()


vector<Save*>* SaveManager::getSaves()
{
  return this->saves;
} // vector<Save*>* SaveManager::getSaves()


Save* SaveManager::getSaveAt(int _index)
{
  vector<Save*>::iterator it = this->saves->begin();
  it += _index;
  Save* save = *it;
  return save;
} // Save* SaveManager::getSaveAt(int _index)


void SaveManager::saveGame(string _saveName)
{
  string fileName = this->saveDataLocation + _saveName += ".save";

  // First create the file.
  rapidxml::xml_document<> *doc = new rapidxml::xml_document<>;

  if(!this->openWriteFile(fileName, doc))
  {
    exit(401);
  } // end if

  // Write the header.
  string header = this->writeHeader(_saveName);

  // Write the player data.
  string player = this->writePlayer();

  // Now write the player data.
  string scene = this->writeScene();

  // Obtain the hotbar data.
  string hotbar = this->writeHotBar();

  // Write the data to the file.
  
  this->writeFile->write(header.c_str(),header.length());
  this->writeFile->write(player.c_str(),player.length());
  this->writeFile->write(scene.c_str(),scene.length());
  this->writeFile->write(hotbar.c_str(),hotbar.length());
  this->writeFile->close();
  delete this->writeFile;
  this->writeFile = NULL;
} // void SaveManager::saveGame()


void SaveManager::loadGame(string _saveName, LoadGameScene* _scene)
{
  string fileName = this->saveDataLocation + _saveName += ".save";

  // First create the file.
  rapidxml::xml_document<> *doc = new rapidxml::xml_document<>;

  if(!this->openReadFile(fileName, doc))
  {
    exit(401);
  } // end if

  vector<char> buffer((istreambuf_iterator<char>(*this->readFile)), istreambuf_iterator<char>());
  buffer.push_back('\0');

  doc->parse<0>(&buffer[0]);

  this->readPlayer(doc);
  _scene->loadSaveScene("GameScene");
  GameManager::getInstance()->getCurrentScene()->pause();

  this->loadPlayerEnvironment(doc);
  this->readMobs(doc);
  this->readHotBar(doc);
  GameManager::getInstance()->getCurrentScene()->resume();

} // void SaveManager::loadGame()


bool SaveManager::openWriteFile(string _fileName, rapidxml::xml_document<> *_doc)
{
  this->writeFile = new ofstream(_fileName);

  return this->writeFile->is_open();
} // bool XMLParser::openFile(string _fileName)


bool SaveManager::openReadFile(string _fileName, rapidxml::xml_document<> *_doc)
{
  this->readFile = new ifstream(_fileName);

  return this->readFile->is_open();
} // bool XMLParser::openFile(string _fileName)


string SaveManager::writeHeader(string _saveName)
{
  rapidxml::xml_document<> doc;

  rapidxml::xml_node<> *node = doc.allocate_node(rapidxml::node_element, "save");
  doc.append_node(node);

  rapidxml::xml_attribute<> *attr = doc.allocate_attribute("name", _saveName.c_str());
  node->append_attribute(attr);

  std::string s;
  rapidxml::print(std::back_inserter(s), doc, 0);
  return s;
} // string SaveManager::writeHeader(string _saveName)


string SaveManager::writePlayer()
{
  Player* player = Player::getInstance();
  
  // Attributes.
  rapidxml::xml_document<> doc;
  rapidxml::xml_node<> *node = doc.allocate_node(rapidxml::node_element, "player");
  doc.append_node(node);

  // Name.
  string value = player->getName();
  rapidxml::xml_attribute<> *attr = doc.allocate_attribute("name", 
    value.c_str());
  node->append_attribute(attr);
  
  // Level data
  int valueInt = player->getLevel();
  string level = Ogre::StringConverter::toString(valueInt);
  node->append_attribute(doc.allocate_attribute("level", level.c_str()));

  // Strength.
  valueInt = player->getStrength();
  string strength = Ogre::StringConverter::toString(valueInt);
  node->append_attribute(doc.allocate_attribute("strength", strength.c_str()));

  // Agility.
  valueInt = player->getAgility();
  string agility = Ogre::StringConverter::toString(valueInt);
  node->append_attribute(doc.allocate_attribute("agility", agility.c_str()));
  
  // Dexterity.
  valueInt = player->getDexterity();
  string dexterity = Ogre::StringConverter::toString(valueInt);
  node->append_attribute(doc.allocate_attribute("dexterity", dexterity.c_str()));

  // Constitution.
  valueInt = player->getConstitution();
  string constitution = Ogre::StringConverter::toString(valueInt);
  node->append_attribute(doc.allocate_attribute("constitution", constitution.c_str()));
  
  // Intelligence.
  valueInt = player->getIntelligence();
  string intelligence = Ogre::StringConverter::toString(valueInt);
  node->append_attribute(doc.allocate_attribute("intelligence", intelligence.c_str()));
  
  // Wisdom.
  valueInt = player->getWisdom();
  string wisdom = Ogre::StringConverter::toString(valueInt);
  node->append_attribute(doc.allocate_attribute("wisdom", wisdom.c_str()));

  // Charisma.
  valueInt = player->getCharisma();
  string charisma = Ogre::StringConverter::toString(valueInt);
  node->append_attribute(doc.allocate_attribute("charisma", charisma.c_str()));

  // Model name.
  string modelName = player->getModelName();
  node->append_attribute(doc.allocate_attribute("modelName", modelName.c_str()));

  // Gender.
  string gender = player->getGender();
  node->append_attribute(doc.allocate_attribute("gender", gender.c_str()));

  // Experience.
  valueInt = player->getExperience();
  string experience = Ogre::StringConverter::toString(valueInt);
  node->append_attribute(doc.allocate_attribute("experience", experience.c_str()));

  // Currency.
  valueInt = player->getCurrency();
  string currency = Ogre::StringConverter::toString(valueInt);
  node->append_attribute(doc.allocate_attribute("currency", currency.c_str()));

  // Skill Points.
  valueInt = player->getSkillPoints();
  string skillPoints = Ogre::StringConverter::toString(valueInt);
  node->append_attribute(doc.allocate_attribute("skillPoints", skillPoints.c_str()));

  // Skillset name.
  string skillset = player->getSkillClass()->getName();
  node->append_attribute(doc.allocate_attribute("skillset", skillset.c_str()));

  // Get the inventory stuff.
  rapidxml::xml_node<>* inventoryNode = doc.allocate_node(rapidxml::node_element, "inventory");
  node->append_node(inventoryNode);

  vector<InventorySlot*>* slots = player->getInventory()->getItems();

  string** inventoryIndexStrings = new string*[slots->size()];
  string** inventoryItemNameStrings = new string*[slots->size()];
  string** inventoryCountStrings = new string*[slots->size()];

  int i=0;
  for(vector<InventorySlot*>::iterator it = slots->begin();
    it != slots->end(); ++it)
  {
    rapidxml::xml_node<>* slotNode = doc.allocate_node(rapidxml::node_element, "slot");
    inventoryNode->append_node(slotNode);

    InventorySlot* slot = *it;
    string* index = new string(Ogre::StringConverter::toString(slot->getIndex()));
    string* itemName;
    if(slot->getItem())
    {
      itemName = new string(slot->getItem()->getName());
    }
    else
    {
      itemName = new string("");
    } // end else

    string* count = new string(Ogre::StringConverter::toString(slot->getCount()));

    slotNode->append_attribute(doc.allocate_attribute("index", index->c_str()));
    slotNode->append_attribute(doc.allocate_attribute("item", itemName->c_str()));
    slotNode->append_attribute(doc.allocate_attribute("count", count->c_str()));

    inventoryIndexStrings[i] = index;
    inventoryItemNameStrings[i] = itemName;
    inventoryCountStrings[i] = count;

    ++i;
  } // end for

  // Equipment.
  // Get the inventory stuff.
  // Head.
  rapidxml::xml_node<>* headNode = doc.allocate_node(rapidxml::node_element, "head");
  node->append_node(headNode);
  string* headItem;
  if(player->getHead())
  {
    headItem = new string(player->getHead()->getName());
  } // end if
  else
  {
    headItem = new string("");
  } // end else
  headNode->append_attribute(doc.allocate_attribute("item", headItem->c_str()));

  // Chest.
  rapidxml::xml_node<>* chestNode = doc.allocate_node(rapidxml::node_element, "chest");
  node->append_node(chestNode);
  string* chestItem;
  if(player->getChest())
  {
    chestItem = new string(player->getChest()->getName());
  } // end if
  else
  {
    chestItem = new string("");
  } // end else
  chestNode->append_attribute(doc.allocate_attribute("item", chestItem->c_str()));

  // Legs.
  rapidxml::xml_node<>* legsNode = doc.allocate_node(rapidxml::node_element, "legs");
  node->append_node(legsNode);
  string* legsItem;
  if(player->getLegs())
  {
    legsItem = new string(player->getLegs()->getName());
  } // end if
  else
  {
    legsItem = new string("");
  } // end else
  legsNode->append_attribute(doc.allocate_attribute("item", legsItem->c_str()));

  // Feet.
  rapidxml::xml_node<>* feetNode = doc.allocate_node(rapidxml::node_element, "feet");
  node->append_node(feetNode);
  string* feetItem;
  if(player->getFeet())
  {
    feetItem = new string(player->getFeet()->getName());
  } // end if
  else
  {
    feetItem = new string("");
  } // end else
  feetNode->append_attribute(doc.allocate_attribute("item", feetItem->c_str()));

  // Weapon.
  rapidxml::xml_node<>* weaponNode = doc.allocate_node(rapidxml::node_element, "weapon");
  node->append_node(weaponNode);
  string* weaponItem;
  if(player->getWeapon())
  {
    weaponItem = new string(player->getWeapon()->getName());
  } // end if
  else
  {
    weaponItem = new string("");
  } // end else
  weaponNode->append_attribute(doc.allocate_attribute("item", weaponItem->c_str()));

  // Skillset details.
  rapidxml::xml_node<>* skillSetNode = doc.allocate_node(rapidxml::node_element, "skillSet");
  node->append_node(skillSetNode);

  SkillSet* playerClass = player->getSkillClass();
  int numSkills = playerClass->getNumSkills();
  string** skillNames = new string*[numSkills];
  string** skillLevels = new string*[numSkills];
  Skill** skills = playerClass->getSkills();
  for(int i=0;i<numSkills;i++)
  {
    rapidxml::xml_node<>* skillNode = doc.allocate_node(rapidxml::node_element, "skill");
    skillSetNode->append_node(skillNode);
    string* name = new string(skills[i]->getName());
    string* level = new string(Ogre::StringConverter::toString(skills[i]->getLevel()));
    skillNames[i] = name;
    skillLevels[i] = level;
    skillNode->append_attribute(doc.allocate_attribute("name", name->c_str()));
    skillNode->append_attribute(doc.allocate_attribute("level", level->c_str()));
  } // end for

  // Now do position. 
  rapidxml::xml_node<>* positionNode = doc.allocate_node(rapidxml::node_element, "position");
  node->append_node(positionNode);
  Ogre::Vector3 position = player->getPosition();
  string xPos = Ogre::StringConverter::toString(position.x);
  string yPos = Ogre::StringConverter::toString(position.y);
  string zPos = Ogre::StringConverter::toString(position.z);
  // Append to node.
  positionNode->append_attribute(doc.allocate_attribute("x", xPos.c_str()));
  positionNode->append_attribute(doc.allocate_attribute("y", yPos.c_str()));
  positionNode->append_attribute(doc.allocate_attribute("z", zPos.c_str()));

  // Now do orientation. 
  rapidxml::xml_node<>* orientationNode = doc.allocate_node(rapidxml::node_element, "orientation");
  node->append_node(orientationNode);
  Ogre::Vector3 orientation = player->getOrientation();
  string xOri = Ogre::StringConverter::toString(orientation.x);
  string yOri = Ogre::StringConverter::toString(orientation.y);
  string zOri = Ogre::StringConverter::toString(orientation.z);
  // Append to node.
  orientationNode->append_attribute(doc.allocate_attribute("x", xOri.c_str()));
  orientationNode->append_attribute(doc.allocate_attribute("y", yOri.c_str()));
  orientationNode->append_attribute(doc.allocate_attribute("z", zOri.c_str()));

  // Completed quests node.
  rapidxml::xml_node<>* completedQuestsNode = 
    doc.allocate_node(rapidxml::node_element, "completedQuests");
  node->append_node(completedQuestsNode);
  vector<Quest*>* completedQuests = Player::getInstance()->getCompletedQuests();

  // Store this in allocated memory.
  int numCompletedQuests = completedQuests->size();
  string** questNames = new string*[numCompletedQuests];

  // Use i to keep check of iterations.
  i=0;
  for(vector<Quest*>::iterator it = completedQuests->begin(); 
    it != completedQuests->end(); ++it)
  {
    rapidxml::xml_node<>* questNode = doc.allocate_node(rapidxml::node_element, "quest");
    completedQuestsNode->append_node(questNode);
    Quest* currentQuest = *it;

    questNames[i] = new string(currentQuest->getName());

    questNode->append_attribute(doc.allocate_attribute("name", questNames[i]->c_str()));

    ++i;
  } // end for

  // Now do the current quest.
  rapidxml::xml_node<>* currentQuestNode = 
    doc.allocate_node(rapidxml::node_element, "currentQuest");
  node->append_node(currentQuestNode);
  Quest* currentQuest = Player::getInstance()->getCurrentQuest();

  string* questName = NULL;
  if(currentQuest)
  {
    questName = new string(currentQuest->getName());
    currentQuestNode->append_attribute(doc.allocate_attribute("name", questName->c_str()));
  
    // Now loop through the objectives and add them in.
    int numObjectives = currentQuest->getNumObjectives();
    AbstractObjective** objectives = currentQuest->getObjectives();
    string* types = new string[numObjectives];
    string* values = new string[numObjectives];
    for(int i=0;i<numObjectives;i++)
    {
      rapidxml::xml_node<>* objectiveNode = doc.allocate_node(rapidxml::node_element, "objective");
      currentQuestNode->append_node(objectiveNode);
      types[i] = objectives[i]->getType();

      objectiveNode->append_attribute(doc.allocate_attribute("type", types[i].c_str()));

      if(types[i] == "collect")
      {
        CollectObjective* castObj = (CollectObjective*)objectives[i];
        values[i] = Ogre::StringConverter::toString(castObj->getAmountCollected());
        objectiveNode->append_attribute(doc.allocate_attribute("value", values[i].c_str()));
      } // end if
      else if(types[i] == "kill")
      {
        KillObjective* castObj = (KillObjective*)objectives[i];
        values[i] = Ogre::StringConverter::toString(castObj->getAmountCollected());
        objectiveNode->append_attribute(doc.allocate_attribute("value", values[i].c_str()));
      } // end else if
      else if(types[i] == "timed")
      {
        TimedObjective* castObj = (TimedObjective*)objectives[i];
        values[i] = CEGUI::PropertyHelper::floatToString(castObj->getTimePassed()).c_str();
        objectiveNode->append_attribute(doc.allocate_attribute("value", values[i].c_str()));
      } // end else if
    } // end for(int i=0;i<numObjectives;i++)
  } // end if(CurrentQuest)
  // Return the final string.
  std::string s;
  rapidxml::print(std::back_inserter(s), doc, 0);

  // Delete all the allocations.
  delete[] inventoryIndexStrings;
  inventoryIndexStrings = NULL;
  delete[] inventoryItemNameStrings;
  inventoryItemNameStrings = NULL;
  delete[] inventoryCountStrings;
  inventoryCountStrings = NULL;
  delete headItem;
  delete chestItem;
  delete legsItem;
  delete feetItem;
  delete weaponItem;
  delete[] skillNames;
  delete[] skillLevels;
  delete[] questNames;

  return s;
} // string SaveManager::writePlayer()


string SaveManager::writeScene()
{
  vector<Mob*>* mobs = MobManager::getInstance()->getMobVector();

  // Attributes.
  rapidxml::xml_document<> doc;
  rapidxml::xml_node<> *node = doc.allocate_node(rapidxml::node_element, "mobs");
  doc.append_node(node);

  int numMobs = mobs->size();
  string* entityIDs = new string[numMobs];
  string* mobName = new string[numMobs];
  string* alive = new string[numMobs];
  string* xPos = new string[numMobs];
  string* yPos = new string[numMobs];
  string* zPos = new string[numMobs];
  string* xOri = new string[numMobs];
  string* yOri = new string[numMobs];
  string* zOri = new string[numMobs];

  int i=0;
  for(vector<Mob*>::iterator it = mobs->begin(); it != mobs->end(); ++it)
  {
    rapidxml::xml_node<> *mobNode = doc.allocate_node(rapidxml::node_element, "mob");
    node->append_node(mobNode);

    Mob* mob = *it;
    entityIDs[i] = Ogre::StringConverter::toString(mob->getEntityIDNumber());
    mobName[i] = mob->getName();
    alive[i] = Ogre::StringConverter::toString(mob->getAlive());
    xPos[i] = CEGUI::PropertyHelper::floatToString(mob->getPosition().x).c_str();
    yPos[i] = CEGUI::PropertyHelper::floatToString(mob->getPosition().y).c_str();
    zPos[i] = CEGUI::PropertyHelper::floatToString(mob->getPosition().z).c_str();
    xOri[i] = CEGUI::PropertyHelper::floatToString(mob->getOrientation().x).c_str();
    yOri[i] = CEGUI::PropertyHelper::floatToString(mob->getOrientation().y).c_str();
    zOri[i] = CEGUI::PropertyHelper::floatToString(mob->getOrientation().z).c_str();

    mobNode->append_attribute(doc.allocate_attribute("id", entityIDs[i].c_str()));
    mobNode->append_attribute(doc.allocate_attribute("name", mobName[i].c_str()));
    mobNode->append_attribute(doc.allocate_attribute("alive", alive[i].c_str()));
    mobNode->append_attribute(doc.allocate_attribute("xPos", xPos[i].c_str()));
    mobNode->append_attribute(doc.allocate_attribute("yPos", yPos[i].c_str()));
    mobNode->append_attribute(doc.allocate_attribute("zPos", zPos[i].c_str()));
    mobNode->append_attribute(doc.allocate_attribute("xOri", xOri[i].c_str()));
    mobNode->append_attribute(doc.allocate_attribute("yOri", yOri[i].c_str()));
    mobNode->append_attribute(doc.allocate_attribute("zOri", zOri[i].c_str()));

    ++i;
  } // end for

  std::string s;
  rapidxml::print(std::back_inserter(s), doc, 0);

  // Delete all the allocations.
  delete[] entityIDs;
  delete[] mobName;
  delete[] alive;
  delete[] xPos;
  delete[] yPos;
  delete[] zPos;
  delete[] xOri;
  delete[] yOri;
  delete[] zOri;

  return s;
} // string SaveManager::writeScene()


string SaveManager::writeHotBar()
{
  int numSlots = HotBar::getSingleton().getNumHotSlots();

  // rapidxml attributes.
  rapidxml::xml_document<> doc;
  rapidxml::xml_node<> *node = doc.allocate_node(rapidxml::node_element, "hotbar");
  doc.append_node(node);

  AbstractHotSlot** slots = HotBar::getSingleton().getSlots();

  string* types = new string[numSlots];
  string* cooldowns = new string[numSlots];
  string* values = new string[numSlots];

  for(int i=0;i<numSlots;i++)
  {
    rapidxml::xml_node<> *hotbarNode = doc.allocate_node(rapidxml::node_element, "slot");
    node->append_node(hotbarNode);

    if(slots[i])
    {
      types[i] = slots[i]->getType();
      hotbarNode->append_attribute(doc.allocate_attribute("type", types[i].c_str()));
      if(slots[i]->getType() == "Skill")
      {
        SkillHotSlot* skillSlot = (SkillHotSlot*)slots[i];
        values[i] = skillSlot->getSkill()->getName();
        hotbarNode->append_attribute(doc.allocate_attribute("skill", values[i].c_str()));
        cooldowns[i] = CEGUI::PropertyHelper::floatToString(skillSlot->getCooldown()).c_str();
        hotbarNode->append_attribute(doc.allocate_attribute("cooldown", 
          cooldowns[i].c_str()));
      } // end if
      else if(slots[i]->getType() == "Item")
      {
        ItemHotSlot* itemSlot = (ItemHotSlot*)slots[i];
        values[i] = Ogre::StringConverter::toString(itemSlot->getInventorySlot()->getIndex());
        hotbarNode->append_attribute(doc.allocate_attribute("item", 
          values[i].c_str()));
      } // end else if
    } // end if
    else
    {
      hotbarNode->append_attribute(doc.allocate_attribute("type", ""));
    } // end else

  } // end for

  std::string s;
  rapidxml::print(std::back_inserter(s), doc, 0);

  delete[] types;
  delete[] values;
  delete[] cooldowns;

  return s;

} // string SaveManager::writeHotBar()
 

void SaveManager::readPlayer(rapidxml::xml_document<> *_doc)
{
  rapidxml::xml_node<> *node = _doc->first_node("player");
  string name = this->getAttrib(node, "name");
  int level = this->getAttribReal(node, "level");
  int strength = this->getAttribReal(node, "strength");
  int agility = this->getAttribReal(node, "agility");
  int dexterity = this->getAttribReal(node, "dexterity");
  int constitution = this->getAttribReal(node, "constitution");
  int intelligence = this->getAttribReal(node, "intelligence");
  int wisdom = this->getAttribReal(node, "wisdom");
  int charisma = this->getAttribReal(node, "charisma");
  string modelName = this->getAttrib(node, "modelName");
  string gender = this->getAttrib(node, "gender");
  int experience = this->getAttribReal(node, "experience");
  int currency = this->getAttribReal(node, "currency");
  int skillPoints = this->getAttribReal(node, "skillPoints");
  string skillSetName = this->getAttrib(node, "skillset");

  // Create an inventory.
  Inventory* playerInventory = new Inventory(Player::getInstance());

  // Load the players items into it.
  rapidxml::xml_node<> *inventoryNode = node->first_node("inventory");
  // Get the child nodes.
  for(rapidxml::xml_node<> *slotNode = inventoryNode->first_node();slotNode; slotNode = slotNode->next_sibling())
  {
    int index = this->getAttribReal(slotNode, "index");
    int count = this->getAttribReal(slotNode, "count");
    string itemName = this->getAttrib(slotNode,"item");

    AbstractItem* item = NULL;
    if(itemName != "")
    {
      item = ItemManager::getInstance()->getItem(itemName);
    } // end if

    if(item)
    {
      playerInventory->addItemAtEmptySlot(item, count);
    } // end if
    else
    {
      playerInventory->addEmptySlot();
    } // end else

  } // end for

  // Initialise the player.
  Player::getInstance()->init(strength, agility, dexterity, constitution, intelligence, wisdom, charisma, modelName, name, skillSetName,
    gender, playerInventory, level, experience, currency, skillPoints);

  // Add completed quests.
  vector<Quest*>* completedQuests = Player::getInstance()->getCompletedQuests();
  
  // Load the players quests into it.
  rapidxml::xml_node<> *completedQuestNode = node->first_node("completedQuests");
  // Get the child nodes.
  for(rapidxml::xml_node<> *completedNode = completedQuestNode->first_node();completedNode; 
    completedNode = completedNode->next_sibling())
  {
    string questName = this->getAttrib(completedNode,"name");
    completedQuests->push_back(QuestManager::getInstance()->createQuest(questName));
  } // end for

  // Add the current quest.
  Quest* currentQuest = Player::getInstance()->getCurrentQuest();
  rapidxml::xml_node<> *currentQuestNode = node->first_node("currentQuest");
  string currentQuestName = this->getAttrib(currentQuestNode, "name");
  currentQuest = QuestManager::getInstance()->createQuest(currentQuestName);
  Player::getInstance()->setCurrentQuest(currentQuest);
  if(currentQuest)
  {
    int numObjectives = currentQuest->getNumObjectives();
    AbstractObjective** objectives = currentQuest->getObjectives();
    int i=0;
    // Get the child nodes.
    for(rapidxml::xml_node<> *objectiveNode = currentQuestNode->first_node();objectiveNode; 
      objectiveNode = objectiveNode->next_sibling())
    {
      string questName = this->getAttrib(objectiveNode,"value");
      if(objectives[i]->getType() == "collect")
      {
        CollectObjective* castObj = (CollectObjective*)objectives[i];
        castObj->setAmountCollected(this->getAttribReal(objectiveNode, "value"));
      } // end if
      else if(objectives[i]->getType() == "kill")
      {
        KillObjective* castObj = (KillObjective*)objectives[i];
        castObj->setAmountCollected(this->getAttribReal(objectiveNode, "value"));
      } // end else if
      else if(objectives[i]->getType() == "timed")
      {
        TimedObjective* castObj = (TimedObjective*)objectives[i];
        castObj->setTimePassed(this->getAttribReal(objectiveNode, "value"));
      } // end else if
      ++i;
    } // end for
    Player::getInstance()->getCurrentQuest()->checkCompletion(0);
  } // end if(currentQuest)

  // Now load the skills.
  SkillSet* skillClass = Player::getInstance()->getSkillClass();
  rapidxml::xml_node<> *skillClassNode = node->first_node("skillSet");
  int i = 0;
  for(rapidxml::xml_node<> *skillNode = skillClassNode->first_node();skillNode; 
      skillNode = skillNode->next_sibling())
  {
    string skillName = this->getAttrib(skillNode, "name");
    int skillLevel = this->getAttribReal(skillNode, "level");

    for(int j=0;j<skillLevel;j++)
    {
      skillClass->upgradeSkill(i);
    } // end for
    ++i;
  } // end for
  

} // void SaveManager::readPlayer(rapidxml::xml_document<> *_doc)


void SaveManager::readMobs(rapidxml::xml_document<> *_doc)
{
  rapidxml::xml_node<> *node = _doc->first_node("mobs");
  
  // Get children of mobs.
  for(rapidxml::xml_node<> *mobNode = node->first_node(); mobNode; mobNode = mobNode->next_sibling())
  {
    // Read the data from the mob dump.
    int entityID = this->getAttribReal(mobNode, "id");

    AbstractEntity* entity = MessageDispatcher::getInstance()->findEntityByID(entityID);
    Mob* mob = MobManager::getInstance()->getMobByID(entityID);

    // Read the rest of the data.
    mob->setAlive(this->getAttribBool(mobNode, "alive"));

    if(!mob->getAlive())
    {
      mob->setCurrentAnimationState(mob->getDieAnimation());
      mob->getCurrentAnimationState()->setLoop(false);
      mob->getCurrentAnimationState()->setEnabled(true);
    } // end if

    double xPos = this->getAttribReal(mobNode, "xPos");
    double yPos = this->getAttribReal(mobNode, "yPos");
    double zPos = this->getAttribReal(mobNode, "zPos");
    double xOri = this->getAttribReal(mobNode, "xOri");
    double yOri = this->getAttribReal(mobNode, "yOri");
    double zOri = this->getAttribReal(mobNode, "zOri");

    mob->setPosition(Ogre::Vector3(xPos,yPos,zPos));
    mob->setOrientation(Ogre::Vector3(xOri,yOri,zOri));

  } // end for


} // void SaveManager::readMobs(rapidxml::xml_document<> *_doc)


void SaveManager::readHotBar(rapidxml::xml_document<> *_doc)
{ // Reads the hotbar data from the save file.

  rapidxml::xml_node<> *node = _doc->first_node("hotbar");
  
  // Get children of mobs.
  int i=0;
  for(rapidxml::xml_node<> *slotNode = node->first_node(); slotNode; slotNode = slotNode->next_sibling())
  {
    // Determine what type of slot this is.
    string type = this->getAttrib(slotNode, "type");
    if(type == "Skill")
    {
      string skillName = this->getAttrib(slotNode, "skill");
      double cooldown = this->getAttribReal(slotNode, "cooldown");

      HotBar::getSingleton().addToHotBar(Player::getInstance()->getSkillClass()->getSkill(skillName),i);
      HotBar::getSingleton().getSlots()[i]->setCooldown(cooldown);

    } // end if
    else if(type == "Item")
    {
      int invSlot = this->getAttribReal(slotNode, "item");
      HotBar::getSingleton().addToHotBar(Player::getInstance()->getInventory()->getSlotByIndex(invSlot),i);
    } // end else if
    ++i;
  } // end for

} // void SaveManager::readHotBar(rapidxml::xml_document<> *_doc)


void SaveManager::loadPlayerEnvironment(rapidxml::xml_document<> *_doc)
{
  rapidxml::xml_node<> *node = _doc->first_node("player");
  // Load in the players equipment.
  rapidxml::xml_node<> *headNode = node->first_node("head");
  string itemName = this->getAttrib(headNode, "item");
  if(itemName != "")
  {
    Player::getInstance()->setHead(ItemManager::getInstance()->getItem(itemName));
  } // end if

  rapidxml::xml_node<> *chestNode = node->first_node("chest");
  itemName = this->getAttrib(chestNode, "item");
  if(itemName != "")
  {
    Player::getInstance()->setChest(ItemManager::getInstance()->getItem(itemName));
  } // end if

  rapidxml::xml_node<> *legsNode = node->first_node("legs");
  itemName = this->getAttrib(legsNode, "item");
  if(itemName != "")
  {
    Player::getInstance()->setLegs(ItemManager::getInstance()->getItem(itemName));
  } // end if

  rapidxml::xml_node<> *feetNode = node->first_node("feet");
  itemName = this->getAttrib(feetNode, "item");
  if(itemName != "")
  {
    Player::getInstance()->setFeet(ItemManager::getInstance()->getItem(itemName));
  } // end if

  rapidxml::xml_node<> *weaponNode = node->first_node("weapon");
  itemName = this->getAttrib(weaponNode, "item");
  if(itemName != "")
  {
    Player::getInstance()->setWeapon(ItemManager::getInstance()->getItem(itemName), 
      GameManager::getInstance()->getCurrentScene()->getSceneManager());
  } // end if

  rapidxml::xml_node<> *positionNode = node->first_node("position");
  Ogre::Vector3 playerPosition = this->parseVector3(positionNode);
  
  rapidxml::xml_node<> *orientationNode = node->first_node("orientation");
  Ogre::Vector3 playerOrientation = this->parseVector3(orientationNode);

  Player::getInstance()->setPosition(playerPosition);
  Player::getInstance()->setOrientation(playerOrientation);

} // void SaveManager::loadPlayerItems(rapidxml::xml_document<> *_doc)


Ogre::String SaveManager::getAttrib(rapidxml::xml_node<>* XMLNode, const Ogre::String &attrib, const Ogre::String &defaultValue)
{
  if(XMLNode->first_attribute(attrib.c_str()))
  {
    return XMLNode->first_attribute(attrib.c_str())->value();
  } // end if
  else
  {
    return defaultValue;
  } // end else
} // Ogre::String SaveManager::getAttrib(rapidxml::xml_node<>* XMLNode, const Ogre::String &attrib, const Ogre::String &defaultValue)


Ogre::Real SaveManager::getAttribReal(rapidxml::xml_node<>* XMLNode, const Ogre::String &attrib, Ogre::Real defaultValue)
{
  if(XMLNode->first_attribute(attrib.c_str()))
  {
    return Ogre::StringConverter::parseReal(XMLNode->first_attribute(attrib.c_str())->value());
  } // end if
  else
  {
    return defaultValue;
  } // end else
} // Ogre::Real SaveManager::getAttribReal(rapidxml::xml_node<>* XMLNode, const Ogre::String &attrib, Ogre::Real defaultValue)


bool SaveManager::getAttribBool(rapidxml::xml_node<>* XMLNode, const Ogre::String &attrib, bool defaultValue)
{
  if(!XMLNode->first_attribute(attrib.c_str()))
  {
    return defaultValue;
  } // end if

  if(Ogre::String(XMLNode->first_attribute(attrib.c_str())->value()) == "true")
  {
    return true;
  } // end if

  return false;
} // bool SaveManager::getAttribBool(rapidxml::xml_node<>* XMLNode, const Ogre::String &attrib, bool defaultValue)


Ogre::Vector3 SaveManager::parseVector3(rapidxml::xml_node<>* XMLNode)
{
  return Ogre::Vector3(
    Ogre::StringConverter::parseReal(XMLNode->first_attribute("x")->value()),
    Ogre::StringConverter::parseReal(XMLNode->first_attribute("y")->value()),
    Ogre::StringConverter::parseReal(XMLNode->first_attribute("z")->value())
    );
} // Ogre::Vector3 SaveManager::parseVector3(rapidxml::xml_node<>* XMLNode)