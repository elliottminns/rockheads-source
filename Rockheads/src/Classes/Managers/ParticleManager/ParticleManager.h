#include "../../../PrecompiledHeaders/stdafx.h"
#ifndef PARTICLE_MANAGER_H
#define PARTICLE_MANAGER_H

class ParticleManager
{
private:
  typedef std::map<string,string> ParticleMap;

  static ParticleManager* instance;
  static bool initialised;
  bool intialised;
  ParticleManager();
  ~ParticleManager();

  int numParticles;
  ParticleMap* particleMap;
  Ogre::ParticleSystem** particles;

  Ogre::SceneManager* sceneManager;

public:

  static ParticleManager& getSingleton();
  static ParticleManager* getSingletonPtr();

  void initialise();
  ParticleMap* loadParticleData();
  void createParticleSystems(Ogre::SceneManager* _sceneManager);
  Ogre::ParticleSystem* getParticleSystem(string _name) const ;
  void releaseParticleSystems();

  void resetParticleSystem(Ogre::ParticleSystem* _system);
  void resetParticleSystem(string _name);

  void attachParticleToEntity(AbstractEntity* _entity, string _particle);
  void removeParticleFromEntity(AbstractEntity* _entity, string _particle);
};

#endif