#include "stdafx.h"
#include "ParticleManager.h"
#include "../../XMLParser/XMLParser.h"
#include "../GameManager/GameManager.h"


ParticleManager* ParticleManager::instance = 0;
bool ParticleManager::initialised = false;

typedef std::map<string,string> ParticleMap;


ParticleManager& ParticleManager::getSingleton()
{
  if(!instance)
  {
    instance = new ParticleManager;
  } // end if
  if(!initialised)
  {
    instance->initialise();
  } // end if

  return *instance;
} // ParticleManager& ParticleManager::getSingleton()


ParticleManager* ParticleManager::getSingletonPtr()
{
  if(!instance)
  {
    instance = new ParticleManager;
  } // end if
  if(!initialised)
  {
    instance->initialise();
  } // end if

  return instance;
} // ParticleManager* ParticleManager::getSingletonPtr()


ParticleManager::ParticleManager()
{
  this->sceneManager = NULL;
} // PaticleManager::ParticleManager()


ParticleManager::~ParticleManager()
{
  this->sceneManager = NULL;
} // PaticleManager::~ParticleManager()


void ParticleManager::initialise()
{
  particleMap = this->loadParticleData();
  this->numParticles = particleMap->size();
  this->particles = new Ogre::ParticleSystem*[this->numParticles];
} // void ParticleManager::initialise()



ParticleMap* ParticleManager::loadParticleData()
{
  string dataFile = "../../data/particles.xml";
  return XMLParser::getInstance()->parseParticleFile(dataFile);
} // vector<string>* ParticleManager::loadParticleData()


void ParticleManager::createParticleSystems(Ogre::SceneManager* _sceneManager)
{
  if(this->sceneManager != _sceneManager)
  {
    this->releaseParticleSystems();
  } // end if

  this->sceneManager = _sceneManager;

  if(!this->initialised)
  {
    ParticleMap::iterator it = this->particleMap->begin();

    for(int i=0;i<this->numParticles;i++)
    {
      string name   = it->first; 
      string system = it->second;
      this->particles[i] = _sceneManager->createParticleSystem(name, system);
      ++it;
    } // end for

    this->initialised = true;
  } // end if
} // void ParticleManager::createParticleSystems(Ogre::SceneManager* _sceneManager)


void ParticleManager::releaseParticleSystems()
{
  if(this->initialised)
  {
    this->initialised = false;
    for(int i=0;i<this->numParticles;i++)
    {
      if(this->particles[i])
      {
        if(this->particles[i]->getParentSceneNode())
        {
          this->particles[i]->detachFromParent();         
        } // end if
        this->particles[i] = NULL;
      } // end if
    } // end for
    if(this->sceneManager)
    {
      this->sceneManager->destroyAllParticleSystems();
      this->sceneManager = NULL;
    }
    

  } // end if
} // void ParticleManager::releaseParticleSystems()


Ogre::ParticleSystem* ParticleManager::getParticleSystem(string _name) const
{
  return GameManager::getInstance()->getCurrentScene()->getSceneManager()->getParticleSystem(_name);
} // void ParticleManager::getParticleSystem(string _name) const


void ParticleManager::resetParticleSystem(Ogre::ParticleSystem* _system)
{
  _system->detachFromParent();
  // Reset the particle system.
  for(unsigned short i=0; i<_system->getNumEmitters(); i++)
  {
    _system->getEmitter(i)->setEnabled(false);
    _system->getEmitter(i)->setMinRepeatDelay(_system->getEmitter(i)->getRepeatDelay());   //This resets the repeatDelay to 0
    _system->getEmitter(i)->setEnabled(true);
  } // end for
} // void ParticleManager::resetParticleSystem(Ogre::ParticleSystem* _particle)


void ParticleManager::resetParticleSystem(string _name)
{
  Ogre::ParticleSystem* system = GameManager::getInstance()->getCurrentScene()->getSceneManager()->getParticleSystem(_name);

  // Reset the particle system.
  for(unsigned short i=0; i<system->getNumEmitters(); i++)
  {
    system->getEmitter(i)->setEnabled(false);

    //This resets the repeatDelay to 0.
    system->getEmitter(i)->setMinRepeatDelay(system->getEmitter(i)->getRepeatDelay());   
    system->getEmitter(i)->setEnabled(true);
  } // end for
} // void ParticleManager::resetParticleSystem(string _name)


void ParticleManager::attachParticleToEntity(AbstractEntity* _entity, string _particle)
{
  if(_particle.length() > 0)
  {
    Ogre::ParticleSystem* system = this->getParticleSystem(_particle);
    if(system)
    {
      system->setEmitting(true);
      this->resetParticleSystem(system);
      _entity->getNode()->attachObject(system);
    } // end if
  } // end if
} // void ParticleManager::attachParticleToEntity(AbstractEntity* _entity, string _particle)


void ParticleManager::removeParticleFromEntity(AbstractEntity* _entity, string _particle)
{
  if(_particle.size() > 0)
  {
    Ogre::ParticleSystem* system = this->getParticleSystem(_particle);
    if(system)
    {
      system->detachFromParent();     
      system->setEmitting(false);
    } // end if
  } // end if
} // void ParticleManager::removeParticleFromEntity(AbstractEntity* _entity, string _particle)