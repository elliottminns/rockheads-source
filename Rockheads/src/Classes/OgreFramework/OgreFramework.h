#include "../../PrecompiledHeaders/stdafx.h"
#ifndef OGRE_FRAMEWORK_H
#define OGRE_FRAMEWORK_H

#define NULL 0

#include <OgreCamera.h>
#include <OgreEntity.h>
#include <OgreLogManager.h>
#include <OgreOverlay.h>
#include <OgreOverlayElement.h>
#include <OgreOverlayManager.h>
#include <OgreRoot.h>
#include <OgreViewport.h>
#include <OgreSceneManager.h>
#include <OgreRenderWindow.h>
#include <OgreConfigFile.h>

#include <OISEvents.h>
#include <OISInputManager.h>
#include <OISKeyboard.h>
#include <OISMouse.h>

#include <OgreALListener.h>
#include <OgreALSoundManager.h>


#include <SdkTrays.h>


#include <iostream>
#include <string>
#include <CEGUI.h>
#include <CEGUI/RendererModules/Ogre/CEGUIOgreRenderer.h>


class OgreFramework : public Ogre::Singleton<OgreFramework>, OIS::KeyListener, OIS::MouseListener 
{
public:
	OgreFramework();
	~OgreFramework();

	bool initOgre(Ogre::String _wndTitle, OIS::KeyListener* _keyListener = 0, OIS::MouseListener *_mouseListener = 0);
	void updateOgre(double _timeSinceLastFrame);

	bool keyPressed(const OIS::KeyEvent &_keyEventRef);
	bool keyReleased(const OIS::KeyEvent &_keyEventRef);

	bool mouseMoved(const OIS::MouseEvent &_evt);
	bool mousePressed(const OIS::MouseEvent &_evt, OIS::MouseButtonID _id);
	bool mouseReleased(const OIS::MouseEvent &_evt, OIS::MouseButtonID _id);
  
  bool CEGUIKeyPressed(const OIS::KeyEvent &_keyEventRef);
  bool CEGUIKeyReleased(const OIS::KeyEvent &_keyEventRef);

	bool CEGUIMouseMoved(const OIS::MouseEvent &_evt);
	bool CEGUIMousePressed(const OIS::MouseEvent &_evt, OIS::MouseButtonID _id);
	bool CEGUIMouseReleased(const OIS::MouseEvent &_evt, OIS::MouseButtonID _id);
  
  // Ogre attributes.
	Ogre::Root*					    root;
	Ogre::RenderWindow*			renderWindow;
	Ogre::Viewport*				  viewport;
	Ogre::Log*					    log;
	Ogre::Timer*				    timer;
  Ogre::String            plugin;
  Ogre::String            resource;

  // OgreAL attributes.
  OgreAL::SoundManager*   soundManager;

  // CEGUI attributes.
  CEGUI::OgreRenderer*    CEGUIRenderer;
  CEGUI::System*          CEGUISystem;
  std::string             GUILook;

  // OIS attributes.
	OIS::InputManager*			inputManager; 
	OIS::Keyboard*				  keyboard;
	OIS::Mouse*					    mouse;

  // Collision attribute.
  Ogre::RaySceneQuery*	rsq;



  OgreBites::SdkTrayManager*	trayManager;


private:
	OgreFramework(const OgreFramework&);
	OgreFramework& operator= (const OgreFramework&);

}; // end Class OgreFramework

#endif