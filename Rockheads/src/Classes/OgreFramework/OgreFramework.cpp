#include "stdafx.h"
#include "OgreFramework.h"
#include <OgreALSoundManager.h>

CEGUI::MouseButton convertButton(OIS::MouseButtonID buttonID);

using namespace Ogre;


template<> OgreFramework* Ogre::Singleton<OgreFramework>::ms_Singleton = 0;


OgreFramework::OgreFramework()
{
  this->root          = 0;
  this->renderWindow  = 0;
  this->viewport      = 0;
  this->log           = 0;
  this->timer         = 0;

  this->inputManager  = 0;
  this->keyboard		  = 0;
  this->mouse			    = 0;

  this->trayManager   = 0;

  this->resource      = "Ogre::StringUtil::BLANK";
  this->plugin        = "Ogre::StringUtil::BLANK";
  this->GUILook       = "GlossySerpent";
} // OgreFramework::OgreFramework()


OgreFramework::~OgreFramework()
{
  OgreFramework::getSingletonPtr()->log->logMessage("Shutdown OGRE...");

  if(this->trayManager)      
  {
    delete this->trayManager;
  } // end if

  if(this->inputManager)		
  {
    OIS::InputManager::destroyInputSystem(this->inputManager);
  } // end if

  if(this->root)
  {
    delete this->root;
  } // end if

  if(this->soundManager)
  {
    this->soundManager->destroyAllSounds();
    //delete this->soundManager;
  } // end if
} // OgreFramework::~OgreFramework()


bool OgreFramework::initOgre(Ogre::String _windowTitle, OIS::KeyListener *_keyListener, OIS::MouseListener *_mouseListener)
{
  Ogre::LogManager* logManager = new Ogre::LogManager();

  this->log = Ogre::LogManager::getSingleton().createLog("OgreLogfile.log", true, true, false);
  this->log->setDebugOutputEnabled(true);

  #ifdef _DEBUG
   this->resource = "resources_d.cfg";
    this->plugin = "plugins_d.cfg";
  #else
  this->resource = "resources.cfg";
  this->plugin = "plugins.cfg";
  #endif

  this->root = new Ogre::Root(this->plugin);

  if(!this->root->showConfigDialog())
  {
    return false;
  } // end if

  this->renderWindow = this->root->initialise(true, _windowTitle);

  this->viewport = this->renderWindow->addViewport(0);
  this->viewport->setBackgroundColour(ColourValue(0.2f, 0.2f, 0.2f, 1.0f));

  this->viewport->setCamera(0);

  size_t hWnd = 0;
  OIS::ParamList paramList;
  this->renderWindow->getCustomAttribute("WINDOW", &hWnd);

  paramList.insert(OIS::ParamList::value_type("WINDOW", Ogre::StringConverter::toString(hWnd)));

  this->inputManager = OIS::InputManager::createInputSystem(paramList);

  this->keyboard = static_cast<OIS::Keyboard*>(this->inputManager->createInputObject(OIS::OISKeyboard, true));
  this->mouse = static_cast<OIS::Mouse*>(this->inputManager->createInputObject(OIS::OISMouse, true));

  this->mouse->getMouseState().height = this->renderWindow->getHeight();
  this->mouse->getMouseState().width	= this->renderWindow->getWidth();

  if(_keyListener == 0)
  {
    this->keyboard->setEventCallback(this);
  } // end if
  else
  {
    this->keyboard->setEventCallback(_keyListener);
  } // end else

  if(_mouseListener == 0)
  {
    this->mouse->setEventCallback(this);
  } // end if
  else
  {
    this->mouse->setEventCallback(_mouseListener);
  } // end else

  Ogre::String secName, typeName, archName;
  Ogre::ConfigFile cf;
  cf.load(this->resource);

  Ogre::ConfigFile::SectionIterator seci = cf.getSectionIterator();
  while (seci.hasMoreElements())
  {
    secName = seci.peekNextKey();
    Ogre::ConfigFile::SettingsMultiMap *settings = seci.getNext();
    Ogre::ConfigFile::SettingsMultiMap::iterator i;
    for (i = settings->begin(); i != settings->end(); ++i)
    {
      typeName = i->first;
      archName = i->second;
      Ogre::ResourceGroupManager::getSingleton().addResourceLocation(archName, typeName, secName);
    } // end for
  } // end while

  Ogre::TextureManager::getSingleton().setDefaultNumMipmaps(5);
  Ogre::ResourceGroupManager::getSingleton().initialiseAllResourceGroups();


  this->trayManager = new OgreBites::SdkTrayManager("AOFTrayMgr", this->renderWindow, this->mouse, 0);
  this->trayManager->hideCursor();


  this->timer = new Ogre::Timer();
  this->timer->reset();

  this->renderWindow->setActive(true);

  this->CEGUIRenderer = &CEGUI::OgreRenderer::bootstrapSystem();
  this->CEGUISystem = CEGUI::System::getSingletonPtr();

  CEGUI::Imageset::setDefaultResourceGroup("Imagesets");
  CEGUI::Font::setDefaultResourceGroup("Fonts");
  CEGUI::Scheme::setDefaultResourceGroup("Schemes");
  CEGUI::WidgetLookManager::setDefaultResourceGroup("LookNFeel");
  CEGUI::WindowManager::setDefaultResourceGroup("Layouts");

  // Select the CEGUI skin.
  CEGUI::SchemeManager::getSingleton().create(this->GUILook + ".scheme");

  // Select the mouse cursor.
  if(this->GUILook == "GlossySerpent")
  {
    CEGUI::System::getSingleton().setDefaultMouseCursor("GlossySerpentCursors", "MouseArrow");
  } // end if
  else
  {
    CEGUI::System::getSingleton().setDefaultMouseCursor(this->GUILook, "MouseArrow");
  } // end else
  CEGUI::MouseCursor::getSingleton().setImage(CEGUI::System::getSingleton().getDefaultMouseCursor());
  CEGUI::System::getSingleton().setDefaultTooltip(this->GUILook + "/Tooltip");
  CEGUI::System::getSingleton().getDefaultTooltip()->setHoverTime(0.02);
  CEGUI::System::getSingleton().getDefaultTooltip()->setVerticalAlignment(CEGUI::VA_TOP);
  // Create the sound manager.
  this->soundManager = new OgreAL::SoundManager();

  return true;
} // bool OgreFramework::initOgre(Ogre::String wndTitle, OIS::KeyListener *pKeyListener, OIS::MouseListener *pMouseListener)


bool OgreFramework::keyPressed(const OIS::KeyEvent &_keyEventRef)
{
  if(this->keyboard->isKeyDown(OIS::KC_SYSRQ))
  {
    this->renderWindow->writeContentsToTimestampedFile("AOF_Screenshot_", ".jpg");
    return true;
  } // end if

  return true;
} // bool OgreFramework::keyPressed(const OIS::KeyEvent &_keyEventRef)


bool OgreFramework::keyReleased(const OIS::KeyEvent &_keyEventRef)
{
  return true;
} // bool OgreFramework::keyReleased(const OIS::KeyEvent &_keyEventRef)


bool OgreFramework::mouseMoved(const OIS::MouseEvent &_evt)
{
  return true;
} // bool OgreFramework::mouseMoved(const OIS::MouseEvent &_evt)


bool OgreFramework::mousePressed(const OIS::MouseEvent &_evt, OIS::MouseButtonID _id)
{
  return true;
} // bool OgreFramework::mousePressed(const OIS::MouseEvent &_evt, OIS::MouseButtonID _id)


bool OgreFramework::mouseReleased(const OIS::MouseEvent &_evt, OIS::MouseButtonID _id)
{
  return true;
} // bool OgreFramework::mouseReleased(const OIS::MouseEvent &_evt, OIS::MouseButtonID _id)


bool OgreFramework::CEGUIKeyPressed(const OIS::KeyEvent &_keyEventRef)
{
  CEGUI::System &sys  = CEGUI::System::getSingleton();
  bool returnValue    = false;
  bool returnValue2   = false;
  returnValue         = sys.injectKeyDown(_keyEventRef.key);
  returnValue2        = sys.injectChar(_keyEventRef.text);
  
  return returnValue || returnValue2;
} // bool OgreFramework::CEGUIKeyPressed(const OIS::KeyEvent &_keyEventRef)


bool OgreFramework::CEGUIKeyReleased(const OIS::KeyEvent &_keyEventRef)
{
  return CEGUI::System::getSingleton().injectKeyUp(_keyEventRef.key);
} // bool OgreFramework::CEGUIKeyReleased(const OIS::KeyEvent &_keyEventRef)


bool OgreFramework::CEGUIMouseMoved(const OIS::MouseEvent &_evt)
{
  bool returnValue = false;
  CEGUI::System &sys = CEGUI::System::getSingleton();
  returnValue = sys.injectMouseMove((float)_evt.state.X.rel, (float)_evt.state.Y.rel);
  // Scroll wheel.
  if(_evt.state.Z.rel)
  {
    return sys.injectMouseWheelChange(_evt.state.Z.rel / 120.0f) || returnValue;
  }
  return returnValue;
} // bool OgreFramework::CEGUIMouseMoved(const OIS::MouseEvent &_evt)


bool OgreFramework::CEGUIMousePressed(const OIS::MouseEvent &_evt, OIS::MouseButtonID _id)
{
  return CEGUI::System::getSingleton().injectMouseButtonDown(convertButton(_id));
} // bool OgreFramework::CEGUIMousePressed(const OIS::MouseEvent &_evt, OIS::MouseButtonID _id)


bool OgreFramework::CEGUIMouseReleased(const OIS::MouseEvent &_evt, OIS::MouseButtonID _id)
{
  return CEGUI::System::getSingleton().injectMouseButtonUp(convertButton(_id));
} // bool OgreFramework::CEGUIMouseReleased(const OIS::MouseEvent &_evt, OIS::MouseButtonID _id)


void OgreFramework::updateOgre(double _timeSinceLastFrame)
{

} // void OgreFramework::updateOgre(double _timeSinceLastFrame)


CEGUI::MouseButton convertButton(OIS::MouseButtonID buttonID)
{
  switch (buttonID)
  {
  case OIS::MB_Left:
      return CEGUI::LeftButton;
 
  case OIS::MB_Right:
      return CEGUI::RightButton;
 
  case OIS::MB_Middle:
      return CEGUI::MiddleButton;
 
  default:
      return CEGUI::LeftButton;
  } // end switch
} // CEGUI::MouseButton convertButton(OIS::MouseButtonID buttonID)

