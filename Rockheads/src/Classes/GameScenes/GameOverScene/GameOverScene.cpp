#include "stdafx.h"
#include "GameOverScene.h"
#include "../../Managers/AudioManager/AudioManager.h"

using namespace Ogre;


GameOverScene::GameOverScene()
{
  this->quit             = false;
  this->questionActive   = false;
  this->frameEvent       = Ogre::FrameEvent();
} // GameOverScene::GameOverScene()


void GameOverScene::enter()
{
  OgreFramework::getSingletonPtr()->log->logMessage("Entering GameOverScene...");

  OgreFramework::getSingleton().trayManager->hideCursor();
 
  this->sceneManager = OgreFramework::getSingletonPtr()->root->createSceneManager(ST_GENERIC, "GameOverSceneManager");
  this->sceneManager->setAmbientLight(Ogre::ColourValue(0.7f, 0.7f, 0.7f));

  this->camera = this->sceneManager->createCamera("PauseCam");
  this->camera->setPosition(Vector3(0, 25, -50));
  this->camera->lookAt(Vector3(0, 0, 0));
  this->camera->setNearClipDistance(1);

  this->camera->setAspectRatio(Real(OgreFramework::getSingletonPtr()->viewport->getActualWidth()) /
    Real(OgreFramework::getSingletonPtr()->viewport->getActualHeight()));

  OgreFramework::getSingletonPtr()->viewport->setCamera(this->camera);

  this->quit = false;

  this->buildGUI();
  this->createScene();
} // void GameOverScene::enter()


void GameOverScene::buildGUI()
{ // Builds the GUI of the scene.
  CEGUI::WindowManager &winManager = CEGUI::WindowManager::getSingleton();
  winManager.destroyAllWindows();

  OgreFramework::getSingleton().trayManager->destroyAllWidgets();

  CEGUI::Window* sheet = winManager.createWindow("DefaultWindow","Sheet");

  // Label of the Game Over Scene.
  CEGUI::Window* title = CEGUI::WindowManager::getSingleton().createWindow(OgreFramework::getSingleton().GUILook + "/StaticText");
  title->setText("Game Over");
  title->setHorizontalAlignment(CEGUI::HA_CENTRE);
  title->setVerticalAlignment(CEGUI::VA_TOP);
  title->setYPosition(CEGUI::UDim(0,60));
  title->setSize(CEGUI::UVector2(CEGUI::UDim(0.35, 0), CEGUI::UDim(0.15, 0)));
  title->setProperty("HorzFormatting","HorzCentred");
  title->setProperty("VertFormatting","VertCentred");
  title->setFont("NorthwoodHigh-48");
  sheet->addChildWindow(title);

  OgreFramework* framework = OgreFramework::getSingletonPtr();

  CEGUI::Window* quitToMenu = winManager.createWindow(framework->GUILook + "/Button", "QuitToMenuButton");
  quitToMenu->setText("Quit To Main Menu");
  quitToMenu->setSize(CEGUI::UVector2(CEGUI::UDim(0.19, 0), CEGUI::UDim(0.07, 0)));
  quitToMenu->setHorizontalAlignment(CEGUI::HA_CENTRE);
  quitToMenu->setYPosition(CEGUI::UDim(0.50,0));
  quitToMenu->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&GameOverScene::buttonHit, this));
  sheet->addChildWindow(quitToMenu);
  

  CEGUI::System::getSingleton().setGUISheet(sheet);
} // void GameOverScene::buildGUI()


void GameOverScene::createScene()
{

} // void GameOverScene::createScene()


void GameOverScene::exit()
{
  OgreFramework::getSingletonPtr()->log->logMessage("Leaving GameOverScene...");

  this->sceneManager->destroyCamera(this->camera);
  if(this->sceneManager)
  {
    OgreFramework::getSingletonPtr()->root->destroySceneManager(this->sceneManager);
  } // end if

  CEGUI::WindowManager::getSingleton().destroyAllWindows();


  OgreFramework::getSingletonPtr()->trayManager->clearAllTrays();
  OgreFramework::getSingletonPtr()->trayManager->destroyAllWidgets();
  OgreFramework::getSingletonPtr()->trayManager->setListener(0);


} // void GameOverScene::exit()


bool GameOverScene::keyPressed(const OIS::KeyEvent &_keyEventRef)
{
  if(OgreFramework::getSingletonPtr()->keyboard->isKeyDown(OIS::KC_ESCAPE) && !this->questionActive)
  {
    this->quit = true;
    return true;
  } // end if

  OgreFramework::getSingletonPtr()->keyPressed(_keyEventRef);

  return true;
} // bool GameOverScene::keyPressed(const OIS::KeyEvent &_keyEventRef)


bool GameOverScene::keyReleased(const OIS::KeyEvent &_keyEventRef)
{
  OgreFramework::getSingletonPtr()->keyReleased(_keyEventRef);

  return true;
} // bool GameOverScene::keyReleased(const OIS::KeyEvent &_keyEventRef)


bool GameOverScene::mouseMoved(const OIS::MouseEvent &_evt)
{
  if(OgreFramework::getSingletonPtr()->CEGUIMouseMoved(_evt))
  {
    return true;
  } // end if
  return true;
} // bool GameOverScene::mouseMoved(const OIS::MouseEvent &_evt)


bool GameOverScene::mousePressed(const OIS::MouseEvent &_evt, OIS::MouseButtonID _id)
{
  if(OgreFramework::getSingletonPtr()->CEGUIMousePressed(_evt, _id))
  {
    return true;
  } // end if
  return true;
} // bool GameOverScene::mousePressed(const OIS::MouseEvent &_evt, OIS::MouseButtonID _id)


bool GameOverScene::mouseReleased(const OIS::MouseEvent &_evt, OIS::MouseButtonID _id)
{
  if(OgreFramework::getSingletonPtr()->CEGUIMouseReleased(_evt, _id))
  {
    return true;
  } // end if

  return true;
} // bool GameOverScene::mouseReleased(const OIS::MouseEvent &_evt, OIS::MouseButtonID _id)


void GameOverScene::update(double _timeSinceLastFrame)
{
  this->frameEvent.timeSinceLastFrame = (Ogre::Real)_timeSinceLastFrame;
  if(this->quit == true)
  {
    popScene();
    return;
  } // end if
} // void GameOverScene::update(double _timeSinceLastFrame)


void GameOverScene::yesNoDialogClosed(const Ogre::DisplayString& _question, bool _yesHit)
{
    if(_yesHit == true)
    {
        this->shutdown();
    } // end if
    else
    {
        //OgreFramework::getSingletonPtr()->trayManager->closeDialog();
    } // end else

    this->questionActive = false;
} // void GameOverScene::yesNoDialogClosed(const Ogre::DisplayString& _question, bool _yesHit)


bool GameOverScene::buttonHit(const CEGUI::EventArgs &_evt)
{
  const CEGUI::WindowEventArgs* args = static_cast<const CEGUI::WindowEventArgs*>(&_evt);
  CEGUI::Window *button = args->window;

  AudioManager::getSingleton().playSound("ClickButton");

  if(button->getName() == "QuitToMenuButton")
  {
    AudioManager::getSingleton().stopSound("Death");
    this->popAllAndPushScene(this->findByName("MenuScene"));
  } // end else if

  return true;

} // void GameOverScene::buttonHit(const CEGUI::EventArgs &_evt)


void GameOverScene::releaseGameScene()
{

} // void GameOverScene::releaseGameScene()