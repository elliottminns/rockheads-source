#include "../../PrecompiledHeaders/stdafx.h"
#ifndef ABSTRACT_SCENE_H
#define ABSTRACT_SCENE_H

#include "../OgreFramework/OgreFramework.h"


class AbstractScene;

class SceneListener
{
public:
	SceneListener(){};
	virtual ~SceneListener(){};

	virtual void manageScene(Ogre::String _sceneName, AbstractScene *_scene) = 0;

	virtual AbstractScene*	findByName(Ogre::String _stateName) = 0;
	virtual void		changeScene(AbstractScene* _scene) = 0;
	virtual bool		pushScene(AbstractScene* _scene) = 0;
	virtual void		popScene() = 0;
	virtual void		pauseScene() = 0;
	virtual void		beginShutdown() = 0;
  virtual void    popAllAndPushScene(AbstractScene* _scene) = 0;
}; // end class AppStateListener


class AbstractScene : public OIS::KeyListener, public OIS::MouseListener//, public OgreBites::SdkTrayListener
{
public:
	static void create(SceneListener* _parent, const Ogre::String name){};

	void destroy(){delete this;}

	virtual void enter() = 0;
	virtual void exit() = 0;
	virtual bool pause(){return true;}
	virtual void resume(){};
	virtual void update(double _timeSinceLastFrame) = 0;
  Ogre::SceneManager* getSceneManager(){return this->sceneManager;};

  Ogre::Camera* getCamera(){return this->camera;};

protected:
	AbstractScene(){};

	AbstractScene*	findByName(Ogre::String _sceneName){return parent->findByName(_sceneName);}
	void		changeScene(AbstractScene* _scene){parent->changeScene(_scene);}
	bool		pushScene(AbstractScene* _scene){return parent->pushScene(_scene);}
	void		popScene(){parent->popScene();}
	void		shutdown(){parent->beginShutdown();}
  void    popAllAndPushScene(AbstractScene* _scene){parent->popAllAndPushScene(_scene);}
  
	SceneListener*			  parent;

	Ogre::Camera*				  camera;
	Ogre::SceneManager*   sceneManager;
  Ogre::FrameEvent      frameEvent;

}; // end class AbstractScene

#define DECLARE_SCENE_CLASS(T)										\
static void create(SceneListener* _parent, const Ogre::String _name)	\
{																		\
	T* myScene = new T();											\
	myScene->parent = _parent;										\
	_parent->manageScene(_name, myScene);							\
}

#endif

