#include "../../../PrecompiledHeaders/stdafx.h"
#ifndef LOADING_SCENE_H
#define LOADING_SCENE_H

#include "../AbstractScene.h"

class LoadingScene : public AbstractScene
{
private:
  bool quit;
  bool initialised;
  CEGUI::ProgressBar* loadingBar;

public:
  LoadingScene();

	DECLARE_SCENE_CLASS(LoadingScene)

	void enter();
	void createScene();
  void buildGUI();
	void exit();

	bool keyPressed(const OIS::KeyEvent &_keyEventRef);
	bool keyReleased(const OIS::KeyEvent &_keyEventRef);

	bool mouseMoved(const OIS::MouseEvent &_evt);
	bool mousePressed(const OIS::MouseEvent &_evt, OIS::MouseButtonID _id);
	bool mouseReleased(const OIS::MouseEvent &_evt, OIS::MouseButtonID _id);

	void update(double _timeSinceLastFrame);
  void resume();

  bool buttonHit(const CEGUI::EventArgs &_args);

  void loadGameManagers();
  void loadFonts();

  void addToLoadingBar(double _amount);

  double getLoadingBarProgress();

};

#endif