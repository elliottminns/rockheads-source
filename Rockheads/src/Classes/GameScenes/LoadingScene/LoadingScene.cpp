#include "stdafx.h"
#include "LoadingScene.h"
#include "../../Managers/SkillManager/SkillManager.h"
#include "../../Managers/ItemManager/ItemManager.h"
#include "../../Managers/MobManager/MobManager.h"
#include "../../Managers/QuestManager/QuestManager.h"
#include "../../Managers/StaticEntityManager/StaticEntityManager.h"
#include "../../Managers/GUIManager/GUIManager.h"
#include "../../Managers/ParticleManager/ParticleManager.h"
#include "../../Managers/DamageTextManager/DamageTextManager.h"
#include "../../Managers/SaveManager/SaveManager.h"
#include "../../Managers/AudioManager/AudioManager.h"

using namespace Ogre;

LoadingScene::LoadingScene()
{
  this->quit        = false;
  this->initialised = false;
  this->frameEvent  = Ogre::FrameEvent();
} // LoadingScene::MenuState()


void LoadingScene::enter()
{
  OgreFramework::getSingletonPtr()->log->logMessage("Entering LoadingScene...");

  this->sceneManager = OgreFramework::getSingletonPtr()->root->createSceneManager(ST_GENERIC, "LoadingSceneMgr");
  this->sceneManager->setAmbientLight(Ogre::ColourValue(0.7f, 0.7f, 0.7f));

  this->camera = this->sceneManager->createCamera("MenuCam");
  this->camera->setPosition(Vector3(0, 25, -50));
  this->camera->lookAt(Vector3(0, 0, 0));
  this->camera->setNearClipDistance(1);

  this->camera->setAspectRatio(Real(OgreFramework::getSingletonPtr()->viewport->getActualWidth()) /
    Real(OgreFramework::getSingletonPtr()->viewport->getActualHeight()));

  OgreFramework::getSingletonPtr()->viewport->setCamera(this->camera);

  this->buildGUI();
  this->createScene();
  this->loadFonts();
  this->loadGameManagers();
  this->loadingBar->setProgress(1.0f);
  OgreFramework::getSingleton().root->renderOneFrame();
} // void LoadingScene::enter()


void LoadingScene::createScene()
{

} // void LoadingScene::createScene()


void LoadingScene::buildGUI()
{

  OgreFramework* framework = OgreFramework::getSingletonPtr();

  // Get the window manager and destroy the windows to clean up the area.
  CEGUI::WindowManager &winManager = CEGUI::WindowManager::getSingleton();
  winManager.destroyAllWindows();

  // Hide the mouse cursor.
  CEGUI::MouseCursor::getSingletonPtr()->hide();
  
  // Create the main sheet.
  CEGUI::Window *sheet = winManager.createWindow("DefaultWindow", "Sheet");
  
  CEGUI::System::getSingleton().setGUISheet(sheet);

  // Create a progress bar.
  this->loadingBar = (CEGUI::ProgressBar*)winManager.createWindow(OgreFramework::getSingleton().GUILook + "/ProgressBar",
    "LoadingBar");
  this->loadingBar->setSize(CEGUI::UVector2(CEGUI::UDim(0.4,0), CEGUI::UDim(0.1,0)));
  this->loadingBar->setHorizontalAlignment(CEGUI::HA_CENTRE);
  this->loadingBar->setVerticalAlignment(CEGUI::VA_CENTRE);
  this->loadingBar->setProgress(0.0f);
  sheet->addChildWindow(this->loadingBar);

  // Load one font.
  if(!CEGUI::FontManager::getSingleton().isDefined("NorthwoodHigh-48"))
  {
    CEGUI::FontManager::getSingleton().createFreeTypeFont("NorthwoodHigh-48", 
      48, true, "NorthwoodHigh.ttf", "Fonts");
  } // end if

  // Create loading text.
  CEGUI::Window* loadingText = winManager.createWindow(OgreFramework::getSingleton().GUILook + "/StaticText", "LoadingText");
  loadingText->setSize(CEGUI::UVector2(CEGUI::UDim(0.4,0), CEGUI::UDim(0.1,0)));
  loadingText->setHorizontalAlignment(CEGUI::HA_CENTRE);
  loadingText->setVerticalAlignment(CEGUI::VA_CENTRE);
  loadingText->setText("Loading");
  loadingText->setFont("NorthwoodHigh-48");
  loadingText->setYPosition(CEGUI::UDim(0.1,0));
  loadingText->setProperty("HorzFormatting", "HorzCentred");
  loadingText->setProperty("VertFormatting", "VertCentred");
  loadingText->setProperty("FrameEnabled", "False");
  loadingText->setProperty("BackgroundEnabled", "False");
  sheet->addChildWindow(loadingText);

} // void LoadingScene::buildGUI()


void LoadingScene::loadGameManagers()
{
  if(!this->initialised)
  {
    ItemManager::getInstance();
    this->addToLoadingBar(0.03);
    OgreFramework::getSingleton().log->logMessage("Item Manager Initialised");

    MobManager::getInstance()->initialise();
    this->addToLoadingBar(0.03);
    OgreFramework::getSingleton().log->logMessage("Mob Manager Initialised");

    QuestManager::getInstance();
    this->addToLoadingBar(0.03);
    OgreFramework::getSingleton().log->logMessage("Quest Manager Initialised");

    StaticEntityManager::getInstance();
    OgreFramework::getSingleton().log->logMessage("Static Entity Manager Initialised");
    new CollisionManager();
    OgreFramework::getSingleton().log->logMessage("Collision Manager Initialised");
    this->addToLoadingBar(0.03);

    SkillManager::getInstance()->initialise();
    this->addToLoadingBar(0.03);
    OgreFramework::getSingleton().log->logMessage("Skill Manager Initialised");

    new GUIManager();
    this->addToLoadingBar(0.03);
    OgreFramework::getSingleton().log->logMessage("GUI Manager Initialised");

    ParticleManager::getSingletonPtr();
    this->addToLoadingBar(0.03);
    OgreFramework::getSingleton().log->logMessage("Particle Manager Initialised");

    DamageTextManager::getSingletonPtr();
    this->addToLoadingBar(0.03);
    OgreFramework::getSingleton().log->logMessage("Damage Text Manager Initialised");

    SaveManager::getSingletonPtr();
    this->addToLoadingBar(0.03);
    OgreFramework::getSingleton().log->logMessage("Save Manager Initialised");

    AudioManager::getSingletonPtr();
    OgreFramework::getSingleton().log->logMessage("Audio Manager Initialised");

    this->initialised = true;
  } // end if
} // void LoadingScene::loadGameManangers()


void LoadingScene::exit()
{
  OgreFramework::getSingletonPtr()->log->logMessage("Leaving LoadingState...");

  this->sceneManager->destroyCamera(this->camera);
  if(this->sceneManager)
  {
    OgreFramework::getSingletonPtr()->root->destroySceneManager(this->sceneManager);
  } // end if

  CEGUI::WindowManager &winManager = CEGUI::WindowManager::getSingleton();
  winManager.destroyAllWindows();

} // void LoadingScene::exit()


bool LoadingScene::keyPressed(const OIS::KeyEvent &_keyEventRef)
{
  if(OgreFramework::getSingletonPtr()->keyboard->isKeyDown(OIS::KC_ESCAPE))
  {
    this->quit = true;
    return true;
  } // end if

  if(OgreFramework::getSingletonPtr()->CEGUIKeyPressed(_keyEventRef))
  {
    return true;
  } // end if

  OgreFramework::getSingletonPtr()->keyPressed(_keyEventRef);
  return true;
} // bool LoadingScene::keyPressed(const OIS::KeyEvent &_keyEventRef)


bool LoadingScene::keyReleased(const OIS::KeyEvent &_keyEventRef)
{
  if(OgreFramework::getSingletonPtr()->CEGUIKeyReleased(_keyEventRef))
  {
    return true;
  } // end if

  OgreFramework::getSingletonPtr()->keyReleased(_keyEventRef);
  return true;
} // bool LoadingScene::keyReleased(const OIS::KeyEvent &_keyEventRef)


bool LoadingScene::mouseMoved(const OIS::MouseEvent &_evt)
{
  if(OgreFramework::getSingletonPtr()->CEGUIMouseMoved(_evt))
  {
    return true;
  } // end if

  OgreFramework::getSingletonPtr()->mouseMoved(_evt);

  return true;
} // bool LoadingScene::mouseMoved(const OIS::MouseEvent &_evt)

bool LoadingScene::mousePressed(const OIS::MouseEvent &_evt, OIS::MouseButtonID _id)
{

  if(OgreFramework::getSingletonPtr()->CEGUIMousePressed(_evt, _id))
  {
    return true;
  } // end if

  OgreFramework::getSingletonPtr()->mousePressed(_evt, _id);

  return true;
} // bool LoadingScene::mousePressed(const OIS::MouseEvent &_evt, OIS::MouseButtonID _id)


bool LoadingScene::mouseReleased(const OIS::MouseEvent &_evt, OIS::MouseButtonID _id)
{
  if(OgreFramework::getSingletonPtr()->CEGUIMouseReleased(_evt, _id))
  {
    return true;
  } // end if

  OgreFramework::getSingletonPtr()->mouseReleased(_evt, _id);

  return true;
} // bool LoadingScene::mouseReleased(const OIS::MouseEvent &_evt, OIS::MouseButtonID _id)


void LoadingScene::update(double _timeSinceLastFrame)
{
  this->frameEvent.timeSinceLastFrame = (Ogre::Real)_timeSinceLastFrame;

  this->changeScene(this->findByName("MenuScene"));
} // void LoadingScene::update(double _timeSinceLastFrame)


bool LoadingScene::buttonHit(const CEGUI::EventArgs &_evt)
{
  const CEGUI::WindowEventArgs* args = static_cast<const CEGUI::WindowEventArgs*>(&_evt);

  return true;
} // void LoadingScene::setQuitOn()


void LoadingScene::loadFonts()
{
    // Load the title font.
  if(!CEGUI::FontManager::getSingleton().isDefined("DejaVuSans-28"))
  {
    CEGUI::FontManager::getSingleton().createFreeTypeFont("DejaVuSans-28", 28, true, "DejaVuSans.ttf", "Fonts");
  } // end if

  // Load the title font.
  if(!CEGUI::FontManager::getSingleton().isDefined("DejaVuSans-12"))
  {
    CEGUI::FontManager::getSingleton().createFreeTypeFont("DejaVuSans-12", 12, true, "DejaVuSans.ttf", "Fonts");
  } // end if

  // Load a smaller font.
  if(!CEGUI::FontManager::getSingleton().isDefined("DejaVuSans-10"))
  {
    CEGUI::FontManager::getSingleton().createFreeTypeFont("DejaVuSans-10", 10, true, "DejaVuSans.ttf", "Fonts");
  } // end if

  // Load a smaller font.
  if(!CEGUI::FontManager::getSingleton().isDefined("DejaVuSans-8"))
  {
    CEGUI::FontManager::getSingleton().createFreeTypeFont("DejaVuSans-8", 8, true, "DejaVuSans.ttf", "Fonts");
  } // end if

  for(int i=6;i<35;i++)
  {
    if(!CEGUI::FontManager::getSingleton().isDefined("NorthwoodHigh-"+CEGUI::PropertyHelper::intToString(i)))
    {
      CEGUI::FontManager::getSingleton().createFreeTypeFont("NorthwoodHigh-"+CEGUI::PropertyHelper::intToString(i), 
        i, true, "NorthwoodHigh.ttf", "Fonts");
    } // end if
  } // end for


  Ogre::FontPtr mFont = Ogre::FontManager::getSingleton().create("DamageFont", "Fonts");
  mFont->setType(Ogre::FT_TRUETYPE);
  mFont->setSource("DamageFont.ttf");
  mFont->setTrueTypeSize(36);

  CEGUI::System::getSingleton().setDefaultFont("DejaVuSans-12");
} // void LoadingScene::loadFonts()


void LoadingScene::resume()
{
  OgreFramework::getSingletonPtr()->log->logMessage("Resuming LoadingScene...");

  this->buildGUI();

  OgreFramework::getSingletonPtr()->viewport->setCamera(this->camera);
  this->quit = false;
} // void LoadingScene::resume()


void LoadingScene::addToLoadingBar(double _amount)
{
  this->loadingBar->adjustProgress(_amount);
  OgreFramework::getSingleton().root->renderOneFrame();

} // void LoadingScene::addToLoadingBar(double _amount)


double LoadingScene::getLoadingBarProgress()
{
  return this->loadingBar->getProgress();
} // double LoadingScene::getLoadingBarProgress()