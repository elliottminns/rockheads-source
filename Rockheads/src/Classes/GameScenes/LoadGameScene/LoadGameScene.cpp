#include "stdafx.h"

#include "LoadGameScene.h"
#include "../../Managers/SaveManager/SaveManager.h"
#include "../../Saving/Save/Save.h"
#include "../../Managers/GUIManager/GUIManager.h"
#include "../../Managers/AudioManager/AudioManager.h"

using namespace Ogre;


LoadGameScene::LoadGameScene()
{
  this->quit             = false;
  this->frameEvent       = Ogre::FrameEvent();
  this->selectedSave     = NULL;
  this->flashTimer = 0;
} // LoadGameScene::LoadGameScene()


void LoadGameScene::enter()
{
  SaveManager::getSingleton().release();
  SaveManager::getSingleton().initialise();
  OgreFramework::getSingletonPtr()->log->logMessage("Entering LoadGameScene...");

  OgreFramework::getSingleton().trayManager->hideCursor();

  this->sceneManager = OgreFramework::getSingletonPtr()->root->createSceneManager(ST_GENERIC, "LoadGameSceneManager");
  this->sceneManager->setAmbientLight(Ogre::ColourValue(0.7f, 0.7f, 0.7f));

  this->camera = this->sceneManager->createCamera("PauseCam");
  this->camera->setPosition(Vector3(0, 25, -50));
  this->camera->lookAt(Vector3(0, 0, 0));
  this->camera->setNearClipDistance(1);

  this->camera->setAspectRatio(Real(OgreFramework::getSingletonPtr()->viewport->getActualWidth()) /
    Real(OgreFramework::getSingletonPtr()->viewport->getActualHeight()));

  OgreFramework::getSingletonPtr()->viewport->setCamera(this->camera);

  this->quit = false;

  this->buildGUI();
  this->createScene();
} // void LoadGameScene::enter()


void LoadGameScene::buildGUI()
{
  CEGUI::WindowManager &winManager = CEGUI::WindowManager::getSingleton();
  winManager.destroyAllWindows();

  OgreFramework::getSingleton().trayManager->destroyAllWidgets();

  CEGUI::Window* sheet = winManager.createWindow("DefaultWindow","Sheet");
  string GUILook = OgreFramework::getSingleton().GUILook;

  OgreFramework* framework = OgreFramework::getSingletonPtr();

  // Return to game button.
  CEGUI::Window* returnToGame = winManager.createWindow(framework->GUILook + "/Button", "ReturnToMainMenu");
  returnToGame->setText("Return To Main Menu");
  returnToGame->setSize(CEGUI::UVector2(CEGUI::UDim(0.19, 0), CEGUI::UDim(0.07, 0)));
  returnToGame->setHorizontalAlignment(CEGUI::HA_CENTRE);
  returnToGame->setVerticalAlignment(CEGUI::VA_BOTTOM);
  returnToGame->setYPosition(CEGUI::UDim(0,-40));
  returnToGame->subscribeEvent(CEGUI::PushButton::EventClicked, 
    CEGUI::Event::Subscriber(&LoadGameScene::buttonHit, this));
  sheet->addChildWindow(returnToGame);

  // Create the save game title.
  CEGUI::Window* title = CEGUI::WindowManager::getSingleton().createWindow(framework->GUILook + "/StaticText");
  title->setText("Load Game");
  title->setHorizontalAlignment(CEGUI::HA_CENTRE);
  title->setYPosition(CEGUI::UDim(0,20));
  title->setSize(CEGUI::UVector2(CEGUI::UDim(0.3, 0), CEGUI::UDim(0.1, 0)));
  title->setProperty("HorzFormatting","HorzCentred");
  title->setProperty("VertFormatting","VertCentred");
  title->setFont("NorthwoodHigh-34");
  sheet->addChildWindow(title);

  // Add an outline border.
  CEGUI::Window* window = 
    CEGUI::WindowManager::getSingleton().createWindow(framework->GUILook + "/SimpleTransparentPanel", "Border");
  window->setHorizontalAlignment(CEGUI::HA_CENTRE);
  window->setVerticalAlignment(CEGUI::VA_CENTRE);
  window->setSize(CEGUI::UVector2(CEGUI::UDim(0.7,0), CEGUI::UDim(0.7,0)));
  sheet->addChildWindow(window);

  // Add a static image border.
  CEGUI::Window* border = winManager.createWindow(GUILook + "/StaticImage", "Saves/Border");
  border->setHorizontalAlignment(CEGUI::HA_CENTRE);
  border->setVerticalAlignment(CEGUI::VA_TOP);
  border->setSize(CEGUI::UVector2(CEGUI::UDim(1.0,0), CEGUI::UDim(0.6,0)));
  window->addChildWindow(border);

  // Add a scroll frame.
  CEGUI::ScrollablePane* scrollPane = (CEGUI::ScrollablePane*)winManager.createWindow(
    OgreFramework::getSingleton().GUILook+"/ScrollablePane", "Saves/Scroll");
  CEGUI::UVector2 scrollSize = CEGUI::UVector2(CEGUI::UDim(1.0,0), CEGUI::UDim(1.0,0));
  scrollPane->setHorizontalAlignment(CEGUI::HA_CENTRE);
  scrollPane->setSize(scrollSize);
  scrollPane->setClippedByParent(true);
  scrollPane->setMouseInputPropagationEnabled(true);
  scrollPane->setShowVertScrollbar(false);
  scrollPane->setShowHorzScrollbar(false);
  border->addChildWindow(scrollPane);

  // Pre define the size of the save slots.
  const int xSizeSlot  = 150;
  const int ySizeSlot  = 150;
  const int xImageSlot = 145;
  const int yImageSlot = 145;
  CEGUI::UVector2 slotSize  = CEGUI::UVector2(CEGUI::UDim(0,xSizeSlot),CEGUI::UDim(0,ySizeSlot));
  CEGUI::UVector2 imageSize = CEGUI::UVector2(CEGUI::UDim(0,xImageSlot),CEGUI::UDim(0,yImageSlot));

  int sizeOfPane = OgreFramework::getSingletonPtr()->viewport->getActualWidth()*0.7;
  const int numSlotsX = sizeOfPane/xSizeSlot;

  // Load in the image.
  CEGUI::ImagesetManager::getSingleton().createFromImageFile("SaveSlotHighlight.png", "SaveSlotHighlight.png");

  // Add a save slot outline.
  CEGUI::Window* saveSlotOutline = winManager.createWindow(GUILook + "/StaticImage",
    "OutlineSaveWindow");
  saveSlotOutline->setSize(slotSize);
  saveSlotOutline->setMouseInputPropagationEnabled(true);
  saveSlotOutline->setClippedByParent(true);
  saveSlotOutline->setProperty("Image", "set:SaveSlotHighlight.png image:full_image");
  saveSlotOutline->setVisible(false);
  saveSlotOutline->setProperty("FrameEnabled", "False");
  saveSlotOutline->setProperty("BackgroundEnabled", "False");
  saveSlotOutline->moveToBack();
  scrollPane->addChildWindow(saveSlotOutline);

  std::vector<Save*>* saves = SaveManager::getSingleton().getSaves();
  std::vector<Save*>::iterator saveIt = saves->begin();

  // Add Slots for each save.
  for(int i=0;i < SaveManager::getSingleton().getNumSaves();i++)
  {
    CEGUI::Window* saveSlot = winManager.createWindow(GUILook + "/StaticText", 
      "SaveSlot-" + CEGUI::PropertyHelper::intToString(i));
    saveSlot->setSize(slotSize);
    saveSlot->setMouseInputPropagationEnabled(true);
    saveSlot->setClippedByParent(true);
    Save* save = *saveIt;
    saveSlot->setText(save->getSaveName().c_str());
    saveSlot->setFont("NorthwoodHigh-18");
    saveSlot->setAlwaysOnTop(true);
    saveSlot->setProperty("HorzFormatting", "WordWrapCentred");

    // Subscribe events.
    saveSlot->subscribeEvent(CEGUI::Window::EventMouseClick,
      CEGUI::Event::Subscriber(&LoadGameScene::handleSaveSlotClicked,this));

    // Set the xPositon of the slot.
    int xPos = xSizeSlot*((i)%numSlotsX);
    int yPos = ySizeSlot*((i)/numSlotsX)+3;

    // Place the position of the slot and add it to the window.
    saveSlot->setPosition(CEGUI::UVector2(CEGUI::UDim(0,xPos),CEGUI::UDim(0,yPos)));
    scrollPane->addChildWindow(saveSlot);

    ++saveIt;
  } // end for

  // Create the text box and save buttons area.
  CEGUI::Window* area = winManager.createWindow(GUILook + "/StaticImage", "Saves/InputArea");
  area->setHorizontalAlignment(CEGUI::HA_CENTRE);
  area->setVerticalAlignment(CEGUI::VA_BOTTOM);
  area->setSize(CEGUI::UVector2(CEGUI::UDim(1.0,0), CEGUI::UDim(0.4,0)));
  window->addChildWindow(area);

  // Create a editable Save Name field.
  int saveNameBoxWidth  = 300;
  int saveNameBoxHeight = 35;
  CEGUI::Editbox* saveNameBox = (CEGUI::Editbox *)winManager.createWindow(GUILook + "/Editbox", "SaveNameBox");
  saveNameBox->setText("");
  saveNameBox->setMaxTextLength(13);
  saveNameBox->setReadOnly(true);
  saveNameBox->setTextMasked(false);
  saveNameBox->setSize(CEGUI::UVector2(CEGUI::UDim(0, saveNameBoxWidth), CEGUI::UDim(0, saveNameBoxHeight)));
  saveNameBox->setHorizontalAlignment(CEGUI::HA_CENTRE);
  saveNameBox->setYPosition(CEGUI::UDim(0, 30));
  saveNameBox->setMaskCodePoint(0x002A);
  saveNameBox->setProperty("TextFormatting","HorzCentred");
  saveNameBox->subscribeEvent(CEGUI::Editbox::EventTextChanged, 
    CEGUI::Event::Subscriber(&LoadGameScene::handleEditBoxTypedIn, this));
  area->addChildWindow(saveNameBox);

  // Create a save button.
  CEGUI::Window* saveGameButton = winManager.createWindow(GUILook + "/Button", "LoadButton");
  saveGameButton->setText("Load");
  saveGameButton->setSize(CEGUI::UVector2(CEGUI::UDim(0, 100), CEGUI::UDim(0, 45)));
  saveGameButton->setHorizontalAlignment(CEGUI::HA_CENTRE);
  saveGameButton->setVerticalAlignment(CEGUI::VA_BOTTOM);
  saveGameButton->setYPosition(CEGUI::UDim(0, -15));
  saveGameButton->subscribeEvent(CEGUI::PushButton::EventClicked, 
    CEGUI::Event::Subscriber(&LoadGameScene::buttonHit, this));
  saveGameButton->disable();
  area->addChildWindow(saveGameButton);

  CEGUI::System::getSingleton().setGUISheet(sheet);
} // void LoadGameScene::buildGUI()


void LoadGameScene::createScene()
{

} // void LoadGameScene::createScene()


void LoadGameScene::exit()
{
  OgreFramework::getSingletonPtr()->log->logMessage("Leaving LoadGameScene...");

  this->sceneManager->destroyCamera(this->camera);
  if(this->sceneManager)
  {
    OgreFramework::getSingletonPtr()->root->destroySceneManager(this->sceneManager);
  } // end if

  CEGUI::WindowManager::getSingleton().destroyAllWindows();

} // void LoadGameScene::exit()


bool LoadGameScene::keyPressed(const OIS::KeyEvent &_keyEventRef)
{
  if(OgreFramework::getSingletonPtr()->keyboard->isKeyDown(OIS::KC_ESCAPE))
  {
    this->quit = true;
    return true;
  } // end if
  else if(OgreFramework::getSingletonPtr()->CEGUIKeyPressed(_keyEventRef))
  { // Inject the CEGUI key press.
    return true;
  } // else if

  OgreFramework::getSingletonPtr()->keyPressed(_keyEventRef);

  return true;
} // bool LoadGameScene::keyPressed(const OIS::KeyEvent &_keyEventRef)


bool LoadGameScene::keyReleased(const OIS::KeyEvent &_keyEventRef)
{
  OgreFramework::getSingletonPtr()->keyReleased(_keyEventRef);

  return true;
} // bool LoadGameScene::keyReleased(const OIS::KeyEvent &_keyEventRef)


bool LoadGameScene::mouseMoved(const OIS::MouseEvent &_evt)
{
  if(OgreFramework::getSingletonPtr()->CEGUIMouseMoved(_evt))
  {
    return true;
  } // end if
  return true;
} // bool LoadGameScene::mouseMoved(const OIS::MouseEvent &_evt)


bool LoadGameScene::mousePressed(const OIS::MouseEvent &_evt, OIS::MouseButtonID _id)
{
  if(OgreFramework::getSingletonPtr()->CEGUIMousePressed(_evt, _id))
  {
    return true;
  } // end if
  return true;
} // bool LoadGameScene::mousePressed(const OIS::MouseEvent &_evt, OIS::MouseButtonID _id)


bool LoadGameScene::mouseReleased(const OIS::MouseEvent &_evt, OIS::MouseButtonID _id)
{
  if(OgreFramework::getSingletonPtr()->CEGUIMouseReleased(_evt, _id))
  {
    return true;
  } // end if

  return true;
} // bool LoadGameScene::mouseReleased(const OIS::MouseEvent &_evt, OIS::MouseButtonID _id)


void LoadGameScene::update(double _timeSinceLastFrame)
{
  this->frameEvent.timeSinceLastFrame = (Ogre::Real)_timeSinceLastFrame;
  CEGUI::WindowManager& winManager = CEGUI::WindowManager::getSingleton();

  // Flash the highlight display.
  if(this->selectedSave)
  {
    this->flashTimer += _timeSinceLastFrame;
  } // end if
  if(this->flashTimer > 250)
  {
    this->flashTimer-= 250;
    winManager.getWindow("OutlineSaveWindow")->setVisible(
      !winManager.getWindow("OutlineSaveWindow")->isVisible());
  } // end if

  if(this->quit == true)
  {
    winManager.destroyAllWindows();
    popScene();
    return;
  } // end if
} // void LoadGameScene::update(double _timeSinceLastFrame)


void LoadGameScene::loadSaveScene(string _sceneName)
{
  this->changeScene(findByName(_sceneName.c_str()));
} // void LoadGameScene::loadSaveScene(string _sceneName)


bool LoadGameScene::buttonHit(const CEGUI::EventArgs &_evt)
{
  const CEGUI::WindowEventArgs* args = static_cast<const CEGUI::WindowEventArgs*>(&_evt);
  CEGUI::Window *button = args->window;
  CEGUI::WindowManager& winManager = CEGUI::WindowManager::getSingleton();

  AudioManager::getSingleton().playSound("SlashButton");

  if(button->getName() == "ReturnToMainMenu")
  {
    AudioManager::getSingleton().playSound("ClickButton");
    this->quit = true;
  } // end else if
  else if(button->getName() == "LoadButton")
  {
    AudioManager::getSingleton().playSound("SlashButton");
    string saveName = winManager.getWindow("SaveNameBox")->getText().c_str();
    SaveManager::getSingleton().loadGame(saveName, this);
  } // end else if

  return true;

} // void LoadGameScene::buttonHit(const CEGUI::EventArgs &_evt)


bool LoadGameScene::handleSaveSlotClicked(const CEGUI::EventArgs &_e)
{
  const CEGUI::WindowEventArgs* args = static_cast<const CEGUI::WindowEventArgs*>(&_e);
  CEGUI::Window* slotWindow = args->window;
  CEGUI::WindowManager& winManager = CEGUI::WindowManager::getSingleton();

  // Move the outline to the slot location.
  winManager.getWindow("OutlineSaveWindow")->setPosition(args->window->getPosition());
  winManager.getWindow("OutlineSaveWindow")->setVisible(true);

  // Disable the editbox and set the text to be the save name.
  winManager.getWindow("SaveNameBox")->setEnabled(false);
  int slotIndex = GUIManager::getSingleton().getSlotEndNumber(args->window);
  Save* save = SaveManager::getSingleton().getSaveAt(slotIndex);
  winManager.getWindow("SaveNameBox")->setText(save->getSaveName());
  this->selectedSave = save;

  AudioManager::getSingleton().playSound("ClickButton");

  return true;
} // bool LoadGameScene::handleSaveSlotClicked(const CEGUI::EventArgs &_e)


bool LoadGameScene::handleEditBoxTypedIn(const CEGUI::EventArgs& _e)
{
  const CEGUI::WindowEventArgs* args = static_cast<const CEGUI::WindowEventArgs*>(&_e);
  CEGUI::Editbox* editBox = (CEGUI::Editbox*)args->window;
  CEGUI::WindowManager& winManager = CEGUI::WindowManager::getSingleton();

  if(editBox->getText().length() <= 0)
  {
    
    winManager.getWindow("LoadButton")->enable();
  } // end if
  else
  {
    winManager.getWindow("LoadButton")->enable();
  } // end else 

  return true;
} // bool LoadGameScene::handleEditBoxTypedIn(const CEGUI::EventArgs& _e)