#include "../../../PrecompiledHeaders/stdafx.h"
#ifndef LOAD_GAME_SCENE_H
#define LOAD_GAME_SCENE_H

#include "../AbstractScene.h"

class Save;

class LoadGameScene : public AbstractScene
{
private:
  bool quit;
  Save* selectedSave;
  int flashTimer;

public:
  LoadGameScene();
  
  DECLARE_SCENE_CLASS(LoadGameScene)

  void enter();
  void createScene();
  void buildGUI();
  void exit();

  bool keyPressed(const OIS::KeyEvent &_keyEventRef);
  bool keyReleased(const OIS::KeyEvent &_keyEventRef);

  bool mouseMoved(const OIS::MouseEvent &_evt);
  bool mousePressed(const OIS::MouseEvent &_evt, OIS::MouseButtonID _id);
  bool mouseReleased(const OIS::MouseEvent &_evt, OIS::MouseButtonID _id);
  
  bool buttonHit(const CEGUI::EventArgs &_evt); 
  bool handleSaveSlotClicked(const CEGUI::EventArgs &_e);
  bool handleEditBoxTypedIn(const CEGUI::EventArgs& _e);

  void update(double _timeSinceLastFrame);

  void loadSaveScene(string _sceneName);

}; // end class PauseScene : public AbstractScene


#endif