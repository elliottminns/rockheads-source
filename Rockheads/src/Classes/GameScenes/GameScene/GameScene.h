#include "../../../PrecompiledHeaders/stdafx.h"
#ifndef GAME_SCENE_H
#define GAME_SCENE_H

#include "../AbstractScene.h"

#include "../../DotSceneLoader/DotSceneLoader.h"
#include "../../Managers/GameManager/GameManager.h"
#include "../../Managers/CollisionManager/CollisionManager.h"
#include "../../GameEntities/DynamicEntities/Mob/Mob.h"
#include "../../HotBar/HotBar.h"


class GameScene : public AbstractScene
{
public:
	GameScene();
  ~GameScene();

	DECLARE_SCENE_CLASS(GameScene)

	void enter();
	void createScene();
	void exit();
	bool pause();
	void resume();

	void moveCamera();
	void getInput();
  void buildGUI();

	bool keyPressed(const OIS::KeyEvent &_keyEventRef);
	bool keyReleased(const OIS::KeyEvent &_keyEventRef);

	bool mouseMoved(const OIS::MouseEvent &_arg);
	bool mousePressed(const OIS::MouseEvent &_arg, OIS::MouseButtonID _id);
	bool mouseReleased(const OIS::MouseEvent &_arg, OIS::MouseButtonID _id);

	void onLeftPressed(const OIS::MouseEvent &_evt);

	void update(double _timeSinceLastFrame);

  bool frameCloseButtonHit(const CEGUI::EventArgs& _e);

  void buildInventory();
  void buildCharacterWindow();
  void buildInspector();
  void buildJournalWindow();
  void buildSkillsWindow();
  void updateInspector(AbstractItem* _item);
  void updateInspector(Skill* _skill);

  // Events
  bool handleItemDroppedEmptyInventory(const CEGUI::EventArgs& _e);
  bool handleItemDroppedContainerInventory(const CEGUI::EventArgs& _e);
  bool handleItemDroppedEmptyEquipment(const CEGUI::EventArgs& _e);
  bool handleItemDroppedContainerEquipment(const CEGUI::EventArgs& _e);
  bool handleItemDroppedContainer(const CEGUI::EventArgs& _e);
  bool handleDragBegan(const CEGUI::EventArgs& _e);
  bool handleDragEnded(const CEGUI::EventArgs& _e);
  bool handleInvSlotClicked(const CEGUI::EventArgs& _e);
  bool handleMouseOver(const CEGUI::EventArgs& _e);
  bool handleMouseLeave(const CEGUI::EventArgs& _e);
  bool handleWindowButtonClicked(const CEGUI::EventArgs& _e);

  void switchToSaveScene();

  int  getSlotLocation(CEGUI::Window* _slot);

  void setDeathTimer(int _timer);

private:

	bool						      quit;

	Ogre::RaySceneQuery*	rsq;

	bool						      leftMouseDown, rightMouseDown;

  Player*               player;
  HotBar*               hotBar;
  
  int  sceneCounter;
  int deathTimer;

  // Inventory window attributes.
  CEGUI::FrameWindow* inventoryWindow;
  bool inventoryOpen;
  CEGUI::UVector2 inventoryPosition;
  CEGUI::UVector2 inventorySize;

  // Inventory window attributes.
  CEGUI::FrameWindow* characterWindow;
  bool characterWindowOpen;
  CEGUI::UVector2 characterWindowPosition;
  CEGUI::UVector2 characterWindowSize;

  // Item Inspector.
  CEGUI::FrameWindow* inspector;
  bool inspectorOpen;
  CEGUI::UVector2 inspectorPosition;
  CEGUI::UVector2 inspectorSize;

  // Journal Window.
  CEGUI::FrameWindow* journalWindow;
  bool journalOpen;
  CEGUI::UVector2 journalPosition;
  CEGUI::UVector2 journalSize;

  // Skills Window.
  CEGUI::FrameWindow* skillsWindow;
  bool skillsWindowOpen;
  CEGUI::UVector2 skillsWindowPosition;
  CEGUI::UVector2 skillsWindowSize;

  // Boolean to handle multiple swapping and repositioning.
  bool swapsBegan;
  bool equipmentReplaced;
  int  firstSwappedLocation;
  int  lastSwappedLocation;
  InventorySlot* heldSlot;

  // Light.
  Ogre::Light* pointLight;

  // Collision Manager.
  CollisionManager* collisionManager;

};

#endif