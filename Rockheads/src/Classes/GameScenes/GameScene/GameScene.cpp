#include "stdafx.h"
#include "GameScene.h"

#include "../../Managers/ItemManager/ItemManager.h"
#include "../../GameObjects/InventorySlot/InventorySlot.h"
#include "../../GameObjects/Items/Armour/Armour.h"
#include "../../GameObjects/Items/Weapon/Weapon.h"
#include "../../Managers/CollisionManager/CollisionManager.h"
#include "../../GameEntities/DynamicEntities/Mob/Mob.h"
#include "../../Managers/MobManager/MobManager.h"
#include "../../Managers/StaticEntityManager/StaticEntityManager.h"
#include "../../Managers/CollisionManager/QueryFlags.h"
#include "../../GameEntities/StaticEntities/NPC/NPC.h"
#include "../../Managers/GUIManager/GUIManager.h"
#include "../../GameEntities/StaticEntities/NPC/Vendor/Vendor.h"
#include "../../GameEntities/StaticEntities/NPC/Vendor/VendorNPC.h"
#include "../../GameEntities/DynamicEntities/EntityClass/Skill/Skill.h"
#include "../../Managers/ParticleManager/ParticleManager.h"
#include "../../Managers/DamageTextManager/DamageTextManager.h"
#include "../../Managers/AudioManager/AudioManager.h"
#include <sstream>

using namespace Ogre;

GameScene::GameScene()
{
  this->leftMouseDown   = false;
  this->rightMouseDown  = false;
  this->quit            = false;
  this->player = Player::getInstance();
  this->sceneCounter = 10;
  this->deathTimer = 0;

  // Inventory initialise.
  this->inventoryWindow   = NULL;
  this->inventoryOpen     = false;
  this->inventorySize     = CEGUI::UVector2(CEGUI::UDim(0, 180), CEGUI::UDim(0, 243));
  this->inventoryPosition = CEGUI::UVector2(CEGUI::UDim(0, 5), CEGUI::UDim(0, -5));
  this->swapsBegan        = false;
  this->heldSlot          = NULL;

  // Character window initialise
  this->characterWindow         = NULL;
  this->characterWindowOpen     = false;
  this->characterWindowSize     = CEGUI::UVector2(CEGUI::UDim(0,180), CEGUI::UDim(0,300));
  this->characterWindowPosition = CEGUI::UVector2(CEGUI::UDim(0,215), CEGUI::UDim(0,-5));

  // Item inspector initialisation.
  this->inspector         = NULL;
  this->inspectorOpen     = false;
  this->inspectorSize     = CEGUI::UVector2(CEGUI::UDim(0,190),CEGUI::UDim(0,235));;
  this->inspectorPosition = CEGUI::UVector2(CEGUI::UDim(0.3-this->inspectorSize.d_x.d_scale/2,0),
    CEGUI::UDim(0.3-this->inspectorSize.d_y.d_scale/2,0));;

  // Journal Window Initialisation.
  this->journalWindow = NULL;
  this->journalOpen = false;
  this->journalPosition = CEGUI::UVector2(CEGUI::UDim(0,-5), CEGUI::UDim(0,-60));
  this->journalSize = CEGUI::UVector2(CEGUI::UDim(0, 240), CEGUI::UDim(0, 300));

  // Skills Window Initialisation.
  this->skillsWindow = NULL;
  this->skillsWindowOpen = false;
  this->skillsWindowPosition = CEGUI::UVector2(CEGUI::UDim(0,-85), CEGUI::UDim(0,-5));
  this->skillsWindowSize = CEGUI::UVector2(CEGUI::UDim(0, 300), CEGUI::UDim(0, 200));

  // Create the hotbar.
  this->hotBar = new HotBar;

} // GameScene::GameScene()


GameScene::~GameScene()
{
  
} // GameScene::~GameScene()


void GameScene::enter()
{
  this->sceneCounter = 10;
  this->deathTimer = 0;
  HotBar::getSingleton().clearHotBar();
  srand((unsigned)time(NULL));
  OgreFramework::getSingletonPtr()->log->logMessage("Entering GameScene...");

  this->sceneManager = OgreFramework::getSingletonPtr()->root->createSceneManager(ST_GENERIC, "GameSceneMgr");

  // Paticle systems, need to go into the manager.
  ParticleManager::getSingleton().createParticleSystems(this->sceneManager);
 
  this->camera = this->sceneManager->createCamera("GameCamera");
  this->camera->setPosition(Vector3(5, 100, 40));
  this->camera->lookAt(Vector3(5, 20, 0));
  this->camera->setNearClipDistance(5);

  this->camera->setAspectRatio(Real(OgreFramework::getSingletonPtr()->viewport->getActualWidth()) /
    Real(OgreFramework::getSingletonPtr()->viewport->getActualHeight()));

  OgreFramework::getSingletonPtr()->viewport->setCamera(this->camera);
  
  // Initialise the collision manager.
  this->collisionManager = CollisionManager::getSingletonPtr();
  this->collisionManager->initialise(this->sceneManager);

  createScene();
  buildGUI();

} // void GameScene::enter()


bool GameScene::pause()
{
  OgreFramework::getSingletonPtr()->log->logMessage("Pausing GameScene...");
  OgreFramework::getSingletonPtr()->viewport->setBackgroundColour(ColourValue(0.2f, 0.2f, 0.2f, 1.0f));
  AudioManager::getSingleton().stopSound("CaveAmbience");
  return true;
} // bool GameScene::pause()


void GameScene::resume()
{
  OgreFramework::getSingletonPtr()->log->logMessage("Resuming GameScene...");
  OgreFramework::getSingletonPtr()->viewport->setBackgroundColour(Ogre::ColourValue(0,0,0,1));
  AudioManager::getSingleton().playSound("CaveAmbience");
  this->buildGUI();


  OgreFramework::getSingletonPtr()->viewport->setCamera(this->camera);
  this->quit = false;

} // void GameScene::resume()


void GameScene::exit()
{
  OgreFramework::getSingletonPtr()->log->logMessage("Leaving GameScene...");

  ParticleManager::getSingleton().releaseParticleSystems();

  this->sceneManager->destroyCamera(this->camera);
  this->sceneManager->destroyQuery(this->rsq);
  if(this->sceneManager)
  {
    OgreFramework::getSingletonPtr()->viewport->setBackgroundColour(ColourValue(0.2f, 0.2f, 0.2f, 1.0f));
    OgreFramework::getSingletonPtr()->root->destroySceneManager(this->sceneManager);
  } // end if

  // Release all the mobs, static entities and player.
  this->player->release();
  this->player = NULL;
  MobManager::getInstance()->releaseVector();
  StaticEntityManager::getInstance()->releaseEntities();

} // void GameScene::exit()


void GameScene::createScene()
{
  srand((unsigned)time(NULL));
  // Create a ray scene query for collision detection.
  this->rsq = this->sceneManager->createRayQuery(Ogre::Ray(), Ogre::SceneManager::LIGHT_TYPE_MASK);
  this->rsq->setSortByDistance(true);
  OgreFramework::getSingletonPtr()->rsq = this->rsq;
  OgreFramework::getSingletonPtr()->viewport->setBackgroundColour(Ogre::ColourValue(0,0,0,1));

  AudioManager::getSingleton().stopSound("Intro");
  AudioManager::getSingleton().playSound("CaveAmbience");

  this->sceneManager->setAmbientLight(Ogre::ColourValue(0.0,0.0,0.0));
  this->sceneManager->setShadowTechnique(Ogre::SHADOWTYPE_STENCIL_ADDITIVE);

  DotSceneLoader* pDotSceneLoader = new DotSceneLoader();
  pDotSceneLoader->parseDotScene("map.scene", "General", this->sceneManager, this->sceneManager->getRootSceneNode());
  delete pDotSceneLoader;

  // Add some fog to the scene.
  Ogre::ColourValue fadeColour(0.9, 0.9, 0.95);

  this->player = Player::getInstance();
  //GameManager::getInstance()->setPlayer(NULL);

  if(!this->player)
  {
    std::exit(901);
  } // end if

  // Create all the player model attributes and assign them using the appropriate methods within the class.
  this->player->loadEntity(this->sceneManager->createEntity("PlayerEntity", this->player->getModelName()));
  this->player->setNode(this->sceneManager->getRootSceneNode()->createChildSceneNode("PlayerNode"));
  this->player->attachEntityToNode();
  this->player->setCurrentAnimationState(this->player->getEntity()->getAnimationState("Idle"));
  this->player->getEntity()->setCastShadows(true);

  // Create an arrow model and attach it to the knee. For the lulz.
  if(this->player->getGender() == "Male")
  {
    Ogre::Entity* arrowEntity  = this->sceneManager->createEntity("ArrowEntity", "arrow.mesh");

    this->player->getEntity()->attachObjectToBone("HLMCA_L_TRACK", arrowEntity);
  } // end if
  this->player->setPosition(Ogre::Vector3(0,8,0));
  
  this->pointLight = this->sceneManager->createLight("PointLight");
  this->pointLight->setType(Ogre::Light::LT_POINT);
  this->pointLight->setPosition(Ogre::Vector3(0,10,0));
  this->pointLight->setDiffuseColour(1.0,1.0,1.0);
  this->pointLight->setSpecularColour(1.0,1.0,1.0);

  // Create the base plate.
  Ogre::Entity* targetPlate = this->sceneManager->createEntity("TargetPlate", "target-ring.mesh");
  Ogre::SceneNode* targetNode = this->sceneManager->getRootSceneNode()->createChildSceneNode("TargetPlateNode");
  targetNode->attachObject(targetPlate);
  targetPlate->removeQueryFlags(targetPlate->getQueryFlags());
  this->sceneManager->getRootSceneNode()->removeChild(targetNode);

  MobManager::getInstance()->initialiseMobs();
  OgreFramework::getSingleton().timer->reset();
  

} // void GameScene::createScene()


bool GameScene::keyPressed(const OIS::KeyEvent &_keyEventRef)
{
  if(OgreFramework::getSingletonPtr()->keyboard->isKeyDown(OIS::KC_SPACE))
  {
    this->player->interact();
  } // end if
  if(OgreFramework::getSingletonPtr()->keyboard->isKeyDown(OIS::KC_A))
  {
     this->player->setMovingLeft(true);
  } // end if

  if(OgreFramework::getSingletonPtr()->keyboard->isKeyDown(OIS::KC_D))
  {
    this->player->setMovingRight(true);
  } // end if

  if(OgreFramework::getSingletonPtr()->keyboard->isKeyDown(OIS::KC_W))
  {
    this->player->setMovingUp(true);
  } // end if

  if(OgreFramework::getSingletonPtr()->keyboard->isKeyDown(OIS::KC_S))
  {
    this->player->setMovingDown(true);
  } // end if

  if(OgreFramework::getSingletonPtr()->keyboard->isKeyDown(OIS::KC_I))
  {
    this->inventoryWindow->setVisible(!this->inventoryWindow->isVisible());
    this->inventoryOpen = this->inventoryWindow->isVisible();
    if(this->inventoryOpen)
    {
      AudioManager::getSingleton().playSound("Item Picked Up");
    } // end if
    else
    {
      AudioManager::getSingleton().playSound("Item Put Down");
    } // end else
    this->inventoryWindow->moveToFront();
  } // end if

  if(OgreFramework::getSingletonPtr()->keyboard->isKeyDown(OIS::KC_J))
  {
    this->journalWindow->setVisible(!this->journalWindow->isVisible());
    this->journalOpen = this->journalWindow->isVisible();
    if(this->journalOpen)
    {
      AudioManager::getSingleton().playSound("Item Picked Up");
    } // end if
    else
    {
      AudioManager::getSingleton().playSound("Item Put Down");
    } // end else
    this->journalWindow->moveToFront();
  } // end if

  if(OgreFramework::getSingletonPtr()->keyboard->isKeyDown(OIS::KC_C))
  {
    this->characterWindow->setVisible(!this->characterWindow->isVisible());
    this->characterWindowOpen = this->characterWindow->isVisible();
    if(this->characterWindowOpen)
    {
      AudioManager::getSingleton().playSound("Item Picked Up");
    } // end if
    else
    {
      AudioManager::getSingleton().playSound("Item Put Down");
    } // end else
    this->characterWindow->moveToFront();
  } // end if

  if(OgreFramework::getSingletonPtr()->keyboard->isKeyDown(OIS::KC_K))
  {
    this->skillsWindowOpen = !this->skillsWindowOpen;
    this->skillsWindow->setVisible(this->skillsWindowOpen);
    if(this->skillsWindowOpen)
    {
      AudioManager::getSingleton().playSound("Item Picked Up");
    } // end if
    else
    {
      AudioManager::getSingleton().playSound("Item Put Down");
    } // end else
    
    this->skillsWindow->moveToFront();
  } // end if

  if(OgreFramework::getSingletonPtr()->keyboard->isKeyDown(OIS::KC_ESCAPE))
  {
    this->inventoryPosition = this->inventoryWindow->getPosition();
    this->journalPosition = this->journalWindow->getPosition();
    this->skillsWindowPosition = this->skillsWindow->getPosition();
    pushScene(findByName("PauseScene"));
    return true;
  } // end if

  if(OgreFramework::getSingletonPtr()->keyboard->isKeyDown(OIS::KC_1))
  {
    this->hotBar->activateHotSlot(0);
    AudioManager::getSingleton().playSound("ClickButton");
  } // end if
  if(OgreFramework::getSingletonPtr()->keyboard->isKeyDown(OIS::KC_2))
  {
    this->hotBar->activateHotSlot(1);
    AudioManager::getSingleton().playSound("ClickButton");
  } // end if
  if(OgreFramework::getSingletonPtr()->keyboard->isKeyDown(OIS::KC_3))
  {
    this->hotBar->activateHotSlot(2);
    AudioManager::getSingleton().playSound("ClickButton");
  } // end if
  if(OgreFramework::getSingletonPtr()->keyboard->isKeyDown(OIS::KC_4))
  {
    this->hotBar->activateHotSlot(3);
    AudioManager::getSingleton().playSound("ClickButton");
  } // end if
  if(OgreFramework::getSingletonPtr()->keyboard->isKeyDown(OIS::KC_5))
  {
    this->hotBar->activateHotSlot(4);
    AudioManager::getSingleton().playSound("ClickButton");
  } // end if
  if(OgreFramework::getSingletonPtr()->keyboard->isKeyDown(OIS::KC_6))
  {
    this->hotBar->activateHotSlot(5);
    AudioManager::getSingleton().playSound("ClickButton");
  } // end if
  if(OgreFramework::getSingletonPtr()->keyboard->isKeyDown(OIS::KC_7))
  {
    this->hotBar->activateHotSlot(6);
    AudioManager::getSingleton().playSound("ClickButton");
  } // end if
  if(OgreFramework::getSingletonPtr()->keyboard->isKeyDown(OIS::KC_8))
  {
    this->hotBar->activateHotSlot(7);
    AudioManager::getSingleton().playSound("ClickButton");
  } // end if

  return true;
} // bool GameScene::keyPressed(const OIS::KeyEvent &_keyEventRef)


bool GameScene::keyReleased(const OIS::KeyEvent &_keyEventRef)
{
  OgreFramework::getSingletonPtr()->keyPressed(_keyEventRef);

  if(!OgreFramework::getSingletonPtr()->keyboard->isKeyDown(OIS::KC_A))
  {
     this->player->setMovingLeft(false);
  } // end if

  if(!OgreFramework::getSingletonPtr()->keyboard->isKeyDown(OIS::KC_D))
  {
    this->player->setMovingRight(false);
  } // end if

  if(!OgreFramework::getSingletonPtr()->keyboard->isKeyDown(OIS::KC_W))
  {
    this->player->setMovingUp(false);
  } // end if

  if(!OgreFramework::getSingletonPtr()->keyboard->isKeyDown(OIS::KC_S))
  {
    this->player->setMovingDown(false);
  } // end if

  return true;
} // bool GameScene::keyReleased(const OIS::KeyEvent &_keyEventRef)


bool GameScene::mouseMoved(const OIS::MouseEvent &_evt)
{
  if(OgreFramework::getSingletonPtr()->CEGUIMouseMoved(_evt))
  {
    return true;
  } // end if
  return true;
} // bool GameScene::mouseMoved(const OIS::MouseEvent &_evt)


bool GameScene::mousePressed(const OIS::MouseEvent &_evt, OIS::MouseButtonID _id)
{
  if(OgreFramework::getSingletonPtr()->CEGUIMousePressed(_evt, _id))
  {
    if(this->swapsBegan)
    {
      int x=0;
    } // if(this->swapsBegan)

    return true;
  } // end if
  if(_id == OIS::MB_Left)
  {
    this->onLeftPressed(_evt);
    this->leftMouseDown = true;
  } // end if
  else if(_id == OIS::MB_Right)
  {
    this->rightMouseDown = true;
  } // end else if

  return true;
} // bool GameScene::mousePressed(const OIS::MouseEvent &evt, OIS::MouseButtonID id)


bool GameScene::mouseReleased(const OIS::MouseEvent &_evt, OIS::MouseButtonID _id)
{
  if(OgreFramework::getSingletonPtr()->CEGUIMouseReleased(_evt, _id))
  {
    return true;
  } // end if

  if(_id == OIS::MB_Left)
  {
    this->leftMouseDown = false;
  } // end if
  else if(_id == OIS::MB_Right)
  {
    this->rightMouseDown = false;
  } // end else if

  return true;
} // bool GameScene::mouseReleased(const OIS::MouseEvent &evt, OIS::MouseButtonID id)


void GameScene::onLeftPressed(const OIS::MouseEvent &_evt)
{
  this->player->attack();
} // void GameScene::onLeftPressed(const OIS::MouseEvent &_evt)


void GameScene::moveCamera()
{
  this->camera->setPosition(this->player->getPosition());
  Ogre::Vector3 cameraMove = Ogre::Vector3(40,50,50);
  this->camera->move(cameraMove);
  this->camera->lookAt(player->getPosition());
} // void GameScene::moveCamera()


void GameScene::getInput()
{ // Use this method for unbuffered input to allow for more than one input command each frame. 

} // void GameScene::getInput()


void GameScene::update(double _timeSinceLastFrame)
{
  this->frameEvent.timeSinceLastFrame = (Ogre::Real)_timeSinceLastFrame;

  // Temporary fix into collision not working on the first instance.
  // Render one frame also not working.
  if(this->sceneCounter > 0)
  {
    this->sceneCounter--;

    if(this->sceneCounter <= 0)
    {
      StaticEntityManager::getInstance()->calculateEntitiesY();
      //MobManager::getInstance()->initialiseMobs();
      MobManager::getInstance()->calculateInitialY();
    } // end if
  } // end if

  OgreFramework::getSingletonPtr()->trayManager->frameRenderingQueued(this->frameEvent);

  if(this->quit == true)
  {
    this->popScene();
    return;
  } // end if

  this->player->update(_timeSinceLastFrame);

  this->pointLight->setPosition(this->player->getPosition().x, this->player->getPosition().y+40, this->player->getPosition().z);
  MobManager::getInstance()->updateMobs(_timeSinceLastFrame);
  StaticEntityManager::getInstance()->update(_timeSinceLastFrame);
  this->hotBar->update(_timeSinceLastFrame);

  // Unbuffered input.
  getInput();
  // Camera moving functions.
  moveCamera();

  DamageTextManager::getSingleton().update(_timeSinceLastFrame);

  MessageDispatcher::getInstance()->dispatchDelayedMessages();
  
  if(!player->getAlive() && this->deathTimer <= 0)
  {
    this->deathTimer = 5000;
  } // end if
  if(this->deathTimer > 0)
  {
    this->deathTimer -= _timeSinceLastFrame;

    if(this->deathTimer <= 0)
    {
      this->pushScene(this->findByName("GameOverScene"));
    } // end if
  } // end if

} // void GameScene::update(double _timeSinceLastFrame)


void GameScene::buildGUI()
{
  string GUILook = OgreFramework::getSingleton().GUILook;

  // Build the inventory GUI.
  // Clean the windows and show the cursor.
  CEGUI::WindowManager::getSingletonPtr()->destroyAllWindows();
  CEGUI::MouseCursor::getSingletonPtr()->show();

  // Create the default window.
  CEGUI::WindowManager& winManager = CEGUI::WindowManager::getSingleton();
  CEGUI::Window* sheet = winManager.createWindow("DefaultWindow","Sheet");
  CEGUI::System::getSingleton().setGUISheet(sheet);
  sheet->setMousePassThroughEnabled(true);
  
  this->buildInventory();
  this->buildCharacterWindow();
  this->buildInspector();
  
  this->buildJournalWindow();
  GUIManager::getSingleton().buildHotBar();
  this->buildSkillsWindow();
  GUIManager::getSingleton().buildCharacterDisplay();
  GUIManager::getSingleton().updateExperienceDisplay();
  GUIManager::getSingleton().updateHealthDisplay();
  GUIManager::getSingleton().updateStaminaDisplay();
  GUIManager::getSingleton().updateCurrencyAmount();
  GUIManager::getSingleton().buildActiveEffectsWindow();
  GUIManager::getSingleton().buildTargettedEnemyWindow();
  GUIManager::getSingleton().buildWindowButtons();
  
} // void GameScene::buildGUI()


void GameScene::buildInventory()
{
  // Create the inventory.
  CEGUI::WindowManager& winManager = CEGUI::WindowManager::getSingleton();
  this->inventoryWindow = (CEGUI::FrameWindow*)winManager.getWindow(
    GUIManager::getSingleton().buildInventoryWindow(this->inventoryWindow, 
    this->inventorySize, this->inventoryPosition, this->inventoryOpen));
  
} // void GameScene::buildInventory()


void GameScene::buildCharacterWindow()
{
  // Create the character window.
  CEGUI::WindowManager& winManager = CEGUI::WindowManager::getSingleton();
  this->characterWindow = (CEGUI::FrameWindow*)winManager.getWindow(
    GUIManager::getSingleton().buildCharacterWindow(this->characterWindow,
    this->characterWindowSize, this->characterWindowPosition, this->characterWindowOpen));

} // void GameScene::buildCharacterWindow(CEGUI::Window* sheet)


void GameScene::buildInspector()
{
  string GUILook = OgreFramework::getSingleton().GUILook;

  // Create the default window.
  CEGUI::WindowManager& winManager = CEGUI::WindowManager::getSingleton();

  CEGUI::Window* sheet = winManager.getWindow("Sheet");

  // Create a frame window to serve as our inventory.
  this->inspector = (CEGUI::FrameWindow*)winManager.createWindow(GUILook + "/FrameWindow", "Inspector");
  this->inspector->setSize(this->inspectorSize);
  this->inspector->setPosition(this->inspectorPosition);
  this->inspector->setVisible(this->inspectorOpen);
  this->inspector->setSizingEnabled(false);
  this->inspector->setCloseButtonEnabled(true);
  this->inspector->setText(" ");
  this->inspector->subscribeEvent(CEGUI::FrameWindow::EventCloseClicked, 
    CEGUI::Event::Subscriber(&GameScene::frameCloseButtonHit, this));
  sheet->addChildWindow(this->inspector);

  // Add item inspector elements - icon image.
  CEGUI::Window* imageIcon = winManager.createWindow(GUILook + "/StaticImage", "Inspector/ImageIcon");
  imageIcon->setSize(CEGUI::UVector2(CEGUI::UDim(0,40), CEGUI::UDim(0,40)));
  imageIcon->setProperty("BackgroundEnabled", "False");
  imageIcon->setProperty("FrameEnabled", "False");
  imageIcon->setPosition(CEGUI::UVector2(CEGUI::UDim(0,5), CEGUI::UDim(0,5)));
  this->inspector->addChildWindow(imageIcon);

  // Add item inspector elements - item name..
  CEGUI::Window* nameText = winManager.createWindow(GUILook + "/StaticText", "Inspector/NameText");
  nameText->setSize(CEGUI::UVector2(CEGUI::UDim(0,100), CEGUI::UDim(0,35)));
  nameText->setProperty("BackgroundEnabled", "False");
  nameText->setProperty("FrameEnabled", "False");
  nameText->setPosition(CEGUI::UVector2(CEGUI::UDim(0,60), CEGUI::UDim(0,10)));
  nameText->setProperty("HorzFormatting", "WordWrapCentred");
  nameText->setProperty("VertFormatting", "VertCentred");
  nameText->setFont("DejaVuSans-10");
  this->inspector->addChildWindow(nameText);

  // Add item inspector elements - item description.
  CEGUI::Window* descText = winManager.createWindow(GUILook + "/StaticText", "Inspector/DescriptionText");
  descText->setSize(CEGUI::UVector2(CEGUI::UDim(0.85,0), CEGUI::UDim(0,125)));
  descText->setProperty("BackgroundEnabled", "False");
  descText->setProperty("FrameEnabled", "False");
  descText->setHorizontalAlignment(CEGUI::HA_CENTRE);
  descText->setVerticalAlignment(CEGUI::VA_BOTTOM);
  descText->setProperty("HorzFormatting", "WordWrapLeftAligned");
  descText->setProperty("VertFormatting", "TopAligned");
  descText->setFont("DejaVuSans-10");
  this->inspector->addChildWindow(descText);

} // void GameScene::buildinspector()


void GameScene::buildJournalWindow()
{
  // Create the Journal window.
  CEGUI::WindowManager& winManager = CEGUI::WindowManager::getSingleton();
  this->journalWindow = (CEGUI::FrameWindow*)winManager.getWindow(
    GUIManager::getSingleton().buildJournalWindow(this->journalWindow,
    this->journalSize, this->journalPosition, this->journalOpen));
} // void GameScene::buildJournalWindow()


void GameScene::buildSkillsWindow()
{
  // Create the skills window.
  CEGUI::WindowManager& winManager = CEGUI::WindowManager::getSingleton();

  this->skillsWindow = (CEGUI::FrameWindow*)winManager.getWindow(
    GUIManager::getSingleton().buildSkillsWindow(this->skillsWindowSize,
    this->skillsWindowPosition, this->skillsWindowOpen));

} // void GameScene::buildSkillsWindow()


void GameScene::updateInspector(AbstractItem* _item)
{
  // Get the attributes.
  CEGUI::Window* imageIcon = this->inspector->getChild("Inspector/ImageIcon");
  CEGUI::Window* nameText  = this->inspector->getChild("Inspector/NameText");
  CEGUI::Window* descText  = this->inspector->getChild("Inspector/DescriptionText");
  this->inspector->moveToFront();

  // Set the attributes.
  imageIcon->setProperty("Image","set:" + _item->getImageName() + " image:full_image");
  nameText->setText(_item->getName());
  descText->setText(_item->getDescription());
  descText->setFont("DejaVuSans-10");

} // void GameScene::updateinspector(AbstractItem* _item)


void GameScene::updateInspector(Skill* _skill)
{
  // Get the attributes.
  CEGUI::Window* imageIcon = this->inspector->getChild("Inspector/ImageIcon");
  CEGUI::Window* nameText  = this->inspector->getChild("Inspector/NameText");
  CEGUI::Window* descText  = this->inspector->getChild("Inspector/DescriptionText");
  this->inspector->moveToFront();

  // Set the attributes.
  imageIcon->setProperty("Image","set:" + _skill->getImageName() + " image:full_image");
  nameText->setText(_skill->getName());
  descText->setText(_skill->getInspectorInformation());
  descText->setFont("DejaVuSans-8");

} // void GameScene::updateinspector(AbstractItem* _item)


bool GameScene::frameCloseButtonHit(const CEGUI::EventArgs& _e)
{
  const CEGUI::WindowEventArgs* args = static_cast<const CEGUI::WindowEventArgs*>(&_e);

  if(args->window->getName() == this->inventoryWindow->getName())
  {
    this->inventoryOpen = false;
    this->inventoryWindow->setVisible(this->inventoryOpen);
  } // end if
  if(args->window->getName() == this->characterWindow->getName())
  {
    this->characterWindowOpen = false;
    this->characterWindow->setVisible(this->characterWindowOpen);
  } // end if
  if(args->window->getName() == this->inspector->getName())
  {
    this->inspectorOpen = false;
    this->inspector->setVisible(this->inspectorOpen);
  } // end if
  if(args->window->getName() == "QuestWindow")
  {
    args->window->destroy();
  } // end if
  if(args->window->getName() == "Journal")
  {
    this->journalOpen = false;
    this->journalWindow->setVisible(this->journalOpen);
    CEGUI::WindowManager& winManager = CEGUI::WindowManager::getSingleton();
    if(winManager.isWindowPresent("RewardInspector"))
    {
      winManager.getWindow("RewardInspector")->destroy();
    } // end if
  }

  return true;
} // bool GameScene::frameCloseButtonHit(const CEGUI::EventArgs& _e)


bool GameScene::handleItemDroppedEmptyInventory(const CEGUI::EventArgs& _e)
{
  // cast the args to the 'real' type so we can get access to extra fields
  const CEGUI::DragDropEventArgs& dd_args = static_cast<const CEGUI::DragDropEventArgs&>(_e);
  
  // If the slot is empty.
  if(!dd_args.window->getChildCount())
  {
    AudioManager::getSingleton().playSound("Item Put Down");

    this->equipmentReplaced = false;

    CEGUI::String parentName = dd_args.dragDropItem->getParent()->getName().substr(0,7);
    
    // If the item was dropped from another inventory slot.
    if(parentName == "InvSlot")
    {
      int slot1 = 0;
      int slot2 = 0;
      if(this->swapsBegan)
      {
        slot1 = this->firstSwappedLocation;
        slot2 = this->getSlotLocation(dd_args.window);     
      } // end if
      else
      {
        slot1 = this->getSlotLocation(dd_args.dragDropItem->getParent());
        slot2 = this->getSlotLocation(dd_args.window);
      } // end else
      this->player->getInventory()->swapSlots(slot1, slot2);
    } // end if(parentName == "InvSlot")
    else
    { // item was dropped from an equipment slot.
      int slot1 = this->getSlotLocation(dd_args.window);
      AbstractItem* equippedItem = this->player->unequipItem(dd_args.dragDropItem->getParent()->getName().c_str(), 
        this->sceneManager);
      this->player->getInventory()->addItemAtSlot(equippedItem,1,slot1);
    } // end else

    // add dragdrop item as child of target if target has no item already
    dd_args.window->addChildWindow(dd_args.dragDropItem);

    // Now we must reset the item position from it's 'dropped' location,
    // since we're now a child of an entirely different window
    dd_args.dragDropItem->setHorizontalAlignment(CEGUI::HA_CENTRE);
    dd_args.dragDropItem->setVerticalAlignment(CEGUI::VA_CENTRE);
  } // end else if(!dd_args.window->getChildCount())
  
  this->swapsBegan = false;
  GUIManager::getSingleton().setDragDropStarted(false);
  return true;

} // bool GameScene::handleItemDroppedEmptyInventory(const CEGUI::EventArgs& args)


bool GameScene::handleItemDroppedContainerInventory(const CEGUI::EventArgs& _e)
{
  // Cast the _e to the drag drop event type so we can get access to extra fields
  const CEGUI::DragDropEventArgs& dd_args = static_cast<const CEGUI::DragDropEventArgs&>(_e);

  if(this->swapsBegan)
  {
    AudioManager::getSingleton().playSound("Item Picked Up");
  } // end if

  if(dd_args.dragDropItem->getParent()->getName().substr(0,7) == "InvSlot")
  { // If the window came from an inventory slot.
    this->equipmentReplaced = false;
    if(!this->swapsBegan)
    {
      this->swapsBegan = true;
      this->firstSwappedLocation = this->getSlotLocation(dd_args.dragDropItem->getParent());
      this->lastSwappedLocation = this->getSlotLocation(dd_args.window->getParent());
    } // end if
    else
    {
      this->lastSwappedLocation = this->getSlotLocation(dd_args.window->getParent());
    } // end else

    this->heldSlot = this->player->getInventory()->getSlotByIndex(this->getSlotLocation(dd_args.window->getParent()));

    Player::getInstance()->getInventory()->swapSlots(this->firstSwappedLocation, this->lastSwappedLocation);

    // Add the dropped window onto the parent.
    dd_args.window->getParent()->addChildWindow(dd_args.dragDropItem);

  } // end if
  else
  { // Item came from the equipment place.
    
    // Set the held slot to be the picked up slot.
    this->heldSlot = this->player->getInventory()->getSlotByIndex(this->getSlotLocation(dd_args.window->getParent()));

    // Get the new slot number and the item to place in it.
    int slot1 = this->getSlotLocation(dd_args.window->getParent());
    AbstractItem* equippedItem = this->player->unequipItem(dd_args.dragDropItem->getParent()->getName().c_str(), 
      this->sceneManager);

    // Add the dropped window onto the parent.
    dd_args.window->getParent()->addChildWindow(dd_args.dragDropItem);

    // Must find an empty new parent for the old window.
    int i=-1;
    CEGUI::WindowManager* winManager = CEGUI::WindowManager::getSingletonPtr();
    CEGUI::Window* newParent = NULL;
    do
    {
      ++i;
      newParent = winManager->getWindow("InvSlot-" + CEGUI::PropertyHelper::intToString(i));  
    } while(newParent->getChildCount() > 0);

    // Add the new details to the slot. .
    this->player->getInventory()->swapSlots(i,slot1);
    this->heldSlot = this->player->getInventory()->getSlotByIndex(i);

    // Add the unquipped item to the old slot in inventory.
    this->player->getInventory()->addItemAtSlot(equippedItem,1,slot1);

    // Remove the window from the old parent and add it to the new parent
    dd_args.window->getParent()->removeChildWindow(dd_args.window->getName());
    newParent->addChildWindow(dd_args.window);

    // Now we must reset the item position from it's 'dropped' location,
    // since we're now a child of an entirely different window
    dd_args.window->setHorizontalAlignment(CEGUI::HA_CENTRE);
    dd_args.window->setVerticalAlignment(CEGUI::VA_CENTRE);

    // Set swaps began to true.
    this->swapsBegan = true;
    this->firstSwappedLocation = i;

  } // end else 

  // Now we must reset the item position from it's 'dropped' location,
  // since we're now a child of an entirely different window
  dd_args.dragDropItem->setHorizontalAlignment(CEGUI::HA_CENTRE);
  dd_args.dragDropItem->setVerticalAlignment(CEGUI::VA_CENTRE);

  // pickup the window beign dropped onto.
  CEGUI::DragContainer* oldContainer = (CEGUI::DragContainer*)dd_args.window;
  oldContainer->pickUp(true);

  return true;

} // bool GameScene::handleItemDroppedContainerInventory(const CEGUI::EventArgs& _e)


bool GameScene::handleItemDroppedContainer(const CEGUI::EventArgs& _e)
{
  // Cast the _e to the drag drop event type so we can get access to extra fields
  const CEGUI::DragDropEventArgs& dd_args = static_cast<const CEGUI::DragDropEventArgs&>(_e);

  // Check to see what type of slot it came from.
  if(dd_args.window->getParent()->getName().substr(0,7) == "InvSlot")
  {
    return this->handleItemDroppedContainerInventory(_e);
  } // end if(dd_args.window->getParent()->getName().substr(0,7) == "InvSlot")
  else
  {
    return this->handleItemDroppedContainerEquipment(_e);
  } // end else


  return true;
} // bool GameScene::handleItemDroppedContainer(const CEGUI::EventArgs& _e)


bool GameScene::handleInvSlotClicked(const CEGUI::EventArgs& _e)
{
  // cast the args to the 'real' type so we can get access to extra fields
  const CEGUI::MouseEventArgs& args = static_cast<const CEGUI::MouseEventArgs&>(_e);

  if(args.button == 1)
  {
    AudioManager::getSingleton().playSound("Item Picked Up");
    if(!this->inspectorOpen)
    {
      this->inspectorOpen = true;
      this->inspector->setVisible(this->inspectorOpen);
    } // end if
    AbstractItem* item = NULL;
    // Find out which type of window was selected in order for the player to get the item.
    if(args.window->getParent()->getName().substr(0,7) == "InvSlot")
    { // Inventory window. Select from the player inventory.
      item = this->player->getInventory()->getSlotByIndex(this->getSlotLocation(args.window->getParent()))->getItem();
    } // end if
    else
    { // Equipment window. Select from the correct equipment slot.
      item = this->player->getEquippedItemDetails(args.window->getParent()->getName().c_str());
    } // end else

    if(item)
    {
      this->updateInspector(item);
    } // end if

  } // if(args.button == CEGUI::MouseButton::RightButton)
  if(args.button == 0)
  {
    CEGUI::WindowManager& winManager = CEGUI::WindowManager::getSingleton();

    if(winManager.isWindowPresent("Shop"))
    {
      InventorySlot* item = NULL;
      // Get the inventory slot from the player.
      if(args.window->getParent()->getName().substr(0,7) == "InvSlot")
      { // Inventory window. Select from the player inventory.
        item = this->player->getInventory()->getSlotByIndex(this->getSlotLocation(args.window->getParent()));
        if(item)
        {
          GUIManager::getSingletonPtr()->mouseClickOnInvForShop(item);
        } // end if
      } // end if
    } // end if
    else
    {
      if(args.window->getParent()->getName().substr(0,7) == "InvSlot")
      {
        if(args.window->getChildCount() == 2 || args.window->getChildCount() == 1)
        {
          AudioManager::getSingleton().playSound("Item Picked Up");
        } // end if
      } // end else if
    } // end else 
  } // end if(args.button == 0)

  return true;
} // bool GameScene::handleinvSlotClicked(const CEGUI::EventArgs& args)


bool GameScene::handleItemDroppedEmptyEquipment(const CEGUI::EventArgs& _e)
{
  // cast the args to the 'real' type so we can get access to extra fields
  const CEGUI::DragDropEventArgs& dd_args = static_cast<const CEGUI::DragDropEventArgs&>(_e);

  if(dd_args.window->getChildCount() == 1)
  { 
    // First see if the move is valid.
    InventorySlot *movedSlot = this->player->getInventory()->getSlotByIndex(this->getSlotLocation(dd_args.dragDropItem->getParent()));
    if(movedSlot)
    {
      if(movedSlot->getItem()->equip(dd_args.window->getName().c_str(), this->sceneManager))
      {
        Player::getInstance()->removeFromInv(this->getSlotLocation(dd_args.dragDropItem->getParent()));

        dd_args.window->addChildWindow(dd_args.dragDropItem);

        // Play the sound.
        AudioManager::getSingleton().playSound("Item Put Down");

        // Now we must reset the item position from it's 'dropped' location,
        // since we're now a child of an entirely different window
        dd_args.dragDropItem->setHorizontalAlignment(CEGUI::HA_CENTRE);
        dd_args.dragDropItem->setVerticalAlignment(CEGUI::VA_CENTRE);
      } // end if
    } // end if
    this->swapsBegan = false;
    GUIManager::getSingleton().setDragDropStarted(false);
  } // end else if(!dd_args.window->getChildCount())

  return true;
} // bool GameScene::handleItemDroppedEmptyEquipment(const CEGUI::EventArgs& args)


bool GameScene::handleItemDroppedContainerEquipment(const CEGUI::EventArgs& _e)
{
  // First check if the item dropping on can be equipped.
  // cast the args to the 'real' type so we can get access to extra fields
  const CEGUI::DragDropEventArgs& dd_args = static_cast<const CEGUI::DragDropEventArgs&>(_e);

  // First backup the original item.
  AbstractItem *item = this->player->backupEquippedItem(dd_args.window->getParent()->getName().c_str())->clone();

  // Now see if the move is valid.
  InventorySlot *movedSlot = this->player->getInventory()->getSlotByIndex(this->getSlotLocation(dd_args.dragDropItem->getParent()));
  if(movedSlot)
  {
    if(movedSlot->getItem()->equip(dd_args.window->getParent()->getName().c_str(), this->sceneManager))
    {
      // Set the old slot to have the old equipped item.
      this->player->getInventory()->addItemAtSlot(item,1,this->getSlotLocation(dd_args.dragDropItem->getParent()));
      CEGUI::Window* oldParent = dd_args.dragDropItem->getParent();

      dd_args.window->getParent()->addChildWindow(dd_args.dragDropItem);
      dd_args.dragDropItem->setHorizontalAlignment(CEGUI::HA_CENTRE);
      dd_args.dragDropItem->setVerticalAlignment(CEGUI::VA_CENTRE);
      // pickup the window beign dropped onto.
      CEGUI::DragContainer* oldContainer = (CEGUI::DragContainer*)dd_args.window;
      oldContainer->pickUp(true);
      oldContainer->getParent()->removeChildWindow(oldContainer);
      oldParent->addChildWindow(oldContainer);

      if(this->swapsBegan)
      {
        AudioManager::getSingleton().playSound("Item Picked Up");
      } // end if
      
    } // end if
  } // end if
  else
  {
    AudioManager::getSingleton().playSound("Item Put Down");
  } // end else 


  return true;
} // bool GameScene::handleItemDroppedContainerEquipment(const CEGUI::EventArgs& _e)


bool GameScene::handleDragBegan(const CEGUI::EventArgs& _e)
{
  // cast the args to the 'real' type so we can get access to extra fields
  const CEGUI::DragDropEventArgs& dd_args = static_cast<const CEGUI::DragDropEventArgs&>(_e);

  dd_args.window->setAlpha(1.0f);
  if(dd_args.window->getParent()->getName().substr(0,7) == "InvSlot")
  {
    this->heldSlot = this->player->getInventory()->getSlotByIndex(this->getSlotLocation(dd_args.window));
  } // end if

  AudioManager::getSingleton().playSound("Item Picked Up");

  GUIManager::getSingleton().setDragDropStarted(true);

  return true;
} // bool GameScene::handleDragBegan(const CEGUI::EventArgs& _e)


bool GameScene::handleDragEnded(const CEGUI::EventArgs& _e)
{

  GUIManager::getSingleton().setDragDropStarted(false);

  const CEGUI::DragDropEventArgs& dd_args = static_cast<const CEGUI::DragDropEventArgs&>(_e);
  dd_args.window->setAlpha(1.0f);
  // If the slot has been replaced.
  if(dd_args.window->getParent()->getChildCount() > 1)
  {
    // Check if the parent was an inventory slot.
    if(dd_args.window->getParent()->getName().substr(0,7) == "InvSlot")
    {
      // Remove it from the parent.
      dd_args.window->getParent()->removeChildWindow(dd_args.window->getID());

      // Add it to the index associated.
      CEGUI::WindowManager* winManager = CEGUI::WindowManager::getSingletonPtr();
      CEGUI::Window* newParent = NULL;
      if(this->swapsBegan)
      {
        newParent = 
          winManager->getWindow("InvSlot-" + CEGUI::PropertyHelper::intToString(this->firstSwappedLocation));
        this->swapsBegan = false;
      } // end if
      else
      {
        newParent = 
          winManager->getWindow("InvSlot-" + CEGUI::PropertyHelper::intToString(this->heldSlot->getIndex()));
      } // end esle
      newParent->addChildWindow(dd_args.window);
    } // end if

  } // end if
  
  this->heldSlot = NULL;

  return true;
} // bool GameScene::handleDragEnd(const CEGUI::EventArgs& _e)


bool GameScene::handleMouseOver(const CEGUI::EventArgs& _e)
{
  const CEGUI::WindowEventArgs* args = static_cast<const CEGUI::WindowEventArgs*>(&_e);
  // Set the alpha to 0.5 when mouse is over the window.
  CEGUI::Window* slotWindow = args->window;
  slotWindow->setAlpha(0.5);

  return true;
} // bool GameScene::handleMouseOver(const CEGUI::EventArgs& _e)


bool GameScene::handleMouseLeave(const CEGUI::EventArgs& _e)
{
  const CEGUI::WindowEventArgs* args = static_cast<const CEGUI::WindowEventArgs*>(&_e);

  // Return the alpha value when the mouse pointer is removed from the window.
  CEGUI::Window* slotWindow = args->window;
  slotWindow->setAlpha(1.0);

  return true;
} // bool GameScene::handleMouseLeave(const CEGUI::EventArgs& _e)


bool GameScene::handleWindowButtonClicked(const CEGUI::EventArgs& _e)
{
  const CEGUI::WindowEventArgs* args = static_cast<const CEGUI::WindowEventArgs*>(&_e);

  // Check to see which button was pressed.
  if(args->window->getName() == "InventoryButton")
  {
    this->inventoryWindow->setVisible(!this->inventoryWindow->isVisible());
    this->inventoryOpen = this->inventoryWindow->isVisible();
    this->inventoryWindow->moveToFront();
    // Play the sound
    if(this->inventoryOpen)
    {
      AudioManager::getSingleton().playSound("Item Picked Up");
    } // end if
    else
    {
      AudioManager::getSingleton().playSound("Item Put Down");
    } // end else
  } // end if
  if(args->window->getName() == "CharacterButton")
  {
    this->characterWindow->setVisible(!this->characterWindow->isVisible());
    this->characterWindowOpen = this->characterWindow->isVisible();
    this->characterWindow->moveToFront();
    // Play the sound.
    if(this->characterWindowOpen)
    {
      AudioManager::getSingleton().playSound("Item Picked Up");
    } // end if
    else
    {
      AudioManager::getSingleton().playSound("Item Put Down");
    } // end else
  } // end if
  if(args->window->getName() == "SkillButton")
  {
    this->skillsWindow->setVisible(!this->skillsWindow->isVisible());
    this->skillsWindowOpen = this->skillsWindow->isVisible();
    this->skillsWindow->moveToFront();
    // Play the sound.
    if(this->skillsWindowOpen)
    {
      AudioManager::getSingleton().playSound("Item Picked Up");
    } // end if
    else
    {
      AudioManager::getSingleton().playSound("Item Put Down");
    } // end else
  } // end if
  if(args->window->getName() == "JournalButton")
  {
    this->journalWindow->setVisible(!this->journalWindow->isVisible());
    this->journalOpen = this->journalWindow->isVisible();
    this->journalWindow->moveToFront();
    // Play the sound.
    if(this->journalOpen)
    {
      AudioManager::getSingleton().playSound("Item Picked Up");
    } // end if
    else
    {
      AudioManager::getSingleton().playSound("Item Put Down");
    } // end else
  } // end if

  return true;
} // bool GameScene::handleWindowButtonClicked(const CEGUI::EventArgs& _e)


void GameScene::switchToSaveScene()
{
  this->pushScene(this->findByName("SaveGameScene"));
} // void GameScene::switchToSaveScene()


int GameScene::getSlotLocation(CEGUI::Window* _slot)
{

  // Get the names of the window.
  string slotName = _slot->getName().c_str();

  // Extract the string for the number of it.
  string::iterator it = slotName.begin();

  while(*it != '-')
  {
    ++it;
  } // end while
    
  ++it;
  string slotStringNumber;

  while(it != slotName.end())
  {
    slotStringNumber += *it;
    ++it;
  } // end while

  // Now convert the strings to ints.
  return CEGUI::PropertyHelper::stringToInt(slotStringNumber.c_str());

} // int GameScene::getSlotLocation(CEGUI::Window* _slot)



void GameScene::setDeathTimer(int _timer)
{
  this->deathTimer = _timer;
} // void GameScene::setDeathTimer(int _timer)