#include "stdafx.h"
#include "NewGameScene.h"
#include "../../Managers/GameManager/GameManager.h"
#include <sstream>
#include "../../Managers/AudioManager/AudioManager.h"
#include "../../Managers/SkillManager/SkillManager.h"

using namespace Ogre;

NewGameScene::NewGameScene()
{
  this->quit                    = false;
  this->errorMessage            = false;
  this->frameEvent              = Ogre::FrameEvent();
  this->characterName           = "";
  this->characterMaleAnimation  = NULL;
  this->characterFemaleAnimation= NULL;
  this->characterMaleEntity     = NULL;
  this->characterMaleNode       = NULL;
  this->characterFemaleEntity   = NULL;
  this->characterFemaleNode     = NULL;
} // NewGameScene::NewGameScene()


void NewGameScene::enter()
{
  OgreFramework::getSingletonPtr()->log->logMessage("Entering NewGameScene...");

  this->sceneManager = OgreFramework::getSingletonPtr()->root->createSceneManager(ST_GENERIC, "NewGameSceneMgr");
  this->sceneManager->setAmbientLight(Ogre::ColourValue(0.7f, 0.7f, 0.7f));

  this->camera = this->sceneManager->createCamera("NewGameSceneCam");
  this->camera->setPosition(Vector3(0, 25, -50));
  this->camera->lookAt(Vector3(0, 0, 0));
  this->camera->setNearClipDistance(1);

  this->camera->setAspectRatio(Real(OgreFramework::getSingletonPtr()->viewport->getActualWidth()) /
    Real(OgreFramework::getSingletonPtr()->viewport->getActualHeight()));

  OgreFramework::getSingletonPtr()->viewport->setCamera(this->camera);

  this->createScene();
  this->buildGUI();
  this->rollStats();
} // void NewGameScene::enter()


void NewGameScene::buildGUI()
{

  string GUILook = OgreFramework::getSingleton().GUILook;


  // Clean the windows and show the cursor.
  CEGUI::WindowManager::getSingletonPtr()->destroyAllWindows();
  CEGUI::MouseCursor::getSingletonPtr()->show();

  // Create the default window.
  CEGUI::WindowManager& winManager = CEGUI::WindowManager::getSingleton();
  CEGUI::Window* sheet = winManager.createWindow("DefaultWindow","Sheet");
  CEGUI::System::getSingleton().setGUISheet(sheet);

  // Create a Label for the screen. @TODO.

  CEGUI::Window* backdrop = CEGUI::WindowManager::getSingleton().createWindow(GUILook + "/SimplePanel");
  float backdropWidth  = 0.37;
  float backdropHeight = 0.80;
  backdrop->setSize(CEGUI::UVector2(CEGUI::UDim(backdropWidth, 0), CEGUI::UDim(backdropHeight, 0)));
  backdrop->setPosition(CEGUI::UVector2(CEGUI::UDim((float)(0.5 - (backdropWidth / 2.0)), 0), 
                                        CEGUI::UDim((float)(0.5 - (backdropHeight / 2.0)), 0)));
  backdrop->setRiseOnClickEnabled(false);
  sheet->addChildWindow(backdrop);

  // Name field label.
  float characterNameLabelWidth  = 0.8;
  float characterNameLabelHeight = 0.10;
  CEGUI::Window* characterNameLabel = CEGUI::WindowManager::getSingleton().createWindow(GUILook + "/StaticText", 
    "CharacterNameLabel");
  characterNameLabel->setText("Character Name");
  characterNameLabel->setSize(CEGUI::UVector2(CEGUI::UDim((float)characterNameLabelWidth, 0), 
                              CEGUI::UDim((float)characterNameLabelHeight, 0)));
  characterNameLabel->setPosition(CEGUI::UVector2(CEGUI::UDim((float)(0.5 - (characterNameLabelWidth / 2)), 0), 
                                  CEGUI::UDim(0.05, 0)));
  characterNameLabel->setProperty("HorzFormatting","HorzCentred");
  characterNameLabel->setProperty("VertFormatting", "TopAligned");
  characterNameLabel->setProperty("BackgroundEnabled", "False");
  characterNameLabel->setProperty("FrameEnabled", "False");
  characterNameLabel->setFont("DejaVuSans-12");
  characterNameLabel->setRiseOnClickEnabled(false);
  characterNameLabel->setAlpha(1.0f);
  backdrop->addChildWindow(characterNameLabel);


  // Create a editable Name field.
  float characterNameBoxWidth  = 0.8;
  float characterNameBoxHeight = 0.5;
  characterNameBox = (CEGUI::Editbox *)winManager.createWindow(GUILook + "/Editbox", "NameBox");
  characterNameBox->setText("");
  characterNameBox->setMaxTextLength(13);
  characterNameBox->setReadOnly(false);
  characterNameBox->setTextMasked(false);
  characterNameBox->setSize(CEGUI::UVector2(CEGUI::UDim(characterNameBoxWidth, 0), CEGUI::UDim(characterNameBoxHeight, 0)));
  characterNameBox->setPosition(CEGUI::UVector2(CEGUI::UDim((float)(0.5 - (characterNameBoxWidth/2)), 0), 
                                                CEGUI::UDim((float)(0.96 - characterNameBoxHeight), 0)));
  characterNameBox->setMaskCodePoint(0x002A);
  characterNameBox->setProperty("TextFormatting","HorzCentred");
  characterNameBox->subscribeEvent(CEGUI::Window::EventTextChanged, 
    CEGUI::Event::Subscriber(&NewGameScene::checkNameBox,this));
  characterNameLabel->addChildWindow(characterNameBox);

  // Create a holder for the dropboxes.
  float holderWidth  = 0.8;
  float holderHeight = 0.4;
  CEGUI::Window* holderBox = CEGUI::WindowManager::getSingleton().createWindow(GUILook + "/StaticText", "HolderBox");
  holderBox->setSize(CEGUI::UVector2(CEGUI::UDim(holderWidth, 0), 
                              CEGUI::UDim(holderHeight, 0)));
  holderBox->setPosition(CEGUI::UVector2(CEGUI::UDim((float)(0.5 - (holderWidth / 2)), 0), 
                                  CEGUI::UDim(0.18, 0)));
  holderBox->setProperty("BackgroundEnabled", "False");
  holderBox->setProperty("FrameEnabled", "False");
  holderBox->setRiseOnClickEnabled(false);
  holderBox->setAlpha(1.0f);
  backdrop->addChildWindow(holderBox);

  // Create a Gender Label.
  float genderLabelWidth  = 0.5;
  float genderLabelHeight = 0.1;
  CEGUI::Window* genderLabel = CEGUI::WindowManager::getSingleton().createWindow(GUILook + "/StaticText", "GenderLabel");
  genderLabel->setText("Gender");
  genderLabel->setSize(CEGUI::UVector2(CEGUI::UDim(genderLabelWidth, 0), 
                              CEGUI::UDim(genderLabelHeight, 0)));
  genderLabel->setPosition(CEGUI::UVector2(CEGUI::UDim((float)(0.5 - (genderLabelWidth / 2)), 0), 
                                  CEGUI::UDim(0, 0)));
  genderLabel->setProperty("HorzFormatting","HorzCentred");
  genderLabel->setProperty("VertFormatting", "TopAligned");
  genderLabel->setProperty("BackgroundEnabled", "False");
  genderLabel->setProperty("FrameEnabled", "False");
  genderLabel->setFont("DejaVuSans-12");
  genderLabel->setRiseOnClickEnabled(true);
  genderLabel->setAlpha(1.0f);
  genderLabel->moveToBack();
  holderBox->addChildWindow(genderLabel);

  // Create the droplist items.
  CEGUI::ListboxTextItem* maleItem = new CEGUI::ListboxTextItem("Male", 0);
  maleItem->setSelectionBrushImage(GUILook, "MultiListSelectionBrush");
  CEGUI::ListboxTextItem* femaleItem = new CEGUI::ListboxTextItem("Female", 1);
  femaleItem->setSelectionBrushImage(GUILook, "MultiListSelectionBrush");

  // Create a Gender droplist.
  float genderListWidth  = 0.8;
  float genderListHeight = 0.35;
  this->genderList = (CEGUI::Combobox* )CEGUI::WindowManager::getSingleton().createWindow(GUILook + "/Combobox", "GenderList");

  this->genderList->setPosition(CEGUI::UVector2(cegui_reldim(0.5f-genderListWidth/2), cegui_reldim(0.1f)));
  this->genderList->setSize(CEGUI::UVector2(cegui_reldim(genderListWidth), cegui_reldim(genderListHeight)));

  this->genderList->setReadOnly(true);
  this->genderList->setVisible(true);
  this->genderList->addItem(maleItem);
  this->genderList->addItem(femaleItem);
  this->genderList->setText("Male");
  this->genderList->setRiseOnClickEnabled(true);
  holderBox->addChildWindow(this->genderList);
  this->genderList->subscribeEvent(CEGUI::Combobox::EventListSelectionAccepted, CEGUI::Event::Subscriber(&NewGameScene::changeModel, this));

  // Class Label.
  float classLabelWidth  = 0.5;
  float classLabelHeight = 0.1;
  CEGUI::Window* classLabel = CEGUI::WindowManager::getSingleton().createWindow(GUILook + "/StaticText", "ClassLabel");
  classLabel->setText("Class");
  classLabel->setSize(CEGUI::UVector2(CEGUI::UDim(genderLabelWidth, 0), 
                              CEGUI::UDim(genderLabelHeight, 0)));
  classLabel->setPosition(CEGUI::UVector2(CEGUI::UDim((float)(0.5 - (classLabelWidth / 2)), 0), 
                                  CEGUI::UDim(0.3, 0)));
  classLabel->setProperty("HorzFormatting","HorzCentred");
  classLabel->setProperty("VertFormatting", "TopAligned");
  classLabel->setProperty("BackgroundEnabled", "False");
  classLabel->setProperty("FrameEnabled", "False");
  classLabel->setFont("DejaVuSans-12");
  classLabel->setRiseOnClickEnabled(true);
  classLabel->setAlpha(1.0f);
  holderBox->addChildWindow(classLabel);


   // Create a Class droplist.
  float classListWidth  = 0.8;
  float classListHeight = 0.45;
  this->classList = (CEGUI::Combobox* )CEGUI::WindowManager::getSingleton().createWindow(GUILook + "/Combobox", "ClassList");
  this->classList->setPosition(CEGUI::UVector2(cegui_reldim(0.5f-classListWidth/2), cegui_reldim(0.4f)));
  this->classList->setSize(CEGUI::UVector2(cegui_reldim(classListWidth), cegui_reldim(classListHeight)));
  this->classList->setReadOnly(true);
  this->classList->setVisible(true);

  // Class droplist texts.
  SkillSet** skillSets = SkillManager::getInstance()->getSkillSets();

  // Loop through the skill sets and create a dropdown list of the class names.
  for(int i=0;i<SkillManager::getInstance()->getNumSkillSets();i++)
  {
    SkillSet* skillSet = skillSets[i];
    CEGUI::ListboxItem* item = new CEGUI::ListboxTextItem(skillSet->getName(), i);
    item->setSelectionBrushImage(GUILook, "MultiListSelectionBrush");
    this->classList->addItem(item);
    if(i == 0)
    {
      this->classList->setText(skillSet->getName());
    } // end if
  } // end for

  holderBox->addChildWindow(this->classList);


  // Create a description box that displays information about the class.
  float classDescriptionWidth  = 0.8;
  float classDescriptionHeight = 0.35;
  this->classDescription = (CEGUI::MultiLineEditbox* )winManager.createWindow(GUILook + "/MultiLineEditbox", "ClassDescription");
  this->classDescription->setFont("DejaVuSans-12");
  this->classDescription->setPosition(CEGUI::UVector2(cegui_reldim(0.5f-classListWidth/2), cegui_reldim(0.45f)));
  this->classDescription->setSize(CEGUI::UVector2(cegui_reldim(classDescriptionWidth), cegui_reldim(classDescriptionHeight)));
  this->classDescription->setReadOnly(true);
  backdrop->addChildWindow(this->classDescription);

  this->classSelected = skillSets[0]->getName();
  this->setDescriptionText();
  
  // Start Button.
  float buttonWidth  = 0.3;
  float buttonHeight = 0.08;
  CEGUI::Window* startGameButton = winManager.createWindow(GUILook + "/Button", "ButtonStartGame");
  startGameButton->setText("Start");
  startGameButton->setSize(CEGUI::UVector2(CEGUI::UDim(buttonWidth, 0), CEGUI::UDim(buttonHeight, 0)));
  startGameButton->setHorizontalAlignment(CEGUI::HA_CENTRE);
  startGameButton->setYPosition(CEGUI::UDim(0.87,0));
  startGameButton->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&NewGameScene::buttonHit, this));
  startGameButton->disable();
  backdrop->addChildWindow(startGameButton);

  // Create the attributes box.
  float attributesListWidth  = 0.3;
  float attributesListHeight = 0.5;
  this->attributesList = static_cast<CEGUI::MultiColumnList*>(winManager.createWindow(GUILook + "/MultiColumnList", "AttributesList"));
  sheet->addChildWindow(this->attributesList);
	this->attributesList->setPosition(CEGUI::UVector2(cegui_reldim(0.01f), cegui_reldim( 0.1f)));
	this->attributesList->setSize(CEGUI::UVector2(cegui_reldim(attributesListWidth), cegui_reldim(attributesListHeight)));
  
  this->attributesList->addColumn("Attribute", 0, CEGUI::UDim(0.69f, 0));
	this->attributesList->addColumn("Value", 1, CEGUI::UDim(0.29, 0));
  this->attributesList->setVisible(true);
  
  this->attributesList->setSelectionMode(CEGUI::MultiColumnList::RowSingle); // MultiColumnList::RowMultiple
	CEGUI::ListboxTextItem* attributeText;
  this->attributesList->addRow();
  this->attributesList->setUserSortControlEnabled(false);
  
  
  // Attribute columns.
  attributeText = new CEGUI::ListboxTextItem("Strength",101);
  attributeText->setSelectionBrushImage(GUILook, "MultiListSelectionBrush");
  this->attributesList->setItem(attributeText, 0, 0);
  
  this->attributesList->addRow();
  attributeText = new CEGUI::ListboxTextItem("Agility",101);
  attributeText->setSelectionBrushImage(GUILook, "MultiListSelectionBrush");
  this->attributesList->setItem(attributeText, 0, 1);

  this->attributesList->addRow();
  attributeText = new CEGUI::ListboxTextItem("Dexterity",102);
  attributeText->setSelectionBrushImage(GUILook, "MultiListSelectionBrush");
  this->attributesList->setItem(attributeText, 0, 2);

  this->attributesList->addRow();
  attributeText = new CEGUI::ListboxTextItem("Constitution",103);
  attributeText->setSelectionBrushImage(GUILook, "MultiListSelectionBrush");
  this->attributesList->setItem(attributeText, 0, 3);

  this->attributesList->addRow();
  attributeText = new CEGUI::ListboxTextItem("Intelligence",104);
  attributeText->setSelectionBrushImage(GUILook, "MultiListSelectionBrush");
  this->attributesList->setItem(attributeText, 0, 4);

  this->attributesList->addRow();
  attributeText = new CEGUI::ListboxTextItem("Wisdom",105);
  attributeText->setSelectionBrushImage(GUILook, "MultiListSelectionBrush");
  this->attributesList->setItem(attributeText, 0, 5);

  this->attributesList->addRow();
  attributeText = new CEGUI::ListboxTextItem("Charisma",106);
  attributeText->setSelectionBrushImage(GUILook, "MultiListSelectionBrush");
  this->attributesList->setItem(attributeText, 0, 6);

  
  // Create a re-roll button.
  float reRollButtonWidth  = 0.5;
  float reRollbuttonHeight = 0.08;
  CEGUI::Window *reRollButton = winManager.createWindow(GUILook +"/Button", "ReRollButton");
  reRollButton->setText("Roll Stats");
  reRollButton->setSize(CEGUI::UVector2(CEGUI::UDim(reRollButtonWidth, 0), CEGUI::UDim(reRollbuttonHeight, 0)));
  reRollButton->setHorizontalAlignment(CEGUI::HA_CENTRE);
  reRollButton->setYPosition(CEGUI::UDim(0.85,0));
  reRollButton->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&NewGameScene::buttonHit, this));
  this->attributesList->addChildWindow(reRollButton);

} // void NewGameScene::buildGUI()


void NewGameScene::createScene()
{
  this->characterPosition = Ogre::Vector3(-15,10,-10);
  this->sceneManager->createLight("Light")->setPosition(75,75,75);
  this->camera->lookAt(this->characterPosition);
  this->characterModel  = "Hero_Test.mesh";
  this->characterMaleEntity = this->sceneManager->createEntity("CharacterMaleEntity", this->characterModel);
  Ogre::Entity* weaponEntity1 = this->sceneManager->createEntity("MaleWeapon", "diamond_club.mesh");
  Ogre::Entity* weaponEntity2 = this->sceneManager->createEntity("FemaleWeapon", "diamond_club.mesh");

  this->characterMaleEntity->attachObjectToBone("HLMR_POINT_TRACK",weaponEntity1);

  this->characterFemaleEntity = this->sceneManager->createEntity("CharacterFemaleEntity", "Hero_Female.mesh");
  this->characterFemaleEntity->attachObjectToBone("BAFFI_R_TRACK",weaponEntity2);

  this->characterMaleNode = this->sceneManager->getRootSceneNode()->createChildSceneNode("characterMaleNode");
  this->characterMaleNode->attachObject(this->characterMaleEntity);
  this->characterMaleNode->setPosition(this->characterPosition);
  this->characterMaleNode->translate(-8.0,4,-17);
  this->characterMaleNode->yaw(Ogre::Degree(130));
  this->characterMaleNode->roll(Ogre::Degree(5));

  this->characterFemaleNode = this->sceneManager->getRootSceneNode()->createChildSceneNode("characterFemaleNode");
  this->characterFemaleNode->attachObject(this->characterFemaleEntity);
  this->characterFemaleNode->setPosition(this->characterPosition);
  this->characterFemaleNode->translate(-8.0,4,-17);
  this->characterFemaleNode->yaw(Ogre::Degree(130));
  this->characterFemaleNode->roll(Ogre::Degree(5));
  this->characterFemaleNode->setVisible(false);
  
  this->characterMaleAnimation = this->characterMaleEntity->getAnimationState("Idle");
  this->characterMaleAnimation->setEnabled(true);
  this->characterMaleAnimation->setLoop(true);

  this->characterFemaleAnimation = this->characterFemaleEntity->getAnimationState("Idle");
  this->characterFemaleAnimation->setEnabled(true);
  this->characterFemaleAnimation->setLoop(true);


} // void NewGameScene::createScene()


void NewGameScene::exit()
{
  this->sceneManager->destroyCamera(this->camera);
  if(this->sceneManager)
  {
    OgreFramework::getSingletonPtr()->root->destroySceneManager(this->sceneManager);
  } // end if

  CEGUI::WindowManager::getSingletonPtr()->destroyAllWindows();

} // void NewGameScene::exit()


bool NewGameScene::keyPressed(const OIS::KeyEvent &_keyEventRef)
{
  if(OgreFramework::getSingletonPtr()->keyboard->isKeyDown(OIS::KC_ESCAPE))
  {
    changeScene(findByName("MenuScene"));
    return true;
  } // end if
  else if(OgreFramework::getSingletonPtr()->CEGUIKeyPressed(_keyEventRef))
  {
    return true;
  } // else if
  
  OgreFramework::getSingletonPtr()->keyPressed(_keyEventRef);
  return true;
} // bool NewGameScene::keyPressed(const OIS::KeyEvent &_keyEventRef)


bool NewGameScene::keyReleased(const OIS::KeyEvent &_keyEventRef)
{
  if(OgreFramework::getSingletonPtr()->CEGUIKeyReleased(_keyEventRef))
  {
    return true;
  } // end else

  OgreFramework::getSingletonPtr()->keyReleased(_keyEventRef);

  return true;
} // bool NewGameScene::keyReleased(const OIS::KeyEvent &_keyEventRef)


bool NewGameScene::mouseMoved(const OIS::MouseEvent &_evt)
{
  if(OgreFramework::getSingletonPtr()->CEGUIMouseMoved(_evt))
  {
    return true;
  } // end if

  return true;
} // bool NewGameScene::mouseMoved(const OIS::MouseEvent &_evt)

bool NewGameScene::mousePressed(const OIS::MouseEvent &_evt, OIS::MouseButtonID _id)
{
  if(OgreFramework::getSingletonPtr()->CEGUIMousePressed(_evt, _id))
  {
    return true;
  } // end if

  return true;
} // bool NewGameScene::mousePressed(const OIS::MouseEvent &_evt, OIS::MouseButtonID _id)


bool NewGameScene::mouseReleased(const OIS::MouseEvent &_evt, OIS::MouseButtonID _id)
{
  if(OgreFramework::getSingletonPtr()->CEGUIMouseReleased(_evt, _id))
  {
    return true;
  } // end if

  return true;
} // bool NewGameScene::mouseReleased(const OIS::MouseEvent &_evt, OIS::MouseButtonID _id)


void NewGameScene::update(double _timeSinceLastFrame)
{
  this->frameEvent.timeSinceLastFrame = (Ogre::Real)_timeSinceLastFrame;

  if(this->quit == true)
  {
    this->shutdown();
    return;
  } // if(this->quit == true)
  
  if(this->characterMaleAnimation)
  {
    this->characterMaleAnimation->addTime(_timeSinceLastFrame/6000);
  } // end if
  if(this->characterFemaleAnimation)
  {
    this->characterFemaleAnimation->addTime(_timeSinceLastFrame/4000);
  } // end if
  
  this->getInput();

  if(strcmp(this->classList->getText().c_str(), this->classSelected.c_str()) != 0)
  {
    this->setDescriptionText();
    this->classSelected = this->classList->getText().c_str();
  } // end if

} // void NewGameScene::update(double _timeSinceLastFrame)


bool NewGameScene::buttonHit(const CEGUI::EventArgs &_evt)
{
  const CEGUI::WindowEventArgs* args = static_cast<const CEGUI::WindowEventArgs*>(&_evt);

  CEGUI::Window *window = args->window;

  if(window->getName() == "ReRollButton")
  {
    AudioManager::getSingleton().playSound("ClickButton");
    this->rollStats();
  } // end if
  if(window->getName() == "ButtonStartGame")
  {
    if(this->characterNameBox->getText().length() > 0)
    {
      AudioManager::getSingleton().playSound("SlashButton");

      this->createPlayer();
      this->changeScene(findByName("GameScene"));
    } // end if
  } // end if

  return true;

} // void NewGameScene::buttonHit(CEGUI::EventArgs &_evt)


void NewGameScene::getInput()
{

} // void NewGameScene::getInput()


void NewGameScene::setDescriptionText()
{
  // Get the skill sets list.
  SkillSet** skillSets = SkillManager::getInstance()->getSkillSets();
  this->classDescription->setText(SkillManager::getInstance()->getSkillSet(this->classList->getText().c_str())->getDescription());
} // void NewGameScene::setDescriptionText()


void NewGameScene::rollStats()
{
  this->attributeStrength       = rand()%11+8;
  this->attributeAgility        = rand()%11+8;
  this->attributeDexterity      = rand()%11+8;
  this->attributeConstitution   = rand()%11+8;
  this->attributeIntelligence   = rand()%11+8;
  this->attributeWisdom         = rand()%11+8;
  this->attributeCharisma       = rand()%11+8;

  std::string attributeString;
  std::stringstream ss[7];// (stringstream::in | stringstream::out);

  ss[0] << this->attributeStrength;
  ss[1] << this->attributeAgility;
  ss[2] << this->attributeDexterity;
  ss[3] << this->attributeConstitution;
  ss[4] << this->attributeIntelligence;
  ss[5] << this->attributeWisdom;
  ss[6] << this->attributeCharisma;

  for(int i=0;i<7;i++)
  {
    ss[i] >> attributeString;
    CEGUI::ListboxTextItem* attributeItem = new CEGUI::ListboxTextItem(attributeString.c_str());
    this->attributesList->setItem(attributeItem,1,i);
  } // end for

} // void NewGameScene::rollStats()


void NewGameScene::createPlayer()
{
  Player* newPlayer = Player::getInstance();

  this->characterName = this->characterNameBox->getText().c_str();

  newPlayer->init(this->attributeStrength,this->attributeAgility,this->attributeDexterity,
    this->attributeConstitution,this->attributeIntelligence,this->attributeWisdom, this->attributeCharisma,
    this->characterModel, this->characterName, this->classSelected, string(this->genderList->getText().c_str()));

} // void NewGameScene::createPlayer


bool NewGameScene::changeModel(const CEGUI::EventArgs& _e)
{
  const CEGUI::WindowEventArgs* args = static_cast<const CEGUI::WindowEventArgs*>(&_e);

  CEGUI::Combobox *window = (CEGUI::Combobox*)args->window;
  
  if(window->getText() == "Male")
  {
    this->characterFemaleNode->setVisible(false);
    this->characterMaleNode->setVisible(true);
    this->characterModel = this->characterMaleEntity->getMesh()->getName();
  } // end if
  if(window->getText() == "Female")
  {
    this->characterFemaleNode->setVisible(true);
    this->characterMaleNode->setVisible(false);
    this->characterModel = this->characterFemaleEntity->getMesh()->getName();
  } // end if
  
  return true;
} // void NewGameScene::changeModel()


bool NewGameScene::checkNameBox(const CEGUI::EventArgs& _e)
{
  const CEGUI::WindowEventArgs* args = static_cast<const CEGUI::WindowEventArgs*>(&_e);

  CEGUI::Editbox *window = (CEGUI::Editbox*)args->window;
  CEGUI::WindowManager& winManager = CEGUI::WindowManager::getSingleton();

  if(window->getText().length() > 0)
  {
    winManager.getWindow("ButtonStartGame")->enable();
  } // end if
  else if(window->getText().length() == 0)
  {
    winManager.getWindow("ButtonStartGame")->disable();
  } // end else if


  return true;
} // bool NewGameScene::checkNameBox(const CEGUI::EventArgs& _e)