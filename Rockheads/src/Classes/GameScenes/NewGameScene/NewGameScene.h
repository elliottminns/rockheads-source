#include "../../../PrecompiledHeaders/stdafx.h"
#ifndef NEW_GAME_SCENE_H
#define NEW_GAME_SCENE_H

#include "../AbstractScene.h"
#include <iostream>
#include <OgreMaterialManager.h>

using namespace std;

class NewGameScene : public AbstractScene
{
private:
	bool quit;
  bool errorMessage;

  string characterName;
  string classSelected;

  // User interface details.
  CEGUI::Editbox* characterNameBox;
  CEGUI::Combobox* genderList;
  CEGUI::Combobox* classList;
  CEGUI::MultiLineEditbox* classDescription;
  CEGUI::MultiColumnList* attributesList;

  // Player Attributes.
  int attributeStrength;
  int attributeAgility;
  int attributeDexterity;
  int attributeConstitution;
  int attributeIntelligence;
  int attributeWisdom;
  int attributeCharisma;

  // Character display node.
  Ogre::Entity* characterMaleEntity;
  Ogre::Entity* characterFemaleEntity;
  Ogre::SceneNode* characterMaleNode;
  Ogre::SceneNode* characterFemaleNode;
  Ogre::AnimationState* characterMaleAnimation;
  Ogre::AnimationState* characterFemaleAnimation;
  Ogre::Vector3 characterPosition;
  Ogre::String characterModel;
  Ogre::MaterialPtr characterMaleMaterial;

public:
  NewGameScene();

  DECLARE_SCENE_CLASS(NewGameScene)

	void enter();
	void createScene();
	void exit();

  void buildGUI();
  void getInput();

  bool keyPressed(const OIS::KeyEvent &_keyEventRef);
	bool keyReleased(const OIS::KeyEvent &_keyEventRef);

	bool mouseMoved(const OIS::MouseEvent &_evt);
	bool mousePressed(const OIS::MouseEvent &_evt, OIS::MouseButtonID _id);
	bool mouseReleased(const OIS::MouseEvent &_evt, OIS::MouseButtonID _id);

  bool buttonHit(const CEGUI::EventArgs &_evt);

	void update(double _timeSinceLastFrame);

  void setDescriptionText();

  void rollStats();

  void createPlayer();
  bool changeModel(const CEGUI::EventArgs& _e);
  bool checkNameBox(const CEGUI::EventArgs& _e);
}; // end class NewGameScene

#endif