#include "stdafx.h"
#include "MenuScene.h"
#include "../../Managers/AudioManager/AudioManager.h"
#include "../../Messaging/MessageDispatcher/MessageDispatcher.h"

using namespace Ogre;

MenuScene::MenuScene()
{
  this->quit        = false;
  this->initialised = false;
  this->frameEvent  = Ogre::FrameEvent();
} // MenuScene::MenuState()


void MenuScene::enter()
{
  OgreFramework::getSingletonPtr()->log->logMessage("Entering MenuState...");

  this->sceneManager = OgreFramework::getSingletonPtr()->root->createSceneManager(ST_GENERIC, "MenuSceneMgr");
  this->sceneManager->setAmbientLight(Ogre::ColourValue(0.7f, 0.7f, 0.7f));

  this->camera = this->sceneManager->createCamera("MenuCam");
  this->camera->setPosition(Vector3(0, 25, -50));
  this->camera->lookAt(Vector3(0, 0, 0));
  this->camera->setNearClipDistance(1);

  this->camera->setAspectRatio(Real(OgreFramework::getSingletonPtr()->viewport->getActualWidth()) /
    Real(OgreFramework::getSingletonPtr()->viewport->getActualHeight()));

  OgreFramework::getSingletonPtr()->viewport->setCamera(this->camera);

  this->buildGUI();
  this->createScene();

  MessageDispatcher::getInstance()->resetIDNumbers();
  
} // void MenuScene::enter()


void MenuScene::createScene()
{
  AudioManager::getSingleton().playSound("Intro");
} // void MenuScene::createScene()


void MenuScene::buildGUI()
{

  OgreFramework* framework = OgreFramework::getSingletonPtr();

  // Get the window manager and destroy the windows to clean up the area.
  CEGUI::WindowManager &winManager = CEGUI::WindowManager::getSingleton();
  winManager.destroyAllWindows();

  // Show the mouse cursor.
  CEGUI::MouseCursor::getSingletonPtr()->show();
  
  // Create the main sheet.
  CEGUI::Window *sheet = winManager.createWindow("DefaultWindow", "Sheet");
  
  CEGUI::System::getSingleton().setGUISheet(sheet);

  CEGUI::System::getSingleton().setDefaultFont("DejaVuSans-12");
  
  // Title of the Game.
  CEGUI::Window* title = CEGUI::WindowManager::getSingleton().createWindow(framework->GUILook + "/StaticText");
  title->setText("Rockheads");
  title->setHorizontalAlignment(CEGUI::HA_CENTRE);
  title->setVerticalAlignment(CEGUI::VA_TOP);
  title->setYPosition(CEGUI::UDim(0,20));
  title->setSize(CEGUI::UVector2(CEGUI::UDim(0.35, 0), CEGUI::UDim(0.15, 0)));
  title->setProperty("HorzFormatting","HorzCentred");
  title->setProperty("VertFormatting","VertCentred");
  title->setFont("NorthwoodHigh-48");
  sheet->addChildWindow(title);


  // Create a new game button.
  CEGUI::Window* newGameButton = winManager.createWindow(framework->GUILook + "/Button", "ButtonNewGame");
  newGameButton->setText("New Game");
  newGameButton->setSize(CEGUI::UVector2(CEGUI::UDim(0.19, 0), CEGUI::UDim(0.07, 0)));
  newGameButton->setPosition(CEGUI::UVector2(CEGUI::UDim (0.5-0.19/2, 0), CEGUI::UDim(0.39,0)));
  newGameButton->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&MenuScene::buttonHit, this));
  sheet->addChildWindow(newGameButton);

  // Create a load game button.
  CEGUI::Window *loadGameButton = winManager.createWindow(framework->GUILook + "/Button", "ButtonLoadGame");
  loadGameButton->setText("Load Game");
  loadGameButton->setSize(CEGUI::UVector2(CEGUI::UDim(0.19, 0), CEGUI::UDim(0.07, 0)));
  loadGameButton->setPosition(CEGUI::UVector2(CEGUI::UDim(0.5-0.19/2, 0), CEGUI::UDim(0.47,0)));
  loadGameButton->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&MenuScene::buttonHit, this));
  sheet->addChildWindow(loadGameButton);

  // Create a load game button.
  CEGUI::Window *optionsButton = winManager.createWindow(framework->GUILook + "/Button", "ButtonOptions");
  optionsButton->setText("Options");
  optionsButton->setSize(CEGUI::UVector2(CEGUI::UDim(0.19, 0), CEGUI::UDim(0.07, 0)));
  optionsButton->setPosition(CEGUI::UVector2(CEGUI::UDim(0.5-0.19/2, 0), CEGUI::UDim(0.55,0)));
  optionsButton->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&MenuScene::buttonHit, this));
  sheet->addChildWindow(optionsButton);

  // Create a quit button.
  CEGUI::Window *quitButton = winManager.createWindow(framework->GUILook + "/Button", "ButtonQuit");
  quitButton->setText("Quit");
  quitButton->setSize(CEGUI::UVector2(CEGUI::UDim(0.19, 0), CEGUI::UDim(0.07, 0)));
  quitButton->setPosition(CEGUI::UVector2(CEGUI::UDim(0.5-0.19/2, 0), CEGUI::UDim(0.63,0)));
  quitButton->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&MenuScene::buttonHit, this));
  sheet->addChildWindow(quitButton);

} // void MenuScene::buildGUI()


void MenuScene::exit()
{
  OgreFramework::getSingletonPtr()->log->logMessage("Leaving MenuState...");

  this->sceneManager->destroyCamera(this->camera);
  if(this->sceneManager)
  {
    OgreFramework::getSingletonPtr()->root->destroySceneManager(this->sceneManager);
  } // end if

  CEGUI::WindowManager &winManager = CEGUI::WindowManager::getSingleton();
  winManager.destroyAllWindows();

} // void MenuScene::exit()


bool MenuScene::keyPressed(const OIS::KeyEvent &_keyEventRef)
{
  if(OgreFramework::getSingletonPtr()->keyboard->isKeyDown(OIS::KC_ESCAPE))
  {
    this->quit = true;
    return true;
  } // end if

  if(OgreFramework::getSingletonPtr()->CEGUIKeyPressed(_keyEventRef))
  {
    return true;
  } // end if

  OgreFramework::getSingletonPtr()->keyPressed(_keyEventRef);
  return true;
} // bool MenuScene::keyPressed(const OIS::KeyEvent &_keyEventRef)


bool MenuScene::keyReleased(const OIS::KeyEvent &_keyEventRef)
{
  if(OgreFramework::getSingletonPtr()->CEGUIKeyReleased(_keyEventRef))
  {
    return true;
  } // end if

  OgreFramework::getSingletonPtr()->keyReleased(_keyEventRef);
  return true;
} // bool MenuScene::keyReleased(const OIS::KeyEvent &_keyEventRef)


bool MenuScene::mouseMoved(const OIS::MouseEvent &_evt)
{
  if(OgreFramework::getSingletonPtr()->CEGUIMouseMoved(_evt))
  {
    return true;
  } // end if

  OgreFramework::getSingletonPtr()->mouseMoved(_evt);

  return true;
} // bool MenuScene::mouseMoved(const OIS::MouseEvent &_evt)

bool MenuScene::mousePressed(const OIS::MouseEvent &_evt, OIS::MouseButtonID _id)
{

  if(OgreFramework::getSingletonPtr()->CEGUIMousePressed(_evt, _id))
  {
    return true;
  } // end if

  OgreFramework::getSingletonPtr()->mousePressed(_evt, _id);

  return true;
} // bool MenuScene::mousePressed(const OIS::MouseEvent &_evt, OIS::MouseButtonID _id)


bool MenuScene::mouseReleased(const OIS::MouseEvent &_evt, OIS::MouseButtonID _id)
{
  if(OgreFramework::getSingletonPtr()->CEGUIMouseReleased(_evt, _id))
  {
    return true;
  } // end if

  OgreFramework::getSingletonPtr()->mouseReleased(_evt, _id);

  return true;
} // bool MenuScene::mouseReleased(const OIS::MouseEvent &_evt, OIS::MouseButtonID _id)


void MenuScene::update(double _timeSinceLastFrame)
{
  this->frameEvent.timeSinceLastFrame = (Ogre::Real)_timeSinceLastFrame;

  if(this->quit == true)
  {
    this->shutdown();
    return;
  } // if(this->quit == true)
} // void MenuScene::update(double _timeSinceLastFrame)


bool MenuScene::buttonHit(const CEGUI::EventArgs &_evt)
{
  const CEGUI::WindowEventArgs* args = static_cast<const CEGUI::WindowEventArgs*>(&_evt);

  AudioManager::getSingleton().playSound("SlashButton");

  if(args->window->getName() == "ButtonNewGame")
  {
    changeScene(findByName("NewGameScene"));
  } // end if
  if(args->window->getName() == "ButtonQuit")
  {
    OgreFramework::getSingleton().soundManager->destroyAllSounds();
    this->quit = true;
  } // end if
  if(args->window->getName() == "ButtonLoadGame")
  {
    this->pushScene(this->findByName("LoadGameScene"));
  } // end if

  return true;
} // void MenuScene::setQuitOn()


void MenuScene::resume()
{
  OgreFramework::getSingletonPtr()->log->logMessage("Resuming MenuScene...");

  this->buildGUI();

  OgreFramework::getSingletonPtr()->viewport->setCamera(this->camera);
  this->quit = false;
} // void MenuScene::resume()