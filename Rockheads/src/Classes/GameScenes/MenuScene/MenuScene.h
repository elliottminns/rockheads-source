#include "../../../PrecompiledHeaders/stdafx.h"
#ifndef MENU_SCENE_H
#define MENU_SCENE_H

#include "../AbstractScene.h"

class MenuScene : public AbstractScene
{
public:
  MenuScene();

	DECLARE_SCENE_CLASS(MenuScene)

	void enter();
	void createScene();
  void buildGUI();
	void exit();

	bool keyPressed(const OIS::KeyEvent &_keyEventRef);
	bool keyReleased(const OIS::KeyEvent &_keyEventRef);

	bool mouseMoved(const OIS::MouseEvent &_evt);
	bool mousePressed(const OIS::MouseEvent &_evt, OIS::MouseButtonID _id);
	bool mouseReleased(const OIS::MouseEvent &_evt, OIS::MouseButtonID _id);

	void update(double _timeSinceLastFrame);
  void resume();

  bool buttonHit(const CEGUI::EventArgs &_args);

private:
	bool quit;
  bool initialised;
}; // end class MenuScene

#endif