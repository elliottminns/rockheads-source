#include "../../../PrecompiledHeaders/stdafx.h"
#ifndef PAUSE_SCENE_H
#define PAUSE_SCENE_H

#include "../AbstractScene.h"

class PauseScene : public AbstractScene
{
public:
  PauseScene();
  
  DECLARE_SCENE_CLASS(PauseScene)

  void enter();
  void createScene();
  void buildGUI();
  void exit();

  bool keyPressed(const OIS::KeyEvent &_keyEventRef);
  bool keyReleased(const OIS::KeyEvent &_keyEventRef);

  bool mouseMoved(const OIS::MouseEvent &_evt);
  bool mousePressed(const OIS::MouseEvent &_evt, OIS::MouseButtonID _id);
  bool mouseReleased(const OIS::MouseEvent &_evt, OIS::MouseButtonID _id);
  
  bool buttonHit(const CEGUI::EventArgs &_evt); 
  void yesNoDialogClosed(const Ogre::DisplayString& _question, bool _yesHit);

  void update(double _timeSinceLastFrame);

  void releaseGameScene();

private:
  bool quit;
  bool questionActive;
}; // end class PauseScene : public AbstractScene

#endif