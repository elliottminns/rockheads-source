#include "stdafx.h"
#include "PauseScene.h"
#include "../../Managers/AudioManager/AudioManager.h"
#include "../../Managers/ParticleManager/ParticleManager.h"

using namespace Ogre;


PauseScene::PauseScene()
{
  this->quit             = false;
  this->questionActive   = false;
  this->frameEvent       = Ogre::FrameEvent();
} // PauseScene::PauseScene()


void PauseScene::enter()
{
  OgreFramework::getSingletonPtr()->log->logMessage("Entering PauseScene...");


  OgreFramework::getSingleton().trayManager->hideCursor();


  this->sceneManager = OgreFramework::getSingletonPtr()->root->createSceneManager(ST_GENERIC, "PausesceneManager");
  this->sceneManager->setAmbientLight(Ogre::ColourValue(0.7f, 0.7f, 0.7f));

  this->camera = this->sceneManager->createCamera("PauseCam");
  this->camera->setPosition(Vector3(0, 25, -50));
  this->camera->lookAt(Vector3(0, 0, 0));
  this->camera->setNearClipDistance(1);

  this->camera->setAspectRatio(Real(OgreFramework::getSingletonPtr()->viewport->getActualWidth()) /
    Real(OgreFramework::getSingletonPtr()->viewport->getActualHeight()));

  OgreFramework::getSingletonPtr()->viewport->setCamera(this->camera);

  this->quit = false;

  this->buildGUI();
  this->createScene();
} // void PauseScene::enter()


void PauseScene::buildGUI()
{
  CEGUI::WindowManager &winManager = CEGUI::WindowManager::getSingleton();
  winManager.destroyAllWindows();


  OgreFramework::getSingleton().trayManager->destroyAllWidgets();


  CEGUI::Window* sheet = winManager.createWindow("DefaultWindow","Sheet");

  // Create a Label for the screen. @TODO.

  OgreFramework* framework = OgreFramework::getSingletonPtr();

  CEGUI::Window* returnToGame = winManager.createWindow(framework->GUILook + "/Button", "ReturnToGameButton");
  returnToGame->setText("Return To Game");
  returnToGame->setSize(CEGUI::UVector2(CEGUI::UDim(0.19, 0), CEGUI::UDim(0.07, 0)));
  returnToGame->setHorizontalAlignment(CEGUI::HA_CENTRE);
  returnToGame->setYPosition(CEGUI::UDim(0.40,0));
  returnToGame->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&PauseScene::buttonHit, this));
  sheet->addChildWindow(returnToGame);

  CEGUI::Window* quitToMenu = winManager.createWindow(framework->GUILook + "/Button", "QuitToMenuButton");
  quitToMenu->setText("Quit To Main Menu");
  quitToMenu->setSize(CEGUI::UVector2(CEGUI::UDim(0.19, 0), CEGUI::UDim(0.07, 0)));
  quitToMenu->setHorizontalAlignment(CEGUI::HA_CENTRE);
  quitToMenu->setYPosition(CEGUI::UDim(0.50,0));
  quitToMenu->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&PauseScene::buttonHit, this));
  sheet->addChildWindow(quitToMenu);

  CEGUI::Window* quitToDesktop = winManager.createWindow(framework->GUILook + "/Button", "QuitToDesktopButton");
  quitToDesktop->setText("Quit To Desktop");
  quitToDesktop->setSize(CEGUI::UVector2(CEGUI::UDim(0.19, 0), CEGUI::UDim(0.07, 0)));
  quitToDesktop->setHorizontalAlignment(CEGUI::HA_CENTRE);
  quitToDesktop->setYPosition(CEGUI::UDim(0.60,0));
  quitToDesktop->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&PauseScene::buttonHit, this));
  sheet->addChildWindow(quitToDesktop);
  

  CEGUI::System::getSingleton().setGUISheet(sheet);
} // void PauseScene::buildGUI()


void PauseScene::createScene()
{

} // void PauseScene::createScene()


void PauseScene::exit()
{
  OgreFramework::getSingletonPtr()->log->logMessage("Leaving PauseScene...");

  this->sceneManager->destroyCamera(this->camera);
  if(this->sceneManager)
  {
    OgreFramework::getSingletonPtr()->root->destroySceneManager(this->sceneManager);
  } // end if

  CEGUI::WindowManager::getSingleton().destroyAllWindows();


  OgreFramework::getSingletonPtr()->trayManager->clearAllTrays();
  OgreFramework::getSingletonPtr()->trayManager->destroyAllWidgets();
  OgreFramework::getSingletonPtr()->trayManager->setListener(0);


} // void PauseScene::exit()


bool PauseScene::keyPressed(const OIS::KeyEvent &_keyEventRef)
{
  if(OgreFramework::getSingletonPtr()->keyboard->isKeyDown(OIS::KC_ESCAPE) && !this->questionActive)
  {
    this->quit = true;
    return true;
  } // end if

  OgreFramework::getSingletonPtr()->keyPressed(_keyEventRef);

  return true;
} // bool PauseScene::keyPressed(const OIS::KeyEvent &_keyEventRef)


bool PauseScene::keyReleased(const OIS::KeyEvent &_keyEventRef)
{
  OgreFramework::getSingletonPtr()->keyReleased(_keyEventRef);

  return true;
} // bool PauseScene::keyReleased(const OIS::KeyEvent &_keyEventRef)


bool PauseScene::mouseMoved(const OIS::MouseEvent &_evt)
{
  if(OgreFramework::getSingletonPtr()->CEGUIMouseMoved(_evt))
  {
    return true;
  } // end if
  return true;
} // bool PauseScene::mouseMoved(const OIS::MouseEvent &_evt)


bool PauseScene::mousePressed(const OIS::MouseEvent &_evt, OIS::MouseButtonID _id)
{
  if(OgreFramework::getSingletonPtr()->CEGUIMousePressed(_evt, _id))
  {
    return true;
  } // end if
  return true;
} // bool PauseScene::mousePressed(const OIS::MouseEvent &_evt, OIS::MouseButtonID _id)


bool PauseScene::mouseReleased(const OIS::MouseEvent &_evt, OIS::MouseButtonID _id)
{
  if(OgreFramework::getSingletonPtr()->CEGUIMouseReleased(_evt, _id))
  {
    return true;
  } // end if

  return true;
} // bool PauseScene::mouseReleased(const OIS::MouseEvent &_evt, OIS::MouseButtonID _id)


void PauseScene::update(double _timeSinceLastFrame)
{
  this->frameEvent.timeSinceLastFrame = (Ogre::Real)_timeSinceLastFrame;
  /*
  OgreFramework::getSingletonPtr()->trayManager->frameRenderingQueued(this->frameEvent);
  */
  if(this->quit == true)
  {
    popScene();
    return;
  } // end if
} // void PauseScene::update(double _timeSinceLastFrame)

/*
void PauseScene::buttonHit(OgreBites::Button *button)
{
    if(button->getName() == "ExitBtn")
    {
        OgreFramework::getSingletonPtr()->trayManager->showYesNoDialog("Sure?", "Really Quit To Desktop?");
        this->questionActive = true;
    } // end if
    else if(button->getName() == "BackToGameBtn")
    {
        this->quit = true;
    } // end else if
    else if(button->getName() == "BackToMenuBtn")
    {
        this->popAllAndPushScene(this->findByName("MenuScene"));
    } // end else if
} // void PauseScene::buttonHit(OgreBites::Button *button)
*/

void PauseScene::yesNoDialogClosed(const Ogre::DisplayString& _question, bool _yesHit)
{
    if(_yesHit == true)
    {
        this->shutdown();
    } // end if
    else
    {
        //OgreFramework::getSingletonPtr()->trayManager->closeDialog();
    } // end else

    this->questionActive = false;
} // void PauseScene::yesNoDialogClosed(const Ogre::DisplayString& _question, bool _yesHit)


bool PauseScene::buttonHit(const CEGUI::EventArgs &_evt)
{
  const CEGUI::WindowEventArgs* args = static_cast<const CEGUI::WindowEventArgs*>(&_evt);
  CEGUI::Window *button = args->window;

  AudioManager::getSingleton().playSound("ClickButton");

  if(button->getName() == "QuitToDesktopButton")
  {
    //OgreFramework::getSingletonPtr()->trayManager->showYesNoDialog("Sure?", "Really Quit To Desktop?");
    //this->questionActive = true;
    ParticleManager::getSingleton().releaseParticleSystems();
    this->shutdown();
  } // end if
  else if(button->getName() == "ReturnToGameButton")
  {
    this->quit = true;
  } // end else if
  else if(button->getName() == "QuitToMenuButton")
  {
    // Release the GameScene.
    this->releaseGameScene();
    this->popAllAndPushScene(this->findByName("MenuScene"));
  } // end else if

  return true;

} // void PauseScene::buttonHit(const CEGUI::EventArgs &_evt)


void PauseScene::releaseGameScene()
{

} // void PauseScene::releaseGameScene()