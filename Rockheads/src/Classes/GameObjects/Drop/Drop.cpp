#include "stdafx.h"
#include "Drop.h"


Drop::Drop()
{
  this->item = NULL;
  this->amountMin = 0;
  this->amountMax = 0;
  this->chance = 0;
} // Drop::Drop()


Drop::Drop(const Drop& _other)
{
  this->amountMin = _other.amountMin;
  this->amountMax = _other.amountMax;
  this->chance = _other.chance;

  if(_other.item)
  {
    this->item = _other.item->clone();
  } // end if
  else
  {
    this->item = NULL;
  } // end else
} // Drop::Drop(const Drop& _other)


Drop::~Drop()
{
  if(this->item)
  {
    delete this->item;
    this->item = NULL;
  } // end if
} // Drop::~Drop()


Drop* Drop::clone() const
{
  return new Drop(*this);
} // Drop* Drop::clone() const


Drop& Drop::operator=(const Drop& _other)
{
  this->amountMin = _other.amountMin;
  this->amountMax = _other.amountMax;
  this->chance = _other.chance;

  if(this->item)
  {
    delete this->item;
    this->item = NULL;
  } // end if
  if(_other.item)
  {
    this->item = _other.item->clone();
  } // end if
  else
  {
    this->item = NULL;
  } // end else

  return *this;

} // Drop& Dropoperator=(const Drop& _other)


AbstractItem* Drop::getItem() const
{
  return this->item;
} // Drop:getItem()


int Drop::getAmountMin() const
{
  return this->amountMin;
} // Drop::getCount()


int Drop::getAmountMax() const
{
  return this->amountMax;
} // Drop::getCount()


int Drop::getChance() const
{
  return this->chance; 
} // int Drop::getChance() const


void Drop::setItem(AbstractItem* _item)
{
  this->item = _item;
} // Drop::setitem(Item _item)


void Drop::setAmountMin(int _amount)
{
  this->amountMin = _amount;
} // Drop::setAmountMin(int _count)


void Drop::setAmountMax(int _amount)
{
  this->amountMax = _amount;
} // Drop::setAmountMax(int _count)


void Drop::setChance(int _chance)
{
  this->chance = _chance;
} // void Drop::setChance(int _chance) 
 