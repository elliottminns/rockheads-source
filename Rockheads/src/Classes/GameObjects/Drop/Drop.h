#include "../../../PrecompiledHeaders/stdafx.h"
#ifndef DROP_H
#define DROP_H

#include "../Items/AbstractItem/AbstractItem.h"

class Drop
{
public:
  Drop();
  Drop(const Drop& _other);
  ~Drop();
  Drop* clone() const;
  Drop& operator=(const Drop& _other);

  AbstractItem* getItem() const;
  int           getAmountMin() const;
  int           getAmountMax() const;
  int           getChance() const;

  void          setItem(AbstractItem* _item);
  void          setAmountMin(int _amount);
  void          setAmountMax(int _amount);
  void          setChance(int _chance);

private:
  AbstractItem* item;
  int           amountMin;
  int           amountMax;
  int           chance;

}; // end class Drop

#endif