#include "../../../../PrecompiledHeaders/stdafx.h"
#ifndef ARMOUR_H
#define ARMOUR_H

#include "../AbstractItem/AbstractItem.h"
#include <iostream>

using namespace std;

class Armour : public AbstractItem
{
private:
  string area;
  string type;
  int defence;

public:
  Armour();
  Armour(const Armour& _other);
  ~Armour();
  Armour* clone() const;
  Armour& operator=(const Armour& _other);

  bool equip(string _slot, Ogre::SceneManager* _sceneManager);

  void setArea(string _area);
  void setType(string _type);
  void setDefence(int _defence);
  
  string getArea() const;
  string getType() const;
  int getDefence() const;

  string getDescription() const;

}; // class Armour : public AbstractItem

#endif