#include "stdafx.h"
#include "Armour.h"
#include "boost\algorithm\string.hpp"
#include "../../../GameEntities/DynamicEntities/Player/Player.h"
#include "CEGUI\CEGUI.h"
#include "../../../GameEntities/DynamicEntities/EntityClass/SkillSet/SkillSet.h"
#include "../../../Managers/SkillManager/SkillManager.h"

Armour::Armour() : AbstractItem()
{

} // Armour::Armour()


Armour::Armour(const Armour& _other) : AbstractItem(_other)
{
  this->area = _other.area;
  this->defence = _other.defence;
  this->type = _other.type;
} // Armour::Armour(const Armour& _other)

Armour::~Armour()
{
  if(this->getVectorFrom())
  {
    delete this->getVectorFrom();
    this->setVectorFrom(NULL);
  } // end if
} // Armour::~Armour()


Armour* Armour::clone() const
{
  return new Armour(*this);
} // Armour* Armour::clone() const


Armour& Armour::operator=(const Armour& _other)
{
  this->area = _other.area;
  this->defence = _other.defence;
  this->type = _other.type;

  if(this->getVectorFrom())
  {
    delete this->getVectorFrom();
    this->setVectorFrom(NULL);
  } // end if
  if(_other.getVectorFrom())
	{
		vector<string>* tempVec = new vector<string>;
    
    for(vector<string>::iterator it = _other.getVectorFrom()->begin(); it != _other.getVectorFrom()->end(); ++it)
    {
      tempVec->push_back(*it);
    } // end for

    this->setVectorFrom(tempVec);
	} // end if
	else
  {
    this->setVectorFrom(NULL);
  } // end else

  this->setName(_other.getName());
  this->setValue(_other.getValue());
  this->setImageName(_other.getImageName());
  this->setStackable(_other.getStackable());

  return *this;
} // Armour& Armour::operator=(const Armour& _other)


bool Armour::equip(string _slot, Ogre::SceneManager* _sceneManager)
{
  string slotTypePrediction = "Slot-" + this->area;

  if(slotTypePrediction == _slot)
  {
    if(this->area == "Head")
    {
      if(Player::getInstance()->getSkillClass()->canEquip(this))
      {
        Player::getInstance()->setHead(this);
        return true;
      } // end if
    } // end if
    else if(this->area == "Chest")
    {
      if(Player::getInstance()->getSkillClass()->canEquip(this))
      {
        Player::getInstance()->setChest(this);
        return true;
      } // end if
    } // end else if
    else if(this->area == "Legs")
    {
      if(Player::getInstance()->getSkillClass()->canEquip(this))
      {
        Player::getInstance()->setLegs(this);
        return true;
      } // end if
    } // end else if
    else if(this->area == "Feet")
    {
      if(Player::getInstance()->getSkillClass()->canEquip(this))
      {
        Player::getInstance()->setFeet(this);
        return true;
      } // end if
    } // end else if
  } // end if

  return false;
} // bool Armour::equip(string _slot)


void Armour::setArea(string _area)
{
  this->area = _area;
} // void Armour::setArea(string _area)


void Armour::setType(string _type)
{
  this->type = _type;
} // void Armour::setType(string _type)


void Armour::setDefence(int _defence)
{
  this->defence = _defence;
} // void Armour::setDefence(int _defence)


string Armour::getArea() const
{
  return this->area;
} // string Armour::getArea()


string Armour::getType() const
{
  return this->type;
} // string Armour::getType()


int Armour::getDefence() const
{
  return this->defence;
} // int Armour::getDefence()

string Armour::getDescription() const
{
  string returnString;

  // Defence value.
  returnString += "Defence: ";
  returnString += CEGUI::PropertyHelper::intToString(this->defence).c_str();
  returnString += "\n";
  returnString += "Type: " + this->getType() + "\n";
  returnString += "Area: " + this->getArea() + "\n";
  returnString += "Class: ";

  int numSkillSets = SkillManager::getInstance()->getNumSkillSets();
  SkillSet** skillSets = SkillManager::getInstance()->getSkillSets();

  bool firstAdded = false;
  for(int i=0;i<numSkillSets;i++)
  {
    if(skillSets[i]->canEquip((Armour*)this))
    {
      if(!firstAdded)
      {
        firstAdded = true;
      } // end if
      else
      {
        returnString += "|";
      } // end else

      returnString+= skillSets[i]->getType();

    } // end if
  } // end for

  return returnString;

} // string Armour::getDescription() const