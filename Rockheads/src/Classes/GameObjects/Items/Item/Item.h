#include "../../../../PrecompiledHeaders/stdafx.h"
#ifndef ITEM_H
#define ITEM_H

#include "../AbstractItem/AbstractItem.h"

class Item : public AbstractItem
{
private:

public:
  Item();
  ~Item();
  Item(const Item& _other);
  Item& operator=(const Item& _other);
  Item* clone() const;

  string getDescription() const;

}; // end class Item

#endif