#include "stdafx.h"
#include "Item.h"



Item::Item() : AbstractItem()
{

} // Item::Item()


Item::Item(const Item& _other) : AbstractItem(_other)
{

} // Item::Item(const Item& _other)


Item::~Item()
{
  if(this->getVectorFrom())
  {
    delete this->getVectorFrom();
    this->setVectorFrom(NULL);
  } // end if
} // Item::~Item()


Item* Item::clone() const
{
  return new Item(*this);
} // Item* Item::clone() const


Item& Item::operator=(const Item& _other)
{
  // Abstract Item Stuff.
  vector<string>* tempFrom = this->getVectorFrom();
  if(this->getVectorFrom())
  {
    delete this->getVectorFrom();
    this->setVectorFrom(NULL);
  } // end if

  if(_other.getVectorFrom())
	{
		tempFrom = new vector<string>;
    
    for(vector<string>::iterator it = _other.getVectorFrom()->begin(); it != _other.getVectorFrom()->end(); ++it)
    {
      tempFrom->push_back(*it);
    } // end for
	} // end if
	else
  {
		tempFrom=NULL;
  } // end else
  this->setVectorFrom(tempFrom);
  this->setName(_other.getName());
  this->setValue(_other.getValue());
  this->setImageName(_other.getImageName());
  this->setStackable(_other.getStackable());

  return *this;
} // Item Item::operator=(const Item& _other)


string Item::getDescription() const
{
  string description = "A " + this->getName();
  return description;
} // string Item::getDescription() const