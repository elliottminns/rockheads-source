#include "stdafx.h"
#include "Weapon.h"
#include "../../../GameEntities/DynamicEntities/Player/Player.h"
#include "CEGUI\CEGUI.h"
#include "../../../Managers/SkillManager/SkillManager.h"

Weapon::Weapon() : AbstractItem()
{
  this->wieldType = ""; 
  this->modelName = "";
  this->minimumDamage = 0;
  this->maximumDamage = 0;
  this->cooldown = 0;
  this->animationType = "";
  this->range = 12;
  this->handed = "";
  this->timing = 0.7;
  this->requiresAmmo = false;
} // Weapon::Weapon()


Weapon::Weapon(const Weapon& _other) : AbstractItem(_other)
{
  this->wieldType = _other.wieldType; 
  this->modelName = _other.modelName;
  this->minimumDamage = _other.minimumDamage;
  this->maximumDamage = _other.maximumDamage;
  this->cooldown = _other.cooldown;
  this->animationType = _other.animationType;
  this->range = _other.range;
  this->handed = _other.handed;
  this->timing = _other.timing;
  this->requiresAmmo = _other.requiresAmmo;
  this->ammoNames = _other.ammoNames;
} // Weapon::Weapon(const Weapon& _other)


Weapon::~Weapon()
{
  if(this->getVectorFrom())
  {
    delete this->getVectorFrom();
    this->setVectorFrom(NULL);
  } // end if
} // Weapon::~Weapon()


Weapon& Weapon::operator=(const Weapon& _other)
{  
  // Weapon stuff.
  this->wieldType = _other.wieldType; 
  this->modelName = _other.modelName;
  this->minimumDamage = _other.minimumDamage;
  this->maximumDamage = _other.maximumDamage;
  this->cooldown = _other.cooldown;
  this->animationType = _other.animationType;
  this->range = _other.range;
  this->handed = _other.handed;
  this->timing = _other.timing;
  this->requiresAmmo = _other.requiresAmmo;
  this->ammoNames = _other.ammoNames;

  // Abstract Item Stuff.
  vector<string>* tempFrom = this->getVectorFrom();
  if(this->getVectorFrom())
  {
    delete this->getVectorFrom();
    this->setVectorFrom(NULL);
  } // end if
  if(_other.getVectorFrom())
	{
		tempFrom = new vector<string>;
    
    for(vector<string>::iterator it = _other.getVectorFrom()->begin(); it != _other.getVectorFrom()->end(); ++it)
    {
      tempFrom->push_back(it->c_str());
    } // end for
	} // end if
	else
  {
		tempFrom=NULL;
  } // end else
  this->setVectorFrom(tempFrom);
  this->setName(_other.getName());
  this->setValue(_other.getValue());
  this->setImageName(_other.getImageName());
  this->setStackable(_other.getStackable());

  return *this;
} // Weapon::operator=(const Weapon& _other)


Weapon* Weapon::clone() const
{
  return new Weapon(*this);
} // Weapon* Weapon::clone() const


bool Weapon::equip(string _slot, Ogre::SceneManager* _sceneManager)
{
  if(_slot == "Slot-Weapon")
  { 
    if(this->wieldType == "Ammo")
    {
      return false;
    } // end if
    else
    {
      if(Player::getInstance()->getSkillClass()->canEquip(this))
      {
        Player::getInstance()->setWeapon(this, _sceneManager);
        return true;
      } // end if
    } // end else
  } // end if
  return false;
} // bool Weapon::equip(string _slot)


void Weapon::setWieldType(string _type)
{
  this->wieldType = _type;
} // void Weapon::setWieldType()


void Weapon::setMinimumDamage(int _damage)
{
  this->minimumDamage = _damage;
} // void Weapon::setMinimumDamage(int _damage)


void Weapon::setMaximumDamage(int _damage)
{
  this->maximumDamage = _damage;
} // void Weapon::setMaximumDamage(int _damage)


void Weapon::setCooldown(double _cooldown)
{
  this->cooldown = _cooldown;
} // void Weapon::setCooldown(double _cooldown)


void Weapon::setModelName(string _modelName)
{
  this->modelName = _modelName;
} // void Weapon::setModelName(string _modelName)


void Weapon::setAnimationType(string _aniType)
{
  this->animationType = _aniType;
} // void Weapon::setAnimationType(string _aniType)


void Weapon::setRange(int _range)
{
  this->range = _range;
} // void Weapon::setRange(int _range)


void Weapon::setHanded(string _handed)
{
  this->handed = _handed;
} // void Weapon::setHanded(int _range)


void Weapon::setTiming(double _timing)
{
  this->timing = _timing;
} // void Weapon::setTiming(double _timing)


void Weapon::setRequiresAmmo(bool _ammo)
{
  this->requiresAmmo = _ammo;
} // void Weapon::setRequiresAmmo(bool _ammo)


void Weapon::addAmmoName(string _name)
{
  this->ammoNames.push_back(_name);
} // void Weapon::addAmmoName(string _name)


string Weapon::getWieldType() const
{
  return this->wieldType;
} // string getWieldType()


int Weapon::getMinimumDamage() const
{
  return this->minimumDamage;
} // int Weapon::getMinimumDamage()


int Weapon::getMaximumDamage() const
{
  return this->maximumDamage;
} // int Weapon::getmaximumDamage()


double Weapon::getCooldown() const
{
  return this->cooldown;
} // double Weapon::getCooldown()


string Weapon::getModelName() const 
{
  return this->modelName;
} // string Weapon::getModelName()


string Weapon::getDescription() const
{
  string returnString;

  // Damage.
  returnString = "Damage: ";
  returnString += CEGUI::PropertyHelper::intToString(this->minimumDamage).c_str();
  returnString += "/";
  returnString += CEGUI::PropertyHelper::intToString(this->maximumDamage).c_str(); 
  returnString += "\n";

  // Delay
  returnString += "Delay: ";
  returnString += CEGUI::PropertyHelper::intToString(this->cooldown).c_str();
  returnString += "\n";

  // Type
  returnString += "Type: " + this->wieldType + "\n";

  // Class
  returnString += "Class: ";

  int numSkillSets = SkillManager::getInstance()->getNumSkillSets();
  SkillSet** skillSets = SkillManager::getInstance()->getSkillSets();

  bool firstAdded = false;
  for(int i=0;i<numSkillSets;i++)
  {
    if(skillSets[i]->canEquip((Weapon*)this))
    {
      if(!firstAdded)
      {
        firstAdded = true;
      } // end if
      else
      {
        returnString += "|";
      } // end else

      returnString+= skillSets[i]->getType();

    } // end if
  } // end for

  return returnString;

  return returnString;

} // string Weapon::getDescription() const


string Weapon::getAnimationType() const
{
  return this->animationType;
} // string Weapon::getAnimationType() const


int Weapon::getRange() const
{
  return this->range;
} // int Weapon::getRange() const


string Weapon::getHanded() const
{
  return this->handed;
} // string Weapon::getHanded() const


double Weapon::getTiming() const
{
  return this->timing;
} // double Weapon::getTiming() const


bool Weapon::getRequiresAmmo() const
{
  return this->requiresAmmo;
} // bool Weapon::getRequiresAmmo() const


vector<string> Weapon::getAmmoNames() const
{
  return this->ammoNames;
} // vector<string> Weapon::getAmmoNames() const