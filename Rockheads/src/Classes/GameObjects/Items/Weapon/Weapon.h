#include "../../../../PrecompiledHeaders/stdafx.h"
#ifndef WEAPON_H
#define WEAPON_H

#include "../AbstractItem/AbstractItem.h"

class Weapon : public AbstractItem
{
private:
  string wieldType;
  int minimumDamage;
  int maximumDamage;
  double cooldown;
  string modelName;
  string animationType;
  string handed;
  int range;
  double timing;
  bool requiresAmmo;
  vector<string> ammoNames;

public:
  
  Weapon();
  ~Weapon();
  Weapon(const Weapon& _other);
  Weapon& operator=(const Weapon& _other);
  Weapon* clone() const;
  
  bool equip(string _slot, Ogre::SceneManager* _sceneManager);

  void setWieldType(string _type);
  void setMinimumDamage(int _damage);
  void setMaximumDamage(int _damage);
  void setCooldown(double _cooldown);
  void setModelName(string _modelName);
  void setAnimationType(string _aniType);
  void setRange(int _range);
  void setHanded(string _handed);
  void setTiming(double _timing);
  void setRequiresAmmo(bool _ammo);
  void addAmmoName(string _name);

  
  string getWieldType() const;
  int getMinimumDamage() const;
  int getMaximumDamage() const;
  double getCooldown() const;
  string getModelName() const;
  string getDescription() const;
  string getAnimationType() const;
  string getHanded() const;
  int getRange() const;
  double getTiming() const;
  bool getRequiresAmmo() const;
  vector<string> getAmmoNames() const;

};

#endif