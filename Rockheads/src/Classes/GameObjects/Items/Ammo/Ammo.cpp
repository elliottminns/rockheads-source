#include "stdafx.h"
#include "Ammo.h"



Ammo::Ammo() : AbstractItem()
{
  this->minDamage = 0;
  this->maxDamage = 0;
  this->description = "";
} // Ammo::Ammo()


Ammo::~Ammo()
{

} // Ammo::~Ammo()


Ammo::Ammo(const Ammo& _other) : AbstractItem(_other)
{
  this->minDamage = _other.minDamage;
  this->maxDamage = _other.maxDamage;
  this->description = _other.description;
} // Ammo::Ammo(const Ammo& _other)


Ammo& Ammo::operator=(const Ammo& _other)
{
  // Ammo copy stuff.
  this->minDamage = _other.minDamage;
  this->maxDamage = _other.maxDamage;
  this->description = _other.description;
  // AbstractItem copy stuff.

  if(this->getVectorFrom())
  {
    delete this->getVectorFrom();
    this->setVectorFrom(NULL);
  } // end if
  if(_other.getVectorFrom())
	{
		vector<string>* tempVec = new vector<string>;
    
    for(vector<string>::iterator it = _other.getVectorFrom()->begin(); it != _other.getVectorFrom()->end(); ++it)
    {
      tempVec->push_back(*it);
    } // end for

    this->setVectorFrom(tempVec);
	} // end if
	else
  {
    this->setVectorFrom(NULL);
  } // end else

  this->setName(_other.getName());
  this->setValue(_other.getValue());
  this->setImageName(_other.getImageName());
  this->setStackable(_other.getStackable());

  return *this;
} // Ammo& Ammo::operator=(const Ammo& _other)


Ammo* Ammo::clone() const
{
  return new Ammo(*this);
} // Ammo* Ammo::clone() const


void Ammo::setMinDamage(int _damage)
{
  this->minDamage = _damage;
} // void Ammo::setMinDamage(int _damage)


void Ammo::setMaxDamage(int _damage)
{
  this->maxDamage = _damage;
} // void Ammo::setMaxDamage(int _damage)


void Ammo::setDescription(string _description)
{
  this->description = _description;
} // void Ammo::setDescription(string _description)


int Ammo::getMinDamage() const
{
  return this->minDamage;
} // int Ammo::getMinDamage() const


int Ammo::getMaxDamage() const
{
  return this->maxDamage;
} // int Ammo::getMaxDamage() const


string Ammo::getDescription() const
{
  string returnString = this->getDescription();

  returnString += "\nDamage: " + Ogre::StringConverter::toString(this->minDamage) + 
    "/" + Ogre::StringConverter::toString(this->maxDamage);

  return returnString;
} // string Ammo::getDescription() const