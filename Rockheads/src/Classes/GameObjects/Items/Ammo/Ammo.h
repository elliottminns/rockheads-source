#include "../../../../PrecompiledHeaders/stdafx.h"
#ifndef AMMO_H
#define AMMO_H

#include "../AbstractItem/AbstractItem.h"

class Ammo : public AbstractItem
{
private:
  int minDamage;
  int maxDamage;
  string description;

public:
  Ammo();
  virtual ~Ammo();
  Ammo(const Ammo& _other);
  virtual Ammo& operator=(const Ammo& _other);
  virtual Ammo* clone() const;

  void setMinDamage(int _damage);
  void setMaxDamage(int _damage);
  void setDescription(string _description);

  int getMinDamage() const;
  int getMaxDamage() const;
  string getDescription() const;

};

#endif