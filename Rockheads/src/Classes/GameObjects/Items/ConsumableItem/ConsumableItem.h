#include "../../../../PrecompiledHeaders/stdafx.h"
#ifndef CONSUMABLE_ITEM_H
#define CONSUMABLE_ITEM_H

#include "../AbstractItem/AbstractItem.h"
#include "../../../Effects/AbstractEffect/AbstractEffect.h"

class ConsumableItem : public AbstractItem
{
private:
  AbstractEffect* effect;
  string effectName;
  int effectLevel;
  bool combatAble;

public:
  ConsumableItem();
  ~ConsumableItem();
  ConsumableItem(const ConsumableItem& _other);
  ConsumableItem& operator=(const ConsumableItem& _other);
  ConsumableItem* clone() const; 

  void setEffect(AbstractEffect* _effect);
  void setEffectName(string _name);
  void setEffectLevel(int _level);
  void setCombatAble(bool _able);

  AbstractEffect* getEffect();
  string getDescription() const;
  int getEffectLevel() const;
  bool getCombatAble() const;

  bool useItem();
}; // end class ConsumableItem

#endif