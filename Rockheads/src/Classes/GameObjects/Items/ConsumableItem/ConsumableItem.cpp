#include "stdafx.h"
#include "ConsumableItem.h"
#include "../../../Managers/EffectManager/EffectManager.h"


ConsumableItem::ConsumableItem() : AbstractItem()
{
  this->effect = NULL;
  this->effectName = "";
  this->combatAble = true;
} // ConsumableItem::ConsumableItem()


ConsumableItem::ConsumableItem(const ConsumableItem& _other) : AbstractItem(_other)
{
  this->effectName = _other.effectName;
  this->combatAble = _other.combatAble;
  if(_other.effect)
  {
    this->effect = _other.effect;
  } // end if
  else
  {
    this->effect = NULL;
  } // end else
} // ConsumableItem::ConsumableItem(const ConsumableItem& _other)


ConsumableItem::~ConsumableItem()
{
  // Abstract Item Attributes.
  if(this->getVectorFrom())
  {
    delete this->getVectorFrom();
    this->setVectorFrom(NULL);
  } // end if

  // ConsumableItem Attributes.
  if(this->effect)
  {
    this->effect = NULL;
  } // end if
} // ConsumableItem::~ConsumableItem()


ConsumableItem* ConsumableItem::clone() const
{
  return new ConsumableItem(*this);
} // ConsumableItem* ConsumableItem::clone() const


void ConsumableItem::setEffect(AbstractEffect* _effect)
{
  this->effect = _effect;
} // void ConsumableItem::setEffect(Effect* _effect)


void ConsumableItem::setEffectName(string _name)
{
  this->effectName = _name;
  this->setEffect(EffectManager::getSingleton().getEffect(this->effectName));
} // void ConsumableItem::setEffectName(string _name)


void ConsumableItem::setEffectLevel(int _level)
{
  this->effectLevel = _level;
} // void ConsumableItem::setEffectLevel(int _level)


void ConsumableItem::setCombatAble(bool _able)
{
  this->combatAble = _able;
} // void ConsumableItem::setCombatAble(bool _able)


AbstractEffect* ConsumableItem::getEffect()
{
  return this->effect;
} // Effect* ConsumableItem::getEffect()


bool ConsumableItem::useItem()
{
  return false;
} // ConsumableItem::useItem()


string ConsumableItem::getDescription() const
{
  string returnString = "";
  if(!this->getHotSlotAble())
  {
    returnString += "Used for the " + this->effectName + " skill.";
  } 
  else
  {
    returnString += this->effect->getDetailsDescription();
  } // end else

  if(!this->combatAble)
  {
    returnString += "\nCannot be used in combat";
  } // end if
  return returnString;
} // string ConsumableItem::getDescription() const


bool ConsumableItem::getCombatAble() const
{
  return this->combatAble;
} // bool ConsumableItem::getCombatAble() const