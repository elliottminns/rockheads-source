#include "stdafx.h"
#include "CraftingItem.h"


CraftingItem::CraftingItem() : AbstractItem()
{

} // CraftingItem::CraftingItem()


CraftingItem::~CraftingItem()
{
  if(this->getVectorFrom())
  {
    delete this->getVectorFrom();
    this->setVectorFrom(NULL);
  } // end if
} // CraftingItem::~CraftingItem()


CraftingItem::CraftingItem(const CraftingItem& _other) : AbstractItem(_other)
{

} // CraftingItem::CraftingItem(const CraftingItem& _other)


CraftingItem& CraftingItem::operator=(const CraftingItem& _other)
{
  // Abstract Item Stuff.
  vector<string>* tempFrom;
  if(this->getVectorFrom())
  {
    delete this->getVectorFrom();
    this->setVectorFrom(NULL);
  } // end if

  if(_other.getVectorFrom())
	{
		tempFrom = new vector<string>;
    
    for(vector<string>::iterator it = _other.getVectorFrom()->begin(); it != _other.getVectorFrom()->end(); ++it)
    {
      tempFrom->push_back(*it);
    } // end for
	} // end if
	else
  {
		tempFrom=NULL;
  } // end else
  this->setVectorFrom(tempFrom);
  this->setName(_other.getName());
  this->setValue(_other.getValue());
  this->setImageName(_other.getImageName());
  this->setStackable(_other.getStackable());

  return *this;
} // CraftingItem& CraftingItem::operator=(const CraftingItem& _other)


CraftingItem* CraftingItem::clone() const
{
  return new CraftingItem(*this);
} // CraftingItem* CraftingItem::clone() const


string CraftingItem::getDescription() const
{
  return this->getFileDescription();
} // string CraftingItem::getDescription() const