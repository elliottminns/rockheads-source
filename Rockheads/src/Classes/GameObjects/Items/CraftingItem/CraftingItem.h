#include "../../../../PrecompiledHeaders/stdafx.h"
#ifndef CRAFTING_ITEM_H
#define CRAFTING_ITEM_H

#include "../AbstractItem/AbstractItem.h"
#include <iostream>
#include <vector>

using namespace std;

class CraftingItem : public AbstractItem
{
private:


public:
  CraftingItem();
  CraftingItem(const CraftingItem& _other);
 ~CraftingItem();
  CraftingItem& operator=(const CraftingItem& _other);
  CraftingItem* clone() const;
  
  string getDescription() const;

}; // end class CraftingItem

#endif