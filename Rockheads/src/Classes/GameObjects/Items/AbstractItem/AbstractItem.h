#include "../../../../PrecompiledHeaders/stdafx.h"
#ifndef ABSTRACT_ITEM_H
#define ABSTRACT_ITEM_H

#include <iostream>
#include <string>
#include <vector>
#include <Ogre.h>

using namespace std;

class AbstractItem
{
private:
  int value;
  string name;
  vector<string>* from;
  string imageName;
  bool stackable;
  bool hotSlotAble;
  string fileDescription;

public:
  
  AbstractItem();
  virtual ~AbstractItem();
  AbstractItem(const AbstractItem& _other);
  virtual AbstractItem& operator=(const AbstractItem& _other);
  virtual AbstractItem* clone() const = 0;
  
  virtual bool equip(string _slotName, Ogre::SceneManager* _sceneMananger){return false;};

  void setValue(int _value);
  void setName(string _name);
  void setImageName(string _name);
  void setVectorFrom(vector<string>* _from);
  void setStackable(bool _stackable);
  void setHotSlotAble(bool _able);
  void setFileDescription(string _description);
  
  int     getValue() const;
  string  getName() const;
  string  getImageName() const;
  vector<string>* getVectorFrom() const;
  bool    getStackable() const;
  string getFileDescription() const;
  virtual string getDescription() const {return " ";};
  bool getHotSlotAble() const;

}; // end class AbstractItem

#endif