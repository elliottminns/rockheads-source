#include "stdafx.h"
#include "AbstractItem.h"


AbstractItem::AbstractItem()
{ 
  this->from = new vector<string>;
  this->stackable = false;
  this->hotSlotAble = false;
  this->fileDescription = "";
} // AbstractItem::AbstractItem()


AbstractItem::~AbstractItem()
{
  if(this->from)
  {
    delete this->from;
    this->from = NULL;
  } // end if
  this->stackable = false;
} // AbstractItem::~AbstractItem()


AbstractItem::AbstractItem(const AbstractItem& _other)
{
  if(_other.from)
  {
    this->from = new vector<string>;
    for(vector<string>::iterator it = _other.from->begin(); it != _other.from->end(); ++it)
    {
      this->from->push_back(*it);
    } // end for
    this->from = _other.from;
  } // end if
  else
  {
    this->from = NULL;
  } // end else
  this->imageName = _other.imageName;
  this->name = _other.name;
  this->value = _other.value;
  this->stackable = _other.stackable;
  this->hotSlotAble = _other.hotSlotAble;
  this->fileDescription = _other.fileDescription;
} // AbstractItem::AbstractItem(const AbstractItem& _otherAbstractItem)


AbstractItem& AbstractItem::operator=(const AbstractItem& _other)
{
  if(this->from)
  {
		delete this->from;
  }
	if(_other.from)
	{
		this->from = new vector<string>;
    
    for(vector<string>::iterator it = _other.from->begin(); it != _other.from->end(); ++it)
    {
      this->from->push_back(it->c_str());
    } // end for
	} // end if
	else
  {
		this->from=NULL;
  } // end else

  this->imageName = _other.imageName;
  this->name = _other.name;
  this->value = _other.value;
  this->hotSlotAble = _other.hotSlotAble;
  this->stackable = _other.stackable;
  this->fileDescription = _other.fileDescription;

	return *this;
} // AbstractItem& AbstractItem::operator=(const AbstractItem& _other)

void AbstractItem::setValue(int _value)
{
  this->value = _value;
} // void AbstractItem::setValue(int _value)


void AbstractItem::setName(string _name)
{
  this->name = _name;
} // void AbstractItem::setName(string _name)


void AbstractItem::setImageName(string _name)
{
  this->imageName = _name;
} // void AbstractItem::setImageName(string _name)


void AbstractItem::setVectorFrom(vector<string>* _from)
{
  this->from = _from;
} // void AbstractItem::setVectorFrom(vector<string>* _from)


void AbstractItem::setStackable(bool _stackable)
{
  this->stackable = _stackable;
} // void AbstractItem::setStackable(bool _stackable)


void AbstractItem::setHotSlotAble(bool _able)
{
  this->hotSlotAble = _able;
} // void AbstractItem::setHotSlotAble(bool _able)


void AbstractItem::setFileDescription(string _description)
{
  this->fileDescription = _description;
} // void AbstractItem::setDescription(string _description) 


int AbstractItem::getValue() const
{
  return this->value;
} // int AbstractItem::getValue()


string AbstractItem::getName() const
{
  return this->name;
} // string AbstractItem::getName()


string AbstractItem::getImageName() const
{
  return this->imageName;
} // string AbstractItem::getModelName()


vector<string>* AbstractItem::getVectorFrom() const
{
  return this->from;
} // vector<string>* AbstractItem::getVectorFrom() const\


bool AbstractItem::getStackable() const
{
  return this->stackable;
} // bool AbstractItem::getStackable() const


bool AbstractItem::getHotSlotAble() const
{
  return this->hotSlotAble;
} // bool AbstractItem::getHotSlotAble() const


string AbstractItem::getFileDescription() const
{
  return this->fileDescription;
} // string AbstractItem::getFileDescription() const