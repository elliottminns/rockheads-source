#include "../../../PrecompiledHeaders/stdafx.h"
#ifndef INVENTORY_SLOT_H
#define INVENTORY_SLOT_H

#include "../Items/AbstractItem/AbstractItem.h"

class InventorySlot
{
public:
  AbstractItem*   getItem();
  int             getCount();

  void            setItem(AbstractItem* _item);
  void            setCount(int _count);
  void setIndex(int _number);

  int getIndex();

private:
  int index;
  AbstractItem*   item;
  int             count;
}; // end class InventorySlot

#endif