#include "stdafx.h"
#include "InventorySlot.h"
#include "../../Managers/GUIManager/GUIManager.h"

AbstractItem* InventorySlot::getItem()
{
  return this->item;
} // InventorySlot::getItem()


int InventorySlot::getCount()
{
  return this->count;
} // Inventory::getCount()


int InventorySlot::getIndex()
{
  return this->index;
} // int InventorySlot::getIndex()


void InventorySlot::setItem(AbstractItem* _item)
{
  this->item = _item;
} // InventorySlot::setItem(Item _item)


void InventorySlot::setCount(int _count)
{
  this->count = _count;
} // InventorySlot::setCount(int _count)


void InventorySlot::setIndex(int _index)
{
  this->index = _index;
} // void InventorySlot::setIndex(int _index)