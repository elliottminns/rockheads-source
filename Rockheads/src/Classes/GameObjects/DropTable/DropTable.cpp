#include "stdafx.h"
#include "DropTable.h"


DropTable::DropTable()
{
  this->drops = NULL;
  this->currencyMax = 0;
  this->currencyMin = 0;
  this->numDrops = 0;
  this->mobs = NULL;
} // DropTable::DropTable()


DropTable::DropTable(const DropTable& _other)
{
  this->currencyMax = _other.currencyMax;
  this->currencyMin = _other.currencyMin;
  this->numDrops = _other.numDrops;


  if(_other.drops)
  {
    this->drops = new Drop*[numDrops];

    for(int i=0;i<numDrops;i++)
    {
      this->drops[i] = _other.drops[i]->clone();
    } // end for

  } // end if
  else
  {
    this->drops = NULL;
  } // end else

  if(_other.mobs)
  {
    this->mobs = new vector<string>;

    for(vector<string>::iterator it = _other.mobs->begin(); it != _other.mobs->end(); ++it)
    {
      this->mobs->push_back(*it);
    } // end for

  } // end if
  else
  {
    this->mobs = NULL;
  } // end else

} // DropTable::DropTable(const DropTable& _other)


DropTable::~DropTable()
{
  if(this->drops)
  {
    delete[] drops;
  } // end if
} // DropTable::~DropTable()


DropTable* DropTable::clone() const
{
  return new DropTable(*this);
} // DropTable* DropTable::clone() const


DropTable& DropTable::operator=(const DropTable& _other)
{
  this->currencyMax = _other.currencyMax;
  this->currencyMin = _other.currencyMin;
  this->numDrops = _other.numDrops;

  if(this->drops)
  {
    delete[] drops;
  } // end if
  if(_other.drops)
  {
    this->drops = new Drop*[numDrops];

    for(int i=0;i<numDrops;i++)
    {
      this->drops[i] = _other.drops[i]->clone();
    } // end for

  } // end if
  else
  {
    this->drops = NULL;
  } // end else

  if(this->mobs)
  {
    delete[] mobs;
  } // end if
  if(_other.mobs)
  {
    this->mobs = new vector<string>;

    for(vector<string>::iterator it = _other.mobs->begin(); it != _other.mobs->end(); ++it)
    {
      this->mobs->push_back(*it);
    } // end for

  } // end if
  else
  {
    this->mobs = NULL;
  } // end else

  return *this;
} // DropTable& DropTable::operator=(const DropTable& _other)


void DropTable::DropItems()
{

} // Droptable::DropItems()


void DropTable::setDrops(vector<Drop*>* _drops)
{
  this->numDrops = _drops->size();

  this->drops = new Drop*[numDrops];

  int i=0;
  for(vector<Drop*>::iterator it = _drops->begin(); it != _drops->end(); ++it)
  { 
    Drop* drop = *it;

    this->drops[i] = drop;

    ++i;
  } // for(vector<Drop*>::iterator it = _drops->begin(); it != _drops->end(); ++it)

} // void DropTable::setDrops(vector<Drop*> *drops)


void DropTable::setCurrencyMin(int _currency)
{
  this->currencyMin = _currency;
} // void DropTable::setCurrencyMin(int _currency)


void DropTable::setCurrencyMax(int _currency)
{
  this->currencyMax = _currency;
} //  void DropTable::setCurrencyMax(int _currency)


void DropTable::setName(string _name)
{
  this->name = _name;
} //  void DropTable::setName(string _name)


void DropTable::addMob(string _mob)
{
  if(this->mobs == NULL)
  {
    this->mobs = new vector<string>;
  } // end if

  this->mobs->push_back(_mob);
} // void DropTable::addMob(string _mob);


Drop** DropTable::getDrops() const
{
  return this->drops;
} // vector<Drop*>* DropTable::getDrops()


int DropTable::getCurrencyMin() const
{
  return this->currencyMin;
} // int DropTable::getCurrencyMin()


int DropTable::getCurrencyMax() const
{
  return this->currencyMax;
} // int DropTable::getCurrencyMax()


string DropTable::getName() const
{
  return this->name;
} // string DropTable::getName()


vector<string>* DropTable::getMobs() const
{
  return this->mobs;
} // vector<string> DropTable::getMobs()


int DropTable::getNumDrops() const
{
  return this->numDrops;
} // int DropTable::getNumDrops() const