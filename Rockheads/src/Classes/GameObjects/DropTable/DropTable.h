#include "../../../PrecompiledHeaders/stdafx.h"
#ifndef DROP_TABLE_H
#define DROP_TABLE_H

#include "../Drop/Drop.h"
#include <iostream>
#include <vector>

using namespace std;

class DropTable
{
public:
  DropTable();
  DropTable(const DropTable& _other);
  ~DropTable();
  DropTable* clone() const;
  DropTable& operator=(const DropTable& _other);

  void DropItems();

  void setDrops(vector<Drop*>* _drops);
  void setCurrencyMin(int _currency);
  void setCurrencyMax(int _currency);
  void setName(string _name);
  void addMob(string _mob);

  Drop** getDrops() const;
  int getCurrencyMin() const;
  int getCurrencyMax() const;
  string getName() const;
  vector<string>* getMobs() const;
  int getNumDrops() const;

private:
  int     numDrops;
  Drop**  drops;
  int     currencyMax;
  int     currencyMin;
  string  name;
  vector<string>* mobs;
}; // end class DropTable

#endif