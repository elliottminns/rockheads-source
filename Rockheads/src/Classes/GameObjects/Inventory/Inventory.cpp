#include "stdafx.h"
#include "Inventory.h"
#include "../../GameScenes/GameScene/GameScene.h"
#include "../../Managers/GUIManager/GUIManager.h"

Inventory::Inventory(AbstractEntity* _owner)
{
  this->items = new vector<InventorySlot*>;

  this->owner = _owner;
  this->previousInventorySize = 0;
} // Inventory::Inventory()


vector<InventorySlot*>* Inventory::getItems()
{
  return this->items;
} // Inventory::getItems()


void Inventory::addItemAtEmptySlot(AbstractItem* _item, int _count)
{
  // Get the index number of this item.
  int indexNumber = 0;
  bool indexFound = false;

  if(_item->getStackable())
  {
    int i=0;
    bool added = false;

    // Iterate through the slot and determine if there is another version.
    for(vector<InventorySlot*>::iterator it = this->items->begin(); it!=this->items->end();++it)
    {
      InventorySlot* invSlot = *it;
      AbstractItem* invItem = invSlot->getItem();
      if(invItem)
      {
        if(invItem->getName() == _item->getName() && added == false)
        {
          added = true;
          invSlot->setCount(invSlot->getCount()+_count);

          if(this->owner == Player::getInstance())
          {
            // Update CEGUI.
            CEGUI::WindowManager& winManager = CEGUI::WindowManager::getSingleton();
            if(winManager.isWindowPresent("PlayerInventory"))
            {
              CEGUI::Window* invSlotWindow = winManager.getWindow("InvSlot-" + CEGUI::PropertyHelper::intToString(i));
              if(invSlotWindow->getChildAtIdx(0))
              {
                CEGUI::DragContainer* container = (CEGUI::DragContainer*)invSlotWindow->getChildAtIdx(0);
                CEGUI::Window* invSlotText = winManager.getWindow(container->getChildAtIdx(1)->getName());
                invSlotText->setText(CEGUI::PropertyHelper::intToString(invSlot->getCount()));
              } // end if
            } // end if
          } // end if(this->owner == Player::getInstance()
          
        } // end if
      } // end if
      ++i;
    } // end for
    if(!added)
    {
      this->addEmptySlot();
      // If no stackable found.
      this->placeItemInEmptySlot(_item, _count);
    } // end if

  } // end if(_item->getStackable())
  else
  {
    for(int i=0;i<_count;i++)
    {
      this->addEmptySlot();
      this->placeItemInEmptySlot(_item, 1);
    } // end for

  } // end else

} // Inventory::addItem(AbstractItem* _item, int _count)


void Inventory::addItemAtSlot(AbstractItem* _item, int _count, int _slot)
{
  for(vector<InventorySlot*>::iterator it = this->items->begin(); it!=this->items->end();++it)
  {
    InventorySlot* slot = *it;
    if(slot->getIndex() == _slot)
    {
      slot->setItem(_item);
      slot->setCount(_count);
      break;
    } // end if
  } // end for
} // void Inventory::addItemAtSlot(AbstractItem* _item, int _count, int _slot)


bool Inventory::removeItem(int _slot, bool _delete)
{
  vector<InventorySlot*>::iterator it = this->items->begin();

  for(int i=0;i<_slot;i++)
  {
    ++it;
    if(it == this->items->end())
    {
      return false;
    } // end if
  } // end for
  InventorySlot* slot = *it;
  slot->setCount(0);
  slot->setItem(NULL);

  return true;
} // Inventory::removeItem(int _slot)


bool Inventory::removeItem(InventorySlot* _slot, bool _delete)
{
  _slot->setItem(NULL);
  _slot->setCount(0);
  GUIManager::getSingletonPtr()->removeItemFromSlot(_slot, _delete);
  this->removeEmptySlot();

  return true;
} // Inventory::removeItem(int _slot)


void Inventory::addEmptySlot()
{
  InventorySlot* emptySlot = new InventorySlot;
  emptySlot->setItem(NULL);
  emptySlot->setCount(0);
  emptySlot->setIndex(this->items->size());
  this->items->push_back(emptySlot);

  if(this->owner == Player::getInstance())
  {
    GUIManager::getSingletonPtr()->addEmptySlotToPlayerInventory(emptySlot);
  } // end if

} // void Inventory::addEmptySlot()


void Inventory::removeEmptySlot()
{
  vector<InventorySlot*>::iterator eraseIt;
  for(vector<InventorySlot*>::iterator it = this->items->begin(); it!=this->items->end();++it)
  {
    InventorySlot* slot = *it;
    if(slot->getItem() == NULL)
    {
      // Finds the last free item in the list.
      eraseIt = it;
    } // end if
  } // end if

  GUIManager::getSingletonPtr()->removeEmptySlotFromPlayerInventory(*eraseIt);
  InventorySlot* slot = *eraseIt;
  delete slot;
  this->items->erase(eraseIt);

  // work out the indexes of the remaining slots.
  int i=0;
  for(vector<InventorySlot*>::iterator it = this->items->begin(); it!=this->items->end();++it)
  {
    InventorySlot* currentSlot = *it;
    // Rename the CEGUI window if there is a difference.
    if(currentSlot->getIndex() != i)
    {
      //GUIManager::getSingleton().renameInventorySlots(currentSlot->getIndex(),i);
    } // end if

    currentSlot->setIndex(i);
    ++i;

    // Rename the CEGUI window.

  } // end if

} // void Inventory::removeEmptySlot()


void Inventory::placeItemInEmptySlot(AbstractItem*  _item, int _count)
{
  int i=0;
  AbstractItem* item = NULL;
  // Iterate through the slot and find an empty slot.
  for(vector<InventorySlot*>::iterator it = this->items->begin(); it!=this->items->end();++it)
  {
    InventorySlot* currentSlot = *it;
    if(currentSlot->getItem() == NULL)
    {
      currentSlot->setItem(_item->clone());
      item = currentSlot->getItem();
      currentSlot->setCount(_count);
      currentSlot->setIndex(i);
      break;
    } // end if
    ++i;
  } // end for

  if(this->owner == Player::getInstance())
  {
    GUIManager::getSingletonPtr()->addItemToPlayerInventory(i, item, _count);
  } // end if

} // void Inventory::placeItemInEmptySlot(AbstractItem*  _item, int _count)


void Inventory::swapSlots(int _slot1, int _slot2)
{
  vector<InventorySlot*>::iterator it1 = this->items->begin();
  vector<InventorySlot*>::iterator it2 = this->items->begin();

  // Add pointer arithmetic.
  it1 += _slot1;
  InventorySlot* slot1 = *it1;

  // Rinse and repeat.
  it2 += _slot2;
  InventorySlot* slot2 = *it2;

  *it1 = slot2;
  *it2 = slot1;

  // Update the indexes.
  InventorySlot* it1Slot = *it1;
  InventorySlot* it2Slot = *it2;
  it1Slot->setIndex(_slot1);
  it2Slot->setIndex(_slot2);

} // void Inventory::swapSlots(int _slot1, int _slot2)


InventorySlot* Inventory::getSlotByIndex(int _index)
{
  // Iterate through the slots and find the index.
  for(vector<InventorySlot*>::iterator it = this->items->begin(); it!=this->items->end();++it)
  {
    InventorySlot* currentSlot = *it;
    if(currentSlot->getIndex() == _index)
    {
      return *it;
    } // end if
  } // end for 

  return NULL;
} // AbstractItem* Inventory::getSlotByIndex(int _index)


int Inventory::getPreviousInventorySize()
{
  return this->previousInventorySize;
} // int getPreviousInventorySize()


void Inventory::setPreviousInventorySize()
{
  if(this->items->size() > (unsigned)this->previousInventorySize)
  {
    this->previousInventorySize = this->items->size();
  } // end if
} // void Inventory::setPreviousInventorySize()


void Inventory::setPreviousInventorySize(int _size)
{
  this->previousInventorySize = _size;
} // void Inventory::setPreviousInventorySize(int _size)