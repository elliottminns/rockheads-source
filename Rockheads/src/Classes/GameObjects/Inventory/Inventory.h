#include "../../../PrecompiledHeaders/stdafx.h"
#ifndef INVENTORY_H
#define INVENTORY_H

#include "../InventorySlot/InventorySlot.h"
#include <iostream>
#include <vector>

using namespace std;

class Inventory
{
private:
  vector<InventorySlot*>* items;
  AbstractEntity* owner;
  int previousInventorySize;

public:
  Inventory(AbstractEntity* _owner);
  ~Inventory();
  vector<InventorySlot*>* getItems();
  void                    addItemAtEmptySlot(AbstractItem*  _item, int _count);
  void                    addItemAtSlot(AbstractItem*  _item, int _count, int _slot);
  bool                    removeItem(int _slot, bool _delete = false);
  bool                    removeItem(InventorySlot* _slot, bool _delete = false);
  void                    addEmptySlot();
  void                    removeEmptySlot();
  void                    placeItemInEmptySlot(AbstractItem*  _item, int _count);
  void                    swapSlots(int _slot1, int _slot2);
  InventorySlot*          getSlotByIndex(int _index);

  int getPreviousInventorySize();
  
  void setPreviousInventorySize();
  void setPreviousInventorySize(int _size);

}; // end class Inventory

#endif