#include "../../../../PrecompiledHeaders/stdafx.h"
#ifndef EDGE_H
#define EDGE_H

class Edge
{
public:
  int   getFromIndex();
  int   getToIndex();
  void  setFromIndex(int _index);
  void  setToIndex(int _index);
  void  setCost(int _cost);

private:
  int   fromIndex;
  int   toIndex;
  int   cost;
}; // end class Edge

#endif