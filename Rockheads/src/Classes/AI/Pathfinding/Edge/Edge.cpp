#include "stdafx.h"
#include "Edge.h"

int Edge::getFromIndex()
{
  return this->fromIndex;
} // Edge::getFromIndex()


int Edge::getToIndex()
{
  return this->toIndex;
} // Edge::getToIndex()


void Edge::setFromIndex(int _index)
{
  this->fromIndex = _index;
} // Edge::setFromIndex(int _index)


void Edge::setToIndex(int _index)
{
  this->toIndex = _index;
} // Edge::setToIndex(int _index)


void Edge::setCost(int _cost)
{
  this->cost = _cost;
} // Edge::setCost(int _cost)