#include "stdafx.h"
#include "Node.h"


Node::Node()
{

} // Node::Node()


Node::~Node()
{

} // Node::~Node()


int Node::getIndexNumber()
{
  return this->indexNumber;
} // Node::getIndxeNumber()


 int Node::getTempFCost()
{
  return this->tempFCost;
} // Node::getTempFCost()


int Node::getTempGCost()
{
  return this->tempGCost;
} // Node::getTempGCost()


int Node::getTempHCost()
{
  return this->tempHCost;
} // Node::getTempHCost()


vector<Edge*> Node::getEdgeList()
{
  return this->edgeList;
} // Node::getEdgeList()


Ogre::Vector3 Node::getPosition()
{
  return this->position;
} // Node::getPosition()


Node* Node::getParentNode()
{
  return this->parentNode;
} // Node::getParentNode()


void Node::setIndexNumber(int _index)
{
  this->indexNumber = _index;
} // Node::setIndexNumber(int _index)


void Node::setTempFCost(int _cost)
{
  this->tempFCost = _cost;
} // Node::setTempFCost(int _cost)


void Node::setTempGCost(int _cost)
{
  this->tempGCost = _cost;
} // Node::setTempGCost(int _cost)


void Node::setTempHCost(int _cost)
{
  this->tempHCost = _cost;
} // Node::setTempHCost(int _cost)
  

void Node::setPosition(Ogre::Vector3 _position)
{
  this->position = _position;
} // Node::setPosition(Vector3D _position)