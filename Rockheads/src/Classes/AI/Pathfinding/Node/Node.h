#include "../../../../PrecompiledHeaders/stdafx.h"
#ifndef NODE_H
#define NODE_H

#include "../../../OgreFramework/OgreFramework.h"
#include "../Edge/Edge.h"
#include <iostream>
#include <vector>

using namespace std;

enum List { NONE, OPEN, CLOSED };

class Node
{
public:
  Node();
  ~Node();
  int           getIndexNumber();
  int           getTempFCost();
  int           getTempGCost();
  int           getTempHCost();
  vector<Edge*> getEdgeList();
  Ogre::Vector3 getPosition();
  Node*         getParentNode();
  void          setIndexNumber(int _index);
  void          setTempFCost(int _cost);
  void          setTempGCost(int _cost);
  void          setTempHCost(int _cost);
  void          setPosition(Ogre::Vector3 _position);

private:
  int           indexNumber;
  int           tempFCost;
  int           tempGCost;
  int           tempHCost;
  vector<Edge*> edgeList;
  Ogre::Vector3 position;
  Node*         parentNode;
}; // end class Node

#endif