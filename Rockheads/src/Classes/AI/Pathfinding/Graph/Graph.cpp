#include "stdafx.h"
#include "Graph.h"


Graph* Graph::instance = NULL;


Graph* Graph::getInstance()
{
    if(instance == NULL)
    {
      instance = new Graph();
    } // end if

    return instance;
} // Graph::getInstance()


void Graph::init()
{

} // Graph::init()


void Graph::release()
{

} // Graph::release()


void Graph::createNodes()
{

} // Graph::createNodes()


void Graph::createEdges()
{

} // Graph::createEdges()


Node* Graph::returnNode(int _index)
{
  return new Node;
} // Graph::returnNode(int _index)


vector<Node*> Graph::findPath(Ogre::Vector3 _start, Ogre::Vector3 _end)
{
  vector<Node*> path;
  return path; 
} // Graph::findPath(Ogre::Vector3 _start, Ogre::Vector3 _end)