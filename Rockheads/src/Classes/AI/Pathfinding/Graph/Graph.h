#include "../../../../PrecompiledHeaders/stdafx.h"
#ifndef GRAPH_H
#define GRAPH_H

#include "../../../OgreFramework/OgreFramework.h"
#include "../Node/Node.h"
#include <iostream>
#include <vector>

using namespace std;

class Graph
{
public:
  static Graph* getInstance();
  void          init();
  void          release();
  void          createNodes();
  void          createEdges();
  Node*         returnNode(int _index);
  vector<Node*> findPath(Ogre::Vector3 _start, Ogre::Vector3 _end);

private:
  static Graph* instance;
  vector<Node*> nodeVector;
}; // end class Graph

#endif