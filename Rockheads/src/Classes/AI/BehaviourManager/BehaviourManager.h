#include "../../../PrecompiledHeaders/stdafx.h"
#ifndef BEHAVIOUR_MANAGER_H
#define BEHAVIOUR_MANAGER_H

#include "../../OgreFramework/OgreFramework.h"
#include "../../AI/Pathfinding/Node/Node.h"
#include <iostream>
#include <vector>

using namespace std;

class Mob;

class BehaviourManager
{
public:
  void init(Mob* _ownerMob);
  void accumulateBehaviours();
  void release();

  void moveToTargetOn(std::vector<Node*> _path);
  void moveToTargetOn(Ogre::Vector3 _dest);
  void moveToTargetOff();

  void setTargetDestination(Ogre::Vector3 _dest);
  void setNextDestionation(Ogre::Vector3 _dest);
  void setSeekOn(bool _on);
  void setAttackOn(bool _on);
  void setWalkOn(bool _on);
  void setRunOn(bool _on);
  void setIdleTimer(double _timer);

  double getIdleTimer();
  bool getWalkOn();
  bool getRunOn();

  Ogre::Vector3 getTargetDestination();

  Ogre::Vector3 Seek(Ogre::Vector3 _target);
  Ogre::Vector3 walk(Ogre::Vector3 _target);
  Ogre::Vector3 run(Ogre::Vector3 _target);

private:
  bool            attackOn;
  bool            moveToTarget;
  bool            followPathOn;
  bool            seekOn;
  bool            walkOn;
  bool            runOn;
  Mob*            ownerMob;
  double          idleTimer;
  vector<Node*>   path;
  Ogre::Vector3  targetDestination;
  Ogre::Vector3  nextDestination;
}; // end class BehavioursManager

#endif