#include "stdafx.h"
#include "BehaviourManager.h"
#include "../../GameEntities/DynamicEntities/Mob/Mob.h"


void BehaviourManager::init(Mob* _ownerMob)
{
  this->ownerMob = _ownerMob;
  this->seekOn = false;
  this->walkOn = false;
  this->runOn  = false;
  this->targetDestination = Ogre::Vector3(0,0,0);
  this->nextDestination = Ogre::Vector3(0,0,0);
} // BehaviourManager::init(Mob _ownerMob)


void BehaviourManager::accumulateBehaviours()
{
  Ogre::Vector3 velocity = Ogre::Vector3(0,0,0);
  
  if(this->seekOn)
  {
    velocity = this->Seek(this->targetDestination);
  } // end if
  if(this->walkOn)
  {
    velocity = this->walk(this->targetDestination);
  } // end if
  if(this->runOn)
  {
    velocity = this->run(this->targetDestination);
  } // end if

  this->ownerMob->setVelocity(velocity);

} // BehaviourManager::accumulateBehaviours()


void BehaviourManager::release()
{

} // BehaviourManager::release()


void BehaviourManager::moveToTargetOn(vector<Node*> _path)
{
  this->moveToTarget = true;
  this->followPathOn = true;
  this->path = _path;
} // BehaviourManager::moveToTargetOn(vector<Node> _path)


void BehaviourManager::moveToTargetOn(Ogre::Vector3 _dest)
{
  this->moveToTarget = true;
  this->targetDestination = _dest;
} // void BehaviourManager::moveToTargetOn(Ogre:: Vector3 _path)


void BehaviourManager::moveToTargetOff()
{
  this->moveToTarget = false;
} // BehaviourManager::moveToTargetOff()


Ogre::Vector3 BehaviourManager::getTargetDestination()
{
  return this->targetDestination;
} // Ogre::Vector3 BehaviourManager::getTargetDestination()


Ogre::Vector3 BehaviourManager::Seek(Ogre::Vector3 _target)
{ // AI Behavior of Seek, takes a target as an argument.
  Ogre::Vector2 targetXZ = Ogre::Vector2(_target.x, _target.z);
  Ogre::Vector2 mobXZ    = Ogre::Vector2(this->ownerMob->getPosition().x, this->ownerMob->getPosition().z);
  double dividedBy       = (targetXZ-mobXZ).squaredLength();

  if(dividedBy == 0)
  {
    return Ogre::Vector3(0,0,0);
  } // end if
  
  Ogre::Vector3 unitVector = Ogre::Vector3(_target-this->ownerMob->getPosition());
  unitVector.x /= dividedBy;
  unitVector.z /= dividedBy;

  Ogre::Vector3 desiredVelocity = unitVector * Mob::RUN_SPEED;

  Ogre::Vector3 behaviourAccn = desiredVelocity-this->ownerMob->getVelocity();

  return behaviourAccn;
} // Ogre::Vector3 BehaviourManager::Seek(Ogre::Vector3 _target)


Ogre::Vector3 BehaviourManager::walk(Ogre::Vector3 _target)
{
  Ogre::Vector3 behaviourAccn = Ogre::Vector3(0,0,0);
  // Find out the angle.
  int angle = this->ownerMob->findAngleBetweenTwoPointsXZ(this->ownerMob->getPosition(), _target);

  double xVel = Mob::WALK_SPEED * sin(angle * (M_PI/180));
  double zVel = Mob::WALK_SPEED * cos(angle * (M_PI/180));
  
  behaviourAccn.x = xVel;
  behaviourAccn.z = zVel;

  this->targetDestination.y = this->ownerMob->getPosition().y;

  return behaviourAccn;
  
} // Ogre::Vector3 BehaviourManager::Walk(Ogre::Vector3 _target)


Ogre::Vector3 BehaviourManager::run(Ogre::Vector3 _target)
{
  Ogre::Vector3 behaviourAccn = Ogre::Vector3(0,0,0);
  // Find out the angle.
  int angle = this->ownerMob->findAngleBetweenTwoPointsXZ(this->ownerMob->getPosition(), _target);

  double xVel = Mob::RUN_SPEED * sin(angle * (M_PI/180));
  double zVel = Mob::RUN_SPEED * cos(angle * (M_PI/180));
  
  behaviourAccn.x = xVel;
  behaviourAccn.z = zVel;

  this->targetDestination.y = this->ownerMob->getPosition().y;

  return behaviourAccn;

} // Ogre::Vector3 BehaviourManager::Walk(Ogre::Vector3 _target)


void BehaviourManager::setSeekOn(bool _on)
{
  this->seekOn = _on;
  this->ownerMob->setVelocity(Ogre::Vector3(0,0,0));
} // void BehaviourManager::setSeekOn(bool _on)


void BehaviourManager::setAttackOn(bool _on)
{
  this->attackOn = _on;
  this->ownerMob->setVelocity(Ogre::Vector3(0,0,0));
} // void BehaviourManager::setAttackOn(bool _on)


void BehaviourManager::setWalkOn(bool _on)
{
  this->walkOn = _on;
  this->ownerMob->setVelocity(Ogre::Vector3(0,0,0));
} // void BehaviourManager::getWalkOn(bool _on)


void BehaviourManager::setRunOn(bool _on)
{
  this->runOn = _on;
  this->ownerMob->setVelocity(Ogre::Vector3(0,0,0));
} // void BehaviourManager::setRunOn(bool _on)


void BehaviourManager::setTargetDestination(Ogre::Vector3 _dest)
{
  this->targetDestination = _dest;
} // void BehaviousManager::setTargetDestination(Ogre::Vector3 _dest)


void BehaviourManager::setNextDestionation(Ogre::Vector3 _dest)
{
  this->nextDestination = _dest;
} // void BehaviourManager::setNextDestionation(Ogre::Vector3 _dest)


void BehaviourManager::setIdleTimer(double _timer)
{
  this->idleTimer = _timer;
} // void BehaviourManager::setIdleTimer(double _timer)


double BehaviourManager::getIdleTimer()
{
  return this->idleTimer;
} // double BehaviourManager::getIdleTimer()


bool BehaviourManager::getWalkOn()
{
  return this->walkOn;
} // bool BehaviourManager::getWalkOn()


bool BehaviourManager::getRunOn()
{
  return this->runOn;
} // bool BehaviourManager::getRunOn()