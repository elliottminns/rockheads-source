#include "../../../PrecompiledHeaders/stdafx.h"
#ifndef STATE_MACHINE_H
#define STATE_MACHINE_H

#include "../States/State/State.h"

template<class EntityType>
class StateMachine
{
private:
  // A pointer to the agent who owns this instance.
  EntityType *owner;
  State<EntityType> *currentState;
  State<EntityType> *previousState;
  State<EntityType> *globalState;

public:
  StateMachine(EntityType *_owner):owner(_owner), currentState(NULL), previousState(NULL), globalState(NULL){};

  // Use these methods to initialise the FSM.
  void setCurrentState(State<EntityType> *s){currentState = s;}
  void setGlobalState(State<EntityType> *s){globalState =s;}
  void setPreviousState(State<EntityType> *s){previousState = s;}

  // Call this to update the FSM.
  void update(double _timeSinceLastFrame)const
  {
    if(globalState) globalState->execute(owner, _timeSinceLastFrame);

    if(currentState) currentState->execute(owner, _timeSinceLastFrame);
  } // void Update()const

  // Change to a new state.
  void changeState(State<EntityType> *_newState)
  {
    if(this->currentState)
    {
      this->previousState = this->currentState;
      this->currentState->exit(owner);
    } // end if
    this->currentState = _newState;
    this->currentState->enter(owner);
  } // void ChangeState(State<EntityType> *newState)

  // Change back to a previous state.
  void revertToPreviousState()
  {
    if(this->previousState == this->currentState)
    {
      
    } // end if
    this->changeState(this->previousState);
  } // void RevertToPreviousState()

  // Accessors.
  State<EntityType>* getCurrentState() const{return this->currentState;}
  State<EntityType>* getGlobalState() const{return this->globalState;}
  State<EntityType>* getPreviousState() const{return this->previousState;}

  // Returns true if the current state�s type is equal to the type of the
  // class passed as a parameter.
  bool isInState(const State<EntityType>& st)const
  {
    return typeid(*currentState) == typeid(st);
  } // bool isInState(const State<EntityType>& st)const

};

#endif