#include "stdafx.h"
#include "Attack.h"
#include "../../../GameEntities/DynamicEntities/Mob/Mob.h"
#include "../Patrol/Patrol.h"
#include "../Return/Return.h"
#include "../../../Effects/AbstractEffect/AbstractEffect.h"

Attack* Attack::instance = NULL;

Attack* Attack::getInstance()
{
  if(instance == NULL)
  {
    instance = new Attack;
  } // end if

  return instance;

} // Attack* Attack::getInstance()


void Attack::enter(Mob* _mob)
{
  _mob->getBehaviours()->setRunOn(false);
  _mob->getBehaviours()->setWalkOn(false);
  _mob->attack();
  Player::getInstance()->setInCombat(true);
} // Attack::enter(Mob* _mob)


void Attack::execute(Mob* _mob, double _timeSinceLastFrame)
{
  Ogre::Vector3 playerPosition = Player::getInstance()->getPosition();
  // Set the orientation to that of the player.
  double degrees = _mob->findAngleBetweenTwoPointsXZ(_mob->getPosition(), playerPosition);
  Ogre::Vector3 orientation = Ogre::Vector3(_mob->getOrientation().x, degrees, _mob->getOrientation().z);
  _mob->setOrientation(orientation);

  int weaponRange = 20;
  if(_mob->getPosition().squaredDistance(playerPosition) > weaponRange*weaponRange)
  {
    _mob->getStateMachine()->revertToPreviousState();
  } // if(_mob->getPosition().squaredDistance(Player::getInstance()->getPosition()) < 16*16)

  if(_mob->getAttackCooldownTimer() <= 0)
  {
    _mob->attack();
  } // end if

  if(!Player::getInstance()->getAlive())
  {
    _mob->getStateMachine()->changeState(Return::getInstance());
  } // end if

  // Check if the player has turned invisible.
  vector<AbstractEffect*>* playersEffects = Player::getInstance()->getActiveEffects();
  
  if(Player::getInstance()->getInvisible())
  {
    _mob->getStateMachine()->changeState(Return::getInstance());
    Player::getInstance()->setInCombat(false);
  } // end if


} // Attack::execute(Mob* _mob, double _timeSinceLastFrame)


void Attack::exit(Mob* _mob)
{
  _mob->getBehaviours()->setAttackOn(false);
} // Attack::exit(Mob* _mob)