#include "../../../../PrecompiledHeaders/stdafx.h"
#include "../State/State.h"

class Mob;

class Attack : public State<Mob>
{
private:
  static Attack* instance;

public:
  static Attack* getInstance();
  void enter(Mob* _mob);
  void execute(Mob* _mob, double _timeSinceLastFrame);
  void exit(Mob* _mob);
}; // end class Attack