#include "../../../../PrecompiledHeaders/stdafx.h"
#ifndef ENGAGE_H
#define ENGAGE_H

#include "../State/State.h"

class Mob;

class Engage : public State<Mob>
{
private:
  Engage(){};
  static Engage* instance;

public:
  static Engage* getInstance();
  void enter(Mob* _mob);
  void execute(Mob* _mob, double _timeSinceLastFrame);
  void exit(Mob* _mob);
}; // end class Engage

#endif