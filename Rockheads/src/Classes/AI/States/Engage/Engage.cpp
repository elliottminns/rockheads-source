#include "stdafx.h"
#include "Engage.h"
#include "../../../GameEntities/DynamicEntities/Player/Player.h"
#include "../../../GameEntities/DynamicEntities/Mob/Mob.h"
#include "../Attack/Attack.h"
#include "../../../Effects/AbstractEffect/AbstractEffect.h"
#include "../Return/Return.h"
#include "../Patrol/Patrol.h"
#include "../../../Managers/AudioManager/AudioManager.h"

Engage* Engage::instance = NULL;


Engage* Engage::getInstance()
{
  if(instance == NULL)
  {
    instance = new Engage;
  } // end if

  return instance;

} // Engage* Engage::getInstance()


void Engage::enter(Mob* _mob)
{
  // Get the players location and set it as the target destination.
  _mob->getBehaviours()->setTargetDestination(Player::getInstance()->getPosition());
  _mob->getBehaviours()->setRunOn(true);
  Player::getInstance()->setInCombat(true);

} // Engage::enter(Mob* _mob)


void Engage::execute(Mob* _mob, double _timeSinceLastFrame)
{
  _mob->getBehaviours()->accumulateBehaviours();

  // Re get the target position in case the player has moved.
  _mob->getBehaviours()->setTargetDestination(Player::getInstance()->getPosition());

  // @Todo, implement a weapon range for the mob.
  int weaponRange = 20;
  if(_mob->getPosition().squaredDistance(Player::getInstance()->getPosition()) <= weaponRange*weaponRange)
  {
    _mob->getStateMachine()->changeState(Attack::getInstance());
  } // end if

  if(!Player::getInstance()->getAlive())
  {
    _mob->getStateMachine()->changeState(Patrol::getInstance());
    Player::getInstance()->setInCombat(false);
  } // end if

  // Check if the player has turned invisible.
  vector<AbstractEffect*>* playersEffects = Player::getInstance()->getActiveEffects();
  
  if(Player::getInstance()->getInvisible())
  {
    Player::getInstance()->setInCombat(false);
    _mob->getStateMachine()->changeState(Return::getInstance());
  } // end if
  else
  {
    Player::getInstance()->setInCombat(true);
  } // end else


} // Engage::execute(Mob* _mob, double _timeSinceLastFrame)


void Engage::exit(Mob* _mob)
{
  _mob->getBehaviours()->setRunOn(false);
} // Engage::exit(Mob* _mob)