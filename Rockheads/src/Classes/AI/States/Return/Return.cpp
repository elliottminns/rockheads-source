#include "stdafx.h"
#include "Return.h"
#include "../Patrol/Patrol.h"


Return* Return::instance = NULL;

Return* Return::getInstance()
{
  if(instance == NULL)
  {
    instance = new Return;
  } // end if

  return instance;

} // Return* Return::getInstance()

void Return::enter(Mob* _mob)
{
  // Set target location as the mobs spawn point.
  _mob->getBehaviours()->setTargetDestination(_mob->getSpawnPoint());
  _mob->getBehaviours()->setRunOn(true);
  
  if(Player::getInstance()->getTargettedEnemy() == _mob)
  {
    Player::getInstance()->setTargettedEnemy(NULL);
  } // end if

} // Return::enter(Mob* _mob)


void Return::execute(Mob* _mob, double _timeSinceLastFrame)
{
  int xPosMob = _mob->getPosition().x;
  int zPosMob = _mob->getPosition().z;
  int targetX = _mob->getSpawnPoint().x;
  int targetZ = _mob->getSpawnPoint().z;

  Ogre::Vector2 mobVec = Ogre::Vector2(xPosMob, zPosMob);
  Ogre::Vector2 targVec = Ogre::Vector2(targetX, targetZ);

  if(mobVec.squaredDistance(targVec) < 25*25)
  {
    _mob->setPosition(_mob->getSpawnPoint());
    _mob->getStateMachine()->setCurrentState(Patrol::getInstance());
    
  } // end if
} // Return::execute(Mob* _mob, double _timeSinceLastFrame)


 void Return::exit(Mob* _mob)
{
  _mob->getBehaviours()->setRunOn(false);
} // Return::Exit(Mob* _mob)