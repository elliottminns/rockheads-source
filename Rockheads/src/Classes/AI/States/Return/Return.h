#include "../../../../PrecompiledHeaders/stdafx.h"
#ifndef RETURN_H
#define RETURN_H

#include "../State/State.h"
#include "../../../GameEntities/DynamicEntities/Mob/Mob.h"

class Return : public State<Mob>
{
private:
  static Return* instance;

public:
  static Return* getInstance();
  void enter(Mob* _mob);
  void execute(Mob* _mob, double _timeSinceLastFrame);
  void exit(Mob* _mob);
}; // end class Return

#endif