#include "../../../../PrecompiledHeaders/stdafx.h"
#ifndef STATE_H
#define STATE_H

#include "../../../Messaging/Message/Message.h"

template<class EntityType>
class State
{
private:

public:
  // Destructor.
  virtual ~State(){}

  // Main functions.
  virtual void enter(EntityType*)=0;    // This will execute when the state is entered.
  virtual void execute(EntityType*, double _timeSinceLastFrame)=0;  // This will be called by the types update function each update state.
  virtual void exit(EntityType*)=0;     // This will execute when the state is exited.

};

#endif