#include "stdafx.h"
#include "Patrol.h"
#include "../Engage/Engage.h"
#include "../../../Managers/CollisionManager/QueryFlags.h"
#include "../../../Managers/AudioManager/AudioManager.h"

Patrol* Patrol::instance = 0;

Patrol* Patrol::getInstance()
{
  if(instance == 0)
  {
    instance = new Patrol;
  } // end if

  return instance;

} // Patrol* Patrol::getInstance()


void Patrol::enter(Mob* _mob)
{
  Ogre::Vector3 destination;
  
  // Find a destination point that is suitable.
  do
  {
    destination = this->findRandomDestination(_mob);
  } while(!CollisionManager::getSingleton().isPointInLineOfSight(_mob->getPosition(), destination, STATIC_WALL));
  
  _mob->getBehaviours()->setWalkOn(true);
  _mob->getBehaviours()->setTargetDestination(destination);
  _mob->getBehaviours()->setIdleTimer(0);
} // Patrol::enter(Mob* _mob)


void Patrol::execute(Mob* _mob, double _timeSinceLastFrame)
{
  _mob->getBehaviours()->accumulateBehaviours();

  if(_mob->getPosition().squaredDistance(_mob->getBehaviours()->getTargetDestination()) < 5*5 && _mob->getBehaviours()->getWalkOn())
  {
    // Turn off walk
    _mob->getBehaviours()->setWalkOn(false);

    // Get a timer for when the next movement begins.
    int timer = rand()%9+1;
    _mob->getBehaviours()->setIdleTimer(timer);

  } // end if
  else if(CollisionManager::getSingleton().collidesWithEntity(_mob->getPosition(), _mob->getPosition()+_mob->getVelocity(),
    0.5f,0.0f,STATIC_WALL))
  {
    // Turn off walk
    _mob->getBehaviours()->setWalkOn(false);

    // Get a timer for when the next movement begins.
    int timer = rand()%9+1;
    _mob->getBehaviours()->setIdleTimer(timer);
  } // end else if

  if(!_mob->getBehaviours()->getWalkOn())
  {
    double idleTimer = _mob->getBehaviours()->getIdleTimer();

    if(idleTimer > 0)
    {
      idleTimer -= _timeSinceLastFrame/1000;
      _mob->getBehaviours()->setIdleTimer(idleTimer);
    } // end if

    if(idleTimer <= 0)
    {
      _mob->getBehaviours()->setTargetDestination(this->findRandomDestination(_mob));
      _mob->getBehaviours()->setWalkOn(true);
    } // end if
  } // end if

  // Check to determine if the player is in aggro range only if the mob is aggressive.
  //@Todo, give each enemy an aggro range.
  
  if(_mob->getAggressive() && Player::getInstance()->getAlive())
  {
    // First check if player isn't invisible.
    if(!Player::getInstance()->getInvisible())   
    {
      // Then check line of sight
      if(CollisionManager::getSingleton().isEntityInLineOfSight(_mob->getPosition(), Player::getInstance(), PLAYER))
      {
        // Check if the player is in range.
        if(_mob->getPosition().squaredDistance(Player::getInstance()->getPosition()) < 25*25)
        {
          // Play the Aggro audio.
          AudioManager::getSingleton().playSound(_mob->getName() + " Aggro");
          _mob->getStateMachine()->changeState(Engage::getInstance());
        } // end if
      } // end if
    } // end if


    

  } // end if


} // Patrol::execute(Mob* _mob, double _timeSinceLastFrame)


void Patrol::exit(Mob* _mob)
{
  _mob->getBehaviours()->setWalkOn(false);
} // Patrol::exit(Mob* _mob)


Ogre::Vector3 Patrol::findRandomDestination(Mob* _mob)
{
  Ogre::Vector3 destination;
  // Create a random radius max radius of the point.

  // @Todo - Create a mob specific radius or behaviour specific.
  int radius = rand()%50+10;

  // Get a random angle.
  int angle = rand()%360;

  int xPosition = radius * sin(angle * (M_PI/180));
  int zPosition = radius * cos(angle * (M_PI/180));

  destination.x = xPosition;
  destination.y = _mob->getPosition().y;
  destination.z = zPosition;

  destination += _mob->getSpawnPoint();

  return destination;
} // Ogre::Vector3 Patrol::findRandomDestination(Mob* _mob)