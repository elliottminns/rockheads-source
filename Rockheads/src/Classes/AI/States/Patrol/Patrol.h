#include "../../../../PrecompiledHeaders/stdafx.h"
#ifndef PATROL_H
#define PATROL_H

#include "../State/State.h"
#include "../../../GameEntities/DynamicEntities/Mob/Mob.h"

class Mob;


class Patrol : public State<Mob>
{
private:
  static Patrol* instance;
  Patrol(){};
  ~Patrol(){};

public:
  static Patrol* getInstance();
  void enter(Mob* _mob);
  void execute(Mob* _mob, double _timeSinceLastFrame);
  void exit(Mob* _mob);

  Ogre::Vector3 findRandomDestination(Mob* _mob);
}; // end class Patrol : public State<Mob>

#endif