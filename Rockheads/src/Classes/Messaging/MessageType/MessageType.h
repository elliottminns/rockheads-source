#include "../../../PrecompiledHeaders/stdafx.h"
#ifndef MESSAGE_TYPE_H
#define MESSAGE_TYPE_H

#include <string>

enum MessageType
{
  MessageTypeDamagedNormal,
  MessageTypeDamagedNoMitigation,
  MessageTypeEngaged,
  MessageTypeExperienceGained,
  MessageTypeInteract,
  MessageTypeAttackMissed
}; // enum MessageType

#endif