#include "stdafx.h"
#include "MessageDispatcher.h"
#include "../../GameEntities/AbstractEntity/AbstractEntity.h"
#include <time.h>
#include "../../GameEntities/DynamicEntities/Player/Player.h"
#include "../../Managers/StaticEntityManager/StaticEntityManager.h"


MessageDispatcher* MessageDispatcher::instance = NULL;


MessageDispatcher::MessageDispatcher()
{
  this->entityList = new list<AbstractEntity*>;
  this->currentIDNumber = 0;
  PriorityQ = new vector<Message>;
  PriorityQ->clear();
} // MessageDispatcher::MessageDispatcher()


MessageDispatcher::~MessageDispatcher()
{
  if(this->entityList)
  {
    delete this->entityList;
    this->entityList = NULL;
  } // end if
  if(this->PriorityQ)
  {
    delete this->PriorityQ;
    this->entityList = NULL;
  } // end if
} // MessageDispatcher::~MessageDispatcher()


MessageDispatcher* MessageDispatcher::getInstance()
{
  if(instance == NULL)
  {
    instance = new MessageDispatcher;
  } // end if

  return instance;
} // MessageDispatcher* MessageDispatcher::GetInstance()


void MessageDispatcher::dispatchMsg(double _delay, int _sender, int _reciever, MessageType _msg, void* _extraInfo)
{
  // Get a pointer to the reciever of the message.
  AbstractEntity *reciever = this->findEntityByID(_reciever);

  // Create the message.
  Message message(_delay, _sender, _reciever, _msg, _extraInfo);

  // if there is no delay, route the message immediately.
  if(_delay <= 0.0)
  {
    // Send the message to the bot.
    discharge(reciever, message);
  } // end if
  else
  {
    double CurrentTime = (double)time(NULL);
    message.waitUntil = CurrentTime + _delay; 

    // Now put it in the queue
    PriorityQ->push_back(message);
  } // end else
  
} // void MessageDispatcher::DispatchMessage(double _delay, int _sender, int _reciever, int _msg, void* _ExtraInfo)


void MessageDispatcher::dispatchDelayedMessages()
{
  // First get the current time.
  double currentTime = (double)time(NULL);

  // Now look at the queue to see if any of the messages need dispatching.
  // Remove all the messages at the front of the queue that have gone past their
  // send-by-date.
  OgreFramework::getSingletonPtr()->log->logMessage(Ogre::StringConverter::toString(PriorityQ->size()));

  int i =0;
  vector<int> removalMessages;
  for(vector<Message>::iterator it = PriorityQ->begin(); it != PriorityQ->end(); ++it)
  {
    Message message = *it;
    // Find the recipient
    if(message.waitUntil < currentTime)
    {
      AbstractEntity* reciever = this->findEntityByID(message.recieverID);
      if(reciever)
      {
        // Send message to the recipient.
        this->discharge(reciever, message);
        
      } // end if
      removalMessages.push_back(i);
    } // end if
    ++i;
  } // end for

  for(vector<int>::reverse_iterator rit = removalMessages.rbegin(); rit != removalMessages.rend(); ++rit)
  {
    int pos = *rit;
    PriorityQ->erase(PriorityQ->begin()+pos);
  } // end for



  /*
  while((PriorityQ->begin()->waitUntil < currentTime) && (PriorityQ->begin()->waitUntil > 0))
  {
    
    // Get the message from the front of the queue.
    
    OgreFramework::getSingletonPtr()->log->logMessage(Ogre::StringConverter::toString((int)PriorityQ->begin()->waitUntil));
    OgreFramework::getSingletonPtr()->log->logMessage(Ogre::StringConverter::toString((int)currentTime));
    

    

  } // end while*/
} // void MessageDispatcher::DispatchDelayedMessages()


void MessageDispatcher::discharge(AbstractEntity* _receiver, const Message& _message)
{
  _receiver->handleMessage(_message);
} // void MessageDispatcher::Discharge(Bot* _receiver, const Message& _message)


int MessageDispatcher::assignIDNumber(AbstractEntity* _entity)
{
  _entity->setEntityIDNumber(this->currentIDNumber);
  ++this->currentIDNumber;
  this->entityList->push_back(_entity);
  return this->currentIDNumber-1;
} // int MessageDispatcher::assignIDNumber()


void MessageDispatcher::resetIDNumbers()
{
  Player::getInstance();
  this->currentIDNumber = 0;
  this->entityList->clear();
  this->assignIDNumber(Player::getInstance());
  StaticEntityManager::getInstance()->registerDropObjectIDs();
} // void MessageDispatcher::resetIDNumbers()


AbstractEntity* MessageDispatcher::findEntityByID(int _id)
{
  for(list<AbstractEntity*>::iterator it = this->entityList->begin(); it != this->entityList->end(); ++it)
  {
    AbstractEntity* entity = *it;
    if(entity->getEntityIDNumber() == _id)
    {
      return entity;
    } // end if
  } // end for

  return NULL;
} // AbstractEntity* MessageDispatcher::findEntityByID(int _id)