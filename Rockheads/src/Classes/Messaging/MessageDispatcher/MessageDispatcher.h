#include "../../../PrecompiledHeaders/stdafx.h"
#ifndef MESSAGE_DISPATCHER_H
#define MESSAGE_DISPATCHER_H

#include "../Message/Message.h"
#include <set>
#include <list>

using namespace std;

class AbstractEntity;

class MessageDispatcher
{
private:
  std::vector<Message>* PriorityQ;
  void discharge(AbstractEntity* _receiver, const Message& _msg);
  MessageDispatcher();
  ~MessageDispatcher();
  int currentIDNumber;
  list<AbstractEntity*>* entityList;

public:
  static MessageDispatcher* instance;
  static MessageDispatcher* getInstance(); 
  void dispatchMsg(double _delay, int _sender, int _reciever, MessageType _msg, void *_extraInfo);
  void dispatchDelayedMessages();
  int  assignIDNumber(AbstractEntity* _entity);
  void resetIDNumbers();
  AbstractEntity* findEntityByID(int _id);
};

#endif
