#include "../../../PrecompiledHeaders/stdafx.h"
#ifndef MESSAGE_H
#define MESSAGE_H

#ifndef NULL
#define NULL 0
#endif

#include "../../Messaging/MessageType/MessageType.h"

struct Message
{
  int senderID;
  int recieverID;
  int msg;
  double waitUntil;
  void* extraInfo;

  Message():waitUntil(-1), senderID(-1), recieverID(-1),msg(-1){};

  Message(double _time, int _sender, int _receiver, int _msg, void* _info = NULL): 
           waitUntil(_time), senderID(_sender), recieverID(_receiver), msg(_msg), extraInfo(_info){};
};

//these telegrams will be stored in a priority queue. Therefore the >
//operator needs to be overloaded so that the PQ can sort the telegrams
//by time priority. Note how the times must be smaller than
//SmallestDelay apart before two Telegrams are considered unique.
const double SmallestDelay = 0.25;


inline bool operator==(const Message& t1, const Message& t2)
{
  return ( fabs(t1.waitUntil-t2.waitUntil) < SmallestDelay) &&
          (t1.senderID == t2.senderID)        &&
          (t1.recieverID == t2.recieverID)    &&
          (t1.msg == t2.msg);
}

inline bool operator<(const Message& t1, const Message& t2)
{
  if (t1 == t2)
  {
    return false;
  }

  else
  {
    return  (t1.waitUntil < t2.waitUntil);
  }
}

inline std::ostream& operator<<(std::ostream& os, const Message& t)
{
  os << "time: " << t.waitUntil << "  Sender: " << t.senderID
     << "   Receiver: " << t.recieverID << "   Msg: " << t.msg;

  return os;
}

//handy helper function for dereferencing the ExtraInfo field of the Telegram 
//to the required type.
template <class T>
inline T DereferenceToType(void* p)
{
  return *(T*)(p);
}

#endif