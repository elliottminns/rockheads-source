#include "stdafx.h"
#include "StunEffect.h"
#include "../../GameEntities/DynamicEntities/DynamicEntity/DynamicEntity.h"
#include "../../Managers/GUIManager/GUIManager.h"


StunEffect::StunEffect() : AbstractEffect()
{
  this->time = 0;
} // StunEffect::StunEffect() : AbstractEffect()


StunEffect::~StunEffect()
{

} // StunEffect::~StunEffect()


StunEffect::StunEffect(const StunEffect& _other) : AbstractEffect(_other)
{
  this->time = _other.time;
} // StunEffect::StunEffect(const StunEffect& _other)


StunEffect* StunEffect::clone() const
{
  return new StunEffect(*this);
} // StunEffect::StunEffect* clone() const


StunEffect& StunEffect::operator=(const StunEffect& _other)
{
  this->setLevel(_other.getLevel());
  this->setName(_other.getName());
  this->setType(_other.getType());
  this->setTime(_other.getTime());
  this->setImage(_other.getImage());
  this->setTargetParticle(_other.getTargetParticle());
  this->setCallerParticle(_other.getCallerParticle());
  return *this;
} // StunEffect& StunEffect::operator=(const StunEffect& _other) const


void StunEffect::initialise()
{
  
} // void StunEffect::initialise()


bool StunEffect::activate(DynamicEntity* _caller, DynamicEntity* _target)
{
  vector<AbstractEffect*>* activeEffects = _target->getActiveEffects();
  bool added = false;

  MessageDispatcher::getInstance()->dispatchMsg(0.0,_caller->getEntityIDNumber(), _target->getEntityIDNumber(), 
    MessageTypeEngaged, _caller);
  for(vector<AbstractEffect*>::iterator it = activeEffects->begin(); it != activeEffects->end(); ++it)
  {
    AbstractEffect* currentEffect = *it;

    // Same Type.
    if(currentEffect->getType() == this->getType())
    {
      // Check to see if the same level.
      if(currentEffect->getLevel() == this->getLevel())
      { // If the same level then restore the amount of time on it rather than adding to it.
        added = true;
        static_cast<StunEffect*>(currentEffect)->setTime(this->getTime());
      } // end if
      else
      { // Make a copy. @Todo - Overload the assignment operator.
        StunEffect* castedCurrentEffect = (StunEffect*)currentEffect;
        castedCurrentEffect = this;
        added = true;
      } // end else
    } // end if

  } // end for

  if(!added)
  {
    StunEffect* effect = this->clone();
    _target->addActiveEffect(effect);
     ParticleManager::getSingleton().attachParticleToEntity(_target, this->getTargetParticle());
  } // end if

  return true;

} // void StunEffect::activate(DynamicEntity* _entity)


bool StunEffect::updateEffect(double _timeSinceLastFrame, DynamicEntity* _owner)
{
  // Increase the amount of time passed.
  this->time -= _timeSinceLastFrame/1000;

  if(this->time <= 0)
  {
    ParticleManager::getSingleton().removeParticleFromEntity(_owner,this->getTargetParticle());
    return true;
  } // end if

  if(this->time <= 10)
  {
    double dotRemainder = this->time - (int)this->time;
    if(dotRemainder>0.5)
    {
      GUIManager::getSingleton().flashOnActiveEffect(this);
    } // end if
    else if(dotRemainder<0.5)
    {
      GUIManager::getSingleton().flashOffActiveEffect(this);
    } // else if(dotRemainder<0.5) 
  } // end if

  return false;
} // bool StunEffect::updateEffect(double _timeSinceLastFrame, DynamicEntity* _entity)


void StunEffect::setTime(double _time)
{
  this->time = _time;
} // void StunEffect::setTime(double _time)


double StunEffect::getTime() const
{
  return this->time; 
} // double StunEffect::getTime() const


string StunEffect::getDetailsDescription() const
{

  Ogre::String returnString = "";
  returnString += "Level: " + Ogre::StringConverter::toString(this->getLevel()) + "\n";
  returnString += "Stuns the target for " + Ogre::StringConverter::toString((int)this->time);
  returnString += " seconds.";

  return returnString;
} // string StunEffect::getDetailsDescription() const