#include "../../../PrecompiledHeaders/stdafx.h"
#ifndef STUN_EFFECT_H
#define STUN_EFFECT_H
#include "../AbstractEffect/AbstractEffect.h"

class DynamicEntity;

class StunEffect : public AbstractEffect
{
private:
  double time;

public:
  StunEffect();
  ~StunEffect();
  StunEffect(const StunEffect& _other);
  StunEffect* clone() const;
  StunEffect& operator=(const StunEffect& _other);

  void initialise();
  bool activate(DynamicEntity* _caller, DynamicEntity* _target);
  bool updateEffect(double _timeSinceLastFrame, DynamicEntity* _entity);

  void setTime(double time);

  double getTime() const;

  string getDetailsDescription() const;

};
#endif