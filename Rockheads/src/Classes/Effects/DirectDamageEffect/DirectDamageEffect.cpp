#include "stdafx.h"
#include "DirectDamageEffect.h"
#include "../../GameEntities/DynamicEntities/DynamicEntity/DynamicEntity.h"


DirectDamageEffect::DirectDamageEffect() : AbstractEffect()
{
  this->damageAmount = 0;
  this->ignoresMitigation = false;
} // DirectDamageEffect::DirectDamageEffect() : AbstractEffect()


DirectDamageEffect::~DirectDamageEffect()
{

} // DirectDamageEffect::~DirectDamageEffect()


DirectDamageEffect::DirectDamageEffect(const DirectDamageEffect& _other) : AbstractEffect(_other)
{
  this->damageAmount = _other.damageAmount;
  this->ignoresMitigation = _other.ignoresMitigation;
} // DirectDamageEffect::DirectDamageEffect(const DirectDamageEffect& _other)


DirectDamageEffect* DirectDamageEffect::clone() const
{
  return new DirectDamageEffect(*this);
} // DirectDamageEffect::DirectDamageEffect* clone() const


void DirectDamageEffect::initialise()
{
  
} // void DirectDamageEffect::initialise()


bool DirectDamageEffect::activate(DynamicEntity* _caller, DynamicEntity* _target)
{
  MessageType msg;
  ParticleManager::getSingleton().attachParticleToEntity(_caller, this->getCallerParticle());

  if(this->ignoresMitigation)
  { 
    msg = MessageTypeDamagedNoMitigation;
  } // end if
  else
  {
    msg = MessageTypeDamagedNormal;
  } // end else

  MessageDispatcher::getInstance()->dispatchMsg(0,_caller->getEntityIDNumber(), _target->getEntityIDNumber(),
    msg, &this->damageAmount);

  ParticleManager::getSingleton().attachParticleToEntity(_target, this->getTargetParticle());

  return true;

} // void DirectDamageEffect::activate(DynamicEntity* _entity)


void DirectDamageEffect::setDamageAmount(int _amount)
{
  this->damageAmount = _amount;
} // void DirectDamageEffect::setHealingAmount(int _amount)


void DirectDamageEffect::setIgnoresMitigation(bool _ignores)
{
  this->ignoresMitigation = _ignores;
} // void DamageOverTimeEffect::setIgnoresMitigation(bool _ignores)


int DirectDamageEffect::getDamageAmount() const
{
  return this->damageAmount;
} // int DirectDamageEffect::getHealingAmount() const


string DirectDamageEffect::getDetailsDescription() const
{

  Ogre::String returnString = "";
  returnString += "Level: " + Ogre::StringConverter::toString(this->getLevel()) + "\n";
  returnString += "Deals " + Ogre::StringConverter::toString(this->damageAmount);
  returnString += " Damage.";
  if(this->ignoresMitigation)
  {
    returnString += " Ignores mitigation";
  } // end if
  else
  {
    returnString += " Is reduced by mitigation.";
  } // end else

  return returnString;
} // string DamageShieldEffect::getDetailsDescription() const