#include "../../../PrecompiledHeaders/stdafx.h"
#ifndef DIRECT_DAMAGE_EFFECT_H
#define DIRECT_DAMAGE_EFFECT_H
#include "../AbstractEffect/AbstractEffect.h"

class DynamicEntity;

class DirectDamageEffect : public AbstractEffect
{
private:
  int damageAmount;
  bool ignoresMitigation;

public:
  DirectDamageEffect();
  ~DirectDamageEffect();
  DirectDamageEffect(const DirectDamageEffect& _other);
  DirectDamageEffect* clone() const;

  void initialise();
  bool activate(DynamicEntity* _caller, DynamicEntity* _target);

  void setDamageAmount(int _amount);
  void setIgnoresMitigation(bool _ignores);
  
  int getDamageAmount() const;

  string getDetailsDescription() const;

};
#endif