#include "stdafx.h"
#include "DefenceIncreaseEffect.h"
#include "../../GameEntities/DynamicEntities/DynamicEntity/DynamicEntity.h"
#include "../../Managers/GUIManager/GUIManager.h"


DefenceIncreaseEffect::DefenceIncreaseEffect() : AbstractEffect()
{
  this->defenceAmount = 0;
  this->time = 0;
} // DefenceIncreaseEffect::DefenceIncreaseEffect() : AbstractEffect()


DefenceIncreaseEffect::~DefenceIncreaseEffect()
{

} // DefenceIncreaseEffect::~DefenceIncreaseEffect()


DefenceIncreaseEffect::DefenceIncreaseEffect(const DefenceIncreaseEffect& _other) : AbstractEffect(_other)
{
  this->defenceAmount = _other.defenceAmount;
  this->time = _other.time;
} // DefenceIncreaseEffect::DefenceIncreaseEffect(const DefenceIncreaseEffect& _other)


DefenceIncreaseEffect* DefenceIncreaseEffect::clone() const
{
  return new DefenceIncreaseEffect(*this);
} // DefenceIncreaseEffect::DefenceIncreaseEffect* clone() const


DefenceIncreaseEffect& DefenceIncreaseEffect::operator=(const DefenceIncreaseEffect& _other)
{
  this->setLevel(_other.getLevel());
  this->setName(_other.getName());
  this->setType(_other.getType());
  this->setImage(_other.getImage());
  this->setTime(_other.getTime());
  this->setDefenceAmount(_other.getDefenceAmount());
  this->setTargetParticle(_other.getTargetParticle());
  this->setCallerParticle(_other.getCallerParticle());
  return *this;
} // DefenceIncreaseEffect& DefenceIncreaseEffect::operator=(const DefenceIncreaseEffect& _other) const


void DefenceIncreaseEffect::initialise()
{
  
} // void DefenceIncreaseEffect::initialise()


bool DefenceIncreaseEffect::activate(DynamicEntity* _caller, DynamicEntity* _target)
{
  vector<AbstractEffect*>* activeEffects = _target->getActiveEffects();
  bool added = false;
  for(vector<AbstractEffect*>::iterator it = activeEffects->begin(); it != activeEffects->end(); ++it)
  {
    AbstractEffect* currentEffect = *it;

    // Same Type.
    if(currentEffect->getType() == this->getType())
    {
      // Check to see if the same level.
      if(currentEffect->getLevel() == this->getLevel())
      { // If the same level then restore the amount of time on it rather than adding to it.
        added = true;
        static_cast<DefenceIncreaseEffect*>(currentEffect)->setTime(this->getTime());
      } // end if
      else
      { // Make a copy. @Todo - Overload the assignment operator.
        DefenceIncreaseEffect* castedCurrentEffect = (DefenceIncreaseEffect*)currentEffect;
        castedCurrentEffect = this;
        added = true;
      } // end else
    } // end if

  } // end for

  if(!added)
  {
    DefenceIncreaseEffect* effect = this->clone();
    _target->addActiveEffect(effect);
    _target->workOutDefenceRating();
    ParticleManager::getSingleton().attachParticleToEntity(_target, this->getTargetParticle());
  } // end if
  return true;
} // void DefenceIncreaseEffect::activate(DynamicEntity* _entity)


bool DefenceIncreaseEffect::updateEffect(double _timeSinceLastFrame, DynamicEntity* _owner)
{
  // Increase the amount of time passed.
  this->time -= _timeSinceLastFrame/1000;

  if(this->time <= 0)
  {
    _owner->setDefenceRating(_owner->getDefenceRating()-this->defenceAmount);
    return true;
  } // end if

  if(this->time <= 10)
  {
    double dotRemainder = this->time - (int)this->time;
    if(dotRemainder>0.5)
    {
      GUIManager::getSingleton().flashOnActiveEffect(this);
    } // end if
    else if(dotRemainder<0.5)
    {
      GUIManager::getSingleton().flashOffActiveEffect(this);
    } // else if(dotRemainder<0.5) 
  } // end if

  return false;
} // bool DefenceIncreaseEffect::updateEffect(double _timeSinceLastFrame, DynamicEntity* _entity)


void DefenceIncreaseEffect::setDefenceAmount(int _amount)
{
  this->defenceAmount = _amount;
} // void DefenceIncreaseEffect::setHealingAmount(int _amount)


void DefenceIncreaseEffect::setTime(double _time)
{
  this->time = _time;
} // void DefenceIncreaseEffect::setTime(double _time)


int DefenceIncreaseEffect::getDefenceAmount() const 
{
  return this->defenceAmount;
} // int DefenceIncreaseEffect::getHealingAmount() const 


double DefenceIncreaseEffect::getTime() const
{
  return this->time; 
} // double DefenceIncreaseEffect::getTime() const


string DefenceIncreaseEffect::getDetailsDescription() const
{

  Ogre::String returnString = "";
  returnString += "Level: " + Ogre::StringConverter::toString(this->getLevel()) + "\n";
  returnString += "Increases the casters defence rating by " + Ogre::StringConverter::toString(this->defenceAmount);
  returnString += " for " + Ogre::StringConverter::toString((int)this->time) + " seconds.";

  return returnString;
} // string DamageShieldEffect::getDetailsDescription() const