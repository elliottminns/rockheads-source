#include "../../../PrecompiledHeaders/stdafx.h"
#ifndef DEFENCE_INCREASE_EFFECT_H
#define DEFENCE_INCREASE_EFFECT_H
#include "../AbstractEffect/AbstractEffect.h"

class DynamicEntity;

class DefenceIncreaseEffect : public AbstractEffect
{
private:
  int defenceAmount;
  double time;

public:
  DefenceIncreaseEffect();
  ~DefenceIncreaseEffect();
  DefenceIncreaseEffect(const DefenceIncreaseEffect& _other);
  DefenceIncreaseEffect* clone() const;
  DefenceIncreaseEffect& operator=(const DefenceIncreaseEffect& _other);

  void initialise();
  bool activate(DynamicEntity* _caller, DynamicEntity* _target);
  bool updateEffect(double _timeSinceLastFrame, DynamicEntity* _entity);

  void setDefenceAmount(int _amount);
  void setTime(double time);
  
  int getDefenceAmount() const;
  double getTime() const;

  string getDetailsDescription() const;

};
#endif