#include "stdafx.h"
#include "CriticalIncreaseEffect.h"
#include "../../GameEntities/DynamicEntities/DynamicEntity/DynamicEntity.h"
#include "../../Managers/GUIManager/GUIManager.h"

CriticalIncreaseEffect::CriticalIncreaseEffect() : AbstractEffect()
{
  this->criticalIncreaseAmount = 0;
  this->time = 0;
} // CriticalIncreaseEffect::CriticalIncreaseEffect() : AbstractEffect()


CriticalIncreaseEffect::~CriticalIncreaseEffect()
{

} // CriticalIncreaseEffect::~CriticalIncreaseEffect()


CriticalIncreaseEffect::CriticalIncreaseEffect(const CriticalIncreaseEffect& _other) : AbstractEffect(_other)
{
  this->criticalIncreaseAmount = _other.criticalIncreaseAmount;
  this->time = _other.time;
} // CriticalIncreaseEffect::CriticalIncreaseEffect(const CriticalIncreaseEffect& _other)


CriticalIncreaseEffect* CriticalIncreaseEffect::clone() const
{
  return new CriticalIncreaseEffect(*this);
} // CriticalIncreaseEffect::CriticalIncreaseEffect* clone() const


CriticalIncreaseEffect& CriticalIncreaseEffect::operator=(const CriticalIncreaseEffect& _other)
{
  this->setLevel(_other.getLevel());
  this->setName(_other.getName());
  this->setType(_other.getType());
  this->setTime(_other.getTime());
  this->setImage(_other.getImage());
  this->setCriticalIncreaseAmount(_other.criticalIncreaseAmount);
  this->setTargetParticle(_other.getTargetParticle());
  this->setCallerParticle(_other.getCallerParticle());
  return *this;
} // CriticalIncreaseEffect& CriticalIncreaseEffect::operator=(const CriticalIncreaseEffect& _other) const


void CriticalIncreaseEffect::initialise()
{
  
} // void CriticalIncreaseEffect::initialise()


bool CriticalIncreaseEffect::activate(DynamicEntity* _caller, DynamicEntity* _target)
{
  vector<AbstractEffect*>* activeEffects = _target->getActiveEffects();
  bool added = false;
  for(vector<AbstractEffect*>::iterator it = activeEffects->begin(); it != activeEffects->end(); ++it)
  {
    AbstractEffect* currentEffect = *it;

    // Same Type.
    if(currentEffect->getType() == this->getType())
    {
      // Check to see if the same level.
      if(currentEffect->getLevel() == this->getLevel())
      { // If the same level then restore the amount of time on it rather than adding to it.
        added = true;
        static_cast<CriticalIncreaseEffect*>(currentEffect)->setTime(this->getTime());
      } // end if
      else
      { // Make a copy. @Todo - Overload the assignment operator.
        CriticalIncreaseEffect* castedCurrentEffect = (CriticalIncreaseEffect*)currentEffect;
        castedCurrentEffect = this;
        added = true;
      } // end else
    } // end if

  } // end for

  if(!added)
  {
    CriticalIncreaseEffect* effect = this->clone();
    _target->addActiveEffect(effect);
    _target->setCriticalChance(_target->getCriticalChance() + this->criticalIncreaseAmount);
    ParticleManager::getSingleton().attachParticleToEntity(_target, this->getTargetParticle());
  } // end if

  return true;
} // void CriticalIncreaseEffect::activate(DynamicEntity* _entity)


bool CriticalIncreaseEffect::updateEffect(double _timeSinceLastFrame, DynamicEntity* _owner)
{
  // Increase the amount of time passed.
  this->time -= _timeSinceLastFrame/1000;

  if(this->time <= 0)
  {
    _owner->setCriticalChance(_owner->getCriticalChance()-this->criticalIncreaseAmount);
    return true;
  } // end if

  if(this->time <= 10)
  {
    double dotRemainder = this->time - (int)this->time;
    if(dotRemainder>0.5)
    {
      GUIManager::getSingleton().flashOnActiveEffect(this);
    } // end if
    else if(dotRemainder<0.5)
    {
      GUIManager::getSingleton().flashOffActiveEffect(this);
    } // else if(dotRemainder<0.5) 
  } // end if

  return false;
} // bool CriticalIncreaseEffect::updateEffect(double _timeSinceLastFrame, DynamicEntity* _entity)


void CriticalIncreaseEffect::setCriticalIncreaseAmount(int _amount)
{
  this->criticalIncreaseAmount = _amount;
} // void CriticalIncreaseEffect::setHealingAmount(int _amount)


void CriticalIncreaseEffect::setTime(double _time)
{
  this->time = _time;
} // void CriticalIncreaseEffect::setTime(double _time)


int CriticalIncreaseEffect::getCriticalIncreaseAmount() const 
{
  return this->criticalIncreaseAmount;
} // int CriticalIncreaseEffect::getHealingAmount() const 


double CriticalIncreaseEffect::getTime() const
{
  return this->time; 
} // double CriticalIncreaseEffect::getTime() const


string CriticalIncreaseEffect::getDetailsDescription() const
{

  Ogre::String returnString = "";
  returnString += "Level: " + Ogre::StringConverter::toString(this->getLevel()) + "\n";
  returnString += "Increases Critical Chance by " + Ogre::StringConverter::toString(this->criticalIncreaseAmount);
  returnString += " for " + Ogre::StringConverter::toString((int)this->time) + " seconds.";

  return returnString;
} // string CriticalIncreaseEffect::getDetailsDescription() const