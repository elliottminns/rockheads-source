#include "../../../PrecompiledHeaders/stdafx.h"
#ifndef CRITICAL_INCREASE_EFFECT_H
#define CRITICAL_INCREASE_EFFECT_H
#include "../AbstractEffect/AbstractEffect.h"

class DynamicEntity;

class CriticalIncreaseEffect : public AbstractEffect
{
private:
  int criticalIncreaseAmount;
  double time;

public:
  CriticalIncreaseEffect();
  ~CriticalIncreaseEffect();
  CriticalIncreaseEffect(const CriticalIncreaseEffect& _other);
  CriticalIncreaseEffect* clone() const;
  CriticalIncreaseEffect& operator=(const CriticalIncreaseEffect& _other);

  void initialise();
  bool activate(DynamicEntity* _caller, DynamicEntity* _target);
  bool updateEffect(double _timeSinceLastFrame, DynamicEntity* _entity);

  void setCriticalIncreaseAmount(int _amount);
  void setTime(double time);
  
  int getCriticalIncreaseAmount() const;
  double getTime() const;

  string getDetailsDescription() const;

};
#endif