#include "stdafx.h"
#include "HealEffect.h"
#include "../../GameEntities/DynamicEntities/DynamicEntity/DynamicEntity.h"


HealEffect::HealEffect() : AbstractEffect()
{
  this->healingAmount = 0;
} // HealEffect::HealEffect() : AbstractEffect()


HealEffect::~HealEffect()
{

} // HealEffect::~HealEffect()


HealEffect::HealEffect(const HealEffect& _other) : AbstractEffect(_other)
{
  this->healingAmount = _other.healingAmount;
} // HealEffect::HealEffect(const HealEffect& _other)


HealEffect* HealEffect::clone() const
{
  return new HealEffect(*this);
} // HealEffect::HealEffect* clone() const


void HealEffect::initialise()
{
  
} // void HealEffect::initialise()


bool HealEffect::activate(DynamicEntity* _caller, DynamicEntity* _target)
{
  ParticleManager::getSingleton().attachParticleToEntity(_caller, this->getCallerParticle());
  _target->heal(this->healingAmount);

  ParticleManager::getSingleton().attachParticleToEntity(_target, this->getTargetParticle());
  return true;
} // void HealEffect::activate(DynamicEntity* _entity)


void HealEffect::setHealingAmount(int _amount)
{
  this->healingAmount = _amount;
} // void HealEffect::setHealingAmount(int _amount)


int HealEffect::getHealingAmount() const
{
  return this->healingAmount;
} // int HealEffect::getHealingAmount() const


string HealEffect::getDetailsDescription() const
{

  Ogre::String returnString = "";
  returnString += "Level: " + Ogre::StringConverter::toString(this->getLevel()) + "\n";
  returnString += "Heals " + Ogre::StringConverter::toString(this->healingAmount);
  returnString += " damage.";

  return returnString;
} // string HealEffect::getDetailsDescription() const