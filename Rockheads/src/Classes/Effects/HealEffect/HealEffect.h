#include "../../../PrecompiledHeaders/stdafx.h"
#ifndef HEAL_EFFECT_H
#define HEAL_EFFECT_H
#include "../AbstractEffect/AbstractEffect.h"

class DynamicEntity;

class HealEffect : public AbstractEffect
{
private:
  int healingAmount;

public:
  HealEffect();
  ~HealEffect();
  HealEffect(const HealEffect& _other);
  HealEffect* clone() const;

  void initialise();
  bool activate(DynamicEntity* _caller, DynamicEntity* _target);

  void setHealingAmount(int _amount);
  
  int getHealingAmount() const;

  string getDetailsDescription() const;

};
#endif