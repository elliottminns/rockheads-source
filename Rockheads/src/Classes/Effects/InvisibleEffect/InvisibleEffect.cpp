#include "stdafx.h"
#include "InvisibleEffect.h"
#include "../../GameEntities/DynamicEntities/DynamicEntity/DynamicEntity.h"
#include "../../Managers/GUIManager/GUIManager.h"

InvisibleEffect::InvisibleEffect() : AbstractEffect()
{
  this->time = 0;
  this->combatAble = false;
} // InvisibleEffect::InvisibleEffect() : AbstractEffect()


InvisibleEffect::~InvisibleEffect()
{

} // InvisibleEffect::~InvisibleEffect()


InvisibleEffect::InvisibleEffect(const InvisibleEffect& _other) : AbstractEffect(_other)
{
  this->time = _other.time;
  this->combatAble = _other.combatAble;
} // InvisibleEffect::InvisibleEffect(const InvisibleEffect& _other)


InvisibleEffect* InvisibleEffect::clone() const
{
  return new InvisibleEffect(*this);
} // InvisibleEffect::InvisibleEffect* clone() const


InvisibleEffect& InvisibleEffect::operator=(const InvisibleEffect& _other)
{
  this->setLevel(_other.getLevel());
  this->setName(_other.getName());
  this->setType(_other.getType());
  this->setTime(_other.getTime());
  this->setImage(_other.getImage());
  this->combatAble = _other.combatAble;
  this->setTargetParticle(_other.getTargetParticle());
  this->setCallerParticle(_other.getCallerParticle());
  return *this;
} // InvisibleEffect& InvisibleEffect::operator=(const InvisibleEffect& _other) const


void InvisibleEffect::initialise()
{
  
} // void InvisibleEffect::initialise()


bool InvisibleEffect::activate(DynamicEntity* _caller, DynamicEntity* _target)
{
  // Check to make sure this is able during combat.
  if(!this->combatAble && _target->getInCombat())
  {
    return false;
  } // end if
  vector<AbstractEffect*>* activeEffects = _target->getActiveEffects();
  bool added = false;
  for(vector<AbstractEffect*>::iterator it = activeEffects->begin(); it != activeEffects->end(); ++it)
  {
    AbstractEffect* currentEffect = *it;

    // Same Type.
    if(currentEffect->getType() == this->getType())
    {
      // Check to see if the same level.
      if(currentEffect->getLevel() == this->getLevel())
      { // If the same level then restore the amount of time on it rather than adding to it.
        added = true;
        static_cast<InvisibleEffect*>(currentEffect)->setTime(this->getTime());
      } // end if
      else
      { 
        InvisibleEffect* castedCurrentEffect = (InvisibleEffect*)currentEffect;
        castedCurrentEffect = this;
        added = true;
      } // end else
    } // end if

  } // end for


  if(!added)
  {
    InvisibleEffect* effect = this->clone();
    _target->addActiveEffect(effect);
    _target->setInvisible(true);

    // Reduce movement speed @Todo - Place the reduction per level in XML.
    _target->setSpeed(_target->getSpeed()*0.47); // Player Speed.

    for(unsigned int i=0; i<_target->getEntity()->getNumSubEntities();i++)
    {
      _target->getEntity()->getSubEntity(i)->setMaterialName(
        _target->getEntity()->getSubEntity(i)->getMaterialName() + "_Invis");
    } // end for

    _target->setInCombat(false);

  } // end if

  // Add the particle.
  ParticleManager::getSingleton().attachParticleToEntity(_target, this->getTargetParticle());

  return true;
} // void InvisibleEffect::activate(DynamicEntity* _entity)


bool InvisibleEffect::updateEffect(double _timeSinceLastFrame, DynamicEntity* _owner)
{
  // Increase the amount of time passed.
  this->time -= _timeSinceLastFrame/1000;

  if(this->time <= 0)
  {
    _owner->setInvisible(false);
    _owner->setSpeed(_owner->getSpeed()*(1.0/0.47));

    // Return the owner to its original materials.
    Ogre::Mesh::SubMeshIterator it = _owner->getEntity()->getMesh()->getSubMeshIterator();
    for(unsigned int i=0; i<_owner->getEntity()->getNumSubEntities(); i++)
    {
      _owner->getEntity()->getSubEntity(i)->setMaterialName(it.getNext()->getMaterialName());

    } // end for

    return true;
  } // end if


  if(this->time <= 10)
  {
    double dotRemainder = this->time - (int)this->time;
    if(dotRemainder>0.5)
    {
      GUIManager::getSingleton().flashOnActiveEffect(this);
    } // end if
    else if(dotRemainder<0.5)
    {
      GUIManager::getSingleton().flashOffActiveEffect(this);
    } // else if(dotRemainder<0.5) 
  } // end if

  return false;
} // bool InvisibleEffect::updateEffect(double _timeSinceLastFrame, DynamicEntity* _entity)


void InvisibleEffect::setCombatAble(bool _combatAble)
{
  this->combatAble = _combatAble;
} // void InvisibleEffect::setCombatAble(bool _combatAble)


void InvisibleEffect::setTime(double _time)
{
  this->time = _time;
} // void InvisibleEffect::setTime(double _time)


bool InvisibleEffect::getCombatAble()
{
  return this->combatAble;
} // bool InvisibleEffect::getCombatAble()


double InvisibleEffect::getTime() const
{
  return this->time; 
} // double InvisibleEffect::getTime() const


string InvisibleEffect::getDetailsDescription() const
{

  Ogre::String returnString = "";
  returnString += "Level: " + Ogre::StringConverter::toString(this->getLevel()) + "\n";
  returnString += "Makes Caster Invisible for ";
  returnString += Ogre::StringConverter::toString((int)this->time) + " seconds.";

  if(this->combatAble)
  {
    returnString += " Works in combat";
  } // end if
  else
  {
    returnString += " Cannot be used when in combat";
  } // end else

  return returnString;
} // string InvisibleEffect::getDetailsDescription() const