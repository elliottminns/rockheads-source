#include "../../../PrecompiledHeaders/stdafx.h"
#ifndef INVISIBLE_EFFECT_H
#define INVISIBLE_EFFECT_H
#include "../AbstractEffect/AbstractEffect.h"

class DynamicEntity;

class InvisibleEffect : public AbstractEffect
{
private:
  bool combatAble;
  double time;

public:
  InvisibleEffect();
  ~InvisibleEffect();
  InvisibleEffect(const InvisibleEffect& _other);
  InvisibleEffect* clone() const;
  InvisibleEffect& operator=(const InvisibleEffect& _other);

  void initialise();
  bool activate(DynamicEntity* _caller, DynamicEntity* _target);
  bool updateEffect(double _timeSinceLastFrame, DynamicEntity* _entity);

  void setCombatAble(bool _combatAble);
  void setTime(double time);


  bool getCombatAble();
  double getTime() const;

  
  string getDetailsDescription() const;

};
#endif