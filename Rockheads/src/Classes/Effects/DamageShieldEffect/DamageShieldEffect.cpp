#include "stdafx.h"
#include "DamageShieldEffect.h"
#include "../../GameEntities/DynamicEntities/DynamicEntity/DynamicEntity.h"
#include "../../Managers/GUIManager/GUIManager.h"


DamageShieldEffect::DamageShieldEffect() : AbstractEffect()
{
  this->damageAmount = 0;
  this->time = 0;
  this->ignoresMitigation = false;
} // DamageShieldEffect::DamageShieldEffect() : AbstractEffect()


DamageShieldEffect::~DamageShieldEffect()
{

} // DamageShieldEffect::~DamageShieldEffect()


DamageShieldEffect::DamageShieldEffect(const DamageShieldEffect& _other) : AbstractEffect(_other)
{
  this->damageAmount = _other.damageAmount;
  this->time = _other.time;
  this->ignoresMitigation = _other.ignoresMitigation;
} // DamageShieldEffect::DamageShieldEffect(const DamageShieldEffect& _other)


DamageShieldEffect* DamageShieldEffect::clone() const
{
  return new DamageShieldEffect(*this);
} // DamageShieldEffect::DamageShieldEffect* clone() const


DamageShieldEffect& DamageShieldEffect::operator=(const DamageShieldEffect& _other)
{
  this->setLevel(_other.getLevel());
  this->setName(_other.getName());
  this->setType(_other.getType());
  this->setTime(_other.getTime());
  this->setImage(_other.getImage());
  this->setDamageAmount(_other.getDamageAmount());
  this->ignoresMitigation = _other.ignoresMitigation;
  this->setTargetParticle(_other.getTargetParticle());
  this->setCallerParticle(_other.getCallerParticle());
  return *this;
} // DamageShieldEffect& DamageShieldEffect::operator=(const DamageShieldEffect& _other) const


void DamageShieldEffect::initialise()
{
  
} // void DamageShieldEffect::initialise()


bool DamageShieldEffect::activate(DynamicEntity* _caller, DynamicEntity* _target)
{
  vector<AbstractEffect*>* activeEffects = _target->getActiveEffects();
  bool added = false;
  for(vector<AbstractEffect*>::iterator it = activeEffects->begin(); it != activeEffects->end(); ++it)
  {
    AbstractEffect* currentEffect = *it;

    // Same Type.
    if(currentEffect->getType() == this->getType())
    {
      // Check to see if the same level.
      if(currentEffect->getLevel() == this->getLevel())
      { // If the same level then restore the amount of time on it rather than adding to it.
        added = true;
        static_cast<DamageShieldEffect*>(currentEffect)->setTime(this->getTime());
      } // end if
      else
      { // Make a copy. @Todo - Overload the assignment operator.
        DamageShieldEffect* castedCurrentEffect = (DamageShieldEffect*)currentEffect;
        castedCurrentEffect = this;
        added = true;
      } // end else
    } // end if

  } // end for

  if(!added)
  {
    DamageShieldEffect* effect = this->clone();
    _target->addActiveEffect(effect);
    ParticleManager::getSingleton().attachParticleToEntity(_target, this->getTargetParticle());
  } // end if

  return true;
} // void DamageShieldEffect::activate(DynamicEntity* _entity)


bool DamageShieldEffect::updateEffect(double _timeSinceLastFrame, DynamicEntity* _entity)
{
  // Increase the amount of time passed.
  this->time -= _timeSinceLastFrame/1000;

  if(this->time <= 0)
  {
    ParticleManager::getSingleton().removeParticleFromEntity(_entity,this->getTargetParticle());
    return true;
  } // end if

  if(this->time <= 10)
  {
    double dotRemainder = this->time - (int)this->time;
    if(dotRemainder>0.5)
    {
      GUIManager::getSingleton().flashOnActiveEffect(this);
    } // end if
    else if(dotRemainder<0.5)
    {
      GUIManager::getSingleton().flashOffActiveEffect(this);
    } // else if(dotRemainder<0.5) 
  } // end if

  return false;
} // bool DamageShieldEffect::updateEffect(double _timeSinceLastFrame, DynamicEntity* _entity)


void DamageShieldEffect::setDamageAmount(int _amount)
{
  this->damageAmount = _amount;
} // void DamageShieldEffect::setHealingAmount(int _amount)


void DamageShieldEffect::setTime(double _time)
{
  this->time = _time;
} // void DamageShieldEffect::setTime(double _time)


void DamageShieldEffect::setIgnoresMitigation(bool _ignores)
{
  this->ignoresMitigation = _ignores;
} // void DamageShieldEffect::setIgnoresMitigation(bool _ignores)


int DamageShieldEffect::getDamageAmount() const 
{
  return this->damageAmount;
} // int DamageShieldEffect::getHealingAmount() const 


double DamageShieldEffect::getTime() const
{
  return this->time; 
} // double DamageShieldEffect::getTime() const


bool DamageShieldEffect::getIgnoresMitigation() const
{
  return this->ignoresMitigation;
} // bool DamageShieldEffect::getIgnoresMitigation() const


string DamageShieldEffect::getDetailsDescription() const
{

  Ogre::String returnString = "";
  returnString += "Level: " + Ogre::StringConverter::toString(this->getLevel()) + "\n";
  returnString += "Creates a shield that damages attackers for " + Ogre::StringConverter::toString(this->damageAmount);
  returnString += ". Lasts " + Ogre::StringConverter::toString((int)this->time) + " seconds.";

  return returnString;
} // string DamageShieldEffect::getDetailsDescription() const