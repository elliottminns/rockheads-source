#include "../../../PrecompiledHeaders/stdafx.h"
#ifndef DAMAGE_SHIELD_EFFECT_H
#define DAMAGE_SHIELD_EFFECT_H
#include "../AbstractEffect/AbstractEffect.h"

class DynamicEntity;

class DamageShieldEffect : public AbstractEffect
{
private:
  int damageAmount;
  double time;
  bool ignoresMitigation;

public:
  DamageShieldEffect();
  ~DamageShieldEffect();
  DamageShieldEffect(const DamageShieldEffect& _other);
  DamageShieldEffect* clone() const;
  DamageShieldEffect& operator=(const DamageShieldEffect& _other);

  void initialise();
  bool activate(DynamicEntity* _caller, DynamicEntity* _target);
  bool updateEffect(double _timeSinceLastFrame, DynamicEntity* _entity);

  void setDamageAmount(int _amount);
  void setTime(double time);
  void setIgnoresMitigation(bool _ignores);

  int getDamageAmount() const;
  double getTime() const;
  bool getIgnoresMitigation() const;

  string getDetailsDescription() const;

};
#endif