#include "../../../PrecompiledHeaders/stdafx.h"
#ifndef DAMAGE_OVER_TIME_EFFECT_H
#define DAMAGE_OVER_TIME_EFFECT_H
#include "../AbstractEffect/AbstractEffect.h"

class DynamicEntity;

class DamageOverTimeEffect : public AbstractEffect
{
private:
  int damageAmount;
  double time;
  double timeCheckpoint;
  bool ignoresMitigation;
  DynamicEntity* caller;

public:
  DamageOverTimeEffect();
  ~DamageOverTimeEffect();
  DamageOverTimeEffect(const DamageOverTimeEffect& _other);
  DamageOverTimeEffect* clone() const;
  DamageOverTimeEffect& operator=(const DamageOverTimeEffect& _other);

  void initialise();
  bool activate(DynamicEntity* _caller, DynamicEntity* _target);
  bool updateEffect(double _timeSinceLastFrame, DynamicEntity* _entity);

  void setDamageAmount(int _amount);
  void setTime(double time);
  void setIgnoresMitigation(bool _ignores);

  int getDamageAmount() const;
  double getTime() const;

  string getDetailsDescription() const;

};
#endif