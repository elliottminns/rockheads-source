#include "stdafx.h"
#include "DamageOverTimeEffect.h"
#include "../../GameEntities/DynamicEntities/DynamicEntity/DynamicEntity.h"
#include "../../Managers/GUIManager/GUIManager.h"

DamageOverTimeEffect::DamageOverTimeEffect() : AbstractEffect()
{
  this->damageAmount = 0;
  this->time = 0;
  this->timeCheckpoint = 0;
  this->ignoresMitigation = false;
  this->caller = NULL;
} // DamageOverTimeEffect::DamageOverTimeEffect() : AbstractEffect()


DamageOverTimeEffect::~DamageOverTimeEffect()
{
  this->caller = NULL;
} // DamageOverTimeEffect::~DamageOverTimeEffect()


DamageOverTimeEffect::DamageOverTimeEffect(const DamageOverTimeEffect& _other) : AbstractEffect(_other)
{
  this->damageAmount = _other.damageAmount;
  this->time = _other.time;
  this->timeCheckpoint = 0;
  this->ignoresMitigation = _other.ignoresMitigation;
  this->caller = NULL;
} // DamageOverTimeEffect::DamageOverTimeEffect(const DamageOverTimeEffect& _other)


DamageOverTimeEffect* DamageOverTimeEffect::clone() const
{
  return new DamageOverTimeEffect(*this);
} // DamageOverTimeEffect::DamageOverTimeEffect* clone() const


DamageOverTimeEffect& DamageOverTimeEffect::operator=(const DamageOverTimeEffect& _other)
{
  this->setLevel(_other.getLevel());
  this->setName(_other.getName());
  this->setType(_other.getType());
  this->setTime(_other.getTime());
  this->setImage(_other.getImage());
  this->setDamageAmount(_other.getDamageAmount());
  this->ignoresMitigation = _other.ignoresMitigation;
  this->setTargetParticle(_other.getTargetParticle());
  this->setCallerParticle(_other.getCallerParticle());
  this->timeCheckpoint = 0;
  if(_other.caller)
  {
    this->caller = _other.caller;
  } // end if
  return *this;
} // DamageOverTimeEffect& DamageOverTimeEffect::operator=(const DamageOverTimeEffect& _other) const


void DamageOverTimeEffect::initialise()
{
  
} // void DamageOverTimeEffect::initialise()


bool DamageOverTimeEffect::activate(DynamicEntity* _caller, DynamicEntity* _target)
{
  vector<AbstractEffect*>* activeEffects = _target->getActiveEffects();
  bool added = false;
  for(vector<AbstractEffect*>::iterator it = activeEffects->begin(); it != activeEffects->end(); ++it)
  {
    AbstractEffect* currentEffect = *it;

    // Same Type.
    if(currentEffect->getType() == this->getType())
    {
      // Check to see if the same level.
      if(currentEffect->getLevel() == this->getLevel())
      { // If the same level then restore the amount of time on it rather than adding to it.
        added = true;
        static_cast<DamageOverTimeEffect*>(currentEffect)->setTime(this->getTime());
      } // end if
      else
      { // Make a copy. @Todo - Overload the assignment operator.
        DamageOverTimeEffect* castedCurrentEffect = (DamageOverTimeEffect*)currentEffect;
        castedCurrentEffect = this;
        added = true;
      } // end else
    } // end if

  } // end for

  if(!added)
  {
    DamageOverTimeEffect* effect = this->clone();
    effect->timeCheckpoint = 0;
    effect->caller = _caller;
    _target->addActiveEffect(effect);
    ParticleManager::getSingleton().attachParticleToEntity(_target, this->getTargetParticle());
  } // end if

  return true;
} // void DamageOverTimeEffect::activate(DynamicEntity* _entity)


bool DamageOverTimeEffect::updateEffect(double _timeSinceLastFrame, DynamicEntity* _owner)
{
  // Increase the amount of time passed.
  this->time -= _timeSinceLastFrame/1000;
  this->timeCheckpoint += _timeSinceLastFrame/1000;

  // Add health to the player if time
  if(this->timeCheckpoint > 3.0)
  {
    this->timeCheckpoint = 0;
    _owner->setHP(_owner->getHP()-this->damageAmount/5);
    MessageType msg;

    if(this->ignoresMitigation)
    { 
      msg = MessageTypeDamagedNoMitigation;
    }
    else
    {
      msg = MessageTypeDamagedNormal;
    } // end else

    static int damagePerTick = this->damageAmount/5;

    MessageDispatcher::getInstance()->dispatchMsg(0,this->caller->getEntityIDNumber(), _owner->getEntityIDNumber(),
      msg, &damagePerTick);
  
  } // end if
  if(this->time <= 0)
  {
    ParticleManager::getSingleton().removeParticleFromEntity(_owner,this->getTargetParticle());
    return true;
  } // end if

  if(this->time <= 10)
  {
    double dotRemainder = this->time - (int)this->time;
    if(dotRemainder>0.5)
    {
      GUIManager::getSingleton().flashOnActiveEffect(this);
    } // end if
    else if(dotRemainder<0.5)
    {
      GUIManager::getSingleton().flashOffActiveEffect(this);
    } // else if(dotRemainder<0.5) 
  } // end if

  return false;
} // bool DamageOverTimeEffect::updateEffect(double _timeSinceLastFrame, DynamicEntity* _entity)


void DamageOverTimeEffect::setDamageAmount(int _amount)
{
  this->damageAmount = _amount;
} // void DamageOverTimeEffect::setHealingAmount(int _amount)


void DamageOverTimeEffect::setTime(double _time)
{
  this->time = _time;
} // void DamageOverTimeEffect::setTime(double _time)


void DamageOverTimeEffect::setIgnoresMitigation(bool _ignores)
{
  this->ignoresMitigation = _ignores;
} // void DamageOverTimeEffect::setIgnoresMitigation(bool _ignores)


int DamageOverTimeEffect::getDamageAmount() const 
{
  return this->damageAmount;
} // int DamageOverTimeEffect::getHealingAmount() const 


double DamageOverTimeEffect::getTime() const
{
  return this->time; 
} // double DamageOverTimeEffect::getTime() const


string DamageOverTimeEffect::getDetailsDescription() const
{

  Ogre::String returnString = "";
  returnString += "Level: " + Ogre::StringConverter::toString(this->getLevel()) + "\n";
  returnString += "Damages target for " + Ogre::StringConverter::toString(this->damageAmount);
  returnString += " over " + Ogre::StringConverter::toString((int)this->time) + " seconds.";

  return returnString;
} // string DamageOverTimeEffect::getDetailsDescription() const