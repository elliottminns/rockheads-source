#include "../../../PrecompiledHeaders/stdafx.h"
#ifndef HEAL_OVER_TIME_EFFECT_H
#define HEAL_OVER_TIME_EFFECT_H
#include "../AbstractEffect/AbstractEffect.h"

class DynamicEntity;

class HealOverTimeEffect : public AbstractEffect
{
private:
  int healingAmount;
  double time;
  double timeCheckpoint;

public:
  HealOverTimeEffect();
  ~HealOverTimeEffect();
  HealOverTimeEffect(const HealOverTimeEffect& _other);
  HealOverTimeEffect* clone() const;
  HealOverTimeEffect& operator=(const HealOverTimeEffect& _other);

  void initialise();
  bool activate(DynamicEntity* _caller, DynamicEntity* _target);
  bool updateEffect(double _timeSinceLastFrame, DynamicEntity* _owner);

  void setHealingAmount(int _amount);
  void setTime(double time);
  
  int getHealingAmount() const;
  double getTime() const;

  
  string getDetailsDescription() const;


};
#endif