#include "stdafx.h"
#include "HealOverTimeEffect.h"
#include "../../GameEntities/DynamicEntities/DynamicEntity/DynamicEntity.h"
#include "../../Managers/GUIManager/GUIManager.h"


HealOverTimeEffect::HealOverTimeEffect() : AbstractEffect()
{
  this->healingAmount = 0;
  this->time = 0;
  this->timeCheckpoint = 0;
} // HealOverTimeEffect::HealOverTimeEffect() : AbstractEffect()


HealOverTimeEffect::~HealOverTimeEffect()
{

} // HealOverTimeEffect::~HealOverTimeEffect()


HealOverTimeEffect::HealOverTimeEffect(const HealOverTimeEffect& _other) : AbstractEffect(_other)
{
  this->healingAmount = _other.healingAmount;
  this->time = _other.time;
  this->timeCheckpoint = 0;
} // HealOverTimeEffect::HealOverTimeEffect(const HealOverTimeEffect& _other)


HealOverTimeEffect* HealOverTimeEffect::clone() const
{
  return new HealOverTimeEffect(*this);
} // HealOverTimeEffect::HealOverTimeEffect* clone() const


HealOverTimeEffect& HealOverTimeEffect::operator=(const HealOverTimeEffect& _other)
{
  this->setLevel(_other.getLevel());
  this->setName(_other.getName());
  this->setType(_other.getType());
  this->setTime(_other.getTime());
  this->setHealingAmount(_other.getHealingAmount());
  this->setImage(_other.getImage());
  this->timeCheckpoint = 0;
  this->setTargetParticle(_other.getTargetParticle());
  this->setCallerParticle(_other.getCallerParticle());
  return *this;
} // HealOverTimeEffect& HealOverTimeEffect::operator=(const HealOverTimeEffect& _other) const


void HealOverTimeEffect::initialise()
{
  
} // void HealOverTimeEffect::initialise()


bool HealOverTimeEffect::activate(DynamicEntity* _caller, DynamicEntity* _target)
{
  vector<AbstractEffect*>* activeEffects = _target->getActiveEffects();
  bool added = false;

  ParticleManager::getSingleton().attachParticleToEntity(_caller, this->getCallerParticle());

  for(vector<AbstractEffect*>::iterator it = activeEffects->begin(); it != activeEffects->end(); ++it)
  {
    AbstractEffect* currentEffect = *it;

    // Same Type.
    if(currentEffect->getType() == this->getType())
    {
      // Check to see if the same level.
      if(currentEffect->getLevel() == this->getLevel())
      { // If the same level then restore the amount of time on it rather than adding to it.
        added = true;
        static_cast<HealOverTimeEffect*>(currentEffect)->setTime(this->getTime());
      } // end if
      else
      { // Make a copy. @Todo - Overload the assignment operator.
        HealOverTimeEffect* castedCurrentEffect = (HealOverTimeEffect*)currentEffect;
        castedCurrentEffect = this;
        added = true;
        GUIManager::getSingleton().flashOnActiveEffect(this);
      } // end else
    } // end if

  } // end for

  if(!added)
  {
    HealOverTimeEffect* effect = this->clone();
    effect->timeCheckpoint = 0;
    _target->addActiveEffect(effect);
    ParticleManager::getSingleton().attachParticleToEntity(_target, this->getTargetParticle());
    
  } // end if

  return true;

} // void HealOverTimeEffect::activate(DynamicEntity* _entity)


bool HealOverTimeEffect::updateEffect(double _timeSinceLastFrame, DynamicEntity* _owner)
{
  // Increase the amount of time passed.
  this->time -= _timeSinceLastFrame/1000;
  this->timeCheckpoint += _timeSinceLastFrame/1000;

  // Add health to the player if time
  if(this->timeCheckpoint > 3.0)
  {
    this->timeCheckpoint = 0;
    _owner->heal(this->healingAmount);
  } // end if

  if(this->time <= 0)
  {
    ParticleManager::getSingleton().removeParticleFromEntity(_owner,this->getTargetParticle());
    return true;
  } // end if

  if(this->time <= 10)
  {
    double dotRemainder = this->time - (int)this->time;
    if(dotRemainder>0.5)
    {
      GUIManager::getSingleton().flashOnActiveEffect(this);
    } // end if
    else if(dotRemainder<0.5)
    {
      GUIManager::getSingleton().flashOffActiveEffect(this);
    } // else if(dotRemainder<0.5) 
  } // end if
  return false;
} // bool HealOverTimeEffect::updateEffect(double _timeSinceLastFrame, DynamicEntity* _entity)


void HealOverTimeEffect::setHealingAmount(int _amount)
{
  this->healingAmount = _amount;
} // void HealOverTimeEffect::setHealingAmount(int _amount)


void HealOverTimeEffect::setTime(double _time)
{
  this->time = _time;
} // void HealOverTimeEffect::setTime(double _time)


int HealOverTimeEffect::getHealingAmount() const 
{
  return this->healingAmount;
} // int HealOverTimeEffect::getHealingAmount() const 


double HealOverTimeEffect::getTime() const
{
  return this->time; 
} // double HealOverTimeEffect::getTime() const


string HealOverTimeEffect::getDetailsDescription() const
{

  Ogre::String returnString = "";
  returnString += "Level: " + Ogre::StringConverter::toString(this->getLevel()) + "\n";
  returnString += "Heals caster for " + Ogre::StringConverter::toString(this->healingAmount);
  returnString += " over " + Ogre::StringConverter::toString((int)this->time) + " seconds.";

  return returnString;
} // string HealOverTimeEffect::getDetailsDescription() const