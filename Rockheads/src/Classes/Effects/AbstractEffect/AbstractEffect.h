#include "../../../PrecompiledHeaders/stdafx.h"
#ifndef ABSTRACT_EFFECT_H
#define ABSTRACT_EFFECT_H

#include "../../Managers/ParticleManager/ParticleManager.h"

class DynamicEntity;

class AbstractEffect
{
private:
  int level;
  string name;
  string type;
  string image;
  string callerParticle;
  string targetParticle;

public:
  AbstractEffect();
  ~AbstractEffect();
  AbstractEffect(const AbstractEffect& _other);
  virtual AbstractEffect* clone() const;

  virtual void initialise()=0;
  virtual bool activate(DynamicEntity* _caller, DynamicEntity* _target)=0;
  virtual bool updateEffect(double _timeSinceLastFrame, DynamicEntity* _owner);

  void setLevel(int _level);
  void setName(string _name);
  void setType(string _type);
  void setImage(string _image);
  void setCallerParticle(string _particle);
  void setTargetParticle(string _particle);

  int getLevel() const;
  string getName() const;
  string getType() const;
  string getImage() const;
  string getCallerParticle() const;
  string getTargetParticle() const;

  virtual string getDetailsDescription() const;

}; // class AbstractEffect

#endif