#include "stdafx.h"
#include "AbstractEffect.h"
#include "../../GameEntities/DynamicEntities/DynamicEntity/DynamicEntity.h"


AbstractEffect::AbstractEffect()
{
  this->level = 0;
  this->name = "";
  this->image = "";
  this->type = "";
  this->targetParticle = "";
  this->callerParticle = "";
} // AbstractEffect::Effect()


AbstractEffect::~AbstractEffect()
{

} // AbstractEffect::~Effect()


AbstractEffect::AbstractEffect(const AbstractEffect& _other)
{
  this->level = _other.level;
  this->name = _other.name;
  this->image = _other.image;
  this->type = _other.type;
  this->targetParticle = _other.targetParticle;
  this->callerParticle = _other.callerParticle;
} // AbstractEffect::AbstractEffect(const AbstractEffect& _other)


AbstractEffect* AbstractEffect::clone() const
{
  return NULL;
} // AbstractEffect* AbstractEffect::clone() const


bool AbstractEffect::updateEffect(double _timeSinceLastFrame, DynamicEntity* _entity)
{ // Returns true if the effect should be removed from the entity.
  return false;
} // void AbstractEffect::updateEffect(double _timeSinceLastFrame)


void AbstractEffect::setLevel(int _level)
{
  this->level = _level;
} // void AbstractEffect::setLevel()


void AbstractEffect::setName(string _name)
{
  this->name = _name;
} // void AbstractEffect::setName(string _name)


void AbstractEffect::setType(string _type)
{
  this->type = _type;
} // void AbstractEffect::setType(string _type)


void AbstractEffect::setImage(string _image)
{
  this->image = _image;
} // void AbstractEffect::setType(string _type)


void AbstractEffect::setCallerParticle(string _particle)
{
  this->callerParticle = _particle;
} // void AbstractEffect::setCallerParticle(string _particle)


void AbstractEffect::setTargetParticle(string _particle)
{
  this->targetParticle = _particle;
} // void AbstractEffect::setTargetParticle(string _particle)


int AbstractEffect::getLevel() const
{
  return this->level;
} // int AbstractEffect::getLevel() const


string AbstractEffect::getName() const
{
  return this->name;
} // string AbstractEffect::getName() const


string AbstractEffect::getType() const
{
  return this->type;
} // string AbstractEffect::getType() const


string AbstractEffect::getImage() const
{
  return this->image;
} // string AbstractEffect::getImage() const

string AbstractEffect::getCallerParticle() const
{
  return this->callerParticle;
} // string AbstractEffect::getCallerParticle() const


string AbstractEffect::getTargetParticle() const
{
  return this->targetParticle;
} // string AbstractEffect::getTargetParticle() const


string AbstractEffect::getDetailsDescription() const
{
  return " ";
} // string AbstractEffect::getDetailsDescription() const