#include "../../../PrecompiledHeaders/stdafx.h"
#ifndef RESTORE_EFFECT_H
#define RESTORE_EFFECT_H
#include "../AbstractEffect/AbstractEffect.h"

class DynamicEntity;

class RestoreEffect : public AbstractEffect
{
private:
  int healingAmount;

public:
  RestoreEffect();
  ~RestoreEffect();
  RestoreEffect(const RestoreEffect& _other);
  RestoreEffect* clone() const;

  void initialise();
  bool activate(DynamicEntity* _owner, DynamicEntity* _target);

  void setHealingAmount(int _amount);
  
  int getHealingAmount() const;

  string getDetailsDescription() const;

};
#endif