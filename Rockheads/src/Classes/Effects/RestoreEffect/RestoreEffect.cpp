#include "stdafx.h"
#include "RestoreEffect.h"
#include "../../GameEntities/DynamicEntities/DynamicEntity/DynamicEntity.h"


RestoreEffect::RestoreEffect() : AbstractEffect()
{
  this->healingAmount = 0;
} // RestoreEffect::RestoreEffect() : AbstractEffect()


RestoreEffect::~RestoreEffect()
{

} // RestoreEffect::~RestoreEffect()


RestoreEffect::RestoreEffect(const RestoreEffect& _other) : AbstractEffect(_other)
{
  this->healingAmount = _other.healingAmount;
} // RestoreEffect::RestoreEffect(const RestoreEffect& _other)


RestoreEffect* RestoreEffect::clone() const
{
  return new RestoreEffect(*this);
} // RestoreEffect::RestoreEffect* clone() const


void RestoreEffect::initialise()
{
  
} // void RestoreEffect::initialise()


bool RestoreEffect::activate(DynamicEntity* _caller, DynamicEntity* _target)
{
  _target->setStamina(_target->getStamina()+this->healingAmount);

  if(_target->getStamina() > _target->getMaxStamina())
  {
    _target->setStamina(_target->getMaxStamina());
  } // end if

  ParticleManager::getSingleton().attachParticleToEntity(_target, this->getTargetParticle());

  return true;

} // void RestoreEffect::activate(DynamicEntity* _entity)


void RestoreEffect::setHealingAmount(int _amount)
{
  this->healingAmount = _amount;
} // void RestoreEffect::setHealingAmount(int _amount)


int RestoreEffect::getHealingAmount() const
{
  return this->healingAmount;
} // int RestoreEffect::getHealingAmount() const


string RestoreEffect::getDetailsDescription() const
{

  Ogre::String returnString = "";
  returnString += "Level: " + Ogre::StringConverter::toString(this->getLevel()) + "\n";
  returnString += "Restores " + Ogre::StringConverter::toString(this->healingAmount);
  returnString += " stamina.";

  return returnString;
} // string RestoreEffect::getDetailsDescription() const