#include "../../../PrecompiledHeaders/stdafx.h"
#ifndef SAVE_H
#define SAVE_H


class Save
{
private:
  string imageName;
  string fileName;
  string saveName;

public:
  Save();


  void setImageName(string _name);
  void setFileName(string _name);
  void setSaveName(string _name);

  string getImageName();
  string getFileName();
  string getSaveName();

}; // end class Save

#endif