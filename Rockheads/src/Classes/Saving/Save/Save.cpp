#include "stdafx.h"
#include "Save.h"


Save::Save()
{
  this->imageName = "";
  this->fileName = "";
  this->saveName = "";
} // Save::Save()


void Save::setImageName(string _name)
{
  this->imageName = _name;
} // void Save::setImageName(string _name)


void Save::setFileName(string _name)
{
  this->fileName = _name;
} // void Save::setFileName(string _name)


void Save::setSaveName(string _name)
{
  this->saveName = _name;
} // void Save::setSaveName(string _name)


string Save::getImageName()
{
  return this->imageName;
} // string Save::getImageName()


string Save::getFileName()
{
  return this->fileName;
} // void Save::getFileName()


string Save::getSaveName()
{
  return this->saveName;
} // void Save::getSaveName()