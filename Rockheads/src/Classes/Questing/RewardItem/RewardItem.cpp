#include "stdafx.h"
#include "RewardItem.h"


RewardItem::RewardItem()
{
  this->itemName = "";
  this->amount = 0;
} // RewardItem::RewardItem()


RewardItem::RewardItem(const RewardItem& _other)
{
  this->amount = _other.amount;
  this->itemName = _other.itemName;

} // RewardItem::RewardItem(const RewardItem& _other)


RewardItem::~RewardItem()
{

} // RewardItem::~RewardItem()


RewardItem& RewardItem::operator=(const RewardItem& _other)
{
  this->amount = _other.amount;
  this->itemName = _other.itemName;

  return *this;
} // RewardItem& RewardItem::operator=(const RewardItem& _other)


RewardItem* RewardItem::clone() const
{
  return new RewardItem(*this);
} // RewardItem* RewardItem::clone() const


string RewardItem::getItemName()
{
  return this->itemName;
} // RewardItem::GetItem()


int RewardItem::getAmount()
{
  return this->amount;
} // int RewardItem::getAmount()


void RewardItem::setItemName(string _name)
{
  this->itemName = _name;
} // RewardItem::setItemReward()


void RewardItem::setAmount(int _amount)
{
  this->amount = _amount;
} // void RewardItem::setAmount(int _amount)





