#include "../../../PrecompiledHeaders/stdafx.h"
#ifndef REWARD_ITEM_H
#define REWARD_ITEM_H

#include "../../GameObjects/Items/AbstractItem/AbstractItem.h"

class RewardItem
{
public:
  RewardItem();
  RewardItem(const RewardItem& _other);
  ~RewardItem();
  RewardItem& operator=(const RewardItem& _other);
  RewardItem* clone() const;

  string getItemName();
  int getAmount();

  void setItemName(string _name);
  void setAmount(int _amount);

private:
  

  string itemName;
  int  amount;
}; // end class RewardItem

#endif