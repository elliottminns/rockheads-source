#include "stdafx.h"
#include "Quest.h"
#include "../Objectives/CollectObjective/CollectObjective.h"
#include "../Objectives/KillObjective/KillObjective.h"
#include "../Objectives/TimedObjective/TimedObjective.h"
#include "../../Managers/GUIManager/GUIManager.h"

Quest::Quest()
{
  this->name = "";
  this->completionDialogue = "";
  this->duringDialogue = "";
  this->numObjectives = 0;
  this->numRewardItems = 0;
  this->openingDialogue = "";
  this->rewardCurrency = 0;
  this->completed = false;
  this->failed = false;
  this->objectives = NULL;
  this->rewardItems = NULL;
  this->openingNPC = "";
  this->closingNPC = "";
  this->requiresPrevious = false;
  this->previousInChain = "";
} // Quest::Quest()


Quest::Quest(const Quest& _other)
{
  this->name = _other.name;
  this->completionDialogue = _other.completionDialogue;
  this->duringDialogue = _other.duringDialogue;
  this->numObjectives = _other.numObjectives;
  this->numRewardItems = _other.numRewardItems;
  this->openingDialogue = _other.openingDialogue;
  this->rewardCurrency = _other.rewardCurrency;
  this->openingNPC = _other.openingNPC;
  this->closingNPC = _other.closingNPC;
  this->requiresPrevious = _other.requiresPrevious;
  this->previousInChain = _other.previousInChain;

  this->completed = false;
  this->failed = false;
  if(_other.objectives)
  {
    this->objectives = new AbstractObjective*[this->numObjectives];
    for(int i=0;i<_other.numObjectives;i++)
    {
      if(_other.objectives[i])
      {
        this->objectives[i] = _other.objectives[i]->clone();
      } // end if
    } // end for
  } // end if
  else
  {
    this->objectives = NULL;
  } // end else

  if(_other.rewardItems)
  {
    this->rewardItems = new RewardItem*[this->numRewardItems];
    for(int i=0;i<_other.numRewardItems;i++)
    {
      if(_other.rewardItems[i])
      {
        this->rewardItems[i] = _other.rewardItems[i]->clone();
      } // end if
    } // end for
  } // end if
  else
  {
    this->rewardItems = NULL;
  } // end else

} // Quest::Quest(const Quest& _quest) 


Quest::~Quest()
{

} // Quest::~Quest();


Quest* Quest::clone() const
{
  return new Quest(*this);
} // Quest* Quest::clone() const


Quest& Quest::operator=(const Quest& _other)
{
  this->name = _other.name;
  this->completionDialogue = _other.completionDialogue;
  this->duringDialogue = _other.duringDialogue;
  this->numObjectives = _other.numObjectives;
  this->numRewardItems = _other.numRewardItems;
  this->openingDialogue = _other.openingDialogue;
  this->rewardCurrency = _other.rewardCurrency;
  this->openingNPC = _other.openingNPC;
  this->closingNPC = _other.closingNPC;
  this->requiresPrevious = _other.requiresPrevious;
  this->previousInChain = _other.previousInChain;
  this->failed = false;
  this->completed = false;
  
  if(this->objectives)
  {
    delete[] this->objectives;
  } // end if

  if(_other.objectives)
  {
    this->objectives = new AbstractObjective*[this->numObjectives];
    for(int i=0;i<_other.numObjectives;i++)
    {
      if(_other.objectives[i])
      {
        this->objectives[i] = _other.objectives[i];
      } // end if
    } // end for
  } // end if
  else
  {
    this->objectives = NULL;
  } // end else

  if(this->rewardItems)
  {
    delete[] rewardItems;
  } // end if

  if(_other.rewardItems)
  {
    this->rewardItems = new RewardItem*[this->numRewardItems];
    for(int i=0;i<_other.numRewardItems;i++)
    {
      if(_other.rewardItems[i])
      {
        this->rewardItems[i] = _other.rewardItems[i]->clone();
      } // end if
    } // end for
  } // end if
  else
  {
    this->rewardItems = NULL;
  } // end else

  return *this;

} // Quest& Quest::operator=(const Quest& _quest);


void Quest::init()
{
  this->completed = false;
  this->failed = false;
  for(int i=0;i<this->numObjectives;i++)
  {
    this->objectives[i]->setCompleted(false);
  } // end if

  if(this->getName() == "The Bridge is Falling")
  {
    AudioManager::getSingleton().stopSound("CaveAmbience");
    AudioManager::getSingleton().playSound("Volcanic Pit");
    GameManager::getInstance()->getCurrentScene()->getSceneManager()->getEntity("BridgeBlocker")->setQueryFlags(NULL);
  } // end if

  for(int i=0;i<this->numObjectives;i++)
  {
    AbstractObjective* obj = this->objectives[i];
    if(obj->getType() == "collect")
    {
      CollectObjective* colObj = (CollectObjective*)obj;
      // Check inventory for items.
      vector<InventorySlot*>* inventory = Player::getInstance()->getInventory()->getItems();

      for(vector<InventorySlot*>::iterator it = inventory->begin(); it != inventory->end(); ++it)
      {
        InventorySlot* slot = *it;
        AbstractItem* item = slot->getItem();
        if(item)
        {
          if(item->getName() == colObj->getItemName())
          {
            colObj->setAmountCollected(slot->getCount());
            colObj->checkCompleted();
          } // end if
        } // end if
      } // end for it
    } // end if
  } // end for i

} // void Quest::init()


void Quest::updateQuest(AbstractItem* _item, int _count)
{
  for(int i=0;i<this->numObjectives;i++)
  {
    if(objectives[i]->getType() == "collect")
    {
      CollectObjective* objective = (CollectObjective*)objectives[i];
      if(_item->getName() == objective->getItemName())
      {
        objective->setAmountCollected(objective->getAmountCollected()+_count);
        objective->checkCompleted();
        GUIManager::getSingleton().updateJournal(this);
      } // end if(_item->getName() == objective->getItemName())
    } // end if(objectives[i]->getType() == "collect")
  } // end for(int i=0)
} // void Quest::updateQuest(AbstractItem* _item)


void Quest::updateQuest(Mob* _mob)
{
  for(int i=0;i<this->numObjectives;i++)
  {
    if(objectives[i]->getType() == "kill")
    {
      KillObjective* objective = (KillObjective*)objectives[i];
      if(!objective->getCompleted())
      {
        string objMobName = objective->getMobName();
        string mobName    = _mob->getName();
        if(mobName.find(objMobName) != string::npos)
        {
          objective->setAmountCollected(objective->getAmountCollected()+1);
          objective->checkCompleted();
          GUIManager::getSingleton().updateJournal(this);
        } // end if(_item->getName() == objective->getItemName())
      } // end if(!objective->getCompleted()
    } // end if(objectives[i]->getType() == "collect")
  } // end for
} // void Quest::updateQuest(Mob* _mob)


bool Quest::checkCompletion(double _timeSinceLastFrame)
{
  int numCompleted = 0;
  for(int i=0;i<this->numObjectives;i++)
  {
    AbstractObjective* objective = this->objectives[i];
    
    if(objective->checkCompleted())
    {
      ++numCompleted;
    } // end if
    else if(objective->getType() == "timed")
    {
      TimedObjective* timedObjective = (TimedObjective*)objective;
      if(!timedObjective->getFailed() && !timedObjective->getCompleted())
      {
        timedObjective->addToTimePassed(_timeSinceLastFrame);
        timedObjective->checkCompleted();
      } // end if
      GUIManager::getSingleton().updateJournal(this);
      if(timedObjective->getFailed())
      {
        this->failed = true;
      } // end if
    } // end if

  } // end for

  if(numCompleted >= this->numObjectives)
  {
    this->completed = true;

    if(this->getName() == "The Bridge is Falling")
    {
      GameManager::getInstance()->getCurrentScene()->getSceneManager()->getEntity("BridgeBlocker")->setQueryFlags(STATIC_WALL);
    } // end if

    GUIManager::getSingleton().updateJournal(this);
    return true;
  } // end if

  if(this->failed)
  {
    AudioManager::getSingleton().stopSound("CaveAmbience");
    AudioManager::getSingleton().stopSound("Volcanic Pit");
    AudioManager::getSingleton().stopSound("Foot");
    Player::getInstance()->die();
  } // end if

  return false;
} // Quest::checkCompletion(string& _dialogue)


void Quest::rewardPlayer()
{
  for(int i=0;i<this->numObjectives;i++)
  {
    AbstractObjective* obj = this->objectives[i];

    if(obj->getType() == "collect")
    {
      CollectObjective* colObj = (CollectObjective*)obj;

      // Loop through the players inventory and find the item.
      vector<InventorySlot*>* inventory = Player::getInstance()->getInventory()->getItems();

      for(vector<InventorySlot*>::iterator it = inventory->begin(); it != inventory->end(); ++it)
      {
        InventorySlot* slot = *it;

        if(slot->getItem())
        {
          // If the items match remove the number of them.
          if(slot->getItem()->getName() == colObj->getItemName())
          {
            // Minus 1 from the number of items in that slot.
            slot->setCount(slot->getCount()-colObj->getAmountNeeded());

            if(slot->getCount() <= 0)
            {
              // set the item to NULL.
              slot->setItem(NULL);
              // Remove item from the inventory.
              Player::getInstance()->removeFromInv(slot, true);
            } // end if
            else
            {
              if(CEGUI::WindowManager::getSingleton().isWindowPresent("PlayerInventory"))
              {
                GUIManager::getSingletonPtr()->updateItemCount(slot);
              } // end if
            } // end else

          } // end if
        } // end if(slot->getItem())
      } // end for

    } // end if

  } // end for

  Player::getInstance()->setCurrency(Player::getInstance()->getCurrency()+this->rewardCurrency);
  for(int i=0;i<this->numRewardItems;i++)
  {
    for(int j=0;j<this->rewardItems[i]->getAmount();j++)
    {
      Player::getInstance()->getInventory()->addItemAtEmptySlot(
        ItemManager::getInstance()->getItem(this->rewardItems[i]->getItemName().c_str()),1);
    } // end if
  } // end for
} // Quest::rewardPlayer()


void Quest::setName(string _name)
{
  this->name = _name;
} // void Quest::setName(string _name)


void Quest::setRewardCurrency(int _amount)
{
  this->rewardCurrency = _amount;
} // void Quest::setRewardCurrency(int _amount)


void Quest::setOpeningDialogue(string _dialogue)
{
  this->openingDialogue = _dialogue;
} // void Quest::setOpeningDialogue(string _dialogue)


void Quest::setDuringDialogue(string _dialogue)
{
  this->duringDialogue = _dialogue;
} // void Quest::setDuringDialogue(string _dialogue)


void Quest::setClosingDialogue(string _dialogue)
{
  this->completionDialogue = _dialogue;
} // void Quest::setClosingDialogue(string _dialogue)


void Quest::setOpeningNPC(string _npc)
{
  this->openingNPC = _npc;
} // void Quest::setOpeningNPC(string _npc)


void Quest::setClosingNPC(string _npc)
{
  this->closingNPC = _npc;
} // void Quest::setClosingNPC(string _npc)


void Quest::setRewards(vector<RewardItem*>* _rewards)
{
  this->numRewardItems = _rewards->size();

  this->rewardItems = new RewardItem*[numRewardItems];

  int i=0;

  for(vector<RewardItem*>::iterator it = _rewards->begin(); it != _rewards->end(); ++it)
  {
    this->rewardItems[i] = *it;
    ++i;
  } // end for

} // void Quest::setRewards(vector<RewardItem*>* _rewards)


void Quest::setObjectives(vector<AbstractObjective*>* _objectives)
{
  this->numObjectives = _objectives->size();

  this->objectives = new AbstractObjective*[numObjectives];

  int i=0;

  for(vector<AbstractObjective*>::iterator it = _objectives->begin(); it != _objectives->end(); ++it)
  {
    this->objectives[i] = *it;
    ++i;
  } // end for
} // void Quest::setObjectives(vector<AbstractObjectives*>* _objectives)


void Quest::setRequiresPrevious(bool _requires)
{
  this->requiresPrevious = _requires;
} // void Quest::setRequiresPrevious(bool _requires)


void Quest::setPreviousInChain(string _name)
{
  this->previousInChain = _name;
} // void Quest::setPreviousInChain(string _name)


string Quest::getName() const
{
  return this->name;
} // string Quest::getName() const


bool Quest::getRequiresPrevious() const
{
  return this->requiresPrevious;
} // bool Quest::getRequiresPrevious() const


string Quest::getPreviousInChain() const
{
  return this->previousInChain;
} // string Quest::getPreviousInChain() const


bool Quest::getCompleted() const
{
  return this->completed;
} // bool Quest::getCompleted() const


string Quest::getClosingNPC() const
{
  return this->closingNPC;
} // string Quest::getClosingNPC() const


string Quest::getOpeningNPC() const
{
  return this->openingNPC;
} // string Quest::getOpeningNPC() const


string Quest::getOpeningDialogue() const
{
  return this->openingDialogue;
} // string Quest::getOpeningDialogue() const


string Quest::getDuringDialogue() const
{
  return this->duringDialogue;
} // string Quest::getDuringDialogue() const


string Quest::getClosingDialogue() const
{
  return this->completionDialogue;
} // string Quest::getClosingDialogue() const


int Quest::getNumRewards() const
{
  return this->numRewardItems;
} // int Quest::getNumRewards() const


RewardItem** Quest::getRewards() const
{
  return this->rewardItems;
} // vector<RewardItem*>* Quest::getRewards() const


int Quest::getRewardCurrency() const
{
  return this->rewardCurrency;
} // int Quest::getRewardCurrency() const


string Quest::getObjectiveText() const
{
  string text = "";
  for(int i=0; i<this->numObjectives; i++)
  {
    AbstractObjective* objective = this->objectives[i];

    if(objective->getCompleted())
    {
      text += "[colour='FF00FF00']";
    } // end if
    else
    {
      text += "[colour='FFFFFFFF']";
    } // end else

    if(objective->getType() == "kill")
    {
      
      KillObjective* kill = (KillObjective*)objective;
      text += Ogre::StringConverter::toString(kill->getAmountCollected());
      text += "/";
      text += Ogre::StringConverter::toString(kill->getAmountNeeded());
      text += " ";
      text += kill->getMobName();
      text += "\n";
    } // end if
    if(objective->getType() == "collect")
    {
      CollectObjective* collect = (CollectObjective*)objective;
      text += Ogre::StringConverter::toString(collect->getAmountCollected());
      text += "/";
      text += Ogre::StringConverter::toString(collect->getAmountNeeded());
      text += " ";
      text += collect->getItemName();
      text += "\n";
    } // end if
    if(objective->getType() == "timed")
    {
      TimedObjective* timed = (TimedObjective*)objective;
      text += Ogre::StringConverter::toString((int)timed->getTimePassed());
      text += "/";
      text += Ogre::StringConverter::toString((int)timed->getTimeInSeconds());
      text += " Seconds";
      text += "\n";
    } // end if
  } // end for

  return text;
} // string getObjectiveText() const


int Quest::getNumObjectives() const
{
  return this->numObjectives;
} // int Quest::getNumObjectives() const


AbstractObjective** Quest::getObjectives() const
{
  return this->objectives;
} // AbstractObjective** Quest::getObjectives() const