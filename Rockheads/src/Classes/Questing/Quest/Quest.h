#include "../../../PrecompiledHeaders/stdafx.h"
#ifndef QUEST_H
#define QUEST_H

#include "../Objectives/AbstractObjective/AbstractObjective.h"
#include "../RewardItem/RewardItem.h"
#include <iostream>
#include <string>

using namespace std;

class Mob;

class Quest
{
private:
  string      name;
  int         numObjectives;
  int         numRewardItems;
  AbstractObjective** objectives;
  RewardItem** rewardItems;
  int         rewardCurrency;
  string      openingDialogue;
  string      duringDialogue;
  string      completionDialogue;
  string      openingNPC;
  string      closingNPC;
  bool        completed;
  bool        failed;
  bool        requiresPrevious;
  string      previousInChain;

public:
  Quest();
  Quest(const Quest& _other);
  ~Quest();
  Quest& operator=(const Quest& _other);
  Quest* clone() const;

  void init();

  void updateQuest(AbstractItem* _item, int _count);
  void updateQuest(Mob* _mob);

  bool checkCompletion(double _timeSinceLastFrame);

  void rewardPlayer();

  void setName(string _name);
  void setOpeningDialogue(string _dialogue);
  void setDuringDialogue(string _dialogue);
  void setClosingDialogue(string _dialogue);
  void setOpeningNPC(string _npc);
  void setClosingNPC(string _npc);
  void setRewardCurrency(int _amount);
  void setRewards(vector<RewardItem*>* _rewards);
  void setObjectives(vector<AbstractObjective*>* _objectives);
  void setRequiresPrevious(bool _requires);
  void setPreviousInChain(string _name);

  string getName() const;
  bool getRequiresPrevious() const;
  string getPreviousInChain() const;
  bool getCompleted() const;
  string getClosingNPC() const;
  string getOpeningNPC() const;
  string getOpeningDialogue() const;
  string getDuringDialogue() const;
  string getClosingDialogue() const;
  int getNumRewards() const;
  RewardItem** getRewards() const;
  int getRewardCurrency() const;
  string getObjectiveText() const;
  int getNumObjectives() const;
  AbstractObjective** getObjectives() const;
}; // end class Quest

#endif