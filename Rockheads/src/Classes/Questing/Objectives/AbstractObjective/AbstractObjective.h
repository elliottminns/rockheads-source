#include "../../../../PrecompiledHeaders/stdafx.h"
#ifndef ABSTRACT_OBJECTIVE_H
#define ABSTRACT_OBJECTIVE_H

#ifndef NULL
#define NULL 0
#endif

class AbstractObjective
{
public:
  AbstractObjective();
  AbstractObjective(const AbstractObjective& _other);
  virtual ~AbstractObjective();
  virtual AbstractObjective* clone() const;
  virtual AbstractObjective& operator=(AbstractObjective& _other);


  virtual bool checkCompleted()=0;

  bool         getCompleted() const;
  bool         getFailed() const;
  string       getType() const;

  void         setCompleted(bool _completed);
  void         setFailed(bool _failed);
  void         setType(string _type);


private:
  string type;
  bool completed;
  bool failed;
}; // end class Objective

#endif