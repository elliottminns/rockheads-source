#include "stdafx.h"
#include "AbstractObjective.h"


AbstractObjective::AbstractObjective()
{
  this->completed = false;
  this->failed = false;
  this->type = "";
} // AbstractObjective::AbstractObjective()


AbstractObjective::AbstractObjective(const AbstractObjective& _other)
{
  this->completed = false;
  this->failed = false;
  this->type = _other.type;
} // AbstractObjective::AbstractObjective(const AbstractObjective& _other)


AbstractObjective::~AbstractObjective()
{

} // AbstractObjective::~AbstractObjective()


AbstractObjective* AbstractObjective::clone() const
{
  return NULL;
} // AbstractObjective* AbstractObjective::clone() const


AbstractObjective& AbstractObjective::operator=(AbstractObjective& _other)
{
  this->completed = false;
  this->failed = false;
  this->type = _other.type;
  return *this;
} // AbstractObjective& AbstractObjective::operator=(AbstractObjective& _other)


bool AbstractObjective::checkCompleted()
{
  return false;
} // Objective::checkCompleted()


bool AbstractObjective::getCompleted() const
{
  return this->completed;
} // Objective::getCompleted()


bool AbstractObjective::getFailed() const
{
  return this->failed;
} // Objective::getFailed()


string AbstractObjective::getType() const
{
  return this->type;
} // string AbstractObjective::getType() const


void AbstractObjective::setCompleted(bool _completed)
{
  this->completed = _completed;
} // Objective::setCompleted(bool _completed)


void AbstractObjective::setFailed(bool _failed)
{
  this->failed = _failed;
} // Objective::setFailed(bool _failed)


void AbstractObjective::setType(string _type) 
{
  this->type = _type;
} // string AbstractObjective::getType() const