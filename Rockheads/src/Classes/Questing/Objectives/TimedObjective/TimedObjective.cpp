#include "stdafx.h"
#include "TimedObjective.h"

TimedObjective::TimedObjective() : AbstractObjective()
{
  this->timeInSeconds = 0;
  this->safePoint = Ogre::Vector3(0,0,0);
  this->radius = 0.0;
  this->timePassed = 0;
} // TimedObjective::TimedObjective()


TimedObjective::TimedObjective(const TimedObjective& _other) : AbstractObjective(_other)
{
  this->timeInSeconds = _other.timeInSeconds;
  this->safePoint = _other.safePoint;
  this->radius = _other.radius;
  this->timePassed = 0;
} // TimedObjective::TimedObjective(const TimedObjective& _other)


TimedObjective::~TimedObjective()
{
  
} // TimedObjective::~TimedObjective()


TimedObjective* TimedObjective::clone() const
{
  return new TimedObjective(*this);
} // TimedObjective* TimedObjective::clone() const


TimedObjective& TimedObjective::operator=(const TimedObjective& _other)
{
  this->timeInSeconds = _other.timeInSeconds;
  this->safePoint = _other.safePoint;
  this->radius = _other.radius;
  this->timePassed = 0;
  this->setCompleted(false);
  this->setFailed(false);
  this->setType(_other.getType());
  return *this;

} // TimedObjective* TimedObjective::operator=(const TimedObjective& _other)


bool TimedObjective::checkCompleted()
{
  if(this->timePassed > this->timeInSeconds)
  {
    this->setFailed(true);
    return false;
  } // end if
  else
  {
    int safePointX = this->safePoint.x;
    int safePointZ = this->safePoint.z;

    int playerX = Player::getInstance()->getPosition().x;
    int playerZ = Player::getInstance()->getPosition().z;

    if(Ogre::Vector2(safePointX, safePointZ).squaredDistance(Ogre::Vector2(playerX,playerZ)) < this->radius*this->radius)
    {
      this->setCompleted(true);
      return true;
    } // end if
  } // end else

  return false;
} // bool TimedObjective::checkCompleted()


void TimedObjective::addToTimePassed(double _timeSinceLastFrame)
{
  this->timePassed += _timeSinceLastFrame/1000;
} // void TimedObjective::addToTimePassed() 


void TimedObjective::setTimeInSeconds(double _time)
{
  this->timeInSeconds = _time;
} // void TimedObjective::setTimeInSeconds(double _time)


void TimedObjective::setSafePoint(Ogre::Vector3 _point)
{
  this->safePoint = _point;
} // void TimedObjective::setSafePoint(Ogre::Vector3 _point)


void TimedObjective::setSafePointX(int _x)
{
  this->safePoint.x = _x;
} // void TimedObjective::setSafePointX(int _x)


void TimedObjective::setSafePointY(int _y)
{
  this->safePoint.y = _y;
} // void TimedObjective::setSafePointY(int _y)


void TimedObjective::setSafePointZ(int _z)
{
  this->safePoint.z = _z;
} // void TimedObjective::setSafePointZ(int _z)


void TimedObjective::setRadius(double _radius)
{
  this->radius = _radius;
} // void TimedObjective::setRadius(double _radius)


void TimedObjective::setTimePassed(double _time)
{
  this->timePassed = _time;
} // void TimedObjective::setTimePassed(double _time) 


double TimedObjective::getTimePassed()
{
  return this->timePassed;
} // double TimedObjective::getTimePassed()


double TimedObjective::getTimeInSeconds()
{
  return this->timeInSeconds;
} // double TimedObjective::getTimeInSeconds()