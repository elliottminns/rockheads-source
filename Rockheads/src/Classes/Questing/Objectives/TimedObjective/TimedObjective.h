#include "../../../../PrecompiledHeaders/stdafx.h"
#ifndef TIMED_OBJECTIVE_H
#define TIMED_OBJECTIVE_H

#include "../AbstractObjective/AbstractObjective.h"

class TimedObjective : public AbstractObjective
{
private:

  double timeInSeconds;
  double timePassed;
  Ogre::Vector3 safePoint;
  double radius;

public:

  TimedObjective();
  TimedObjective(const TimedObjective& _other);
  ~TimedObjective();
  TimedObjective* clone() const;
  TimedObjective& operator=(const TimedObjective& _other);

  bool checkCompleted();
  void addToTimePassed(double _timeSinceLastFrame);

  void setTimeInSeconds(double _time);
  void setSafePoint(Ogre::Vector3 _point);
  void setSafePointX(int _x);
  void setSafePointY(int _y);
  void setSafePointZ(int _z);
  void setRadius(double _radius);
  void setTimePassed(double _time);

  double getTimePassed();
  double getTimeInSeconds();
  
}; 

#endif