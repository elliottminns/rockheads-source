#include "../../../../PrecompiledHeaders/stdafx.h"
#ifndef KILL_OBJECTIVE_H
#define KILL_OBJECTIVE_H

#include "../AbstractObjective/AbstractObjective.h"
#include "../../../GameEntities/DynamicEntities/Mob/Mob.h"

class KillObjective : public AbstractObjective
{

private:

  string mobName;
  int    amountNeeded;
  int    amountCollected;

public:
  KillObjective();
  KillObjective(const KillObjective& _other);
  ~KillObjective();
  KillObjective* clone() const;
  KillObjective& operator=(const KillObjective& _other);

  string getMobName() const;
  int    getAmountNeeded() const;
  int    getAmountCollected() const;

  void   setMobName(string _mobName);
  void   setAmountNeeded(int _amount);
  void   setAmountCollected(int _amount);

  bool  checkCompleted();

}; // end class KillObjective

#endif