#include "stdafx.h"
#include "KillObjective.h"

KillObjective::KillObjective() : AbstractObjective()
{
  this->mobName = "";
  this->amountNeeded = 0;
  this->amountCollected = 0;
} // CollectObjective::CollectObjective()


KillObjective::KillObjective(const KillObjective& _other) : AbstractObjective(_other)
{
  this->mobName = _other.mobName;
  this->amountNeeded = _other.amountNeeded;
  this->amountCollected = 0;
} // KillObjective::KillObjective(const KillObjective& _other)


KillObjective::~KillObjective()
{
  
} // KillObjective::~KillObjective()


KillObjective* KillObjective::clone() const
{
  return new KillObjective(*this);
} // KillObjective* KillObjective::clone() const


KillObjective& KillObjective::operator=(const KillObjective& _other)
{
  this->mobName = _other.mobName;  
  this->amountNeeded = _other.amountNeeded;
  this->amountCollected = 0;
  this->setCompleted(false);
  this->setFailed(false);
  this->setType(_other.getType());
  return *this;

} // KillObjective* KillObjective::operator=(const KillObjective& _other)


string KillObjective::getMobName() const
{
  return this->mobName;
} // KillObjective::getMobName() const


int KillObjective::getAmountNeeded() const
{
  return this->amountNeeded;
} // int KillObjective::getAmount() const


int KillObjective::getAmountCollected() const
{
  return this->amountCollected;
} // int KillObjective::getAmountCollected() const


void KillObjective::setMobName(string _mobName)
{
  this->mobName = _mobName;
} // KillObjective::setMobName(string _mobName)


void KillObjective::setAmountNeeded(int _amount)
{
  this->amountNeeded = _amount;
} // KillObjective::setAmount(int _amount)


void KillObjective::setAmountCollected(int _amount)
{
  this->amountCollected = _amount;
} // void KillObjective::setAmountCollected(int _amount)


bool KillObjective::checkCompleted()
{
  if(this->amountNeeded <= this->amountCollected)
  {
    this->setCompleted(true);
    return true; 
  } // end if

  return false;
} // KillObjective::checkCompleted()