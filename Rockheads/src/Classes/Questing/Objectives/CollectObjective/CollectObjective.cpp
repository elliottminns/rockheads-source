#include "stdafx.h"
#include "CollectObjective.h"



CollectObjective::CollectObjective() : AbstractObjective()
{
  this->itemName = "";
  this->amountNeeded = 0;
  this->amountCollected = 0;
} // CollectObjective::CollectObjective()


CollectObjective::CollectObjective(const CollectObjective& _other) : AbstractObjective(_other)
{

  this->itemName = _other.itemName;  
  this->amountNeeded = _other.amountNeeded;
  this->amountCollected = 0;
} // CollectObjective::CollectObjective(const CollectObjective& _other)


CollectObjective::~CollectObjective()
{
 
} // CollectObjective::~CollectObjective()


CollectObjective* CollectObjective::clone() const
{
  return new CollectObjective(*this);
} // CollectObjective* CollectObjective::clone() const


CollectObjective& CollectObjective::operator=(const CollectObjective& _other)
{
  this->itemName = _other.itemName;  
  this->amountNeeded = _other.amountNeeded;
  this->setCompleted(_other.getCompleted());
  this->setFailed(_other.getFailed());
  this->setType(_other.getType());
  this->amountCollected = 0;

  return *this;

} // CollectObjective* CollectObjective::operator=(const CollectObjective& _other)


string CollectObjective::getItemName() const
{
  return this->itemName;
} // CollectObjective::getItem() const


int CollectObjective::getAmountNeeded() const
{
  return this->amountNeeded;
} // CollectObjective::getAmount() const


int CollectObjective::getAmountCollected() const
{
  return this->amountCollected;
} // int CollectObjective::getAmountCollected() const


void CollectObjective::setItemName(string _name)
{
  this->itemName = _name;
} // CollectObjective::setItem(Item _item)


void CollectObjective::setAmountNeeded(int _amount)
{
  this->amountNeeded = _amount;
} // CollectObjective::setAmount(int _amount)


void CollectObjective::setAmountCollected(int _amount)
{
  this->amountCollected = _amount;

} // void CollectObjective::setAmountCollected(int _amount)


bool CollectObjective::checkCompleted()
{ 
  if(this->amountNeeded <= this->amountCollected)
  {
    this->setCompleted(true);
    return true; 
  } // end if

  return false;
} // CollectObjective::checkCompleted()