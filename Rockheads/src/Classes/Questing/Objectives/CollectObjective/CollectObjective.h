#include "../../../../PrecompiledHeaders/stdafx.h"
#ifndef COLLECT_OBJECTIVE_H
#define COLLECT_OBJECTIVE_H

#include "../AbstractObjective/AbstractObjective.h"
#include "../../../GameObjects/Items/AbstractItem/AbstractItem.h"

class CollectObjective : public AbstractObjective
{
private:
  string itemName;
  int    amountNeeded;
  int    amountCollected;

public:
  CollectObjective();
  CollectObjective(const CollectObjective& _other);
 ~CollectObjective();
  CollectObjective* clone() const;
  CollectObjective& operator=(const CollectObjective& _other);


  string getItemName() const;
  int    getAmountNeeded() const;
  int    getAmountCollected() const;

  void   setItemName(string _name);
  void   setAmountNeeded(int _amount);
  void   setAmountCollected(int _amount);

  bool   checkCompleted();
}; // end class CollectObjective

#endif