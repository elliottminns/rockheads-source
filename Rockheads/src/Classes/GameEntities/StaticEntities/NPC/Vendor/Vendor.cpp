#include "stdafx.h"
#include "Vendor.h"
#include "../../../DynamicEntities/Player/Player.h"
#include "../../../../Managers/GUIManager/GUIManager.h"
#include "../../../../Managers/StaticEntityManager/StaticEntityManager.h"
#include "../../../../Managers/CollisionManager/QueryFlags.h"
#include "../../../../Managers/AudioManager/AudioManager.h"

Vendor* Vendor::instance = NULL;

Vendor* Vendor::getInstance()
{
  if(instance == NULL)
  {
    instance = new Vendor();
  } // end if

  return instance;
} // Vendor::GetInstance()


void Vendor::init()
{
  this->itemInQuestion = NULL;
  this->ownerOfItem = NULL;

  if(!this->initialised)
  {
    this->initialised = true;

    // Create the vendors inventory.
    this->inventory = new Inventory(StaticEntityManager::getInstance()->getNPC("Winstone Chalkhill"));

    int numItems = ItemManager::getInstance()->getNumItems();
    AbstractItem** items = ItemManager::getInstance()->getItems();
    for(int i=0;i<numItems;i++)
    {
      AbstractItem* item = items[i];
      vector<string>* from = item->getVectorFrom();

      for(vector<string>::iterator it = from->begin(); it != from->end(); ++it)
      {
        string fromName = *it;
        if(fromName == "Merchant")
        {
          int count = 1;
          if(item->getStackable())
          {
            count = 99;
          } // end if
          this->inventory->addItemAtEmptySlot(item,count);
        } // end if
      } // end for

    } // end for

  } // end if
} // Vendor::init()


void Vendor::release()
{

} // Vendor::release()


Inventory* Vendor::getInventory()
{
  return this->inventory;
} // Inventory* Vendor::getInventory()


void Vendor::setInventory()
{

} // Vendor::setInventory()


void Vendor::sellItem()
{
  AudioManager::getSingleton().playSound("Transaction");
  // Get the players currency and check to make sure the player has enough to buy item.
  int playerCurrency = Player::getInstance()->getCurrency();
  if(playerCurrency > this->itemInQuestionValue)
  {
    // Reduce the players currency and add item to inventory.
    Player::getInstance()->setCurrency(playerCurrency - this->itemInQuestionValue);
    Player::getInstance()->getInventory()->addItemAtEmptySlot(this->itemInQuestion->getItem(),1);
  } // end if
} // AbstractItem* Vendor::SellItem()


void Vendor::buyItem()
{
  AudioManager::getSingleton().playSound("Transaction");
  // Get the players currency and add the value to it.
  int playerCurrency = Player::getInstance()->getCurrency();
  Player::getInstance()->setCurrency(playerCurrency + this->itemInQuestionValue);

  // Minus 1 from the number of items in that slot.
  this->itemInQuestion->setCount(this->itemInQuestion->getCount()-1);

  if(Player::getInstance()->getCurrentQuest())
  {
    Player::getInstance()->getCurrentQuest()->updateQuest(this->itemInQuestion->getItem(),-1);
  } // end if

  if(this->itemInQuestion->getCount() <= 0)
  {
    // set the item to NULL.
    this->itemInQuestion->setItem(NULL);
    // Remove item from the inventory.
    Player::getInstance()->removeFromInv(this->itemInQuestion, true);
    GUIManager::getSingletonPtr()->clearShopDetails();
  } // end if
  else
  {
    GUIManager::getSingletonPtr()->updateItemCount(this->itemInQuestion);
  } // end else
} // int Vendor::buyItem()


int Vendor::workOutValue()
{
  int value = 0;
  
  if(this->itemInQuestion && this->ownerOfItem)
  {
    AbstractItem* item = this->itemInQuestion->getItem();

    int baseValue = item->getValue();
    int playerCharisma = Player::getInstance()->getCharisma();
    if(this->ownerOfItem != Player::getInstance())
    {
      value = ((double)(((double)(122-playerCharisma)/100.) * (double)baseValue));
    } // end if
    else
    {
      value = ((double)(((double)(78-playerCharisma)/100) * (double)baseValue));
    } // end else
   
  } // end if
  this->itemInQuestionValue = value;
  return value;
} // int Vendor::workOutValue()


void Vendor::setItemInQuestion(InventorySlot* _slot)
{
  this->itemInQuestion = _slot;
} // void Vendor::setItemInQuestion(InventorySlot* _slot


void Vendor::setOwnerOfItem(AbstractEntity* _owner)
{
  this->ownerOfItem = _owner;
} // void Vendor::setOwnerOfItem(AbstractEntity* _owner)


InventorySlot* Vendor::getItemInQuestion()
{
  return this->itemInQuestion;
} // InventorySlot* Vendor::getItemInQuestion()


AbstractEntity* Vendor::getOwnerOfItem()
{
  return this->ownerOfItem;
} // AbstractEntity* Vendor::getOwnerOfItem()


