#include "../../../../../PrecompiledHeaders/stdafx.h"
#ifndef VENDOR_H
#define VENDOR_H
#include "../NPC.h"
#include "../../../../GameObjects/Inventory/Inventory.h"

class Vendor
{
private:
  static Vendor* instance;
  Vendor(){this->initialised = false;};
  Vendor(const Vendor& _other){};
  Vendor* clone() const{return NULL;};
  Inventory* inventory;

  bool initialised;

  AbstractEntity* ownerOfItem;
  InventorySlot* itemInQuestion;
  int itemInQuestionValue;

public:
  static Vendor* getInstance();
  
  void          init();
  void          update(double _timeSinceLastFrame);
  void          release();

  
  void sellItem();
  void buyItem();
  void handleInteract();
  int workOutValue();

  void setItemInQuestion(InventorySlot* _slot);
  void setOwnerOfItem(AbstractEntity* _owner);
  void setInventory();

  Inventory*    getInventory();
  InventorySlot* getItemInQuestion();
  AbstractEntity* getOwnerOfItem();

}; // end class Vendor

#endif