#include "stdafx.h"
#include "VendorNPC.h"
#include "../../../../Managers/CollisionManager/QueryFlags.h"
#include "../../../../Managers/CollisionManager/CollisionManager.h"
#include "../../../../Managers/GUIManager/GUIManager.h"
#include "../../../DynamicEntities/Player/Player.h"
#include "../../../../Managers/AudioManager/AudioManager.h"

VendorNPC::VendorNPC() : NPC()
{
  
} // VendorNPC::VendorNPC()


VendorNPC::VendorNPC(const VendorNPC& _other) : NPC(_other)
{

} // VendorNPC::VendorNPC(const NPC& _other)


VendorNPC::~VendorNPC() 
{
  if(this->getQuest())
  {
    this->setQuest(NULL);
  } // end if
} // VendorNPC::~VendorNPC()


VendorNPC* VendorNPC::clone() const
{
  return new VendorNPC(*this);
} // VendorNPC* VendorNPC::clone() const


VendorNPC& VendorNPC::operator=(const VendorNPC& _other)
{
  this->setName(_other.getName());
  this->setQuestName(_other.getQuestName());
  this->setType(_other.getType());

  // NPC attributes.
  if(this->getQuest())
  {
    this->setQuest(NULL);
  } // end if
  if(_other.getQuest())
  {
    this->setQuest(_other.getQuest());
  } // end if
  else
  {
    this->setQuest(NULL);
  } // end else

  // Static Entity attributes.
  this->setAttackable(_other.getAttackable());

  // Abstract Entity Attributes.
  this->setName(_other.getName());
  this->setModelName(_other.getModelName());
  this->setOrientation(_other.getOrientation());
  this->setPosition(_other.getPosition());
  this->setDimensions(_other.getDimensions());

  return *this;
} // NPC NPC::operator=(const NPC& _other)


void VendorNPC::init()
{
  this->vendor = Vendor::getInstance();
  this->vendor->init();
  if(this->getEntity())
  {
    this->getEntity()->setQueryFlags(NPC_FLAG);
  } // end if
  if(this->getCurrentAnimationState())
  {
    this->getCurrentAnimationState()->setEnabled(true);
    this->getCurrentAnimationState()->setLoop(true);
    this->setAnimationReducer(1);
  } // end if

  // Get the Y position to the floor.
  this->setActive(true);
} // void VendorNPC::init()


void VendorNPC::update(double _timeSinceLastFrame)
{
  this->updateAnimation(_timeSinceLastFrame);
} // Vendor::update(double _timer)


void VendorNPC::release()
{
  this->vendor = NULL;
} // Vendor::release()


void VendorNPC::handleInteract()
{
  GUIManager::getSingletonPtr()->destroyShopWindow();

  AudioManager::getSingleton().playSound("NPC Greet");

  if(this->getQuest())
  { // If the NPC has a quest.

    if(Player::getInstance()->getCurrentQuest())
    { // If the player has a current quest.

      // Check to see if the players current quest is this one.
      if(Player::getInstance()->getCurrentQuest()->getName() == this->getQuest()->getName())
      {
        // Work out if the quest is completed and this is the quest ender.
        if(Player::getInstance()->getCurrentQuest()->getCompleted() && this->getQuest()->getClosingNPC() == this->getName())
        { // Complete the player quest.getQuest
          GUIManager::getSingleton().clearJournal();
          Player::getInstance()->getCurrentQuest()->rewardPlayer();
          Player::getInstance()->getCompletedQuests()->push_back(Player::getInstance()->getCurrentQuest());
          Player::getInstance()->setCurrentQuest(NULL);

          // Display the completed quest box.
          GUIManager::getSingletonPtr()->buildCompletedQuestWindow(this->getQuest(), this);

        } // end if
        else
        { // Display the Vendor Dialogue box quest box.
          GUIManager::getSingletonPtr()->buildActiveVendorQuestWindow(this->getQuest(), this);
        } // end else

        return;
      } // end if(Player::getInstance()->getCurrentQuest()->getName() == this->quest->getName())
      else
      {
        // Display the standard placeholder.
        GUIManager::getSingletonPtr()->buildNoQuestVendorWindow(this);
      } // end else
    } // end if
    else
    {
      vector<Quest*>* playersCompleted = Player::getInstance()->getCompletedQuests();

      // Determine if the quest has already been completed.
      bool hasBeenCompleted = false;
      bool playerIsEligable = true;

      if(this->getQuest()->getRequiresPrevious())
      {
        playerIsEligable = false;
      } // end if

      for(vector<Quest*>::iterator it = playersCompleted->begin(); it != playersCompleted->end(); ++it)
      {
        Quest* itQuest = *it;
        if(this->getQuest()->getName() == itQuest->getName())
        {
          hasBeenCompleted = true;
        } // end if

        if(this->getQuest()->getRequiresPrevious())
        {
          if(itQuest->getName() == this->getQuest()->getPreviousInChain())
          {
            playerIsEligable = true;
          } // end if
        } // end if

      } // end for


      // If the quest has been completed, display the placeholder.
      if(hasBeenCompleted)
      {
        // Display the placeholder with completed text.
        GUIManager::getSingletonPtr()->buildNoQuestVendorWindow(this);

      } // end if
      else if(!playerIsEligable)
      {
        // Display the standard placeholder.
        GUIManager::getSingletonPtr()->buildNoQuestVendorWindow(this);
      } // end else if
      else
      { // Display the quest.
        GUIManager::getSingletonPtr()->buildAvailableVendorQuestWindow(this->getQuest(), this);
      } // end else
    } // end else
  } // end if
  // Display the box with two buttons for quest or shop depending on if a quest is available | active.
} // void VendorNPC::handleInteract()
