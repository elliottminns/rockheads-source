#include "../../../../../PrecompiledHeaders/stdafx.h"
#ifndef VENDOR_NPC_H
#define VENDOR_NPC_H

#include "../NPC.h"
#include "Vendor.h"

class VendorNPC : public NPC
{
private:
  Vendor* vendor;

public:
  VendorNPC();
  VendorNPC(const VendorNPC& _other);
  ~VendorNPC();
  VendorNPC* clone() const;
  VendorNPC& operator=(const VendorNPC& _other);

  void          init();
  void          update(double _timeSinceLastFrame);
  void          release();

  void handleInteract();

};
#endif