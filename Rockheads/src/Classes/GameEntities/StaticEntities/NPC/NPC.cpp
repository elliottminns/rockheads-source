#include "stdafx.h"
#include "NPC.h"
#include "../../../Managers/CollisionManager/QueryFlags.h"
#include "../../../Managers/CollisionManager/CollisionManager.h"
#include "../../DynamicEntities/Player/Player.h"
#include "../../../Managers/GUIManager/GUIManager.h"
#include "../../../Managers/AudioManager/AudioManager.h"

NPC::NPC() : StaticEntity()
{
  this->quest = NULL;
  this->name = "";
  this->questName = "";
  this->type = "";
} // NPC::NPC()


NPC::NPC(const NPC& _other) : StaticEntity(_other)
{
  this->name = _other.name;
  this->questName = _other.questName;
  this->type = _other.type;

  if(_other.quest)
  {
    this->quest = _other.quest;
  } // end if
  else
  {
    this->quest = NULL;
  } // end else
} // NPC::NPC(const NPC& _other)


NPC::~NPC() 
{
  if(this->quest)
  {
    this->quest = NULL;
  } // end if
} // NPC::~NPC()


NPC* NPC::clone() const
{
  return new NPC(*this);
} // NPC* NPC::clone() const


NPC& NPC::operator=(const NPC& _other)
{
  this->name = _other.name;
  this->questName = _other.questName;
  this->type = _other.type;

  // NPC attributes.
  if(this->quest)
  {
    delete this->quest;
    this->quest = NULL;
  } // end if
  if(_other.quest)
  {
    this->quest = _other.quest;
  } // end if
  else
  {
    this->quest = NULL;
  } // end else

  // Static Entity attributes.
  this->setAttackable(_other.getAttackable());

  // Abstract Entity Attributes.
  this->setName(_other.getName());
  this->setModelName(_other.getModelName());
  this->setOrientation(_other.getOrientation());
  this->setPosition(_other.getPosition());
  this->setDimensions(_other.getDimensions());

  return *this;
} // NPC NPC::operator=(const NPC& _other)


void NPC::init()
{
  if(this->getEntity())
  {
    this->getEntity()->setQueryFlags(NPC_FLAG);
  } // end if
  if(this->getCurrentAnimationState())
  {
    this->getCurrentAnimationState()->setEnabled(true);
    this->getCurrentAnimationState()->setLoop(true);
    this->setAnimationReducer(1);
  } // end if
 
  this->setActive(true);

} // NPC::init()


void NPC::update(double _timeSinceLastFrame)
{
  this->updateAnimation(_timeSinceLastFrame);
} // NPC::update(double _timer)


void NPC::release()
{

} // NPC::release()


void NPC::handleMessage(const Message& _message)
{
  if(_message.msg == MessageTypeInteract)
  {
    this->handleInteract();
  } // end if
} // void NPC::handleMessage(const Message& _message)


string NPC::getName() const
{
  return this->name;
} // string NPC::getName() const


string NPC::getType() const
{
  return this->type;
} // string NPC::getType() const


string NPC::getQuestName() const
{
  return this->questName;
} // string NPC::getQuestName() const


Quest* NPC::getQuest() const
{
  return this->quest;
} // NPC::getQuest() const


void NPC::setName(string _name)
{
  this->name = _name;
} // void NPC::setName(string _name)


void NPC::setType(string _type)
{
  this->type = _type;
} // void NPC::setType(string _type)


void NPC::setQuestName(string _name)
{
  this->questName = _name;
} // void NPC::setQuestName(string _name)


void NPC::setQuest(Quest* _quest)
{
  this->quest = _quest;
} // NPC::setQuest(Quest _quest)


void NPC::handleInteract()
{
  AudioManager::getSingleton().playSound("NPC Greet");
  this->handleQuestInteract();
} //void NPC::handleInteract()


void NPC::handleQuestInteract()
{
  // Check that there is a quest associated with this NPC.
  if(this->quest)
  {  
    if(Player::getInstance()->getCurrentQuest())
    {
      // Check to see if the players current quest is this one.
      if(Player::getInstance()->getCurrentQuest()->getName() == this->quest->getName())
      {
        // Work out if the quest is completed and this is the quest ender.
        if(Player::getInstance()->getCurrentQuest()->getCompleted() && this->quest->getClosingNPC() == this->getName())
        { // Complete the player quest.
          GUIManager::getSingleton().clearJournal();
          Player::getInstance()->getCurrentQuest()->rewardPlayer();
          Player::getInstance()->getCompletedQuests()->push_back(Player::getInstance()->getCurrentQuest());
          Player::getInstance()->setCurrentQuest(NULL);

          // Display the completed quest box.
          GUIManager::getSingletonPtr()->buildCompletedQuestWindow(this->quest, this);

        } // end if
        else
        { // Display the active quest box.
          GUIManager::getSingletonPtr()->buildActiveQuestWindow(this->quest, this);
        } // end else

        return;
      } // end if(Player::getInstance()->getCurrentQuest()->getName() == this->quest->getName())
    } // end if(Player::getInstance()->getCurrentQuest())

    vector<Quest*>* playersCompleted = Player::getInstance()->getCompletedQuests();

    // Determine if the quest has already been completed.
    bool hasBeenCompleted = false;
    bool playerIsEligable = true;

    if(this->quest->getRequiresPrevious())
    {
      playerIsEligable = false;
    } // end if

    for(vector<Quest*>::iterator it = playersCompleted->begin(); it != playersCompleted->end(); ++it)
    {
      Quest* itQuest = *it;
      if(this->quest->getName() == itQuest->getName())
      {
        hasBeenCompleted = true;
      } // end if

      if(this->quest->getRequiresPrevious())
      {
        if(itQuest->getName() == this->quest->getPreviousInChain())
        {
          playerIsEligable = true;
        } // end if
      } // end if

    } // end for


    // If the quest has been completed, display the placeholder.
    if(hasBeenCompleted)
    {
      // Display the placeholder with completed text.
      GUIManager::getSingletonPtr()->buildNoQuestDialogueBox(this);

    } // end if
    else if(!playerIsEligable)
    {
      // Display the standard placeholder.
      GUIManager::getSingletonPtr()->buildNoQuestDialogueBox(this);

    } // end else if
    else
    { // Display the quest.
      GUIManager::getSingletonPtr()->buildAvailableQuestWindow(this->quest, this);
    } // end if

  } // end if
} // void NPC::handleQuestInteract()