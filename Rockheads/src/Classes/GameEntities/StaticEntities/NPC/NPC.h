#include "../../../../PrecompiledHeaders/stdafx.h"
#ifndef NPC_H
#define NPC_H

#include "../StaticEntity/StaticEntity.h"
#include "../../../Questing/Quest/Quest.h"

class NPC : public StaticEntity
{
private:
  Quest* quest;
  string name;
  string type;
  string questName;

public:
  NPC();
  NPC(const NPC& _other);
  virtual ~NPC();
  virtual NPC* clone() const;
  virtual NPC& operator=(const NPC& _other);

  virtual void init();
  virtual void release();
  virtual void update(double _timeSinceLastFrame);
  virtual void handleMessage(const Message& _message);
  virtual void handleInteract();
  void handleQuestInteract();

  Quest* getQuest() const;
  string getName() const;
  string getType() const;
  string getQuestName() const;

  void setName(string _name);
  void setType(string _type);
  void setQuestName(string _name);
  void setQuest(Quest* _quest);
}; // end class NPC

#endif