#include "stdafx.h"
#include "DroppedCurrency.h"


DroppedCurrency::DroppedCurrency() : DroppedObject()
{
  this->amount = 0;
} // DroppedCurrency::DroppedCurrency()


DroppedCurrency::DroppedCurrency(const DroppedCurrency& _other) : DroppedObject(_other)
{
  this->amount = _other.amount;
} // DroppedCurrency::DroppedCurrency(const DroppedCurrency& _other)


DroppedCurrency::~DroppedCurrency()
{
  // Abstract Entity Destructor
  if(this->getEntity())
  {
    delete this->getEntity();
    this->loadEntity(NULL);
  } // end if
  if(this->getNode())
  {
    delete this->getNode();
    this->setNode(NULL);
  } // end if
  if(this->getCurrentAnimationState())
  {
    delete this->getCurrentAnimationState();
    this->setCurrentAnimationState(NULL);
  } // end if
} // DroppedCurrency::~DroppedCurrency()


DroppedCurrency* DroppedCurrency::clone() const
{
  return new DroppedCurrency(*this);
} // DroppedCurrency* DroppedCurrency::clone() const


DroppedCurrency& DroppedCurrency::operator=(DroppedCurrency& _other)
{
  // Static Entity attributes.
  this->setAttackable(_other.getAttackable());

  // Abstract Entity Attributes.
  this->setName(_other.getName());
  this->setModelName(_other.getModelName());
  this->setOrientation(_other.getOrientation());
  this->setPosition(_other.getPosition());
  this->setDimensions(_other.getDimensions());

  return *this;
} // DroppedCurrency& DroppedCurrency::operator=(DroppedCurrency& _other)


void DroppedCurrency::init()
{

} // DroppedCurrency::init()


void DroppedCurrency::render()
{

} // DroppedCurrency::render()


void DroppedCurrency::update(double _timeSinceLastFrame)
{

} // DroppedCurrency::update(double _timeSinceLastFrame)


void DroppedCurrency::release()
{

} // DroppedCurrency::release()


void DroppedCurrency::pickedUp()
{

} // DroppedCurrency::pickedUp()
