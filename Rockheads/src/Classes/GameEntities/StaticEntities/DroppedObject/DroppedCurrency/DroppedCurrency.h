#include "../../../../../PrecompiledHeaders/stdafx.h"
#ifndef DROPPED_CURRENCY_H
#define DROPPED_CURRENCY_H

#include "../DroppedObject.h"

class DroppedCurrency :public DroppedObject
{
public:
  DroppedCurrency();
  DroppedCurrency(const DroppedCurrency& _other);
  ~DroppedCurrency();
  DroppedCurrency* clone() const;
  DroppedCurrency& operator=(DroppedCurrency& _other);

  void  init();
  void  render();
  void  update(double _timeSinceLastFrame);
  void  release();
  void  pickedUp();

private:
  int   amount;
}; // end class DroppedCurrency

#endif