#include "../../../../PrecompiledHeaders/stdafx.h"
#include "../StaticEntity/StaticEntity.h"
#include "../../../GameObjects/DropTable/DropTable.h"

class DroppedObject : public StaticEntity
{
private:
  DropTable* dropTable;
  int indexInVector;

public:
  DroppedObject();
  DroppedObject(const DroppedObject& _other);
  DroppedObject(DropTable* _table, Ogre::SceneManager* _sceneManager, AbstractEntity* _dropper);
  ~DroppedObject();
  DroppedObject* clone() const;
  DroppedObject& operator=(const DroppedObject& _other);

  void setDropTable(DropTable* dropTable);
  DropTable* getDropTable();
  void loadEntity(Ogre::Entity* _entity);

  void handleMessage(const Message& _message);
  void pickedUp();
}; // end class DroppedObject