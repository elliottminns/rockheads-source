#include "stdafx.h"
#include "DroppedItem.h"


DroppedItem::DroppedItem() : DroppedObject()
{
  this->dropTable = NULL;
} // DroppedItem::DroppedItem()


DroppedItem::DroppedItem(const DroppedItem& _other) : DroppedObject(_other)
{
  item = _other.item->clone();
  amount = _other.amount;
} // DroppedItem::DroppedItem(const DroppedItem& _other)


DroppedItem::~DroppedItem()
{
  // DroppedItem Destructor
  if(this->item)
  {
    delete this->item;
    this->item = NULL;
  } // end if

  // Abstract Entity Destructor
  if(this->getEntity())
  {
    delete this->getEntity();
    this->loadEntity(NULL);
  } // end if
  if(this->getNode())
  {
    delete this->getNode();
    this->setNode(NULL);
  } // end if
  if(this->getCurrentAnimationState())
  {
    delete this->getCurrentAnimationState();
    this->setCurrentAnimationState(NULL);
  } // end if
} // DroppedItem::~DroppedItem()


DroppedItem* DroppedItem::clone() const
{
  return new DroppedItem(*this);
} // DroppedItem* DroppedItem::clone() const


DroppedItem& DroppedItem::operator=(const DroppedItem& _other)
{
  // Dropped item attributes.
  if(this->item)
  {
    delete this->item;
    this->item = NULL;
  } // end if
  if(_other.item)
  {
    this->item = _other.item->clone();
  } // end if
  else
  {
    this->item = NULL;
  } // end else

  // Static Entity attributes.
  this->setAttackable(_other.getAttackable());

  // Abstract Entity Attributes.
  this->setName(_other.getName());
  this->setModelName(_other.getModelName());
  this->setOrientation(_other.getOrientation());
  this->setPosition(_other.getPosition());
  this->setDimensions(_other.getDimensions());

  return *this;
} // DroppedItem& DroppedItem::operator=(const DroppedItem& _other)



void DroppedItem::init()
{

} // DroppedItem::init()


void DroppedItem::render()
{

} // DroppedItem::render()


void DroppedItem::update(double _timeSinceLastFrame)
{

} // DroppedItem::update(double _timeSinceLastFrame)


void DroppedItem::release()
{

} // DroppedItem::release()


void DroppedItem::pickedUp()
{

} // DroppedItem::pickedUp()
