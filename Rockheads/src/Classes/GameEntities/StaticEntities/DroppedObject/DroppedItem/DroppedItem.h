#include "../../../../../PrecompiledHeaders/stdafx.h"
#ifndef DROPPED_ITEM_H
#define DROPPED_ITEM_H

#include "../DroppedObject.h"
#include "../../../../GameObjects/Items/AbstractItem/AbstractItem.h"

class DroppedItem : public DroppedObject
{
public:
  DroppedItem();
  DroppedItem(const DroppedItem& _other);
  ~DroppedItem();
  DroppedItem* clone() const;
  DroppedItem& operator=(const DroppedItem& _other);


  void    init();
  void    render();
  void    update(double _timeSinceLastFrame);
  void    release();
  void    pickedUp();

private:
  DropTable* dropTable;
}; // end class DroppedItem

#endif