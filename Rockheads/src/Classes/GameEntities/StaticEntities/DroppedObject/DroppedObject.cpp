#include "stdafx.h"
#include "DroppedObject.h"
#include "../../../GameEntities/DynamicEntities/Player/Player.h"


DroppedObject::DroppedObject() : StaticEntity()
{
  this->dropTable = NULL;
} // DroppedObject::DroppedObject() : StaticEntity()


DroppedObject::DroppedObject(const DroppedObject& _other) : StaticEntity(_other)
{
  if(_other.dropTable)
  {
    this->dropTable = _other.dropTable;
  } // end if
  else
  {
    this->dropTable = NULL;
  } // end else
} // DroppedObject::DroppedObject(const DroppedObject& _other) : StaticEntity(_other)


DroppedObject::DroppedObject(DropTable* _table, Ogre::SceneManager* _sceneManager, AbstractEntity* _dropper) : StaticEntity()
{
  this->setDropTable(_table);
  this->loadEntity(_sceneManager->createEntity(_dropper->getEntity()->getName() + "Drop", "dropped_item.mesh"));
  this->setNode(_sceneManager->getRootSceneNode()->createChildSceneNode(_dropper->getNode()->getName() + "Drop"));
  this->attachEntityToNode();
  
  // @TODO work out a random x and z for the drop.
  int x = _dropper->getPosition().x + 7;
  int z = _dropper->getPosition().z + 7;

  this->setPosition(Ogre::Vector3(x,_dropper->getPosition().y-8, _dropper->getPosition().z));

  this->setActive(true);
} // DroppedObject::DroppedObject(DropTable* _table, Ogre::SceneManager* _sceneManager, AbstractEntity* _dropper)


DroppedObject::~DroppedObject()
{
  this->dropTable = NULL;
} // DroppedObject::~DroppedObject()


DroppedObject* DroppedObject::clone() const
{
  return new DroppedObject(*this);
} // DroppedObject* DroppedObject::clone() const


DroppedObject& DroppedObject::operator=(const DroppedObject& _other)
{
  return *this;
} // DroppedObject& DroppedObject::operator=(const DroppedObject& _other)


void DroppedObject::setDropTable(DropTable* dropTable)
{
  this->dropTable = dropTable;
} // void DroppedObject::setDropTable(DropTable* dropTable)


DropTable* DroppedObject::getDropTable()
{
  return this->dropTable;
} // DropTable* DroppedObject::getDropTable()


void DroppedObject::pickedUp()
{
  AudioManager::getSingleton().playSound("Item Put Down");
  Player::getInstance()->pickUpDrop(this);
  this->setActive(false);
} // void DroppedObject::pickedUp()


void DroppedObject::loadEntity(Ogre::Entity* _entity)
{
  this->setEntity(_entity);
} // void DroppedObject::loadEntity()


void DroppedObject::handleMessage(const Message& _message)
{
  if(_message.msg == MessageTypeInteract)
  {
    this->pickedUp();
  } // end if
} // void DroppedObject::handleMessage()