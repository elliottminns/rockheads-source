#include "stdafx.h"
#include "StaticEntity.h"


StaticEntity::StaticEntity() : AbstractEntity()
{
  attackable = false;
} // StaticEntity::StaticEntity() : AbstractEntity()


StaticEntity::StaticEntity(const StaticEntity& _other) :AbstractEntity(_other)
{
  attackable = _other.attackable;
} // StaticEntity::StaticEntity(const StaticEntity& _other) :AbstractEntity(_other)


StaticEntity::~StaticEntity()
{
  // Abstract Entity Destructor
  if(this->getEntity())
  {
    this->loadEntity(NULL);
  } // end if
  if(this->getNode())
  {
    this->setNode(NULL);
  } // end if
  if(this->getCurrentAnimationState())
  {
    this->setCurrentAnimationState(NULL);
  } // end if
} // StaticEntity::~StaticEntity()


StaticEntity& StaticEntity::operator=(const StaticEntity& _other)
{
  // Static Entity attributes.
  this->attackable = _other.attackable;

  // Abstract Entity Attributes.
  this->setName(_other.getName());
  this->setModelName(_other.getModelName());
  this->setOrientation(_other.getOrientation());
  this->setPosition(_other.getPosition());
  this->setDimensions(_other.getDimensions());

  return *this;
} // StaticEntity::StaticEntity& operator=(const StaticEntity& _other)


bool StaticEntity::getAttackable() const
{
  return this->attackable;
} // StaticEntity::getAttackable()


void StaticEntity::setAttackable(bool _attackable)
{
  this->attackable = _attackable;
} // StaticEntity::setAttackable(bool _attackable)


void StaticEntity::handleMessage(const Message& _message)
{

} // void StaticEntity::handleMessage(const Message& _message)