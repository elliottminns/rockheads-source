#include "../../../../PrecompiledHeaders/stdafx.h"
#ifndef STATIC_ENTITY_H
#define STATIC_ENTITY_H

#include "../../AbstractEntity/AbstractEntity.h"


class StaticEntity : public AbstractEntity
{
public:
  StaticEntity();
  StaticEntity(const StaticEntity& _other);
  virtual ~StaticEntity();
  virtual StaticEntity* clone() const = 0{return NULL;};
  virtual StaticEntity& operator=(const StaticEntity& _other);
  
  bool getAttackable() const;
  void setAttackable(bool _attackable);
  virtual void handleMessage(const Message& _message);
private:
  bool attackable;
}; // end class StaticEntity

#endif