#include "../../../../PrecompiledHeaders/stdafx.h"
#include "../StaticEntity/StaticEntity.h"
#include "../../../GameObjects/DropTable/DropTable.h"

class BreakableObject : public StaticEntity
{
public:
  BreakableObject();
  BreakableObject(const BreakableObject& _other);
  ~BreakableObject();
  BreakableObject* clone() const;
  BreakableObject& operator=(const BreakableObject& _other);

  void init();
  void release();
  void render();
  void update(double _timeSinceLastFrame);
  void breakObject();
  bool getSmashed();
  void setSmashed(bool _smashed);

private:
  bool        smashed;
  DropTable*  dropTable;
}; // end class BreakableObject