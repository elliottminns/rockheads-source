#include "stdafx.h"
#include "BreakableObject.h"


BreakableObject::BreakableObject() : StaticEntity()
{
  this->dropTable = NULL;
} // BreakableObject::BreakableObject()


BreakableObject::BreakableObject(const BreakableObject& _other) : StaticEntity(_other)
{
  if(_other.dropTable)
  {
    this->dropTable = _other.dropTable->clone();
  } // end if
  else
  {
    this->dropTable = NULL;
  } // end else
} // BreakableObject::BreakableObject(const BreakableObject& _other)


BreakableObject::~BreakableObject()
{
  // BreakableObject Destructor.
  if(this->dropTable)
  {
    delete this->dropTable;
    this->dropTable = NULL;
  } // end if

  // Abstract Entity Destructor
  if(this->getEntity())
  {
    delete this->getEntity();
    this->setEntity(NULL);
  } // end if
  if(this->getNode())
  {
    delete this->getNode();
    this->setNode(NULL);
  } // end if
  if(this->getCurrentAnimationState())
  {
    delete this->getCurrentAnimationState();
    this->setCurrentAnimationState(NULL);
  } // end if
} // BreakableObject::~BreakableObject()


BreakableObject* BreakableObject::clone() const
{
  return new BreakableObject(*this);
} // BreakableObject* BreakableObject::clone() const


BreakableObject& BreakableObject::operator=(const BreakableObject& _other)
{
  // Breakable Object Attributes.
  if(this->dropTable)
  {
    delete this->dropTable;
    this->dropTable = NULL;
  } // end if

  if(_other.dropTable)
  {
    this->dropTable = _other.dropTable->clone();
  } // end if
  else
  {
    this->dropTable = NULL;
  } // end else

  // Static Entity attributes.
  this->setAttackable(_other.getAttackable());

  // Abstract Entity Attributes.
  this->setName(_other.getName());
  this->setModelName(_other.getModelName());
  this->setOrientation(_other.getOrientation());
  this->setPosition(_other.getPosition());
  this->setDimensions(_other.getDimensions());

  return *this;
} // BreakableObject& BreakableObject::operator=(const BreakableObject& _other)


void BreakableObject::init()
{

} // BreakableObject::init()


void BreakableObject::release()
{

} // BreakableObject::release()


void BreakableObject::render()
{

} // BreakableObject::render()


void BreakableObject::update(double _timeSinceLastFrame)
{

} // BreakableObject::update(double _timeSinceLastFrame)


void BreakableObject::breakObject()
{
  this->smashed = true;
} // BreakableObject::breakObject()


bool BreakableObject::getSmashed()
{
  return this->smashed;
} // BreakableObject::getSmashed()


void BreakableObject::setSmashed(bool _smashed)
{
  this->smashed = _smashed;
} // BreakableObject::setSmashed(bool _smashed)