#include "../../../../PrecompiledHeaders/stdafx.h"
#ifndef SAVE_POINT_H
#define SAVE_POINT_H

#include "../StaticEntity/StaticEntity.h"
#include "../../../Messaging/Message/Message.h"

class SavePoint : public StaticEntity
{
private:
  int rotationSpeed;
  Ogre::ParticleSystem* particles;

public:
  SavePoint();
  SavePoint(const SavePoint& _other);
  ~SavePoint();
  SavePoint* clone() const;
  SavePoint& operator=(const SavePoint& _other);

  void init();
  void update(double _timer);
  void release();
  void loadEntity(Ogre::Entity* _entity);

  void handleMessage(const Message& _message);

}; // end class SavePoint

#endif