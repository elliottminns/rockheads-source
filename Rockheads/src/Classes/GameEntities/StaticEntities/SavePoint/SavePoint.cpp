#include "stdafx.h"
#include "SavePoint.h"
#include "../../../Messaging/MessageType/MessageType.h"
#include "../../../Managers/CollisionManager/CollisionManager.h"
#include "../../../Managers/GUIManager/GUIManager.h"
#include "../../../Messaging/MessageDispatcher/MessageDispatcher.h"


SavePoint::SavePoint() : StaticEntity()
{
  this->setModelName("Save_Point.mesh");
} // SavePoint::SavePoint()


SavePoint::SavePoint(const SavePoint& _other) : StaticEntity(_other)
{
  this->setModelName("Save_Point.mesh");
} // SavePoint::SavePoint(const SavePoint& _other)


SavePoint::~SavePoint()
{
  // Abstract Entity Destructor
  if(this->getEntity())
  {
    this->loadEntity(NULL);
  } // end if
  if(this->getNode())
  {
    this->setNode(NULL);
  } // end if
  if(this->getCurrentAnimationState())
  {
    this->setCurrentAnimationState(NULL);
  } // end if
} // SavePoint::~SavePoint()


SavePoint* SavePoint::clone() const
{
  return new SavePoint(*this);
} // SavePoint::SavePoint* clone() const


SavePoint& SavePoint::operator=(const SavePoint& _other)
{
  // Static Entity attributes.
  this->setAttackable(_other.getAttackable());

  // Abstract Entity Attributes.
  this->setName(_other.getName());
  this->setModelName(_other.getModelName());
  this->setOrientation(_other.getOrientation());
  this->setPosition(_other.getPosition());
  this->setDimensions(_other.getDimensions());

  return *this;
} // SavePoint& SavePoint::operator=(const SavePoint& other)


void SavePoint::init()
{
  this->rotationSpeed = 30.0f;

  string name = "SaveParticle-" + Ogre::StringConverter::toString(this->getEntityIDNumber());
  this->particles = GameManager::getInstance()->getCurrentScene()->getSceneManager()->createParticleSystem(name, "save-sparkle");
  this->getNode()->attachObject(this->particles);
  this->setActive(true);
} // SavePoint::init()


void SavePoint::update(double _timeSinceLastFrame)
{
  Ogre::Vector3 currentOrientation = this->getOrientation();
  currentOrientation.y += this->rotationSpeed*(_timeSinceLastFrame/1000);
  if(currentOrientation.y >= 360)
  {
    currentOrientation.y -= 360;
  } // end if
  this->setOrientation(currentOrientation);
} // SavePoint::update(double _timer)


void SavePoint::release()
{
  this->particles = NULL;
} // SavePoint::release()


void SavePoint::loadEntity(Ogre::Entity* _entity)
{
  if(_entity)
  {
    this->setEntity(_entity);

    this->setDimensions(_entity->getBoundingBox().getSize());

  } // end if



} // void SavePoint::loadEntity(Ogre::Entity* _entity)


void SavePoint::handleMessage(const Message& _message)
{
  if(_message.msg == MessageTypeInteract)
  {
    GUIManager::getSingleton().buildSavePointWindow();
  } // end if
} // void SavePoint::handleMessage(const Message& _message)