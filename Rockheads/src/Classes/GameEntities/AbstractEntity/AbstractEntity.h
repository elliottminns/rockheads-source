#include "../../../PrecompiledHeaders/stdafx.h"
#ifndef ABSTRACT_ENTITY_H
#define ABSTRACT_ENTITY_H

#include "../../OgreFramework/OgreFramework.h"
#include "../../Messaging/Message/Message.h"
#include "../../Messaging/MessageDispatcher/MessageDispatcher.h"
#include <iostream>
#include <string.h>

using namespace std;

class AbstractEntity
{
public:
  AbstractEntity();
  AbstractEntity(const AbstractEntity& _other);virtual 
  ~AbstractEntity();
  virtual AbstractEntity* clone() const = 0;
  virtual AbstractEntity& operator=(const AbstractEntity& _other);

  virtual void      init();
  virtual void      update(double _timeSinceLastFrame);
  virtual void      release();
  virtual void      checkCollision();

  // Accessors.
  Ogre::Vector3     getPosition() const;
  Ogre::Vector3     getDimensions() const;
  Ogre::Vector3     getSpawnPoint() const;
  Ogre::Vector3     getOrientation() const;
  string            getName() const;
  int               getEntityIDNumber() const;
  bool              getActive() const;
  Ogre::String      getModelName() const;
  Ogre::Entity*     getEntity() const;
  Ogre::SceneNode*  getNode() const;
  Ogre::AnimationState* getCurrentAnimationState() const;
  Ogre::AnimationState* getIdleAnimation() const;
  Ogre::AnimationState* getPreviousAnimationState() const;
  int               getAnimationReducer() const;

  // Mutators.
  void              setPosition(Ogre::Vector3 _position);
  void              setSpawnPoint(Ogre::Vector3 _spawnPoint);
  void              setDimensions(Ogre::Vector3 _dimensions);
  void              setOrientation(Ogre::Vector3 _orientation);
  void              setName(string _name);
  void              setModelName(Ogre::String _model);
  void              setActive(bool _active);
  void              setAnimationReducer(int _reducer);
  void              setEntityIDNumber(int _number);
  void              setScale(double _scale);
  

  virtual void      loadEntity(Ogre::Entity* _entity);
  void              setNode(Ogre::SceneNode* _node);
  void              scaleNode(double _amount);
  void              setCurrentAnimationState(Ogre::AnimationState* _state);
  void              setPreviousAnimationState(Ogre::AnimationState* _state);
  void              setIdleAnimation(Ogre::AnimationState* _state);
  void              clearAnimationStates();

  virtual void      handleMessage(const Message& _message);

  bool              attachEntityToNode();
  double            findAngleBetweenTwoPointsXZ(Ogre::Vector3 _pointA, Ogre::Vector3 _pointB);
  virtual void      updateAnimation(double _timeSinceLastFrame);

  virtual void      calculateYPosition();

protected:
  void setEntity(Ogre::Entity* _entity);

private:
  Ogre::Vector3     position;
  Ogre::Vector3     spawnPoint;
  Ogre::Vector3     dimensions;
  Ogre::Vector3     orientation;
  string            name;
  Ogre::String      modelName;
  Ogre::Entity*     entity;
  Ogre::SceneNode*  node;
  Ogre::AnimationState* currentAnimationState;
  Ogre::AnimationState* idleAnimation;
  Ogre::AnimationState* previousAnimationState;
  int               entityIDNumber;
  bool              active;
  int               animationReducer;
  double            scale;
}; // end class Entity

#endif