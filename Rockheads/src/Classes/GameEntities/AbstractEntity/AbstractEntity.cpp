#include "stdafx.h"
#include "AbstractEntity.h"
#include "../../Messaging/MessageDispatcher/MessageDispatcher.h"

#define _USE_MATH_DEFINES

#include <math.h>
#include <cmath>

AbstractEntity::AbstractEntity()
{
  this->position = Ogre::Vector3(0,0,0);
  this->dimensions = Ogre::Vector3(0,0,0);;
  this->orientation = Ogre::Vector3(0,0,0);;
  this->name = "";
  this->modelName = "";
  this->entity = NULL;
  this->node = NULL;
  this->currentAnimationState = NULL;
  this->idleAnimation         = NULL;
  MessageDispatcher::getInstance()->assignIDNumber(this);
  this->active = false;
  this->animationReducer = 1;
  this->scale = 1.0;
} // AbstractEntity::AbstractEntity()


AbstractEntity::AbstractEntity(const AbstractEntity& _other)
{
  this->name = _other.name;
  this->modelName = _other.modelName;
  this->idleAnimation = _other.idleAnimation;
  this->currentAnimationState = _other.currentAnimationState;
  this->entityIDNumber = MessageDispatcher::getInstance()->assignIDNumber(this);
  this->animationReducer = 1;
  this->scale = _other.scale;
} // AbstractEntity::AbstractEntity(const AbstractEntity& _other)


AbstractEntity::~AbstractEntity()
{
  if(this->entity)
  {
    this->entity = NULL;
  } // end if
  if(this->node)
  {
    this->node = NULL;
  } // end if
  if(this->currentAnimationState)
  {
    this->currentAnimationState = NULL;
  } // end if
  if(this->idleAnimation)
  {
    this->idleAnimation = NULL;
  } // end if
} // AbstractEntity::~AbstractEntity()


AbstractEntity* AbstractEntity::clone() const
{
  AbstractEntity* entity = NULL;
  return entity;
} // AbstractEntity* AbstractEntity::clone() const


AbstractEntity& AbstractEntity::operator=(const AbstractEntity& _other)
{
  this->name = _other.name;
  this->modelName = _other.modelName;
  this->orientation = _other.orientation;
  this->position = _other.position;
  this->dimensions = _other.dimensions;
  this->currentAnimationState = _other.currentAnimationState;
  this->idleAnimation = _other.idleAnimation;
  this->animationReducer = 1;
  return *this;
} // AbstractEntity& AbstractEntity::operator=(const AbstractEntity& _other)


void AbstractEntity::init()
{

} // Entity::init()


void AbstractEntity::update(double _timeSinceLastFrame)
{

} // Entity::update(double _timer)


void AbstractEntity::release()
{

} // Entity::release()


void AbstractEntity::checkCollision()
{

} // Entity::checkCollision()


Ogre::Vector3 AbstractEntity::getPosition() const
{
  return this->position;
} // Entity::getPosition()


Ogre::Vector3 AbstractEntity::getSpawnPoint() const
{
  return this->spawnPoint;
} // Ogre::Vector3 AbstractEntity::getSpawnPoint() const


Ogre::Vector3 AbstractEntity::getDimensions() const
{
  if(this->name != "Snake")
  {
    return this->getEntity()->getBoundingBox().getSize()*this->scale;
  } // end if
  else
  {
    Ogre::Vector3 returnDimension = this->getEntity()->getBoundingBox().getSize();

    returnDimension.y = 4;
    return returnDimension;
  } // end else 
} // Entity::getDimensions()


Ogre::Vector3 AbstractEntity::getOrientation() const
{
  
  return this->orientation;
} // Ogre::Vector3 AbstractEntity::getOrientation()


string AbstractEntity::getName() const
{
  return this->name;
} // Entity::getString()


int AbstractEntity::getEntityIDNumber() const
{
  return this->entityIDNumber;
} // int AbstractEntity::getEntityIDNumber() const


Ogre::String AbstractEntity::getModelName() const
{
  return this->modelName;
} // Ogre::String AbstractEntity::GetModel() const


bool AbstractEntity::getActive() const
{
  return this->active;
} // bool AbstractEntity::getActive() const


Ogre::Entity* AbstractEntity::getEntity() const
{
  return this->entity;
} // Ogre::Entity* AbstractEntity::getEntity()


Ogre::SceneNode* AbstractEntity::getNode() const
{
  return this->node;
} // Ogre::Node* AbstractEntity::getNode()


Ogre::AnimationState* AbstractEntity::getCurrentAnimationState() const
{
  return this->currentAnimationState;
} // Ogre::AnimationState* AbstractEntity::getCurrentAnimationState()


Ogre::AnimationState* AbstractEntity::getIdleAnimation() const
{
  return this->idleAnimation;
} // Ogre::AnimationState *getIdleAnimation() const


int AbstractEntity::getAnimationReducer() const
{
  return this->animationReducer;
} // int AbstractEntity::getAnimationReducer() const


Ogre::AnimationState* AbstractEntity::getPreviousAnimationState() const
{
  return this->previousAnimationState;
} // Ogre::AnimationState* AbstractEntity::getPreviousAnimationState() const


void AbstractEntity::setPosition(Ogre::Vector3 _position)
{
  this->position = _position;
  if(this->node)
  {
    node->setPosition(_position);
  } // end if
} // void AbstractEntity::setPosition(Ogre::Vector3 _position)


void AbstractEntity::setSpawnPoint(Ogre::Vector3 _spawnPoint)
{
  this->spawnPoint = _spawnPoint;
} // void AbstractEntity::setSpawnPoint(Ogre::Vector3 _spawnPoint)


void AbstractEntity::setDimensions(Ogre::Vector3 _dimensions)
{
  this->dimensions = _dimensions;
} // Entity::setDimensions(Vector3D _dimensions)


void AbstractEntity::setOrientation(Ogre::Vector3 _orientation)
{
  this->orientation = _orientation;

  if(this->node)
  {
    this->node->resetOrientation();
    this->node->yaw(Ogre::Degree(this->orientation.y));
  } // end if
} // void AbstractEntity::setOrientation(Ogre::Vector3 _orientation)


void AbstractEntity::setName(string _name)
{
  this->name = _name;
} // Entity::SetString(string _string)


void AbstractEntity::setEntityIDNumber(int _number)
{
  this->entityIDNumber = _number;
} // int AbstractEntity::getEntityIDNumber() const


void AbstractEntity::setScale(double _scale)
{
  this->scale = _scale;
} // void AbstractEntity::setScale(int _scale)


void AbstractEntity::setAnimationReducer(int _reducer)
{
  this->animationReducer = _reducer;
} // void AbstractEntity::setAnimationReducer(int _reducer)


void AbstractEntity::setModelName(Ogre::String _model)
{
  this->modelName = _model;
} // Entity::setModel(Ogre::Entity _model)


void AbstractEntity::loadEntity(Ogre::Entity* _entity)
{
  this->setEntity(_entity);
  if(!_entity)
  {
    return;
  } // end if
  if(_entity->getAllAnimationStates()->hasAnimationState("Idle"))
  {
    this->idleAnimation = _entity->getAnimationState("Idle");
    this->setCurrentAnimationState(idleAnimation);
  } // end if 

  this->setDimensions(_entity->getMesh()->getBounds().getSize());
} // void AbstractEntity::loadEntity(Ogre::Entity* _entity)


void AbstractEntity::setEntity(Ogre::Entity* _entity)
{
  this->entity = _entity;
  if(_entity)
  {
    this->dimensions = _entity->getBoundingBox().getSize();
  } // end if
} // void AbstractEntity::setEntity(Ogre::Entity* _entity)


void AbstractEntity::setNode(Ogre::SceneNode* _node)
{
  this->node = _node;
  this->scaleNode(this->scale);
} // void AbstractEntity::setNode(Ogre::Node* _node)


void AbstractEntity::scaleNode(double _amount)
{
  if(this->node)
  {
    this->node->scale(Ogre::Vector3(_amount,_amount,_amount));
  } // end if
} // void AbstractEntity::scaleNode(double _amount)


void AbstractEntity::setCurrentAnimationState(Ogre::AnimationState* _state)
{
  if(_state == NULL)
  {
    this->currentAnimationState = NULL;
    return;
  } // end if
  if(this->currentAnimationState)
  {
    if(this->currentAnimationState->getLoop())
    {
      this->previousAnimationState = this->currentAnimationState;
    } // end if
    this->currentAnimationState->setEnabled(false);
    this->currentAnimationState->setLoop(false);
    this->currentAnimationState = NULL;
  } // end if
  this->currentAnimationState = _state;
  if(this->currentAnimationState)
  {
    this->currentAnimationState->setTimePosition(0);
  } // end if
} // void AbstractEntity::setCurrentAnimationState(Ogre::AnimationState* _state)


void AbstractEntity::setPreviousAnimationState(Ogre::AnimationState* _state)
{
  this->previousAnimationState = _state;
} // void AbstractEntity::setPreviousAnimationState(Ogre::AnimationState* _state)


void AbstractEntity::setIdleAnimation(Ogre::AnimationState* _state)
{
  this->idleAnimation = _state;
} // void AbstractEntity::setIdleAnimation(Ogre::AnimationState* _state)


void AbstractEntity::setActive(bool _active)
{
  this->active = _active;
} // void AbstractEntity::setActive(bool _active)


bool AbstractEntity::attachEntityToNode()
{
  if(this->entity && this->node)
  {
    this->node->attachObject(this->entity);
    this->node->setPosition(this->position);
    return true;
  } // end if

  return false;
} // bool AbstractEntity::attachEntityToNode()


double AbstractEntity::findAngleBetweenTwoPointsXZ(Ogre::Vector3 _pointA, Ogre::Vector3 _pointB)
{ // Finds the angle in degrees along the z axis from point A to point B.
  double angle = ((atan2(_pointB.x - _pointA.x, _pointB.z - _pointA.z)) * (180/M_PI));
  angle += 0;
  if(angle >= 360)
  {
    angle -= 360;
  } // end if

  return angle;
} // int AbstractEntity::findAngleBetweenTwoPointsXZ(Ogre::Vector3 pointA, Ogre::Vector3, pointB)


void AbstractEntity::updateAnimation(double _timeSinceLastFrame)
{
  if(this->currentAnimationState)
  {
    if(this->currentAnimationState->getEnabled())
    {
      this->currentAnimationState->addTime((Ogre::Real)(_timeSinceLastFrame/1000/this->animationReducer));
      if(!this->currentAnimationState->getLoop())
      {
        if(this->currentAnimationState->getTimePosition() >= this->currentAnimationState->getLength())
        {
          this->setCurrentAnimationState(this->idleAnimation);
          this->currentAnimationState->setLoop(true);
          this->currentAnimationState->setEnabled(true);
        } // end if
      } // end if
    } // end if
  } // end if
} // void AbstractEntity::updateAnimations(double _timeSinceLastFrame) 


void AbstractEntity::handleMessage(const Message& _message)
{

} // void AbstractEntity::handleMessage(const Message& _message)


void AbstractEntity::calculateYPosition()
{
  if(this->node && this->entity)
  {
    // Get the Y position to the floor.
    double heightAdjust = this->getDimensions().y/2*this->scale;
    
    if(this->name == "Bat")
    {
      heightAdjust += 7;
    } // end if

    CollisionManager::getSingleton().setHeightAdjust(heightAdjust);
    CollisionManager::getSingleton().calculateY(this->getNode(),false,false,1.0f,STATIC_FLOOR);
    this->setPosition(this->getNode()->getPosition());
  } // end if
} // void AbstractEntity::calculateYPosition()