#include "../../../../PrecompiledHeaders/stdafx.h"
#ifndef DYNAMIC_ENTITY_H
#define DYNAMIC_ENTITY_H

#include "../../AbstractEntity/AbstractEntity.h"
#include "../EntityClass/SkillSet/SkillSet.h"
#include "../../../DamageText/DamageText.h"

class AbstractEffect;

class DynamicEntity : public AbstractEntity
{
private:
  int           level;
  int           strength;
  int           dexterity;
  int           agility;
  int           constitution;
  int           intelligence;
  int           wisdom;
  int           charisma;
  int           maxHP;
  int           hp;
  int           maxStamina;
  int           stamina;
  int           armourDefence;
  bool          attacking;
  int           baseDamage;
  bool          alive;
  double        attackCooldownTimer;
  int           mitigation;
  int           attackRating;
  int           defenceRating;
  int           criticalChance;
  bool          inCombat;
  bool          invisible;
  int           staminaRegen;
  int           staminaRegenTimer;
  vector<AbstractEffect*>* activeEffects;

  Ogre::Vector3 velocity;
  Ogre::Vector3 speed;
  SkillSet*     skillClass;

  // Animation states for the dynamic entities.
  Ogre::AnimationState* runAnimation;
  Ogre::AnimationState* dieAnimation;
  Ogre::AnimationState* attackAnimation;
  Ogre::AnimationState* spellAnimation;

public:
  // Constructors and Destructors.
  DynamicEntity();
  DynamicEntity(const DynamicEntity& _other);
  virtual ~DynamicEntity();
  virtual DynamicEntity* clone() const = 0;

  virtual void  init();
  virtual void  update(double _timeSinceLastFrame);
  virtual void  release();
  virtual void  die();
  virtual void  move(double _timeSinceLastFrame);
  virtual void  attack();
  virtual void  takeDamageWithMitigation(int _damage, int _senderID);
  virtual void  takeFullDamage(int _damage, int _senderID);
  void heal(int _damage);
  virtual void  handleMessage(const Message& _message);
  void          updateAnimation(double _timeSinceLastFrame);
  virtual void  updateActiveEffects(double _timeSinceLastFrame);
  void regenStamina(double _timeSinceLastFrame);
  void playAnimation(string _animation);

  // Accessors.
  int           getLevel() const;
  int           getStrength() const;
  int           getDexterity() const;
  int           getAgility() const;
  int           getConstitution() const;
  int           getIntelligence() const;
  int           getWisdom() const;
  int           getCharisma() const;
  int           getMaxHP() const;
  int           getHP() const;
  int           getStamina() const;
  int           getMaxStamina() const;
  int           getArmourDefence() const;
  int           getMitigation() const;
  int           getAttackRating() const;
  int           getDefenceRating() const;
  int           getCriticalChance() const;
  Ogre::Vector3 getVelocity() const;
  Ogre::Vector3 getSpeed() const;
  SkillSet*     getSkillClass() const;
  bool          getAttacking() const;
  int           getBaseDamage() const;
  bool          getAlive() const;
  double        getAttackCooldownTimer() const;
  bool          getInCombat() const; 
  bool          getInvisible() const;
  vector<AbstractEffect*>* getActiveEffects() const;
  static const int WALK_SPEED = 14;
  static const int RUN_SPEED  = 30;
  Ogre::AnimationState* getAttackAnimation() const;
  Ogre::AnimationState* getRunAnimation() const;
  Ogre::AnimationState* getDieAnimation() const;
  Ogre::AnimationState* getSpellAnimation() const;

  // Mutators.
  void          setLevel(int _level);
  void          setStrength(int _strength);
  void          setDexterity(int _dexterity);
  void          setAgility(int _agility);
  void          setConstitution(int _constitution);
  void          setIntelligence(int _intelligence);
  void          setWisdom(int _wisdom);
  void          setCharisma(int _charisma);
  void          setHP(int _hp);
  void          setMaxHP(int _hp);
  void          setStamina(int _stamina);
  void          setMaxStamina(int _stamina);
  void          setArmourDefence(int _defence);
  void          setDefenceRating(int _defenceRating);
  void          setCriticalChance(int _chance);
  void          setVelocity(Ogre::Vector3 _velocity);
  void          setSpeed(Ogre::Vector3 _speed);
  void          setSkillClass(SkillSet* _skillClass);
  void          setAttackAnimation(Ogre::AnimationState* _anim);
  void          setRunAnimation(Ogre::AnimationState* _anim);
  void          setDieAnimation(Ogre::AnimationState* _anim);
  void          setSpellAnimation(Ogre::AnimationState* _state);
  void          setAttacking(bool _attacking);
  void          setBaseDamage(int _damage);
  void          setAlive(bool _alive);
  void          setAttackCooldownTimer(double _timer);
  void          setInCombat(bool _inCombat);
  void          setInvisible(bool _invisible);
  virtual void  loadEntity(Ogre::Entity* _entity);
  void          addActiveEffect(AbstractEffect* _effect);

  // Formula Functions.
  int workOutHP();
  int workOutStamina();
  int workOutBaseDamage();
  int workOutMitigation();
  int workOutAttackRating();
  int workOutDefenceRating();
  int workOutStaminaRegen();
  bool rollForHit(const DynamicEntity& _other, bool& _criticalResult);

}; // end class DynamicEntity

#endif