#include "stdafx.h"
#include "DynamicEntity.h"
#include "../Player/Player.h"
#include "../../StaticEntities/DroppedObject/DroppedObject.h"
#include "../../../Managers/StaticEntityManager/StaticEntityManager.h"
#include "../../../Managers/CollisionManager/QueryFlags.h"
#include "../../../Effects/AbstractEffect/AbstractEffect.h"
#include "../../../Effects/DamageShieldEffect/DamageShieldEffect.h"
#include "../../../Effects/DefenceIncreaseEffect/DefenceIncreaseEffect.h"
#include "../../../Managers/GUIManager/GUIManager.h"
#include "../../../Messaging/MessageType/MessageType.h"
#include "../../../Managers/DamageTextManager/DamageTextManager.h"
#include "../../../Managers/AudioManager/AudioManager.h"

const int DAMAGE_SHIELD_SENDER = -1;

DynamicEntity::DynamicEntity() : AbstractEntity()
{
  this->agility       = 0;
  this->charisma      = 0;
  this->constitution  = 0;
  this->dexterity     = 0;
  this->hp            = 0;
  this->intelligence  = 0;
  this->level         = 0;
  this->stamina       = 0;
  this->strength      = 0;
  this->velocity      = Ogre::Vector3(0.0,0.0,0.0);
  this->speed         = Ogre::Vector3(0.2,0.0,0.2);
  this->wisdom        = 0;
  this->armourDefence = 0;
  this->mitigation    = 0;
  this->attackRating  = 0;
  this->defenceRating = 0;
  this->skillClass    = NULL;
  this->attackAnimation = NULL;
  this->runAnimation    = NULL;
  this->dieAnimation    = NULL;
  this->spellAnimation  = NULL;
  this->attacking       = false;
  this->alive           = false;
  this->criticalChance  = 2;
  this->activeEffects   = new vector<AbstractEffect*>;
  this->inCombat = false;
  this->invisible = false;
  this->staminaRegenTimer = 0;
} // DynamicEntity::DynamicEntity()


DynamicEntity::DynamicEntity(const DynamicEntity& _other) : AbstractEntity(_other)
{
  this->agility       = _other.agility;
  this->charisma      = _other.charisma;
  this->constitution  = _other.constitution;
  this->dexterity     = _other.dexterity;
  this->hp            = _other.hp;
  this->maxHP         = _other.maxHP;
  this->intelligence  = _other.intelligence;
  this->level         = _other.level;
  this->stamina       = _other.stamina;
  this->strength      = _other.strength;
  this->wisdom        = _other.wisdom;
  this->armourDefence = _other.armourDefence;
  this->mitigation    = _other.mitigation;
  this->defenceRating = _other.defenceRating;
  this->attackRating  = _other.attackRating;
  this->staminaRegenTimer = 0;
  this->attacking     = false;
  this->setModelName(_other.getModelName());
  this->setName(_other.getName());
  this->activeEffects = new vector<AbstractEffect*>;
  this->inCombat = false;
  this->invisible = false;
  this->setNode(NULL);
  this->loadEntity(NULL);

  if(_other.skillClass)
  {
    this->skillClass = _other.skillClass->clone();
  } // end if
  else
  {
    this->skillClass = NULL;
  } // end else

  this->attackAnimation = _other.attackAnimation;
  this->runAnimation    = _other.runAnimation;
  this->dieAnimation    = _other.dieAnimation;
  this->spellAnimation  = _other.spellAnimation;

  this->setPosition(Ogre::Vector3(0,0,0));
  this->velocity = Ogre::Vector3(0.0,0.0,0.0);
  this->speed = Ogre::Vector3(20.0,0.0,20.0);

} // DynamicEntity::DynamicEntity(const DynamicEntity& _other)


DynamicEntity::~DynamicEntity()
{

  // Dynamic Entity Destructor.
  if(this->skillClass)
  {
    delete skillClass;
    skillClass = NULL;
  } // end if

  this->setEntity(NULL);
  this->setNode(NULL);
  this->setCurrentAnimationState(NULL);
  this->setIdleAnimation(NULL);
  this->attackAnimation = NULL;
  this->runAnimation = NULL;
  this->dieAnimation = NULL;
  this->spellAnimation = NULL;

  if(this->activeEffects)
  {
    delete this->activeEffects;
  } // end if
} // DynamicEntity::~DynamicEntity()


void DynamicEntity::init()
{
  this->alive = true;
} // DynamicEntity::init()


void DynamicEntity::update(double _timeSinceLastFrame)
{

} // DynamicEntity::update(double _timer)


void DynamicEntity::release()
{

} // DynamicEntity::release()


void DynamicEntity::move(double _timeSinceLastFrame)
{
  if(!this->attacking)
  {
    this->setPosition(this->getPosition() + this->velocity*_timeSinceLastFrame/1000);

    if(this->getVelocity().x < 0 || this->getVelocity().x > 0 || this->getVelocity().z < 0 || this->getVelocity().z > 0)
    {
      int orientationDegrees = this->findAngleBetweenTwoPointsXZ(this->getPosition(), this->getPosition()+this->getVelocity());
      this->setOrientation(Ogre::Vector3(this->getOrientation().x, orientationDegrees, this->getOrientation().z));
      CollisionManager::getSingleton().setHeightAdjust(this->getDimensions().y/2);
      CollisionManager::getSingletonPtr()->calculateY(this->getNode(),false,false,1.0f,STATIC_FLOOR);
      this->setPosition(this->getNode()->getPosition());

      // Determine the animation state of the entity and set it to run if currently not.
      if(this->getCurrentAnimationState() != this->getRunAnimation())
      {
        this->setCurrentAnimationState(this->getRunAnimation());
        this->setAnimationReducer(1);
        this->getCurrentAnimationState()->setEnabled(true);
        this->getCurrentAnimationState()->setLoop(true);
      } // end if

    } // end if
    else
    {
      if(this->getCurrentAnimationState() == this->getRunAnimation())
      {
        this->setCurrentAnimationState(this->getIdleAnimation());
        this->setAnimationReducer(5);
        this->getCurrentAnimationState()->setEnabled(true);
        this->getCurrentAnimationState()->setLoop(true);
      } // end if

    } // end else
  } // end if(!this->attacking()

} // DynamicEntity::move()


void DynamicEntity::attack()
{

} // DynamicEntity::attack()


void DynamicEntity::takeDamageWithMitigation(int _damage, int _senderID)
{
  if(this->alive)
  {
    int afterMitigation;
    if(this->mitigation >= _damage)
    {
      afterMitigation = 1;
    } // end if
    else
    {
      afterMitigation = _damage-this->mitigation;
    } // end else
    
    this->takeFullDamage(afterMitigation, _senderID);

  } // end if
} // DynamicEntity::takeDamageWithMitigation(int _damage)


void DynamicEntity::takeFullDamage(int _damage, int _senderID)
{
  if(this->alive)
  {
    ParticleManager::getSingleton().attachParticleToEntity(this, "damage");
    
    DamageTextManager::getSingleton().createDamageText(this, _damage);

    

    this->setHP(this->getHP()-_damage);

    if((DynamicEntity*)Player::getInstance()->getTargettedEnemy() == this)
    {
      GUIManager::getSingleton().updateTargettedEnemyHealth(((double)this->getHP()/(double)this->getMaxHP()));
    } // end if
    
    // -1 == DamageShield
    if(_senderID != -1)
    {
      // Add the sender to the players target.
      if(this == Player::getInstance())
      {
        // Add this to the players targetted mob if the player does not have one.
        if(!Player::getInstance()->getTargettedEnemy())
        {
          Player::getInstance()->setTargettedEnemy((Mob*)MessageDispatcher::getInstance()->findEntityByID(_senderID));
        } // end if
      } // end if

      // Check to see if the current entity has a damage shield.
      vector<AbstractEffect*>* effects = this->getActiveEffects();
      for(vector<AbstractEffect*>::iterator it = effects->begin(); it != effects->end(); ++it)
      {
        AbstractEffect* effect = *it;
        if(effect->getType() == "damageShield")
        { // A damage shield is present.
          DamageShieldEffect* dsEffect = (DamageShieldEffect*)effect;
          static int damage = 0;
          damage = dsEffect->getDamageAmount();
          bool ingoresMitigation = dsEffect->getIgnoresMitigation();

          // Use a tenary operator to determine the message type.
          MessageType msg;
          msg = ingoresMitigation ? MessageTypeDamagedNoMitigation : MessageTypeDamagedNormal;

          // Send a message.
          MessageDispatcher::getInstance()->dispatchMsg(0,DAMAGE_SHIELD_SENDER,_senderID, msg, &damage);

          break;
        } // end if

      } // end for


    } // end if
    
    if(this->getHP() <=0)
    {
      this->die();
    } // end if
    else
    {
      if(this == Player::getInstance())
      {
        AudioManager::getSingleton().playSound(Player::getInstance()->getGender() + " Damage");
      } // end if
      else
      {
        AudioManager::getSingleton().playSound(this->getName() + " Damage");
      } // end else
    } // end else 
  } // end if this->alive;
} // void  DynamicEntity::takeFullDamage(int _damage)


void DynamicEntity::heal(int _health)
{
  if(this->alive)
  {
    int oldHP = this->hp;
    this->setHP(this->getHP()+_health);
    if(this->hp > this->maxHP)
    {
      this->setHP(this->maxHP);
    } // end if

    if(oldHP < this->hp)
    {
      DamageTextManager::getSingleton().createHealText(this, this->hp-oldHP);
    } // end if
  } // end if(this->alive)
} // void DynamicEntity::heal(int _damage)


void DynamicEntity::die()
{
  // Play the death animation and set the entity to dead.
  this->setCurrentAnimationState(this->getDieAnimation());
  this->getCurrentAnimationState()->setLoop(false);
  this->getCurrentAnimationState()->setEnabled(true);
  this->alive = false;

  // Play this entities die sound.
  AudioManager::getSingleton().playSound(this->getName() + " Death");

  // Send a message to the player so that they may gain some experience.
  MessageDispatcher::getInstance()->dispatchMsg(0.0,this->getEntityIDNumber(), Player::getInstance()->getEntityIDNumber(),
    MessageTypeExperienceGained, this);

  // Create a drop near the enemy.
  DropTable* dropTable = ItemManager::getInstance()->getDropTable(this->getName());
  if(dropTable)
  {
    StaticEntityManager::getInstance()->createDroppedObject(dropTable, 
      GameManager::getInstance()->getCurrentScene()->getSceneManager(), this);
  } // end if

  if((DynamicEntity*)Player::getInstance()->getTargettedEnemy() == this)
  {
    Player::getInstance()->setTargettedEnemy(NULL);
  } // end if

  Player::getInstance()->setInCombat(false);

} // void DynamicEntity::die()


int DynamicEntity::getLevel() const
{
  return this->level;
} // DynamicEntity::getLevel()


int DynamicEntity::getStrength() const
{
  return this->strength;
} // DynamicEntity::getStrength()


int DynamicEntity::getDexterity() const
{
  return this->dexterity;
} // DynamicEntity::getDexterity()


int DynamicEntity::getAgility() const
{
  return this->agility;
} // DynamicEntity::getAgility() const


int DynamicEntity::getConstitution() const
{
  return this->constitution;
} // DynamicEntity::getConstitution() const


int DynamicEntity::getIntelligence() const
{
  return this->intelligence;
} // DynamicEntity::getIntelligence() const


int DynamicEntity::getWisdom() const
{
  return this->wisdom;
} // DynamicEntity::getWisdom() const


int DynamicEntity::getCharisma() const
{
  return this->charisma;
} // int DynamicEntity::getCharisma() const


int DynamicEntity::getHP() const
{
  return this->hp;
} // DynamicEntity::getHP() const


int DynamicEntity::getMaxHP() const
{
  return this->maxHP;
} // int DynamicEntity::getMaxHP() const


int DynamicEntity::getStamina() const
{
  return this->stamina;
} // DynamicEntity::getStamina() const


int DynamicEntity::getMaxStamina() const
{
  return this->maxStamina;
} // int DynamicEntity::getMaxStamina() const


int DynamicEntity::getArmourDefence() const
{
  return this->armourDefence;
} // int DynamicEntity::getArmourDefence() const


int DynamicEntity::getMitigation() const
{
  return this->mitigation;
} // int DynamicEntity::getMitigation() const


int DynamicEntity::getAttackRating() const
{
  return this->attackRating;
} // int DynamicEntity::getAttackRating() const


int DynamicEntity::getDefenceRating() const
{
  return this->defenceRating;
} // int DynamicEntity::getDefenceRating() const


int DynamicEntity::getCriticalChance() const
{
  return this->criticalChance;
} // int DynamicEntity::getCriticalChance() const


bool DynamicEntity::getAlive() const
{
  return this->alive;
} // bool DynamicEntity::getAlive() const


Ogre::Vector3 DynamicEntity::getVelocity() const
{
  return this->velocity;
} // DynamicEntity::getVelocity() const


Ogre::Vector3 DynamicEntity::getSpeed() const
{
  return this->speed;
} // Ogre::Vector3 DynamicEntity::getSpeed()


SkillSet* DynamicEntity::getSkillClass() const
{
  return this->skillClass;
} // DynamicEntity::getSkillClass()


int DynamicEntity::getBaseDamage() const
{
  return this->baseDamage;
} // int DynamicEntity::getBaseDamage() const


double DynamicEntity::getAttackCooldownTimer() const
{
  return this->attackCooldownTimer;
} // double DynamicEntity::getCooldownTimer() const


bool DynamicEntity::getInCombat() const
{
  return this->inCombat;
} // bool DynamicEntity::getInCombat() const


bool DynamicEntity::getInvisible() const
{
  return this->invisible;
} // bool DynamicEntity::getInvisible() const


Ogre::AnimationState* DynamicEntity::getAttackAnimation() const
{
  return this->attackAnimation;
} // Ogre::AnimationState* DynamicEntity::getAttackAnimation() const


Ogre::AnimationState* DynamicEntity::getRunAnimation() const
{
  return this->runAnimation;
} // Ogre::AnimationState* DynamicEntity::getRunAnimation() const


Ogre::AnimationState* DynamicEntity::getDieAnimation() const
{
  return this->dieAnimation;
} // Ogre::AnimationState* DynamicEntity::getDieAnimation() const


Ogre::AnimationState* DynamicEntity::getSpellAnimation() const
{
  return this->spellAnimation;
} // Ogre::AnimationState* DynamicEntity::getSpellAnimation() const


bool DynamicEntity::getAttacking() const
{
  return this->attacking;
} // bool DynamicEntity::getAttacking() const


void DynamicEntity::setLevel(int _level)
{
  this->level = _level;
} // DynamicEntity::setLevel(int _level)


void DynamicEntity::setStrength(int _strength)
{
  this->strength = _strength;
} // DynamicEntity::setStrength(int _strength)


void DynamicEntity::setDexterity(int _dexterity)
{
  this->dexterity = _dexterity;
} // DynamicEntity::setDexterity(int _dexterity)


void DynamicEntity::setAgility(int _agility)
{
  this->agility = _agility;
} // DynamicEntity::setAgility(int _agility)


void DynamicEntity::setConstitution(int _constitution)
{
  this->constitution = _constitution;
} // DynamicEntity::setConstitution(int _constitution)


void DynamicEntity::setIntelligence(int _intelligence)
{
  this->intelligence = _intelligence;
} // DynamicEntity::setIntelligence(int _intelligence)


void DynamicEntity::setWisdom(int _wisdom)
{
  this->wisdom = _wisdom;
} // DynamicEntity::setWisdom(int _wisdom)


void DynamicEntity::setCharisma(int _charisma)
{
  this->charisma = _charisma;
} // void DynamicEntity::setCharisma(int _charisma)


void DynamicEntity::setHP(int _hp)
{
  this->hp = _hp;
  if(this == Player::getInstance())
  {
    GUIManager::getSingleton().updateHealthDisplay();
  } // end if
} // DynamicEntity::setHP(int _hp)


void DynamicEntity::setMaxHP(int _hp)
{
  this->maxHP = _hp;
  if(this == Player::getInstance())
  {
    GUIManager::getSingleton().updateHealthDisplay();
  } // end if
} // void DynamicEntity::setMaxHP(int _hp)


void DynamicEntity::setStamina(int _stamina)
{
  this->stamina = _stamina;
  if(this == Player::getInstance())
  {
    GUIManager::getSingleton().updateStaminaDisplay();
  } // end if
} // DynamicEntity::setStamina(int _stamina)


void DynamicEntity::setMaxStamina(int _stamina)
{
  this->maxStamina = _stamina;
  if(this == Player::getInstance())
  {
    GUIManager::getSingleton().updateStaminaDisplay();
  } // end if
} // void DynamicEntity::setMacStamina(int _stamina)


void DynamicEntity::setArmourDefence(int _defence)
{
  this->armourDefence = _defence;
} // void DynamicEntity::setArmourDefence(int _defence)


void DynamicEntity::setDefenceRating(int _defenceRating)
{
  this->defenceRating = _defenceRating;
} // void DynamicEntity::setDefenceRating(int _defenceRating)


void DynamicEntity::setCriticalChance(int _chance)
{
  this->criticalChance = _chance;
} // void DynamicEntity::setCriticalRating(int _rating)


void DynamicEntity::setVelocity(Ogre::Vector3 _velocity)
{
  this->velocity = _velocity;
} // DynamicEntity::setVelocity(Vector3D _velocity)


void DynamicEntity::setSpeed(Ogre::Vector3 _speed)
{
  this->speed = _speed;
} // void DynamicEntity::setSpeed(Ogre::Vector3 _velocity)


void DynamicEntity::setSkillClass(SkillSet* _skillClass) 
{
  this->skillClass = _skillClass;
} // DynamicEntity::setSkillClass(SkillSet _skillClass)


void DynamicEntity::setAttackAnimation(Ogre::AnimationState* _anim)
{
  this->attackAnimation = _anim;
} // void DynamicEntity::setAttackAnimation(Ogre::AnimationState* _anim


void DynamicEntity::setRunAnimation(Ogre::AnimationState* _anim)
{
  this->runAnimation = _anim;
} // void DynamicEntity::setRunAnimation(Ogre::AnimationState* _anim)


void DynamicEntity::setDieAnimation(Ogre::AnimationState* _anim)
{
  this->dieAnimation = _anim;
} // void DynamicEntity::setDieAnimation(Ogre::AnimationState* _anim)


void DynamicEntity::setSpellAnimation(Ogre::AnimationState* _anim)
{
  this->spellAnimation = _anim;
} // void DynamicEntity::setSpellAnimation(Ogre::AnimationState* _anim)


void DynamicEntity::setAttacking(bool _attacking)
{
  this->attacking = _attacking;
} // void DynamicEntity::setAttacking(bool _attacking)


void DynamicEntity::setBaseDamage(int _damage)
{
  this->baseDamage = _damage;
} // void DynamicEntity::setBaseDamage(int _damage)


void DynamicEntity::setAlive(bool _alive)
{
  this->alive = _alive;
} // void DynamicEntity::setAlive(bool _alive)


void DynamicEntity::setAttackCooldownTimer(double _timer)
{
  this->attackCooldownTimer = _timer;
} // void DynamicEntity::setCooldownTimer(double _timer)


void DynamicEntity::setInCombat(bool _inCombat)
{
  this->inCombat = _inCombat;
} // void DynamicEntity::setInCombat(bool _inCombat)


void DynamicEntity::setInvisible(bool _invisible)
{
  this->invisible = _invisible;
} // void DynamicEntity::setInvisible(bool _invisible)


void DynamicEntity::loadEntity(Ogre::Entity* _entity)
{
  this->setEntity(_entity);
  if(!_entity)
  {
    return;
  } // end if
  this->setIdleAnimation(_entity->getAnimationState("Idle"));
  this->runAnimation = _entity->getAnimationState("Run");
  this->dieAnimation = _entity->getAnimationState("Die");
  
  Ogre::AnimationStateSet* animationState = _entity->getAllAnimationStates();
  
  if(animationState->hasAnimationState("Spell"))
  {
    this->spellAnimation = _entity->getAnimationState("Spell");
  } // end if
  if(animationState->hasAnimationState("Attack"))
  {
    this->attackAnimation = _entity->getAnimationState("Attack");
  } // end if

} // void DynamicEntity::setEntity(Ogre::Entity* _entity)


void DynamicEntity::addActiveEffect(AbstractEffect* _effect)
{
  this->activeEffects->push_back(_effect);
  if(this == Player::getInstance())
  {
    GUIManager::getSingleton().addActiveEffect(_effect);
  } // end if
} // void DynamicEntity::addActiveEffect(AbstractEffect* _effect)


vector<AbstractEffect*>* DynamicEntity::getActiveEffects() const
{
  return this->activeEffects;
} // vector<AbstractEffects*>* DynamicEntity::getActiveEffects() const


int DynamicEntity::workOutHP()
{
  return ((this->level * 70) + (2 * this->constitution * this->level));
} // void DynamicEntity::workOutHP()


int DynamicEntity::workOutStamina()
{
  return ((this->level * 100) + (3 * this->wisdom * this->level));
} // void DynamicEntity::workOutStamina();


int DynamicEntity::workOutBaseDamage()
{
  int x = this->strength;
  return baseDamage = (this->level * x - 1) * 2;
} // int DynamicEntity::workOutBaseDamage()


int DynamicEntity::workOutMitigation()
{
  this->mitigation = this->constitution * this->level + this->armourDefence;
  return this->mitigation;
} // int DynamicEntity::workOutMitigation()


int DynamicEntity::workOutDefenceRating()
{
  this->defenceRating = ((double)this->agility + ((double)(this->agility/(double)10) * (double)(this->level*this->level))) * 40.0;

  // Check for an active effect which will increase the defence rating.
  for(vector<AbstractEffect*>::iterator it = this->activeEffects->begin(); it != this->activeEffects->end(); ++it)
  {
    AbstractEffect* effect = *it;
    if(effect->getType() == "defenceIncrease")
    {
      this->defenceRating += static_cast<DefenceIncreaseEffect*>(effect)->getDefenceAmount();
    } // end if
  } // end for
  return this->defenceRating;
} // int DynamicEntity::workOutDefenceRating()


int DynamicEntity::workOutAttackRating()
{
  // Paranoid double casting.
  this->attackRating = ((double)this->dexterity + 
    (((double)this->dexterity/10) * ((double)this->level*(double)this->level))) * (double)100.0;
  return this->attackRating;
} // int DynamicEntity::workOutAttackRating()


int DynamicEntity::workOutStaminaRegen()
{
  this->staminaRegen = ((double)6000*(double)(level+level/2)) - ((this->level*2)*(112 * this->intelligence));
  if(this->staminaRegen < 50)
  {
    this->staminaRegen = 50;
  } // end if
  return this->staminaRegen;
} // int DynamicEntity::workOutStaminaRegen()


bool DynamicEntity::rollForHit(const DynamicEntity& _other, bool& _criticalResult)
{
  // Roll based between 0 and the AR.
  int roll = rand()%this->attackRating;

  // If the AR is greater than the enemies DR then the player has hit.
  if(roll > _other.defenceRating)
  {
    // Now determine if the hit was in the top 2% of the AR.
    _criticalResult = (roll*100 > (this->attackRating*(100-this->criticalChance)));

    return true;
  } // end if
  else
  {
    return false;
  } // end else

} // bool DynamicEntity::rollForHit(const DynamicEntity& _other)


void DynamicEntity::handleMessage(const Message& _message)
{
  if(_message.msg == MessageTypeDamagedNormal)
  {
    int damage = *(int*)_message.extraInfo;
    this->takeDamageWithMitigation(damage, _message.senderID);
  } // end if
  if(_message.msg == MessageTypeDamagedNoMitigation)
  {
    int damage = *(int*)_message.extraInfo;
    this->takeFullDamage(damage, _message.senderID);
  } // end if
} // void handleMessage(const Message& _message)


void DynamicEntity::updateAnimation(double _timeSinceLastFrame)
{
  if(this->getCurrentAnimationState())
  {
    if(this->getCurrentAnimationState()->getEnabled())
    {
      this->getCurrentAnimationState()->addTime((Ogre::Real)(_timeSinceLastFrame/1000/this->getAnimationReducer()));
      if(!this->getCurrentAnimationState()->getLoop())
      {
        if(this->getCurrentAnimationState()->getTimePosition() >= this->getCurrentAnimationState()->getLength() && this->getAlive())
        {
          this->setCurrentAnimationState(this->getIdleAnimation());
          this->getCurrentAnimationState()->setLoop(true);
          this->getCurrentAnimationState()->setEnabled(true);
        } // end if
      } // end if
    } // end if
  } // end if
} // void DynamicEntity::updateAnimation(double _timeSinceLastFrame)


void DynamicEntity::updateActiveEffects(double _timeSinceLastFrame)
{
  vector<int> erasePos;

  int i=0;
  for(vector<AbstractEffect*>::iterator it = this->activeEffects->begin(); it != this->activeEffects->end(); ++it)
  {
    AbstractEffect* effect = *it;

    if(effect->updateEffect(_timeSinceLastFrame, this))
    {
      if(this == Player::getInstance())
      {
        GUIManager::getSingleton().removeActiveEffect(effect);
      } // end if

      delete effect;
      erasePos.push_back(i);
    } // end if
    ++i;
  } // end for

  for(vector<int>::reverse_iterator it = erasePos.rbegin(); it != erasePos.rend(); ++it)
  {
    int pos = *it;

    this->activeEffects->erase(this->activeEffects->begin()+pos);

  } // end for
} // void DynamicEntity::updateActiveEffects(double _timeSinceLastFrame)


void DynamicEntity::regenStamina(double _timeSinceLastFrame)
{
  if(this->stamina < this->maxStamina)
  {
    this->staminaRegenTimer += _timeSinceLastFrame;
    while(this->staminaRegenTimer > this->staminaRegen)
    {
      this->setStamina(this->getStamina()+1);
      if(this->stamina > this->maxStamina)
      {
        this->setStamina(this->maxStamina);
      } // end if
      this->staminaRegenTimer -= this->staminaRegen;
    } // end while
  } // end if
} // void DynamicEntity::regenStamina(double _timeSinceLastFrame)


void DynamicEntity::playAnimation(string _animation)
{
  if(_animation == "attack")
  {
    this->setCurrentAnimationState(this->getAttackAnimation());
    this->setAttacking(true);
    this->getCurrentAnimationState()->setEnabled(true);
    this->getCurrentAnimationState()->setLoop(false);
  } // end if
  else if(_animation == "spell")
  {
    this->setCurrentAnimationState(this->getSpellAnimation());
    this->getCurrentAnimationState()->setEnabled(true);
    this->getCurrentAnimationState()->setLoop(false);
  } // end if
  else if(_animation == "die")
  {
    this->setCurrentAnimationState(this->getDieAnimation());
    this->getCurrentAnimationState()->setEnabled(true);
    this->getCurrentAnimationState()->setLoop(false);
  } // end if
  else if(_animation == "idle")
  {
    this->setCurrentAnimationState(this->getIdleAnimation());
    this->getCurrentAnimationState()->setEnabled(true);
    this->getCurrentAnimationState()->setLoop(true);
  } // end if
  else if(_animation == "run")
  {
    this->setCurrentAnimationState(this->getRunAnimation());
    this->getCurrentAnimationState()->setEnabled(true);
    this->getCurrentAnimationState()->setLoop(true);
  } // end else if
} // void DynamicEntity::playAnimation(string _animation)
 