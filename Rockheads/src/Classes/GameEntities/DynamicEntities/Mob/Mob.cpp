#include "stdafx.h"
#include "Mob.h"
#include "../../../AI/States/Patrol/Patrol.h"
#include "../../../AI/States/Engage/Engage.h"
#include "../../../AI/States/Attack/Attack.h"
#include "../../../Managers/CollisionManager/QueryFlags.h"
#include "../../../Effects/AbstractEffect/AbstractEffect.h"
#include "../../../GameScenes/AbstractScene.h"
#include "../../../Managers/DamageTextManager/DamageTextManager.h"
#include "../../../Managers/AudioManager/AudioManager.h"


Mob::Mob() : DynamicEntity()
{
  this->stateMachine = new StateMachine<Mob>(this);
  this->behaviours = new BehaviourManager;
  this->aggressive = false;
  this->attackSpeed = 3.0;
} // Mob::Mob()


Mob::Mob(const Mob& _other) : DynamicEntity(_other)
{
  this->weaponMin = _other.weaponMin;
  this->weaponMax = _other.weaponMax;
  this->stateMachine = new StateMachine<Mob>(this);
  this->behaviours = new BehaviourManager;
  this->aggressive = _other.aggressive;
  this->attackSpeed = _other.attackSpeed;
} // Mob::Mob(const Mob& _other)


Mob::~Mob()
{
  this->release();
} // Mob::~Mob()


Mob& Mob::operator=(const Mob& _other)
{
  this->setEntityIDNumber(MessageDispatcher::getInstance()->assignIDNumber(this));

  // Mob attributes.
  this->weaponMin = _other.weaponMin;
  this->weaponMax = _other.weaponMax;
  this->aggressive = _other.aggressive;
  this->attackSpeed = _other.attackSpeed;

  if(!this->behaviours)
  {
    this->behaviours = new BehaviourManager;
  } // end if
  if(!this->stateMachine)
  {
    this->stateMachine = new StateMachine<Mob>(this);
  } // end if

  // Dynamic Entity attributes.
  this->setStamina(_other.getStamina());
  this->setCharisma(_other.getCharisma());
  this->setConstitution(_other.getConstitution());
  this->setDexterity(_other.getDexterity());
  this->setHP(_other.getHP());
  this->setMaxHP(_other.getMaxHP());
  this->setIntelligence(_other.getIntelligence());
  this->setLevel(_other.getLevel());
  this->setModelName(_other.getModelName());
  this->setName(_other.getName());
  this->setStamina(_other.getStamina());
  this->setStrength(_other.getStrength());
  this->setWisdom(_other.getWisdom());
  this->setVelocity(Ogre::Vector3(0.0,0.0,0.0));
  this->setSpeed(Ogre::Vector3(0.2,0.0,0.2));


  if(_other.getSkillClass())
  {
    SkillSet* skillSet = _other.getSkillClass()->clone();
    this->setSkillClass(skillSet);
  } // end if
  else
  {
    this->setSkillClass(NULL);
  } // end else

  // Abstract class data attributes.
  this->setPosition(Ogre::Vector3(0,0,0));
  this->setModelName(_other.getModelName());
  this->setName(_other.getName());
  this->setNode(NULL);
  this->setEntity(NULL);
  this->setOrientation(_other.getOrientation());

  return *this;
} // Mob& Mob::operator=(const Mob& _other)


Mob* Mob::clone() const
{
  return new Mob(*this);
} // Mob* Mob::clone() const


void Mob::init()
{ 
  this->setHP(this->getMaxHP());
  this->workOutBaseDamage();
  this->workOutAttackRating();
  this->workOutDefenceRating();
  this->workOutMitigation();
  this->setAlive(true);
  this->setActive(true);
  this->behaviours->init(this);
  this->stateMachine->changeState(Patrol::getInstance());
  this->setCurrentAnimationState(this->getIdleAnimation());
  this->getCurrentAnimationState()->setEnabled(true);
  this->getCurrentAnimationState()->setLoop(true);
  this->setAttackCooldownTimer(0);
  this->setCriticalChance(2);
  this->getBehaviours()->setIdleTimer(1.5);
  this->getBehaviours()->setWalkOn(false);

  if(this->getEntity())
  {
    this->getEntity()->setQueryFlags(MOB);
  } // end if

} // Mob::init()


void Mob::update(double _timeSinceLastFrame)
{ 
  vector<AbstractEffect*>* effects = this->getActiveEffects();
  bool stunned = false;
  for(vector<AbstractEffect*>::iterator it = effects->begin(); it != effects->end(); ++it)
  {
    AbstractEffect* effect = *it;
    if(effect->getType() == "stun")
    {
      stunned = true;
      break;
    } // end if
  } // end for
  
  // Update the effects regardless of stun.
  this->updateActiveEffects(_timeSinceLastFrame);

  if(!stunned)
  {
    if(this->getCurrentAnimationState() == this->getIdleAnimation())
    {
      this->setAnimationReducer(5);
    } // end if
    else
    {
      this->setAnimationReducer(1);
    } // end else

    this->updateAnimation(_timeSinceLastFrame);

    if(this->getAlive())
    {
      this->stateMachine->update(_timeSinceLastFrame);
      this->move(_timeSinceLastFrame);
    } // end if

    if(this->getAttacking() && this->getCurrentAnimationState() != this->getAttackAnimation())
    {
      this->setAttacking(false);
    } // end if

    if(this->getAttackCooldownTimer() > 0)
    {
      this->setAttackCooldownTimer(this->getAttackCooldownTimer()-_timeSinceLastFrame/1000);
    } // if(this->getAttackCooldownTimer() > 0)
  } // end if(!stunned)

  if(this == Player::getInstance()->getTargettedEnemy())
  {
    Ogre::SceneNode* targetPlate = (Ogre::SceneNode*)this->getNode()->getChild("TargetPlateNode");
    targetPlate->setPosition(0,-this->getDimensions().y/2+0.1,0);
  } // end if
} // Mob::update(double _timeSinceLastFrame)


void Mob::release()
{
  // Mob attributes.
  if(this->behaviours)
  {
    delete this->behaviours;
    this->behaviours = NULL;
  } // end if
  if(this->stateMachine)
  {
    delete this->stateMachine;
    this->stateMachine = NULL;
  } // end if

  // Dynamic Entity attributes.
  if(this->getSkillClass())
  {
    delete this->getSkillClass();
    this->setSkillClass(NULL);
  } // end if

  // Abstract Entity Destructor
  this->loadEntity(NULL);
  this->setNode(NULL);
  this->setCurrentAnimationState(NULL);
} // Mob::release()


void Mob::attack()
{
  // @Todo, implement individual mob cooldown timers.
  if(this->getAttackCooldownTimer() <= 0)
  {
    AudioManager::getSingleton().playSound(this->getName() + " Attack");
    // Set attacking to be true so the mob can't move.
    this->setAttacking(true);

    // Determine if you have hit the nearest mob or not.
    bool critical = false;
    if(this->rollForHit(*Player::getInstance(), critical))
    {
      // Deal the damage and send a message to the player.
      static int damageDealt = 0;
      damageDealt = this->workOutDamage();

      // Determine if you have criticalled.
      if(critical)
      {
        damageDealt *= 2;
      } // end if

      MessageDispatcher::getInstance()->dispatchMsg(0.4, this->getEntityIDNumber(), 
        Player::getInstance()->getEntityIDNumber(),MessageTypeDamagedNormal, &damageDealt);
    } // end if
    else
    {
      MessageDispatcher::getInstance()->dispatchMsg(0.4, this->getEntityIDNumber(),
        Player::getInstance()->getEntityIDNumber(),MessageTypeAttackMissed, NULL);
    } // end else
    
    // Change to the attack animation.
    this->setCurrentAnimationState(this->getAttackAnimation());
    this->getCurrentAnimationState()->setEnabled(true);
    this->getCurrentAnimationState()->setLoop(false);
    this->setAttackCooldownTimer(this->attackSpeed);
  } // end if
} // void Mob::attack()


BehaviourManager* Mob::getBehaviours() const
{
  return this->behaviours;
} // Mob::getBehaviours()


StateMachine<Mob>* Mob::getStateMachine() const
{
  return this->stateMachine;
} // StateMachine<Mob>* Mob::getStateMachine() const


int Mob::getWeaponMin() const
{
  return this->weaponMin;
} // int Mob::getWeaponMin() const


int Mob::getWeaponMax() const
{
  return this->weaponMax;
} // int Mob::getWeaponMax() const


bool Mob::getAggressive() const
{
  return this->aggressive;
} // bool Mob::getAggressive() const


void Mob::setWeaponMin(int _min)
{
  this->weaponMin = _min;
} // void setWeaponMin(int _min)


void Mob::setWeaponMax(int _max)
{
  this->weaponMax = _max;
} // void Mob::setWeaponMax(int _max)


void Mob::setClassType(string _class)
{
  if(_class == "BB")
  {
    // turn into BB
  } // end if
  if(_class == "SS")
  {
    // turn into SS
  } // end if
  if(_class == "MM")
  {
    // turn into BB
  } // end if
} // void Mob::setClassType(string _class)


void Mob::setBehaviours(BehaviourManager* _manager)
{
  this->behaviours = _manager;
} // void setBehaviours(BehaviourManager* _manager)


void Mob::setStateMachine(StateMachine<Mob>* _machine)
{
  this->stateMachine = _machine;
} // void Mob::setStateMachine(StateMachine<Mob>* _machine) 


void Mob::setAggressive(bool _aggressive)
{
  this->aggressive = _aggressive;
} // void Mob::setAggressive(bool _aggressive)


void Mob::setAttackSpeed(double _speed)
{
  this->attackSpeed = _speed;
} // void Mob::setAttackSpeed(double _speed)


void Mob::handleMessage(const Message& _message)
{
  if(_message.msg == MessageTypeDamagedNormal)
  {
    int damage = *(int*)_message.extraInfo;
    this->takeDamageWithMitigation(damage, _message.senderID);
    if(this->getStateMachine()->getCurrentState() != Engage::getInstance()
      && this->getStateMachine()->getCurrentState() != Attack::getInstance())
    {
      this->getStateMachine()->changeState(Engage::getInstance());
    } // end if
  } // end if
  else if(_message.msg == MessageTypeDamagedNoMitigation)
  {
    int damage = *(int*)_message.extraInfo;
    this->takeFullDamage(damage, _message.senderID);
    if(this->getStateMachine()->getCurrentState() != Engage::getInstance()
      && this->getStateMachine()->getCurrentState() != Attack::getInstance())
    {
      this->getStateMachine()->changeState(Engage::getInstance());
    } // end if
  } // end else if
  else if(_message.msg == MessageTypeEngaged)
  {
    this->getStateMachine()->changeState(Engage::getInstance());
  } // end if
  else if(_message.msg == MessageTypeAttackMissed)
  {
    DamageTextManager::getSingleton().createMissText(this);
    AudioManager::getSingleton().playSound("Missed Attack");
    // Play the Aggro audio.
    this->getStateMachine()->changeState(Engage::getInstance());
  } // end else if
   
} // void handleMessage(const Message& _message)


int Mob::workOutDamage()
{
  int r = rand()%this->weaponMax + weaponMin+1;
  return this->getBaseDamage() + r;
} // int Player::workOutDamage()