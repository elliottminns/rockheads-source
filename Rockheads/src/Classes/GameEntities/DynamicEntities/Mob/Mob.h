#include "../../../../PrecompiledHeaders/stdafx.h"
#ifndef MOB_H
#define MOB_H

#include "../DynamicEntity/DynamicEntity.h"
#include "../../../AI/BehaviourManager/BehaviourManager.h"
#include "../../../AI/StateMachine/StateMachine.h"
#include "../Player/Player.h"
#include "../../../GameObjects/DropTable/DropTable.h"

class Mob : public DynamicEntity
{
public:
  Mob();
  Mob(const Mob& _other);
  virtual ~Mob();
  virtual Mob* clone() const;
  virtual Mob& operator=(const Mob& _other);

  virtual void init();
  virtual void update(double _timeSinceLastFrame);
  virtual void release();
  void attack();
  

  BehaviourManager* getBehaviours() const;
  StateMachine<Mob>* getStateMachine() const; 
  int getWeaponMin() const;
  int getWeaponMax() const;
  bool getAggressive() const;

  void setClassType(string _class);
  void setWeaponMin(int _min);
  void setWeaponMax(int _max);
  void setBehaviours(BehaviourManager* _manager);
  void setStateMachine(StateMachine<Mob>* _machine);
  void setAggressive(bool _aggressive);
  void setAttackSpeed(double _speed);

  void handleMessage(const Message& _message);
  int workOutDamage();

private:
  StateMachine<Mob>*  stateMachine;
  int                 weaponMax;
  int                 weaponMin;
  BehaviourManager*   behaviours;
  Player*             thePlayer;
  bool                aggressive;
  double              attackSpeed;
}; // end class Mob

#endif