#include "stdafx.h"
#include "Boss.h"


Boss::Boss() : Mob()
{
  this->secondClass = new SkillSet;
} // Boss::Boss()


Boss::Boss(const Boss& _other) : Mob(_other)
{
  if(this->secondClass)
  {
    this->secondClass = NULL;
  } // end if
  if(_other.secondClass)
  {
    this->secondClass = _other.secondClass;
  } // end if
  else
  {
    this->secondClass = NULL;
  } // end else
} // Boss:Boss(const Boss& _other)


Boss::~Boss()
{
  // Boss attributes.
  if(this->secondClass)
  {
    delete this->secondClass;
    this->secondClass = NULL;
  } // end if

  // Mob attributes
  if(this->getBehaviours())
  {
    delete this->getBehaviours();
    this->setBehaviours(NULL);
  } // end if
  if(this->getStateMachine())
  {
    delete this->getStateMachine();
    this->setStateMachine(NULL);
  } // end if

  // Dynamic Entity attributes.
  if(this->getSkillClass())
  {
    delete this->getSkillClass();
    this->setSkillClass(NULL);
  } // end if

  // Abstract Entity Destructor
  if(this->getEntity())
  {
    this->setEntity(NULL);
  } // end if
  if(this->getNode())
  {
    this->setNode(NULL);
  } // end if
  if(this->getCurrentAnimationState())
  {
    this->setCurrentAnimationState(NULL);
  } // end if

} // Boss::~Boss()


Boss& Boss::operator=(const Boss& _other)
{
  // Boss attributes.
  this->secondClass = _other.getSkillClass()->clone();
  if(!this->getBehaviours())
  {
    this->setBehaviours(new BehaviourManager);
  } // end if
  if(!this->getStateMachine())
  {
    this->setStateMachine(new StateMachine<Mob>(this));
  } // end if

  // Mob Attributes.
  this->setWeaponMin(_other.getWeaponMin());
  this->setWeaponMax(_other.getWeaponMax());

  // Dynamic Entity Attributes.
  this->setStamina(_other.getStamina());
  this->setCharisma(_other.getCharisma());
  this->setConstitution(_other.getConstitution());
  this->setDexterity(_other.getDexterity());
  this->setHP(_other.getHP());
  this->setMaxHP(_other.getMaxHP());
  this->setIntelligence(_other.getIntelligence());
  this->setLevel(_other.getLevel());
  this->setModelName(_other.getModelName());
  this->setName(_other.getName());
  this->setStamina(_other.getStamina());
  this->setStrength(_other.getStrength());
  this->setWisdom(_other.getWisdom());
  this->setVelocity(Ogre::Vector3(0.0,0.0,0.0));
  this->setSpeed(Ogre::Vector3(0.2,0.0,0.2));

  if(this->getSkillClass())
  {
    delete this->getSkillClass();
    this->setSkillClass(NULL);
  } // end if
  if(_other.getSkillClass())
  {
    SkillSet* skillSet = _other.getSkillClass()->clone();
    this->setSkillClass(skillSet);
  } // end if
  else
  {
    this->setSkillClass(NULL);
  } // end else

  // Abstract Entity Attributes.
  this->setPosition(Ogre::Vector3(0,0,0));
  this->setModelName(_other.getModelName());
  this->setName(_other.getName());
  this->setNode(NULL);
  this->setEntity(NULL);
  this->setOrientation(_other.getOrientation());

  return *this;

} // Boss& Boss::operator=(const Boss& _other)


Boss* Boss::clone() const
{
  return new Boss(*this);
} // Boss::~Boss()


SkillSet* Boss::getSecondClass()
{
  return this->secondClass;
} // Boss::getSecondClass()


void Boss::setSecondClass(SkillSet* _class)
{
  this->secondClass = _class;
} // Boss::setSecondClass(SkillSet* _class)


void Boss::setSecondClass(string _class)
{
  if(_class == "BB")
  {
    // turn into BB
  } // end if
  if(_class == "SS")
  {
    // turn into SS
  } // end if
  if(_class == "MM")
  {
    // turn into BB
  } // end if
} // void Mob::setClassType(string _class)
