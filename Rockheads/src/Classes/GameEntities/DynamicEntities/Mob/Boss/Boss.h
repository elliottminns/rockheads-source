#include "../../../../../PrecompiledHeaders/stdafx.h"
#ifndef BOSS_H
#define BOSS_H

#include "../Mob.h"

class Boss : public Mob
{
public:
  // Constructors and Destructors.
  Boss();
  ~Boss();
  Boss(const Boss& _other);
  Boss& operator=(const Boss& _other);
  Boss* clone() const;

  SkillSet* getSecondClass();
  void      setSecondClass(SkillSet* _class);
  void      setSecondClass(string _class);

private:
  SkillSet* secondClass;
}; // end class Boss

#endif