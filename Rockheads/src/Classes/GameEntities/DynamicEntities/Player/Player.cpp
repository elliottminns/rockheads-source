#include "stdafx.h"
#include "Player.h"
#include "../../../Managers/ItemManager/ItemManager.h"
#include "../../../Managers/StaticEntityManager/StaticEntityManager.h"
#include "../../StaticEntities/DroppedObject/DroppedObject.h"
#include "../../../GameObjects/Items/Armour/Armour.h"
#include "../../../GameObjects/Items/Weapon/Weapon.h"
#include "../../../Managers/CollisionManager/CollisionManager.h"
#include "../../../Managers/CollisionManager/QueryFlags.h"
#include "../../../Managers/MobManager/MobManager.h"
#include "../../../Questing/Quest/Quest.h"
#include "../../../Managers/QuestManager/QuestManager.h"
#include "../../../Managers/GUIManager/GUIManager.h"
#include "../../../Managers/SkillManager/SkillManager.h"
#include "../../../Effects/AbstractEffect/AbstractEffect.h"
#include "../../../Effects/InvisibleEffect/InvisibleEffect.h"
#include "../../../Managers/DamageTextManager/DamageTextManager.h"
#include "../../../GameObjects/Items/Ammo/Ammo.h"
#include "../../../Managers/AudioManager/AudioManager.h"
#include "../../../Managers/GameManager/GameManager.h"
#include "../../../GameScenes/GameScene/GameScene.h"

Player* Player::instance = NULL;


Player* Player::getInstance()
{
    if(instance == NULL)
    {
      instance = new Player();
    } // end i

    return instance;
} // Player::getInstance()


Player::Player() : DynamicEntity()
{
  this->setAttackCooldownTimer(0);
} // Player::Player()


Player::Player(const Player& _other) : DynamicEntity(_other)
{
  this->setAttackCooldownTimer(0);
} // Player::Player(const Player& _other)


void Player::init(int _strength, int _agility, int _dexterity, int _constitution, int _intelligence,
  int _wisdom, int _charisma, Ogre::String _modelName, Ogre::String _charName, Ogre::String _skillSetName, 
  string _gender, Inventory* _inv, int _level, int _experience, int _currency, int _skillPoints)
{
  this->setSpeed(Ogre::Vector3(30.0, 0.0, 30.0));
  this->movingDown    = false;
  this->movingLeft    = false;
  this->movingRight   = false;
  this->movingUp      = false;
  this->setStrength(_strength);
  this->setAgility(_charisma);
  this->setDexterity(_dexterity);
  this->setConstitution(_constitution);
  this->setIntelligence(_intelligence);
  this->setWisdom(_wisdom);
  this->setCharisma(_charisma);
  this->setLevel(_level);
  this->setModelName(_modelName);
  this->setName(_charName);
  this->setVelocity(Ogre::Vector3(0.0,0.0,0.0));
  this->head   = NULL;
  this->chest  = NULL;
  this->legs   = NULL;
  this->feet   = NULL;
  this->weapon = NULL;
  this->experience = _experience;
  this->currency = _currency;
  this->gender = _gender;
  this->targettedEnemy = NULL;
  this->skillPoints = _skillPoints;
  this->setInvisible(false);

  if(!_inv)
  {
    this->inventory = new Inventory(this);
    this->setArmourDefence(0);
    this->inventory->addItemAtEmptySlot(ItemManager::getInstance()->getItem("Spiced Antelope"),5);
    this->inventory->addItemAtEmptySlot(ItemManager::getInstance()->getItem("Green Herb Leaf"), 5);
    this->inventory->addItemAtEmptySlot(ItemManager::getInstance()->getItem("Red Herb Leaf"), 5);
    this->inventory->addItemAtEmptySlot(ItemManager::getInstance()->getItem("Wooden Dagger"),1);
  } // end if
  else
  {
    this->inventory = _inv;
  } // end else
  
  // Check if there is a skillset.
  if(!this->getSkillClass())
  {
    this->setSkillClass(SkillManager::getInstance()->createSkillSet(_skillSetName));
  } // end if

  this->getSkillClass()->initialise();
  
  // Set starting stats.
  this->setMaxHP(this->workOutHP());
  this->setMaxStamina(this->workOutStamina());
  this->workOutBaseDamage();
  this->setActive(true);
  this->setAlive(true);
  this->setHP(this->getMaxHP());
  this->setStamina(this->getMaxStamina());
  this->workOutRequiredExperience();
  this->workOutMitigation();
  this->workOutAttackRating();
  this->workOutDefenceRating();
  this->workOutStaminaRegen();

  this->setAttackCooldownTimer(0);

  this->currentQuest = NULL;
  this->completedQuests = new vector<Quest*>;

} // Player::init()


void Player::update(double _timeSinceLastFrame)
{
  vector<AbstractEffect*>* effects = this->getActiveEffects();
  bool stunned = false;
  for(vector<AbstractEffect*>::iterator it = effects->begin(); it != effects->end(); ++it)
  {
    AbstractEffect* effect = *it;
    if(effect->getType() == "stun")
    {
      stunned = true;
      break;
    } // end if
  } // end for
  
  // Update the effects regardless of stun
  
  if(this->getAlive())
  {
    this->updateActiveEffects(_timeSinceLastFrame);
    this->regenStamina(_timeSinceLastFrame);
  } // end if

  if(!stunned)
  {
    if(this->getAlive())
    {
      if(this->getAttacking() && this->getCurrentAnimationState() != this->getAttackAnimation())
      {
        this->setAttacking(false);
      } // end if

      if(!this->getAttacking())
      {
        this->move(_timeSinceLastFrame);
      } // end if

      // Add to the cooldown.
      double attackCooldown = this->getAttackCooldownTimer();
      if(attackCooldown > 0)
      {
        attackCooldown -= _timeSinceLastFrame/1000;
        this->setAttackCooldownTimer(attackCooldown);
      } // end if

    } // end if(this->getAlive())

    // Compensate for the speedy idle animation.
    if(this->getCurrentAnimationState() == this->getIdleAnimation())
    {
      this->setAnimationReducer(5);
    } // end if
    else
    {
      this->setAnimationReducer(1);
    } // end else

    this->updateAnimation(_timeSinceLastFrame);
  } // end if(!stunned)
  
  if(this->currentQuest)
  {
    this->currentQuest->checkCompletion(_timeSinceLastFrame);
  }  // end if

} // Player::update(double _timer)


void Player::release()
{

  if(this->getSkillClass())
  {
    delete this->getSkillClass();
    this->setSkillClass(NULL);
  } // end if

  this->setEntity(NULL);
  this->setCurrentAnimationState(NULL);
  this->setNode(NULL);
  this->setIdleAnimation(NULL);
  this->setAttackAnimation(NULL);
  this->setRunAnimation(NULL);
  this->setDieAnimation(NULL);
  this->setPreviousAnimationState(NULL);
} // Player::release()


void Player::attack()
{
  AudioManager::getSingleton().stopSound("Foot Step");
  if(this->getAttackCooldownTimer() <= 0 && this->getAlive())
  {
    if(this->getInvisible())
    {
      // Set the remainder timer on the invis effect to 0.
      vector<AbstractEffect*>* effects = this->getActiveEffects();
      for(vector<AbstractEffect*>::iterator it = effects->begin(); it != effects->end(); ++it)
      {
        AbstractEffect* effect = *it;

        if(effect->getType() == "invis")
        {
          static_cast<InvisibleEffect*>(effect)->setTime(0);
        } // end fi
      } // end for
    } // end if(this->getInvisible()


    this->setCurrentAnimationState(this->getAttackAnimation());
    this->getAttackAnimation()->setEnabled(true);
    this->getAttackAnimation()->setLoop(false);
    this->getAttackAnimation()->setTimePosition(0);
    Weapon* theWeapon = (Weapon*)this->weapon;
    // Set the range of the attack.
    int weaponRange = 12;
    double weaponTiming = 0.6;
    int additionalDamage = 0;
    // If the player has a weapon assign this weapons attack range.
    if(theWeapon)
    {
      this->setAttackCooldownTimer(theWeapon->getCooldown());
      weaponRange = theWeapon->getRange();
      weaponTiming = theWeapon->getTiming();

      if(theWeapon->getRequiresAmmo())
      {
        // Check to make sure the player has an ammo type.
        vector<string> ammoNames = theWeapon->getAmmoNames();
        vector<InventorySlot*>* invSlots = this->getInventory()->getItems();

        bool ammoAvailable = false;
        // Loop through the inventory.
        for(vector<InventorySlot*>::iterator it = invSlots->begin(); it != invSlots->end(); ++it)
        {
          InventorySlot* slot = *it;
          AbstractItem* item = slot->getItem();
          if(item)
          {
            for(vector<string>::iterator jt = ammoNames.begin(); jt != ammoNames.end(); ++jt)
            {
              string name = *jt;
              if(name == item->getName())
              {
                ammoAvailable = true;

                // Work out the additional damage from the ammo.
                Ammo* ammo = (Ammo*)item;
                int minDamage = ammo->getMinDamage();
                int maxDamage = ammo->getMaxDamage();
                additionalDamage = rand()%maxDamage + minDamage;

                slot->setCount(slot->getCount()-1);

                if(slot->getCount() <= 0)
                {
                  // set the item to NULL.
                  slot->setItem(NULL);
                  // Remove item from the inventory.
                  Player::getInstance()->removeFromInv(slot, true);
                } // end if
                else
                {
                  GUIManager::getSingletonPtr()->updateItemCount(slot);
                } // end else
                break;
              } // end if(name == item->getName())
            } // end for(vector<string>::iterator jt = ammoNames.begin(); jt != ammoNames.end(); ++jt)
          } // end if(item)
        } // end for(vector<InventorySlot*>::iterator it = invSlots->begin(); it != invSlots->end(); ++it)
   
        if(!ammoAvailable)
        {
          return;
        } // end if
      } // end if

    } // end if
    else
    {
      this->setAttackCooldownTimer(2.0f);
    } // end else

    this->setAttacking(true);

    // Now turn towards the nearest mob.
    Mob* nearestMob = MobManager::getInstance()->getClosestMob(this->getPosition());
    if(nearestMob)
    {
      int orientationDegrees = this->findAngleBetweenTwoPointsXZ(this->getPosition(), nearestMob->getPosition());
      this->setOrientation(Ogre::Vector3(this->getOrientation().x, orientationDegrees, this->getOrientation().z));

      // Check that the mob is in range of the players weapon range.
      if(nearestMob->getPosition().squaredDistance(this->getPosition()) < weaponRange*weaponRange)
      {
        // Set the mob as the target.
        this->setTargettedEnemy(nearestMob);
        this->setInCombat(true);

        // Determine if you have hit the nearest mob or not.
        bool critical = false;
        if(this->rollForHit(*nearestMob, critical))
        {
          static int totalDamage = 0;
          totalDamage = this->workOutDamage();
          // Add the additional Damage (Used with range weapons);
          totalDamage += additionalDamage;
          if(critical)
          {
            totalDamage *= 2;
          } // end if*critical)

          MessageDispatcher::getInstance()->dispatchMsg(weaponTiming,this->getEntityIDNumber(), 
            nearestMob->getEntityIDNumber(), MessageTypeDamagedNormal, &totalDamage);
        } // end if
        else
        {
          MessageDispatcher::getInstance()->dispatchMsg(weaponTiming, this->getEntityIDNumber(),
             nearestMob->getEntityIDNumber(),MessageTypeAttackMissed, NULL);
        } // end else
      } // end if
    } // end if(nearestMob)
  } // end if(this->cooldownTimer <= 0)
} // Player::attack()


void Player::move(double _timeSinceLastFrame)
{
  this->setVelocity(Ogre::Vector3(0,0,0));

  // Moving down only.
  if(this->movingDown && !this->movingUp && !this->movingLeft && !this->movingRight)
  {
    Ogre::Vector3 newVelocity = this->getVelocity();
    newVelocity.z = this->getSpeed().z;
    this->setVelocity(newVelocity);
  } // end if

  // Moving up only.
  if(this->movingUp && !this->movingDown && !this->movingLeft && !this->movingRight)
  {
    Ogre::Vector3 newVelocity = this->getVelocity();
    newVelocity.z = -this->getSpeed().z;
    this->setVelocity(newVelocity);
  } // end if

  // Moving left only.
  if(this->movingLeft && !this->movingUp && !this->movingDown && !this->movingRight)
  {
    Ogre::Vector3 newVelocity = this->getVelocity();
    newVelocity.x = -this->getSpeed().x;
    this->setVelocity(newVelocity);
  } // end if

  // Moving right only.
  if(this->movingRight && !this->movingUp && !this->movingLeft && !this->movingDown)
  {
    Ogre::Vector3 newVelocity = this->getVelocity();
    newVelocity.x = this->getSpeed().x;
    this->setVelocity(newVelocity);
  } // end if

  // Moving North East.
  if(this->movingUp && this->movingRight && !this->movingLeft && !this->movingDown)
  {
    Ogre::Vector3 newVelocity = this->getVelocity();
    newVelocity.x = this->getSpeed().x*0.7;
    newVelocity.z = -this->getSpeed().z*0.7;
    this->setVelocity(newVelocity);
  } // end if

  // Moving North West.
  if(this->movingUp && this->movingLeft && !this->movingRight && !this->movingDown)
  {
    Ogre::Vector3 newVelocity = this->getVelocity();
    newVelocity.x = -this->getSpeed().x*0.7;
    newVelocity.z = -this->getSpeed().z*0.7;
    this->setVelocity(newVelocity);
  } // end if

  // Moving South West
  if(this->movingDown && this->movingLeft && !this->movingRight && !this->movingUp)
  {
    Ogre::Vector3 newVelocity = this->getVelocity();
    newVelocity.x = -this->getSpeed().x*0.7;
    newVelocity.z = this->getSpeed().z*0.7;
    this->setVelocity(newVelocity);
  } // if(this->movingDown && this->movingLeft && !this->movingRight && !this->movingUp)

  // Moving South East
  if(this->movingDown && this->movingRight && !this->movingLeft && !this->movingUp)
  {
    Ogre::Vector3 newVelocity = this->getVelocity();
    newVelocity.x = (Ogre::Real)this->getSpeed().x*0.7;
    newVelocity.z = (Ogre::Real)this->getSpeed().z*0.7;
    this->setVelocity(newVelocity);
  } // if(this->movingDown && this->movingLeft && !this->movingRight && !this->movingUp)

  this->setVelocity(this->getVelocity()*(Ogre::Real)(_timeSinceLastFrame/1000));

  // If any movement is occuring within the player then determine his orientation.
  if(this->movingRight || this->movingUp || this->movingLeft || this->movingDown)
  { // Now work out the orientation.

    AudioManager::getSingleton().playSound("Foot Step");

    int orientationDegrees = this->findAngleBetweenTwoPointsXZ(this->getPosition(), this->getPosition()+this->getVelocity()*100);
    this->setOrientation(Ogre::Vector3(this->getOrientation().x, orientationDegrees, this->getOrientation().z));

    // Get rid of any interact windows.
    GUIManager::getSingleton().destroyNPCWindows();

    // Set the animation states.
    if(this->getCurrentAnimationState() != this->getEntity()->getAnimationState("Run"))
    {
      if(this->getCurrentAnimationState())
      {
        this->getCurrentAnimationState()->setEnabled(false);
      } // end if
    } // end if

    // Apply the moving animation state to the model.
    if(this->getCurrentAnimationState() != this->getEntity()->getAnimationState("Run") 
      || !this->getCurrentAnimationState()->getEnabled())
    {
      this->setCurrentAnimationState(this->getEntity()->getAnimationState("Run"));
      this->getCurrentAnimationState()->setEnabled(true);
      this->getCurrentAnimationState()->setLoop(true);
    } // end if

    // Destroy the shop window.
    GUIManager::getSingletonPtr()->destroyShopWindow();
    
    // Then determine if there is any collision.
    Ogre::Vector3 collisionResult;
    if(CollisionManager::getSingleton().collidesWithEntity(this->getPosition(), 
    this->getPosition()+this->getVelocity(),5.0f, 0.0f, STATIC_WALL))
    {
      this->setVelocity(Ogre::Vector3(0,0,0));
    } // end if
    
    // Do y adjustment.
    this->calculateYPosition();

  } // end if(this->movingRight || this->movingUp || this->movingLeft || this->movingDown)
  else if(this->getCurrentAnimationState() == this->getEntity()->getAnimationState("Run"))
  {
    AudioManager::getSingleton().stopSound("Foot Step");
    this->getCurrentAnimationState()->setEnabled(false);
    this->getCurrentAnimationState()->setLoop(false);
    if(!this->getCurrentAnimationState()->getEnabled() && this->getCurrentAnimationState() != this->getEntity()->getAnimationState("Idle"))
    {
      this->setCurrentAnimationState(this->getEntity()->getAnimationState("Idle"));
      this->getCurrentAnimationState()->setEnabled(true);
      this->getCurrentAnimationState()->setLoop(true);
      this->getCurrentAnimationState()->setWeight(0.8);
    } // end if
  } // end else if
  
  // Set the new position based on the velocity of the player.
  this->setPosition(this->getPosition() + (this->getVelocity()));

} // Player::move()


void Player::removeFromInv(int _slot, bool _delete)
{
  this->inventory->removeItem(_slot, _delete);
} // Player::removeFromInv(int _slot)


void Player::removeFromInv(InventorySlot* _slot, bool _delete)
{
  this->inventory->removeItem(_slot, _delete);
} // void Player::removeFromInv(InventorySlot* _slot)


void Player::addToInv(AbstractItem* _item, int _amount)
{

} // Player::addToInv(Item _item, int _amount)


void Player::levelUp()
{
  if(this->getLevel() < 10)
  {
    AudioManager::getSingleton().playSound("Level Up");
    // Minus the experience required. 
    this->experience -= this->experienceRequired;

    this->setLevel(this->getLevel()+1);
    this->workOutRequiredExperience();
    this->setSkillPoints(this->getSkillPoints()+1);
    if(this->experience >= this->experienceRequired)
    {
      this->levelUp();
    } // end if

    // Create a static text saying the level and class.
    CEGUI::Window* levelAndClass = CEGUI::WindowManager::getSingleton().getWindow("LevelAndClass");
    string levelAndClassText = "Level ";
    levelAndClassText += CEGUI::PropertyHelper::intToString(this->getLevel()).c_str();
    levelAndClassText += " ";
    levelAndClassText += this->getSkillClass()->getName();
    levelAndClass->setText(levelAndClassText.c_str());

    // Update the stats.
    this->setBaseDamage(this->workOutBaseDamage());
    this->setMaxHP(this->workOutHP());
    this->setHP(this->getMaxHP());
    this->workOutStaminaRegen();
    this->setMaxStamina(this->workOutStamina());
    this->setStamina(this->getMaxStamina());
    this->workOutAttackRating();
    this->setDefenceRating(this->workOutDefenceRating());
  } // end if
  
} // Player::levelUp()


void Player::die()
{
  // Play the death animation and set the entity to dead.
  this->setCurrentAnimationState(this->getDieAnimation());
  this->getCurrentAnimationState()->setLoop(false);
  this->getCurrentAnimationState()->setEnabled(true);
  this->setAlive(false);
  AudioManager::getSingleton().playSound("Death");
} // void Player::die()


void Player::loadEntity(Ogre::Entity* _entity)
{
  this->setEntity(_entity);
  if(!_entity)
  {
    return;
  } // end if
  this->setIdleAnimation(_entity->getAnimationState("Idle"));
  this->setRunAnimation(_entity->getAnimationState("Run"));
  this->setDieAnimation(_entity->getAnimationState("Die"));
  
  Ogre::AnimationStateSet* animationState = _entity->getAllAnimationStates();
  
  if(animationState->hasAnimationState("Spell"))
  {
    this->setSpellAnimation(_entity->getAnimationState("Spell"));
  } // end if
  if(animationState->hasAnimationState("OneHandSlash"))
  {
    this->setAttackAnimation(_entity->getAnimationState("OneHandSlash"));
  } // end if

  this->getEntity()->setQueryFlags(PLAYER);

} // void Player::setEntity(Ogre::Entity* _entity)



Inventory* Player::getInventory() const
{
  return this->inventory;
} // Player::getInventory()


AbstractItem* Player::getWeapon() const
{
  return this->weapon;
} // Player::getWeapon()


AbstractItem* Player::getHead() const
{
  return this->head;
} // Player::getHead()


AbstractItem* Player::getChest() const
{
  return this->chest;
} // Player::getChest()


AbstractItem* Player::getLegs() const
{
  return this->legs;
} // Player::getLegs()


AbstractItem* Player::getFeet() const
{
  return this->feet;
} // Player::getFeet()


int Player::getExperience() const
{
  return this->experience;
} // Player::getExp()


int Player::getExperienceRequired() const
{
  return this->experienceRequired;
} // int Player::getExperienceRequired() const


int Player::getCurrency() const
{
  return this->currency;
} // int Player::getCurrency() const


Quest* Player::getCurrentQuest() const
{
  return this->currentQuest;
} // Quest* Player::getQuest() const


vector<Quest*>* Player::getCompletedQuests() const
{
  return this->completedQuests;
} // vector<Quest*> Player::getCompletedQuests() const


string Player::getGender() const
{
  return this->gender;
} // string Player::getGender() const


Mob* Player::getTargettedEnemy() const
{
  return this->targettedEnemy;
} // Mob* Player::getTargettedEnemy() const


int Player::getSkillPoints() const
{
  return this->skillPoints;
} // int Player::getSkillPoints() const 


void Player::setInventory(Inventory* _inventory)
{
  this->inventory = _inventory;
} // Player::setInventory(Inventory _inventory)


bool Player::setWeapon(AbstractItem* _weapon, Ogre::SceneManager* _sceneManager)
{
  this->weapon = _weapon;
  if(_sceneManager)
  {
    _sceneManager->destroyEntity("PlayerWeapon");
  } // end if
  if(_weapon)
  {
    Weapon* theWeapon = (Weapon*)this->weapon;
    if(_sceneManager)
    {
      this->setWeaponModel(_sceneManager->createEntity("PlayerWeapon", theWeapon->getModelName()));
      this->setAttackAnimation(this->getEntity()->getAnimationState(theWeapon->getAnimationType()));
    } // end if
  } // end if
  else
  {
    if(this->getEntity())
    {
      this->setAttackAnimation(this->getEntity()->getAnimationState("OneHandSlash"));
    } // end if
  } // end else
  return true;
} // Player::setWeapon(Item _weapon)


bool Player::setHead(AbstractItem* _head)
{
  this->head = _head;
  this->updateArmourDefence();
  return false;
} // Player::setHead(Item _head)


bool Player::setChest(AbstractItem* _chest)
{
  this->chest = _chest;
  this->updateArmourDefence();
  return false;
} // Player::setChest(Item _chest)


bool Player::setLegs(AbstractItem* _legs)
{
  this->legs = _legs;
  this->updateArmourDefence();
  return false;
} // Player::setLegs(Item _legs)


bool Player::setFeet(AbstractItem* _feet)
{
  this->feet = _feet;
  this->updateArmourDefence();
  return false;
} // Player::setWeapon(Item _feet)


void Player::setExperience(int _experience)
{
  this->experience = _experience;

  if(this == Player::getInstance())
  {
    GUIManager::getSingleton().updateExperienceDisplay();
  } // end if


} // Player::setExperience(int _experience)


void Player::setCurrency(int _currency)
{
  this->currency = _currency;
  GUIManager::getSingleton().updateCurrencyAmount();
} // void Player::setCurrency(int _currency)


void Player::setMovingDown(bool _movingDown)
{
  this->movingDown = _movingDown;
} // void DynamicEntity::setMovingDown(bool _movingDown)


void Player::setMovingUp(bool _movingUp)
{
  this->movingUp = _movingUp;
} // void DynamicEntity::setMovingUp(bool _movingUp)


void Player::setMovingLeft(bool _movingLeft)
{
  this->movingLeft = _movingLeft;
} // void DynamicEntity::setMovingLeft(bool _movingLeft)


void Player::setMovingRight(bool _movingRight)
{
  this->movingRight = _movingRight;
} // void DynamicEntity::setMovingRight(bool _movingRight)

void Player::addExperience(int _experience)
{
 this->experience += _experience;
} // Player::addExperience(int _experience)


bool Player::useSkill(int _skill)
{
  return false;
} // Player::useSkill(int _skill)


bool Player::useItem(int _slot)
{
  return false;
} // Player::useItem(int _slot)


void Player::interact()
{
  StaticEntity* nearestEntity = StaticEntityManager::getInstance()->findClosestEntity(this->getPosition());

  if(nearestEntity)
  {
    if(nearestEntity->getPosition().squaredDistance(this->getPosition()) < 18*18)
    {
      MessageDispatcher::getInstance()->dispatchMsg(0.0,this->getEntityIDNumber(),nearestEntity->getEntityIDNumber(),
        MessageTypeInteract,this);
    } // end if
  } // end if
} // void Player::interact()


void Player::pickUpDrop(DroppedObject* _object)
{
  // Remove the object from the scene.
  _object->setActive(false);
  Ogre::SceneManager * sceneManager = GameManager::getInstance()->getCurrentScene()->getSceneManager();
  sceneManager->destroySceneNode(_object->getNode());
  sceneManager->destroyEntity(_object->getEntity());

  // Obtain the drop table and insert it into the players inventory.
  DropTable* dropTable = _object->getDropTable();
  
  // First of all give the player the currency.
  int currencyObtained = 0;
  if(dropTable->getCurrencyMax() > 0)
  {
    currencyObtained = rand()%dropTable->getCurrencyMax() + dropTable->getCurrencyMin();
  } // end if

  this->setCurrency(this->getCurrency()+currencyObtained);

  // Now loop through the drops and place the items in the inventory.]
  Drop** drops = dropTable->getDrops();
  for(int i=0;i<dropTable->getNumDrops();i++)
  {
    Drop* drop  = drops[i];
    int chance = rand()%100+1;
    if(chance < drop->getChance())
    {
      int amount = rand()%drop->getAmountMax() + drop->getAmountMin();
      AbstractItem* item = drops[i]->getItem();

      this->getInventory()->addItemAtEmptySlot(item,amount);

      if(this->currentQuest)
      {
        for(int j=0;j<amount;j++)
        {
          this->currentQuest->updateQuest(item,1);
        } // end for
      } // end if
    } // end if
  } // end for

} // void Player::pickUpDrop(DroppedObject* _object)


void Player::updateArmourDefence()
{
  int armourDefenceValue = 0;

  Armour* armour = (Armour*)this->head;
  if(armour)
  {
    armourDefenceValue += armour->getDefence();
  } // end if

  armour = (Armour*)this->chest;
  if(armour)
  {
    armourDefenceValue += armour->getDefence();
  } // end if

  armour = (Armour*)this->legs;
  if(armour)
  {
    armourDefenceValue += armour->getDefence();
  } // end if

  armour = (Armour*)this->feet;
  if(armour)
  {
    armourDefenceValue += armour->getDefence();
  } // end if

  this->setArmourDefence(armourDefenceValue);
  this->workOutMitigation();

} // void Player::updateArmourDefence()


string Player::getStatValueString()
{

  typedef CEGUI::PropertyHelper conv;

  string returnString;

  returnString += conv::intToString(this->getStrength()).c_str();
  returnString += "\n";
  returnString += conv::intToString(this->getAgility()).c_str();
  returnString += "\n";
  returnString += conv::intToString(this->getDexterity()).c_str();
  returnString += "\n";
  returnString += conv::intToString(this->getConstitution()).c_str();
  returnString += "\n";
  returnString += conv::intToString(this->getIntelligence()).c_str();
  returnString += "\n";
  returnString += conv::intToString(this->getWisdom()).c_str();
  returnString += "\n";
  returnString += conv::intToString(this->getCharisma()).c_str();
  
  return returnString;

} // string Player::getStatValueString()


AbstractItem* Player::unequipItem(string _slot, Ogre::SceneManager* _sceneManager)
{
  AbstractItem* returnItem = NULL;
  if(_slot == "Slot-Head")
  {
    returnItem = this->getHead();
    this->setHead(NULL);
  } // end if
  if(_slot == "Slot-Chest")
  {
    returnItem = this->getChest();
    this->setChest(NULL);
  } // end if
  if(_slot == "Slot-Legs")
  {
    returnItem = this->getLegs();
    this->setLegs(NULL);
  } // end if
  if(_slot == "Slot-Feet")
  {
    returnItem = this->getFeet();
    this->setFeet(NULL);
  } // end if
  if(_slot == "Slot-Weapon")
  {
    returnItem = this->getWeapon();
    this->setWeapon(NULL, _sceneManager);
  } // end if
  return returnItem;
} // void Player::unequipItem(string _slot)


AbstractItem* Player::backupEquippedItem(string _slot)
{
  AbstractItem* returnItem = NULL;
  if(_slot == "Slot-Head")
  {
    returnItem = this->getHead();
  } // end if
  if(_slot == "Slot-Chest")
  {
    returnItem = this->getChest();
  } // end if
  if(_slot == "Slot-Legs")
  {
    returnItem = this->getLegs();
  } // end if
  if(_slot == "Slot-Feet")
  {
    returnItem = this->getFeet();
  } // end if
  if(_slot == "Slot-Weapon")
  {
    returnItem = this->getWeapon();
  } // end if
  return returnItem;
} // void Player::backupEquippedItem(string _slot)


AbstractItem* Player::getEquippedItemDetails(string _slot) const
{
  AbstractItem* returnItem = NULL;
  if(_slot == "Slot-Head")
  {
    returnItem = this->getHead();
  } // end if
  if(_slot == "Slot-Chest")
  {
    returnItem = this->getChest();
  } // end if
  if(_slot == "Slot-Legs")
  {
    returnItem = this->getLegs();
  } // end if
  if(_slot == "Slot-Feet")
  {
    returnItem = this->getFeet();
  } // end if
  if(_slot == "Slot-Weapon")
  {
    returnItem = this->getWeapon();
  } // end if
  return returnItem;
} // AbstractItem* Player::getEquippedItemDetails(string _slot) const


void Player::setWeaponModel(Ogre::Entity* _model)
{
  Weapon* weaponCast = (Weapon*)this->weapon;

  if(weaponCast->getHanded() == "right")
  {
    if(this->gender == "Male")
    {
      this->getEntity()->attachObjectToBone("HLMR_POINT_TRACK", _model);
    } // end if
    else
    {
      this->getEntity()->attachObjectToBone("BAFFI_R_TRACK", _model);
      _model->getParentNode()->setScale(3.0,3.0,3.0);
    } // end else 
  } // end if
  else
  {
    if(this->gender == "Male")
    {
      this->getEntity()->attachObjectToBone("HLML_POINT_TRACK", _model);
    } // end if
    else
    {
      this->getEntity()->attachObjectToBone("BAFFI_L_TRACK", _model);
      _model->getParentNode()->setScale(3.0,3.0,3.0);
    } // end else 
  } // end else
} // void Player::setWeaponModel()


void Player::setCurrentQuest(Quest* _quest)
{
  this->currentQuest = _quest;
} // void Player::setCurrentQuest(Quest* _quest)


void Player::setCompletedQuests(vector<Quest*>* _quests)
{
  this->completedQuests = _quests;
} // void Player::setCompletedQuests(vector<Quests*>* _quests)


void Player::setTargettedEnemy(Mob* _enemy)
{
  if(_enemy)
  {
    if(this->targettedEnemy != _enemy)
    {
      Ogre::SceneManager* sceneManager = GameManager::getInstance()->getCurrentScene()->getSceneManager();
      Ogre::Entity* targetRing = sceneManager->getEntity("TargetPlate");
      Ogre::SceneNode* targetRingNode = sceneManager->getSceneNode("TargetPlateNode");
      if(targetRingNode->getParentSceneNode())
      {
        targetRingNode->getParentSceneNode()->removeChild(targetRingNode);
      } // end if
      _enemy->getNode()->addChild(targetRingNode);
      targetRingNode->translate(0, -_enemy->getDimensions().y/2+0.3, 0);
    
      double xDimension = _enemy->getDimensions().x;
      double zDimension = _enemy->getDimensions().z;
      Ogre::Vector3 nodeDimensions = this->getEntity()->getBoundingBox().getSize();

      if(this->targettedEnemy)
      {
        if(this->targettedEnemy->getName() == "Snake")
        {
          xDimension *= 0.4;
          zDimension *= 0.5;
        } // end if
      } // end if

      double xScale = (xDimension/nodeDimensions.x)+0.2;
      double zScale = (zDimension/nodeDimensions.z)+0.2;
      double scale = (xScale+zScale)/2+0.4;

      targetRingNode->setScale(scale, 0, scale);
    } // end if

  } // end if
  else
  {
    Ogre::SceneManager* sceneManager = GameManager::getInstance()->getCurrentScene()->getSceneManager();
    Ogre::SceneNode* targetRingNode = sceneManager->getSceneNode("TargetPlateNode");
    if(targetRingNode->getParentSceneNode())
    {
      targetRingNode->getParentSceneNode()->removeChild(targetRingNode);
    } // end if
  } // end else
  
  this->targettedEnemy = _enemy;
  GUIManager::getSingleton().updateTargettedEnemyWindow(_enemy);

} // void Player::setTargettedEnemy(Mob* _enemy)


void Player::setSkillPoints(int _skillPoints)
{
  this->skillPoints = _skillPoints;

  // Call the gui manager.
  GUIManager::getSingleton().updateSkillPoints();

} // int Player::getSkillPoints()


int Player::workOutDamage()
{
  int weaponMin = 0;
  int weaponMax = 5;
  if(this->weapon)
  {
    Weapon* weap = (Weapon*)this->weapon;
    weaponMin = weap->getMinimumDamage();
    weaponMax = weap->getMaximumDamage();
  } // end if
  int r = rand()%weaponMax + weaponMin+1;
  return this->getBaseDamage() + r;
} // int Player::workOutDamage()


void Player::gainExperience(Mob* _mob)
{
  // Mob has died so also check it against our kill quest if we have it.
  if(this->currentQuest)
  {
    this->currentQuest->updateQuest(_mob);
  } // end if

  int experienceGained = ((double)(50*(double)this->getLevel())) * ((double)_mob->getLevel()/(double)this->getLevel());
  this->setExperience(this->getExperience()+experienceGained);

  if(this->experienceRequired <= this->experience)
  {
    this->levelUp();
  } // end if
} // void Player::gainExperience()


void Player::workOutRequiredExperience()
{
  int levelExp = 1000 * (exp((float)this->getLevel()) / exp((float)this->getLevel()/1.4)) - 1000;
  if(this->getLevel() > 1)
  {
    int prevLevelExp = 1000 * (exp((float)this->getLevel()-1) / exp(((float)this->getLevel()-1)/1.4)) - 1000;
    levelExp -= prevLevelExp;
  } // end if

  this->experienceRequired = levelExp;

  if(this == Player::getInstance())
  {
    GUIManager::getSingleton().updateExperienceDisplay();
  } // end if
} // void Player::workOutRequiredExperience()


void Player::handleMessage(const Message& _message)
{
  if(_message.msg == MessageTypeDamagedNormal)
  {
    int damage = *(int*)_message.extraInfo;
    this->takeDamageWithMitigation(damage, _message.senderID);
  } // end if
  if(_message.msg == MessageTypeDamagedNoMitigation)
  {
    int damage = *(int*)_message.extraInfo;
    this->takeFullDamage(damage, _message.senderID);
  } // end if
  if(_message.msg == MessageTypeExperienceGained)
  {
    Mob* theMob = (Mob*)_message.extraInfo;
    this->gainExperience(theMob);
  } // end if
  else if(_message.msg == MessageTypeAttackMissed)
  {
    DamageTextManager::getSingleton().createMissText(this);
    
    // Add the sender to the players target.
    if(this == Player::getInstance())
    {
      // Add this to the players targetted mob if the player does not have one.
      if(!Player::getInstance()->getTargettedEnemy())
      {
        Player::getInstance()->setTargettedEnemy(
          (Mob*)MessageDispatcher::getInstance()->findEntityByID(_message.senderID));
      } // end if
    } // end if
  } // end else if
} // void Player::handleMessage(const Message& _message)



