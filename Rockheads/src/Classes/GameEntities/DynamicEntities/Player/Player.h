#include "../../../../PrecompiledHeaders/stdafx.h"
#ifndef PLAYER_H
#define PLAYER_H

#include "../DynamicEntity/DynamicEntity.h"
#include "../../../GameObjects/Inventory/Inventory.h"
#include "../../../GameObjects/Items/AbstractItem/AbstractItem.h"
#include "../../../Managers/CollisionManager/CollisionManager.h"

class Weapon;
class Armour;
class Mob;
class DroppedObject;
class Quest;

class Player : public DynamicEntity
{
private:

  Player();
  Player(const Player& _other);
  ~Player(){};
  Player& operator=(const Player& _other){Player* player=NULL; return *player;};
  Player* clone() const {Player* player = NULL; return player;};

  static          Player* instance;
  Inventory*      inventory;
  AbstractItem*   weapon;
  AbstractItem*   head;
  AbstractItem*   chest;
  AbstractItem*   legs;
  AbstractItem*   feet;
  int             experience;
  int             experienceRequired;
  int             currency;
  bool            movingDown;
  bool            movingUp;
  bool            movingLeft;
  bool            movingRight;
  Quest*          currentQuest;
  std::vector<Quest*>* completedQuests;
  int             skillPoints;
  
  CollisionManager* CollisionManager;
  string gender;
  Mob* targettedEnemy;

public:
  static    Player* getInstance();
  void      init(int _strength, int _agility, int _dexterity, int _constitution, int _intelligence,
                 int _wisdom, int _charisma, Ogre::String _modelName, Ogre::String _charName, 
                 Ogre::String _skillSetName, string _gender, Inventory* _inv=NULL, int _level=1, int _experience=0,
                 int _currency=100,   int _skillPoints=1);
  void      update(double _timeSinceLastFrame);
  void      release();
  void      attack();
  void      move(double _timeSinceLastFrame);
  void      removeFromInv(int _slot, bool _delete = false);
  void      removeFromInv(InventorySlot* _slot, bool _delete = false);
  void      addToInv(AbstractItem* _item, int _amount);
  void      levelUp();
  void      die();
  int       workOutDamage();
  void      loadEntity(Ogre::Entity* _entity);

  // Accessors.
  Inventory* getInventory() const;
  AbstractItem* getWeapon() const;
  AbstractItem* getHead() const;
  AbstractItem* getChest() const;
  AbstractItem* getLegs() const;
  AbstractItem* getFeet() const;
  int getExperience() const;
  int getExperienceRequired() const;
  int getCurrency() const;
  Quest* getCurrentQuest() const;
  vector<Quest*>* getCompletedQuests() const;
  string getGender() const;
  Mob* getTargettedEnemy() const;
  int getSkillPoints() const;

  // Mutators
  void      setInventory(Inventory* _inventory);
  bool      setWeapon(AbstractItem* _weapon, Ogre::SceneManager* _sceneManager);
  bool      setHead(AbstractItem* _head);
  bool      setChest(AbstractItem* _chest);
  bool      setLegs(AbstractItem* _legs);
  bool      setFeet(AbstractItem* _feet);
  void      setExperience(int _experience);
  void      setCurrency(int _currency);
  void      setMovingDown(bool _movingDown);
  void      setMovingUp(bool _movingUp);
  void      setMovingLeft(bool _movingLeft);
  void      setMovingRight(bool _movingRight);
  void      setWeaponModel(Ogre::Entity* _model);
  void      setCurrentQuest(Quest* _quest);
  void      setCompletedQuests(vector<Quest*>* _quests);
  void      setTargettedEnemy(Mob* _enemy);
  void      setSkillPoints(int _skillPoints);

  void      addExperience(int _experience);
  bool      useSkill(int _skill);
  bool      useItem(int _slot);
  void      interact();
  void      pickUpDrop(DroppedObject* _object);
  void      updateArmourDefence();  
  void      gainExperience(Mob* _mob);
  void      workOutRequiredExperience();
  void      handleMessage(const Message& _message);

  AbstractItem* unequipItem(string _slot, Ogre::SceneManager* _sceneManager);
  AbstractItem* backupEquippedItem(string _slot);
  AbstractItem* getEquippedItemDetails(string _slot) const;
  string        getStatValueString();
}; // end class Player

#endif