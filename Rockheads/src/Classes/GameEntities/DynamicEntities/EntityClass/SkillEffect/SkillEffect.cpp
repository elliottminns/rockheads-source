#include "stdafx.h"
#include "SkillEffect.h"
#include "../../../../Effects/AbstractEffect/AbstractEffect.h"
#include "../../../AbstractEntity/AbstractEntity.h"


SkillEffect::SkillEffect()
{
  this->chance = 0;
  this->effect = NULL;
} // SkillEffect::SkillEffect()


SkillEffect::~SkillEffect()
{
  if(this->effect)
  {
    delete this->effect;
    this->effect = NULL;
  } // end if
} // ~SkillEffect::~SkillEffect()


SkillEffect::SkillEffect(const SkillEffect& _other)
{
  this->chance = _other.chance;
  this->effect = _other.effect->clone();
} // SkillEffect(const SkillEffects& _other)


SkillEffect* SkillEffect::clone() const
{
  return new SkillEffect(*this);
} // SkillEffect* SkillEffect::clone() const


bool SkillEffect::activate(DynamicEntity* _owner, DynamicEntity* _target)
{
  if(_target)
  {
    if(this->chance < 100)
    {
      int roll = rand()%100 + 1;

      if(roll < this->chance)
      {
        if(!this->effect->activate(_owner, _target))
        {
          return false;
        } //end if
      } // end if
    } // end if
    else
    {
      if(!this->effect->activate(_owner, _target))
      {
        return false;
      } // end if
    } // end else
  } // end if

  return true;

} // void SkillEffect::activate(DynamicEntity* _owner, DynamicEntity* _target)


void SkillEffect::setChance(int _chance)
{
  this->chance = _chance;
} // void SkillEffect::setChance(int _chance)


void SkillEffect::setEffect(AbstractEffect* _effect)
{
  this->effect = _effect;
} // void SkillEffect::setEffect(AbstractEffect* effect)


int SkillEffect::getChance()
{
  return this->chance;
} // int SkillEffect::getChance()


AbstractEffect* SkillEffect::getEffect()
{
  return this->effect;
} // AbstractEffect* SkillEffect::getEffect()