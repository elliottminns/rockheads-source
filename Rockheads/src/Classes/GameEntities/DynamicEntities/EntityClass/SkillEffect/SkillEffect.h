#include "../../../../../PrecompiledHeaders/stdafx.h"
#ifndef SKILL_EFFECT_H
#define SKILL_EFFECT_H

class AbstractEffect;
class DynamicEntity;

class SkillEffect
{
private:
  int chance;
  AbstractEffect* effect;

public:
  SkillEffect();
  ~SkillEffect();
  SkillEffect(const SkillEffect& _other);
  virtual SkillEffect* clone() const;
  bool activate(DynamicEntity* _owner, DynamicEntity* _target);

  void setChance(int _chance);
  void setEffect(AbstractEffect* _effect);

  int getChance();
  AbstractEffect* getEffect();

}; 

#endif