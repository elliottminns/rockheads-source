#include "stdafx.h"
#include "SkillSet.h"
#include "../Skill/Skill.h"
#include "../../../../Managers/SkillManager/SkillManager.h"
#include "../../../../GameObjects/Items/Armour/Armour.h"
#include "../../../../GameObjects/Items/Weapon/Weapon.h"

SkillSet::SkillSet()
{
  this->numSkills  = 0;
  this->type       = "";               
  this->skills     = NULL;
  this->name       = "";
  this->armourType = NULL;
  this->weaponType = NULL;
} // SkillSet::SkillSet()


SkillSet::SkillSet(const SkillSet& _other)
{
  this->type       = _other.type;              
  this->name       = _other.name;
  this->numSkills  = _other.numSkills;

  if(_other.skills)
  {
    this->skills = new Skill*[_other.numSkills];

    for(int i=0;i<_other.numSkills;i++)
    {
      this->skills[i] = _other.skills[i]->clone();
    } // end for
  } // end if
  else
  {
    this->skills = NULL;
  } // end else

  if(_other.armourType)
  {
    this->armourType = _other.armourType;
  } // end if
  else
  {
    this->armourType = NULL;
  } // end else

  if(_other.weaponType)
  {
    this->weaponType = _other.weaponType;
  } // end if
  else
  {
    this->weaponType = NULL;
  } // end else

} // SkillSet::SkillSet(const SkillSet& _other)


SkillSet::~SkillSet()
{
  if(this->skills)
  {
    delete[] this->skills;
    this->skills = NULL;
  } // end if
} // SkillSet::~SkillSet();


SkillSet& SkillSet::operator=(const SkillSet& _other)
{
  this->numSkills  = _other.numSkills;
  this->type       = _other.type;              
  this->name       = _other.name;
  
  if(this->skills)
  {
    delete this->skills;
    this->skills = NULL;
  } // end if
  if(_other.skills)
  {
    this->skills = new Skill*[_other.numSkills];

    for(int i=0;i<_other.numSkills;i++)
    {
      this->skills[i] = _other.skills[i]->clone();
    } // end for
  } // end if
  else
  {
    this->skills = NULL;
  } // end else

  if(this->armourType)
  {
    delete this->armourType;
    this->armourType = NULL;
  } // end if
  if(_other.armourType)
  {
    this->armourType = _other.armourType;
  } // end if
  else
  {
    this->armourType = NULL;
  } // end else

  if(this->weaponType)
  {
    delete this->weaponType;
    this->weaponType = NULL;
  } // end if
  if(_other.weaponType)
  {
    this->weaponType = _other.weaponType;
  } // end if
  else
  {
    this->weaponType = NULL;
  } // end else

  return *this;
} // SkillSet& SkillSet::operator=(const SkillSet& _other)


SkillSet* SkillSet::clone() const
{
  return new SkillSet(*this);
} // SkillSet* SkillSet::clone() const


void SkillSet::initialise()
{
  for(int i=0;i<this->numSkills;i++)
  {
    if(this->skills[i])
    {
      this->skills[i]->setLevel(0);
    } // end if
  } // end for
} // void SkillSet::initialise()


void SkillSet::upgradeSkill(int _skill)
{
  // Get the level, increment it and the skillname from the current skill in position.
  int level = this->skills[_skill]->getLevel();
  level += 1;
  string skillName = this->skills[_skill]->getName();
 
  // Delete the skill.
  delete this->skills[_skill];
  this->skills[_skill] = SkillManager::getInstance()->createSkill(skillName, level);

} // void SkillSet::upgradeSkill(int _skill)


bool SkillSet::canEquip(Armour* _armour)
{
  for(vector<string>::iterator it = this->armourType->begin(); 
    it != this->armourType->end(); ++it)
  { // Determines whether or not this skill set can use this armour.
    // Looks through the string vector to see if a matching armour type is found.
    if(*it == _armour->getType())
    {
      return true;
    } // end if
  } // end for 

  return false;
} // bool SkillSet::canEquip(Armour* _armour)


bool SkillSet::canEquip(Weapon* _weapon)
{ // Determines whether or not this skill set can use this weapon.
  // Looks through the string vector to see if a matching wield type is found.

  for(vector<string>::iterator it = this->weaponType->begin(); 
    it != this->weaponType->end(); ++it)
  {
    if(*it == _weapon->getWieldType())
    {
      return true;
    } // end if
  } // end for 

  return false;
} // bool SkillSet::canEquip(Weapon* _weapon)


int SkillSet::getNumSkills() const
{
  return this->numSkills;
} // int SkillSet::getNumSkills const


Skill* SkillSet::getSkill(int _skill) const
{
  return this->skills[_skill];
} // Skill* SkillSet::getSkill(int _skill) const


Skill* SkillSet::getSkill(string _name) const
{
  for(int i=0;i<this->numSkills;i++)
  {
    if(this->skills[i]->getName() == _name)
    {
      return this->skills[i];
    } // end if
  } // end for

  return NULL;
} // Skill* SkillSet::getSkill(string _name) const
 

Skill** SkillSet::getSkills() const
{
  return this->skills;
} // Skill** SkillSet::getSkills() const


string SkillSet::getName() const
{
  return this->name;
} // string SkillSet::getName() const


string SkillSet::getType() const
{
  return this->type;
} // string SkillSet::getType() const


vector<string>* SkillSet::getArmourType() const
{
  return this->armourType;
} // vector<string>* SkillSet::getArmourType() const


vector<string>* SkillSet::getWeaponType() const
{
  return this->weaponType;
} // vector<string>* SkillSet::getArmourType() const


string SkillSet::getDescription() const
{
  return this->description;
} // string SkillSet::getDescription() const


void SkillSet::setNumSkills(int _num) 
{
  this->numSkills = _num;
} // void SkillSet::setNumSkills(int _num) 


void SkillSet::setSkills(vector<Skill*>* _skills)
{
  this->numSkills = _skills->size();

  this->skills = new Skill*[this->numSkills];

  int i=0;
  for(vector<Skill*>::iterator it = _skills->begin(); it != _skills->end(); ++it)
  {
    this->skills[i] = *it;
    ++i;
  } // end for
} // void SkillSet::setSkills(vector<Skill*>* _skills)


void SkillSet::setName(string _name)
{
  this->name = _name;
} // void SkillSet::setName(string _name)


void SkillSet::setType(string _type)
{
  this->type = _type;
} // void SkillSet::setType(string _type)


void SkillSet::setArmourType(vector<string>* _armourType)
{
  this->armourType = _armourType;
} // void SkillSet::setArmourType(vector<string>* _armourType)


void SkillSet::setWeaponType(vector<string>* _weaponType)
{
  this->weaponType = _weaponType;
} // void SkillSet::setWeaponType(vector<string>* _weaponType)


void SkillSet::addArmourType(string _armourType)
{
  if(!this->armourType)
  {
    this->armourType = new vector<string>;
  } // end if
  this->armourType->push_back(_armourType);
} // void SkillSet::addArmourType(string _armourType)


void SkillSet::addWeaponType(string _weaponType)
{
  if(!this->weaponType)
  {
    this->weaponType = new vector<string>;
  } // end if
  this->weaponType->push_back(_weaponType);
} // void SkillSet::addWeaponType(string _weaponType)
 

void SkillSet::setDescription(string _description)
{
  this->description = _description;
} // void SkillSet::setDescription(string _description)