#include "../../../../../PrecompiledHeaders/stdafx.h"
#ifndef SKILL_SET_H
#define SKILL_SET_H

#include <vector>
class Armour;
class Weapon;

using namespace std;

class Skill;

class SkillSet
{
private:
  int numSkills;
  string  type;               
  Skill** skills;
  string  name;
  vector<string>* armourType;
  vector<string>* weaponType;
  string description;

public: 
  SkillSet();
  SkillSet(const SkillSet& _other);
  ~SkillSet();
  SkillSet& operator=(const SkillSet& _other);
  SkillSet* clone() const;

  void initialise();
  void upgradeSkill(int _skill);

  bool canEquip(Armour* _armour);
  bool canEquip(Weapon* _weapon);

  int getNumSkills() const;
  Skill* getSkill(int _skill) const;
  Skill* getSkill(string _name) const;
  Skill** getSkills() const;
  string getName() const;
  string getType() const;
  vector<string>* getArmourType() const;
  vector<string>* getWeaponType() const;
  string getDescription() const;

  void setNumSkills(int _num);
  void setSkills(vector<Skill*>* _skills);
  void setName(string _name);
  void setType(string _type);
  void setArmourType(vector<string>* _armourType);
  void setWeaponType(vector<string>* _weaponType);
  void addArmourType(string _armourType);
  void addWeaponType(string _weaponType);
  void setDescription(string _description);


}; // end class SkillSet

#endif