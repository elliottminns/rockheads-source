#include "../../../../../PrecompiledHeaders/stdafx.h"
#ifndef SKILL_H
#define SKILL_H

#include <iostream>
#include <string.h>

class SkillEffect;
class DynamicEntity;

using namespace std;

class Skill
{
private:
  int numEffects;
  SkillEffect** effects;

  string effectType;
  string name;
  string description;
  string imageName;
  string classType;
  bool   effectRequired;
  string requiredEffect;
  string targetType;
  int    cooldown;
  int    level;
  bool   reagentRequired;
  string reagentName;
  int    staminaCost;
  int    range;
  string animationType;

public:
  Skill();
  Skill(const Skill& _otherSkill);
  Skill& operator=(const Skill& _otherSkill);
  Skill* clone() const;
  ~Skill();   

  bool activate(DynamicEntity* _owner);

  void setEffects(vector<SkillEffect*>* _effects);
  void setEffectType(string _type);
  void setName(string _name);
  void setImageName(string _name);
  void setLevel(int _level);
  void setCooldown(int _cooldown);
  void setClassType(string _classType);
  void setDescription(string _description);
  void setTargetType(string _targetType);
  void setEffectRequired(bool _required);
  void setRequiredEffectName(string _name);
  void setReagentRequired(bool _required);
  void setRequiredReagentName(string _name);
  void setStaminaCost(int _cost);
  void setAnimationType(string _type);

  SkillEffect** getEffects();
  int getNumEffects();
  string getName();
  string getImageName();
  string getDescription();
  string getTargetType();
  int getLevel();
  int getCooldown();
  string getAnimationType();

  bool    upgradeSkill();
  string getInspectorInformation();


}; // end class Skill

#endif