#include "stdafx.h"
#include "Skill.h"
#include "../../DynamicEntity/DynamicEntity.h"
#include "../SkillEffect/SkillEffect.h"
#include "../../Player/Player.h"
#include "../../../../Managers/MobManager/MobManager.h"
#include "../../../../Managers/GUIManager/GUIManager.h"
#include "../../../../Effects/AbstractEffect/AbstractEffect.h"

Skill::Skill()
{
  this->cooldown  = 0;
  this->level     = 0;
  this->numEffects = 0;
  this->effects    = NULL;
  this->imageName = "";
  this->requiredEffect = "";
  this->description = "";
  this->classType   = "";
  this->effectType  = "";
  this->targetType = "";
  this->effectRequired = false;
  this->reagentRequired = false;
  this->range = 0;
  this->animationType = "";
  
} // Skill::Skill


Skill::Skill(const Skill& _other)
{
  this->classType = _other.classType;
  this->cooldown = _other.cooldown;
  this->description = _other.description;
  this->effectType = _other.effectType;
  this->imageName = _other.imageName;
  this->level = _other.level;
  this->name = _other.name;
  this->requiredEffect = _other.requiredEffect;
  this->targetType = _other.targetType;
  this->reagentRequired = _other.reagentRequired;
  this->reagentName = _other.reagentName;
  this->staminaCost = _other.staminaCost;
  this->effectRequired = _other.effectRequired;
  this->requiredEffect = _other.requiredEffect;
  this->targetType = _other.targetType;
  this->range = _other.range;
  this->animationType = _other.animationType;

  if(_other.effects)
  {
    this->numEffects = _other.numEffects;
    this->effects = new SkillEffect*[this->numEffects];
    for(int i=0;i<this->numEffects;i++)
    {
      this->effects[i] = _other.effects[i]->clone();
    } // end for
  } // end if
  else
  {
    this->effects = NULL;
  } // end else

} // Skill::Skill(const Skill& _other)

Skill* Skill::clone() const
{
  return new Skill(*this);
} // Skill* Skill::clone() const


Skill& Skill::operator=(const Skill& _other)
{
  this->classType = _other.classType;
  this->cooldown = _other.cooldown;
  this->description = _other.description;
  this->effectType = _other.effectType;
  this->imageName = _other.imageName;
  this->level = _other.level;
  this->name = _other.name;
  this->requiredEffect = _other.requiredEffect;
  this->targetType = _other.targetType;
  this->range = _other.range;
  this->animationType = _other.animationType;

  if(this->effects)
  {
    delete[] this->effects;
  } // end if
  if(_other.effects)
  {
    this->numEffects = _other.numEffects;
    this->effects = new SkillEffect*[this->numEffects];
    for(int i=0;i<this->numEffects;i++)
    {
      this->effects[i] = _other.effects[i]->clone();
    } // end for
  } // end if
  else
  {
    this->effects = NULL;
  } // end else

  return *this;
} // Skill Skill::operator=(const Skill& _other)


Skill::~Skill()
{
  delete[] this->effects;
} // Skill::~Skill()


bool Skill::activate(DynamicEntity* _owner)
{

  // Check to see if the skill requires a reagent.
  if(this->reagentRequired)
  {
    // Search through the _owner inventory if it is a player.
    if(_owner == Player::getInstance())
    {
      bool reagentAvailable = false;

      // Get the players inventory slot.
      vector<InventorySlot*>* invSlots = Player::getInstance()->getInventory()->getItems();

      for(vector<InventorySlot*>::iterator it = invSlots->begin(); it != invSlots->end(); ++it)
      {
        InventorySlot* slot = *it;

        if(slot->getItem())
        {
          if(slot->getItem()->getName() == this->reagentName)
          {
            slot->setCount(slot->getCount()-1);
            reagentAvailable = true;
            GUIManager::getSingletonPtr()->updateItemCount(slot);
            break;
          } // end if(slot->getItem()->getName() == this->reagentName)
        } // end if(slot->getItem())

      } // end for

      if(!reagentAvailable)
      {
        return false;
      } // end if

    } // end if(_owner == Player::getInstance())
  } // end if(this->reagentRequired)

  if(this->effectRequired)
  {
    bool effectActive = false;

    // Loop through the effects and determine if the effect is currently active.

    vector<AbstractEffect*>* effects = _owner->getActiveEffects();

    for(vector<AbstractEffect*>::iterator it = effects->begin(); it != effects->end(); ++it)
    {
      AbstractEffect* effect = *it;
      if(effect->getType() == this->requiredEffect)
      {
        effectActive = true;
        break;
      } // end if
    } // end for

    if(!effectActive)
    {
      return false;
    } // end if

  } // end if(this->effectRequired)

  // Now check for stamina availability.
  if(_owner->getStamina() < this->staminaCost)
  {
    return false;
  } // end if

  bool returnValue = true;

  for(int i=0;i<this->numEffects;i++)
  {
    if(this->effects[i])
    {
      DynamicEntity* target;
      if(this->targetType == "self")
      {
        target = _owner;
      } // end if
      else if(this->targetType == "enemy")
      {
        if(_owner == Player::getInstance())
        {
          target = MobManager::getInstance()->getClosestMob(_owner->getPosition());
          Player::getInstance()->setTargettedEnemy((Mob*)target);

          if(!target)
          {
            return false;
          } // end if

          // Check for the range.
          int rangeAmount = this->range;
          if(this->range <= 0)
          {
            Weapon* weapon = (Weapon*)Player::getInstance()->getWeapon();
            if(weapon)
            {
              rangeAmount = weapon->getRange();
            } // end if
            else
            {
              rangeAmount = 12;
            } // end else
          } // end if
          
          if(target->getPosition().squaredDistance(_owner->getPosition()) > rangeAmount*rangeAmount)
          {
            return false;
          } // end if

        } // end if
        else
        {
          target = Player::getInstance();
        } // end else
      } // end else if

      if(!this->effects[i]->activate(_owner, target))
      {
        return false;
      } // end if

    } // end if
  } // end for

  // Skill was successful - decrement stamina and play animation.
  _owner->setStamina(_owner->getStamina()-this->staminaCost);
  _owner->playAnimation(this->animationType);
  AudioManager::getSingleton().playSound(this->name);
  return true;
} // void Skill::activate(DynamicEntity* _owner)
 

void Skill::setEffects(vector<SkillEffect*>* _effects)
{
  this->numEffects = _effects->size();
  this->effects = new SkillEffect*[this->numEffects];

  int i=0;
  for(vector<SkillEffect*>::iterator it = _effects->begin(); it != _effects->end(); ++it)
  {
    this->effects[i] = *it;
    ++i;
  } // end if

} // void Skill::setEffect(Effect* _effect)


void Skill::setEffectType(string _type)
{
  this->effectType = _type;
} // void Skill::setEffectType(string _type)


void Skill::setName(string _name)
{
  this->name = _name;
} // void Skill::setName(string _name)


void Skill::setImageName(string _name)
{
  this->imageName = _name;
} // void Skill::setImageName(string _name)


void Skill::setLevel(int _level)
{
  this->level = _level;
} // void Skill::setLevel(int _level)


void Skill::setCooldown(int _cooldown)
{
  this->cooldown = _cooldown;
} // void Skill::setCooldown(int _cooldown)


void Skill::setClassType(string _classType)
{
  this->classType = _classType;
} // void Skill::setClassType(string _classType)


void Skill::setDescription(string _description)
{
  this->description = _description;
} // void Skill::setDescription(string _description)


void Skill::setTargetType(string _targetType)
{
  this->targetType = _targetType;
} // void Skill::setTargetType(string _targetType)


void Skill::setEffectRequired(bool _required)
{
  this->effectRequired = _required;
} // void Skill::setEffectRequired(bool _required)


void Skill::setRequiredEffectName(string _name)
{
  this->requiredEffect = _name;
} // void Skill::setRequiredEffectName(string _name)


void Skill::setReagentRequired(bool _required)
{
  this->reagentRequired = _required;
} // void Skill::setReagentRequired(bool _required)


void Skill::setRequiredReagentName(string _name)
{
  this->reagentName = _name;
} // void Skill::setRequiredReagentName(string _name)


void Skill::setStaminaCost(int _cost)
{
  this->staminaCost = _cost;
} // void Skill::setStaminaCost(int _cost)


void Skill::setAnimationType(string _type)
{
  this->animationType = _type;
} // void Skill::setAnimationType(string _type)


SkillEffect** Skill::getEffects()
{
  return this->effects;
} // Effect* Skill::getEffect()


int Skill::getNumEffects()
{
  return this->numEffects;
} // int Skill::getNumEffects()


string Skill::getName()
{
  return this->name;
} // string Skill::getName()


string Skill::getImageName()
{
  return this->imageName;
} // string Skill::getImageName()


string Skill::getDescription()
{
  return this->description;
} // string Skill::getDescription()


int Skill::getLevel()
{
  return this->level;
} // int Skill::getLevel()


int Skill::getCooldown()
{
  return this->cooldown;
} // int Skill::getCooldown()


string Skill::getAnimationType()
{
  return this->animationType;
} // string Skill::getAnimationType()


string Skill::getInspectorInformation()
{
  string returnString = this->description;

  returnString += "\n";

  // Get the description details.
  for(int i=0;i<this->numEffects;i++)
  {
    returnString += this->effects[i]->getEffect()->getDetailsDescription();
    returnString += "\n";
  } // end for

  // Add the cooldown and stamina cost.
  returnString += "Stamina: " + Ogre::StringConverter::toString(this->staminaCost) + "\n";
  returnString += "Cooldown: " + Ogre::StringConverter::toString(this->cooldown) + "\n";

  return returnString;

} // string Skill::getInspectorInformation()