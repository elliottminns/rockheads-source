#include "../../../PrecompiledHeaders/stdafx.h"
#ifndef SKILL_HOT_SLOT
#define SKILL_HOT_SLOT

#include "../AbstractHotSlot/AbstractHotSlot.h"

class Skill;

class SkillHotSlot : public AbstractHotSlot
{
private:
  Skill* skill;

public:
  SkillHotSlot();
  ~SkillHotSlot();
  bool activate();
  string getImageName();
  void update(double _timeSinceLastFrame);

  void setSkill(Skill* _skill);
  Skill* getSkill();

}; 

#endif