#include "stdafx.h"
#include "SkillHotSlot.h"
#include "../../GameEntities/DynamicEntities/DynamicEntity/DynamicEntity.h"
#include "../../GameEntities/DynamicEntities/EntityClass/Skill/Skill.h"
#include "../../Managers/GUIManager/GUIManager.h"
#include "../../Effects/AbstractEffect/AbstractEffect.h"
#include "../../Effects/InvisibleEffect/InvisibleEffect.h"
#include "../../GameEntities/DynamicEntities/EntityClass/SkillEffect/SkillEffect.h"


SkillHotSlot::SkillHotSlot()
{
  this->skill = NULL;
  this->type  = "Skill";
} // SkillHotSlot::SkillHotSlot()


SkillHotSlot::~SkillHotSlot()
{
  this->skill = NULL;
} // SkillHotSlot::~SkillHotSlot() 


bool SkillHotSlot::activate()
{
  if(this->getCooldown() <= 0)
  {
    // Use the skill.
    if(this->skill)
    {
      if(this->skill->activate(Player::getInstance()))
      {
        bool invisUsed = false;
        for(int i=0;i<this->skill->getNumEffects();i++)
        { // Check to make sure invis wasnt called from this skill.
          if(this->skill->getEffects()[i]->getEffect()->getType() == "invis")
          {
            invisUsed = true;
          } // end if
        } // end for

        if(Player::getInstance()->getInvisible() && !invisUsed)
        {
          // Set the remainder timer on the invis effect to 0.
          vector<AbstractEffect*>* effects = Player::getInstance()->getActiveEffects();
          for(vector<AbstractEffect*>::iterator it = effects->begin(); it != effects->end(); ++it)
          {
            AbstractEffect* effect = *it;

            if(effect->getType() == "invis")
            {
              static_cast<InvisibleEffect*>(effect)->setTime(0);
            } // end fi
          } // end for
        } // end if(this->getInvisible()

        // Get the cooldown of the skill but times by 1000 to use an int and compensate
        // for the time in miliseconds.
        this->setCooldown(this->skill->getCooldown()*1000);
        double percentage = ((this->getCooldown()/1000)/this->skill->getCooldown());
        GUIManager::getSingleton().updateHotSlotCooldown(this->getIndex(), percentage);
      } // end if
    } // end if

  } // end if

  return true;
} // bool SkillHotSlot::activate()


string SkillHotSlot::getImageName()
{
  return this->skill->getImageName();
} // string SkillHotSlot::getImageName()


void SkillHotSlot::update(double _timeSinceLastFrame)
{
  if(this->getCooldown() > 0)
  {
    this->setCooldown(this->getCooldown() - (int)_timeSinceLastFrame);
    double percentage = ((this->getCooldown()/1000)/this->skill->getCooldown());
    GUIManager::getSingleton().updateHotSlotCooldown(this->getIndex(), percentage);
  } // end if
} // void SkillHotSlot::update(double _timeSinceLastFrame)


void SkillHotSlot::setSkill(Skill* _skill)
{
  this->skill = _skill;
} // void SkillHotSlot::setSkill(Skill* _skill)


Skill* SkillHotSlot::getSkill()
{
  return this->skill;
} // Skill* SkillHotSlot::getSkill()