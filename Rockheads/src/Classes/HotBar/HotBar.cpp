#include "stdafx.h"
#include "HotBar.h"
#include "Ogre.h"
#include "AbstractHotSlot\AbstractHotSlot.h"
#include "ItemHotSlot\ItemHotSlot.h"
#include "SkillHotSlot\SkillHotSlot.h"


template<> HotBar* Ogre::Singleton<HotBar>::ms_Singleton = 0;


HotBar::HotBar()
{
  this->initialise();
} // HotBar::HotBar()


HotBar::~HotBar()
{
  this->release();
} // HotBar::~HotBar()


void HotBar::initialise()
{
  this->numHotSlots = 8;
  this->hotSlots = new AbstractHotSlot*[this->numHotSlots];

  for(int i=0;i<this->numHotSlots;i++)
  {
    this->hotSlots[i] = NULL;
  } // end for
} // void HotBar::initialise()


void HotBar::release()
{
  delete[] this->hotSlots;
} // void HotBar::release()


void HotBar::activateHotSlot(int _index)
{
  if(this->hotSlots[_index])
  {
    this->hotSlots[_index]->activate();
  } // end if
} // void HotBar::activateHotSlot(int _index)


void HotBar::update(double _timeSinceLastFrame)
{
  for(int i=0;i<this->numHotSlots;i++)
  {
    if(this->hotSlots[i])
    {
      this->hotSlots[i]->update(_timeSinceLastFrame);
    } // end if
  } // end for
} // void HotBar::update(double _timeSinceLastFrame)


void HotBar::addToHotBar(InventorySlot* _slot, int _index)
{
  if(this->hotSlots[_index])
  {
    delete this->hotSlots[_index];
    this->hotSlots[_index] = NULL;
  } // end if

  ItemHotSlot* hotSlot = new ItemHotSlot;

  hotSlot->setInventorySlot(_slot);
  hotSlot->setItem((ConsumableItem*)_slot->getItem());
  hotSlot->setIndex(_index);

  this->hotSlots[_index] = hotSlot;

} // void HotBar::addToHotBar(InventorySlot* _slot, int _index)


void HotBar::addToHotBar(Skill* _skill, int _index)
{
  if(this->hotSlots[_index])
  {
    delete this->hotSlots[_index];
    this->hotSlots[_index] = NULL;
  } // end if

  SkillHotSlot* hotSlot = new SkillHotSlot;
  hotSlot->setIndex(_index);
  hotSlot->setSkill(_skill);
  this->hotSlots[_index] = hotSlot;

} // void HotBar::addToHotBar(Skill* _skill, int _index)


void HotBar::removeFromHotBar(int _index)
{
  if(this->hotSlots[_index])
  {
    delete this->hotSlots[_index];
    this->hotSlots[_index] = NULL;
  } // end if
} // void HotBar::removeFromHotBar(int _index)


void HotBar::clearHotBar()
{
  for(int i=0;i<this->numHotSlots;i++)
  {
    this->removeFromHotBar(i);
  } // end for
} // void HotBar::clearHotBar()


AbstractHotSlot** HotBar::getSlots()
{
  return this->hotSlots;
} // AbstractHotSlot** HotBar::getSlots()


int HotBar::getNumHotSlots()
{
  return this->numHotSlots;
} // int HotBar::getNumHotSlots()