#include "../../../PrecompiledHeaders/stdafx.h"
#ifndef ABSTRACT_HOT_SLOT_H
#define ABSTRACT_HOT_SLOT_H

class AbstractHotSlot
{
private:
  double cooldown;
  int index;

protected:
  string type;

public:
  AbstractHotSlot();
  ~AbstractHotSlot();

  virtual bool activate() =0;
  virtual void update(double _timeSinceLastFrame);
  virtual string getImageName() =0;
  double getCooldown();
  int getIndex();
  string getType();

  void setCooldown(double _cooldown);
  void setIndex(int _index);

};

#endif