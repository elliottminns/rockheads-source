#include "stdafx.h"
#include "AbstractHotSlot.h"



AbstractHotSlot::AbstractHotSlot()
{
  type = "";
} // AbstractHotSlot::AbstractHotSlot()


AbstractHotSlot::~AbstractHotSlot()
{
 
} // AbstractHotSlot::~AbstractHotSlot()


void AbstractHotSlot::update(double _timeSinceLastFrame)
{
  
} // void AbstractHotSlot::update(double _timeSinceLastFrame)


double AbstractHotSlot::getCooldown()
{
  return this->cooldown;
} // double AbstractHotSlot::getCooldown()


int AbstractHotSlot::getIndex()
{
  return this->index;
} // int AbstractHotSlot::getIndex()


void AbstractHotSlot::setCooldown(double _cooldown)
{
  this->cooldown = _cooldown;
} // void AbstractHotSlot::setCooldown(double _cooldown)


void AbstractHotSlot::setIndex(int _index)
{
  this->index = _index;
} // void AbstractHotSlot::setIndex(int _index)


string AbstractHotSlot::getType()
{
  return this->type;
} // string AbstractHotSlot::getType()