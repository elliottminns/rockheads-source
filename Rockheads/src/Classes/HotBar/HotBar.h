#include "../../PrecompiledHeaders/stdafx.h"
#ifndef HOT_BAR_H
#define HOT_BAR_H

#include "Ogre.h"

class AbstractHotSlot;
class InventorySlot;
class Skill;

class HotBar : public Ogre::Singleton<HotBar>
{
private:
  int numHotSlots;
  AbstractHotSlot** hotSlots;


public:
  HotBar();
  ~HotBar();

  void initialise();
  void release();
  void activateHotSlot(int _index);
  void update(double _timeSinceLastFrame);

  void addToHotBar(InventorySlot* _slot, int _index);
  void addToHotBar(Skill* _skill, int _index);
  void removeFromHotBar(int _index);
  void clearHotBar();

  AbstractHotSlot** getSlots();
  int getNumHotSlots();


}; 

#endif