#include "stdafx.h"
#include "ItemHotSlot.h"
#include "../../Managers/GUIManager/GUIManager.h"
#include "../HotBar.h"
#include "../../GameObjects/Items/ConsumableItem/ConsumableItem.h"



ItemHotSlot::ItemHotSlot()
{
  this->type = "Item";
} // ItemHotSlot::ItemHotSlot()


ItemHotSlot::~ItemHotSlot()
{
  this->item = NULL;
  this->slot = NULL;
} // ItemHotSlot::~ItemHotSlot()


bool ItemHotSlot::activate()
{
  if(this->slot->getCount() > 0)
  {
    if(this->item)
    {
      if(!this->item->getCombatAble() && Player::getInstance()->getInCombat())
      {
        return false;
      } // end if

      if(item->getEffect())
      {
        item->getEffect()->activate(Player::getInstance(), Player::getInstance());
      } // end if
    } // end if(this->item)

    this->slot->setCount(this->slot->getCount()-1);
    AudioManager::getSingleton().playSound("Heal");

    if(this->slot->getCount() <= 0)
    {
      Player::getInstance()->removeFromInv(this->slot);
      GUIManager::getSingletonPtr()->removeFromHotSlot(this->getIndex());
      HotBar::getSingletonPtr()->removeFromHotBar(this->getIndex());
    } // end if
    else
    {
      GUIManager::getSingletonPtr()->updateItemCount(this->slot);
      GUIManager::getSingletonPtr()->updateHotSlotCount(this->slot, this->getIndex());
    } // end else
  } // end if

  return true;
} // bool ItemHotSlot::activate()


string ItemHotSlot::getImageName()
{
  return this->item->getImageName();
} // string SkillHotSlot::getImageName()


void ItemHotSlot::setInventorySlot(InventorySlot* _slot)
{
  this->slot = _slot;
} // void ItemHotSlot::setInventorySlot(InventorySlot* _slot)


void ItemHotSlot::setItem(ConsumableItem* _item)
{
  this->item = _item;
} // void ItemHotSlot::setItem(ConsumableItem* item)


void ItemHotSlot::setCooldown(double _cooldown)
{
  this->cooldown = _cooldown;
} // void ItemHotSlot::setCooldown(double _cooldown)


InventorySlot* ItemHotSlot::getInventorySlot()
{
  return this->slot;
} // InventorySlot* ItemHotSlot::getInventorySlot()


ConsumableItem* ItemHotSlot::getItem()
{
  return this->item;
} // ConsumableItem* ItemHotSlot::getItem()


double ItemHotSlot::getCooldown()
{
  return this->cooldown;
} // double ItemHotSlot::getCooldown()