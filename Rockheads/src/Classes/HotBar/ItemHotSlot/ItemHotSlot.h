#include "../../../PrecompiledHeaders/stdafx.h"
#ifndef ITEM_HOT_SLOT_H
#define ITEM_HOT_SLOT_H

#include "../AbstractHotSlot/AbstractHotSlot.h"

class ItemHotSlot : public AbstractHotSlot
{
private:
  InventorySlot* slot;
  ConsumableItem* item;
  double cooldown;

public:
  ItemHotSlot();
  ~ItemHotSlot();
  bool activate();
  string getImageName();
  
  void setInventorySlot(InventorySlot* _slot);
  void setItem(ConsumableItem* _item);
  void setCooldown(double _cooldown);

  InventorySlot* getInventorySlot();
  ConsumableItem* getItem();
  double getCooldown();

};

#endif