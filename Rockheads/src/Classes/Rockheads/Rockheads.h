#include "../../PrecompiledHeaders/stdafx.h"
#ifndef ROCKHEADS_H
#define ROCKHEADS_H

#include "../OgreFramework/OgreFramework.h"
#include "../Managers/GameManager/GameManager.h"


class Rockheads
{
public:
	Rockheads();
	~Rockheads();

	void startRockheads();

private:
	GameManager*	gameManager;
};

#endif // #ifndef __Rockheads_h_
