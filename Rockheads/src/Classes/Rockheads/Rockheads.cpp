#include "stdafx.h"
#include "Rockheads.h"

#include "../GameScenes/MenuScene/MenuScene.h"
#include "../GameScenes/GameScene/GameScene.h"
#include "../GameScenes/PauseScene/PauseScene.h"
#include "../GameScenes/NewGameScene/NewGameScene.h"
#include "../GameScenes/SaveGameScene/SaveGameScene.h"
#include "../GameScenes/LoadGameScene/LoadGameScene.h"
#include "../GameScenes/LoadingScene/LoadingScene.h"
#include "../GameScenes/GameOverScene/GameOverScene.h"

Rockheads::Rockheads()
{
	gameManager = NULL;
} // Rockheads::Rockheads()


Rockheads::~Rockheads()
{
  gameManager->beginShutdown();

  delete CollisionManager::getSingletonPtr();
  delete OgreFramework::getSingletonPtr();
} // Rockheads::~Rockheads()


void Rockheads::startRockheads()
{
	new OgreFramework();
	if(!OgreFramework::getSingletonPtr()->initOgre("Rockheads", 0, 0))
  {
		return;
  } // end if

	OgreFramework::getSingletonPtr()->log->logMessage("Game initialized!");

  gameManager = GameManager::getInstance();

  //TestScene::create(gameManager, "TestScene");
	MenuScene::create(gameManager, "MenuScene");
	GameScene::create(gameManager, "GameScene");
  PauseScene::create(gameManager, "PauseScene");
  NewGameScene::create(gameManager, "NewGameScene");
  SaveGameScene::create(gameManager, "SaveGameScene");
  LoadGameScene::create(gameManager, "LoadGameScene");
  LoadingScene::create(gameManager, "LoadingScene");
  GameOverScene::create(gameManager, "GameOverScene");

	gameManager->start(gameManager->findByName("LoadingScene"));
} // void Rockheads::startRockheads()