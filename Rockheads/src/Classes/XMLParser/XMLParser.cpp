#include "stdafx.h"
#include "XMLParser.h"
#include "../GameObjects/Items/Weapon/Weapon.h"
#include "../GameObjects/Items/Armour/Armour.h"
#include "../GameObjects/Items/Item/Item.h"
#include "../GameObjects/Items/CraftingItem/CraftingItem.h"
#include "../GameObjects/Items/ConsumableItem/ConsumableItem.h"
#include "../GameEntities/DynamicEntities/EntityClass/Skill/Skill.h"
#include "../GameEntities/DynamicEntities/Mob/Mob.h"
#include "../GameEntities/DynamicEntities/Mob/Boss/Boss.h"
#include "../GameObjects/DropTable/DropTable.h"
#include "../../Headers/rapidxml.h"
#include "../Questing/Objectives/AbstractObjective/AbstractObjective.h"
#include "../Questing/Objectives/CollectObjective/CollectObjective.h"
#include "../Questing/Objectives/KillObjective/KillObjective.h"
#include "../Questing/Objectives/TimedObjective/TimedObjective.h"
#include "../Questing/Quest/Quest.h"
#include "../Questing/RewardItem/RewardItem.h"
#include "../GameEntities/StaticEntities/NPC/NPC.h"
#include "../GameEntities/StaticEntities/NPC/Vendor/VendorNPC.h"
#include "../Effects/AbstractEffect/AbstractEffect.h"
#include "../Effects/HealEffect/HealEffect.h"
#include "../Effects/HealOverTimeEffect/HealOverTimeEffect.h"
#include "../Effects/RestoreEffect/RestoreEffect.h"
#include "../Effects/DamageOverTimeEffect/DamageOverTimeEffect.h"
#include "../Effects/DirectDamageEffect/DirectDamageEffect.h"
#include "../Effects/DamageShieldEffect/DamageShieldEffect.h"
#include "../Effects/CriticalIncreaseEffect/CriticalIncreaseEffect.h"
#include "../Effects/DefenceIncreaseEffect/DefenceIncreaseEffect.h"
#include "../Effects/InvisibleEffect/InvisibleEffect.h"
#include "../Effects/StunEffect/StunEffect.h"
#include "../GameEntities/DynamicEntities/EntityClass/SkillSet/SkillSet.h"
#include "../Managers/SkillManager/SkillManager.h"
#include "../GameEntities/DynamicEntities/EntityClass/SkillEffect/SkillEffect.h"
#include "../Managers/EffectManager/EffectManager.h"
#include "../GameObjects/Items/Ammo/Ammo.h"
#include "../Managers/GameManager/GameManager.h"
#include "../GameScenes/LoadingScene/LoadingScene.h"

XMLParser* XMLParser::instance = NULL;

XMLParser* XMLParser::getInstance()
{
  if(!instance)
  {
    instance = new XMLParser;
  } // end if

  return instance;

} // XMLParser* XMLParser::getInstance()


vector<Weapon*>* XMLParser::parseWeaponFile(string _fileName)
{
  rapidxml::xml_document<> *doc = new rapidxml::xml_document<>;

  if(!this->openFile(_fileName, doc))
  {
    exit(401);
  } // end if

  vector<char> buffer((istreambuf_iterator<char>(*this->file)), istreambuf_iterator<char>());
  buffer.push_back('\0');

  doc->parse<0>(&buffer[0]);

  string nodeString;
  string attributeString;
  string valueString;
  Weapon* weapon = NULL;
  vector<Weapon*>* weapons = new vector<Weapon*>;

  // Get the first node.
  for(rapidxml::xml_node<> *node = doc->first_node();node; node = node->next_sibling())
  {
    nodeString = node->name();
    if(nodeString == "weapon")
    {
      weapon = new Weapon;
    } // end if
    for(rapidxml::xml_attribute<> *attr = node->first_attribute();attr; attr = attr->next_attribute())
    {
      attributeString = attr->name();
      valueString = attr->value();
      this->weaponToken(attributeString, valueString, weapon);
    } // end for

    weapons->push_back(weapon);

  } // end for

  delete doc;
  doc = NULL;

  delete this->file;
  file = NULL;

  return weapons;

} // void XMLParser::parseFile(vector<string> attributes, vector<sstream> values)


vector<Armour*>* XMLParser::parseArmourFile(string _fileName)
{
  rapidxml::xml_document<> *doc = new rapidxml::xml_document<>;

  if(!this->openFile(_fileName, doc))
  {
    exit(401);
  } // end if

  vector<char> buffer((istreambuf_iterator<char>(*this->file)), istreambuf_iterator<char>());
  buffer.push_back('\0');

  doc->parse<0>(&buffer[0]);

  string nodeString;
  string attributeString;
  string valueString;
  Armour* armour = NULL;
  vector<Armour*>* armours = new vector<Armour*>;

  // Get the first node.
  for(rapidxml::xml_node<> *node = doc->first_node();node; node = node->next_sibling())
  {
    nodeString = node->name();
    if(nodeString == "armour")
    {
      armour = new Armour;
    } // end if
    for(rapidxml::xml_attribute<> *attr = node->first_attribute();attr; attr = attr->next_attribute())
    {
      attributeString = attr->name();
      valueString = attr->value();
      this->armourToken(attributeString, valueString, armour);
    } // end for

    armours->push_back(armour);

  } // end for

  delete doc;
  doc = NULL;

  delete this->file;
  file = NULL;

  return armours;
} // vector<Armour*>* XMLParser::parseArmourFile(string _fileName)


vector<Item*>* XMLParser::parseItemFile(string _fileName)
{
  rapidxml::xml_document<> *doc = new rapidxml::xml_document<>;;

  if(!this->openFile(_fileName, doc))
  {
    exit(401);
  } // end if

  vector<char> buffer((istreambuf_iterator<char>(*this->file)), istreambuf_iterator<char>());
  buffer.push_back('\0');

  doc->parse<0>(&buffer[0]);

  string nodeString;
  string attributeString;
  string valueString;
  Item* item = NULL;
  vector<Item*>* items = new vector<Item*>;

  // Get the first node.
  for(rapidxml::xml_node<> *node = doc->first_node();node; node = node->next_sibling())
  {
    nodeString = node->name();
    if(nodeString == "item")
    {
      item = new Item;
    } // end if
    for(rapidxml::xml_attribute<> *attr = node->first_attribute();attr; attr = attr->next_attribute())
    {
      attributeString = attr->name();
      valueString = attr->value();
      this->itemToken(attributeString, valueString, item);
    } // end for

    items->push_back(item);

  } // end for

  delete doc;
  doc = NULL;

  delete this->file;
  file = NULL;

  return items;
} // vector<Item*>* XMLParser::parseItemFile(string _fileName)


vector<CraftingItem*>* XMLParser::parseCraftingFile(string _fileName)
{
  rapidxml::xml_document<> *doc = new rapidxml::xml_document<>;;

  if(!this->openFile(_fileName, doc))
  {
    exit(401);
  } // end if

  vector<char> buffer((istreambuf_iterator<char>(*this->file)), istreambuf_iterator<char>());
  buffer.push_back('\0');

  doc->parse<0>(&buffer[0]);

  string nodeString;
  string attributeString;
  string valueString;
  CraftingItem* item = NULL;
  vector<CraftingItem*>* items = new vector<CraftingItem*>;

  // Get the first node.
  for(rapidxml::xml_node<> *node = doc->first_node();node; node = node->next_sibling())
  {
    nodeString = node->name();
    if(nodeString == "crafting")
    {
      item = new CraftingItem;
    } // end if
    for(rapidxml::xml_attribute<> *attr = node->first_attribute();attr; attr = attr->next_attribute())
    {
      attributeString = attr->name();
      valueString = attr->value();
      this->craftingToken(attributeString, valueString, item);
    } // end for

    items->push_back(item);

  } // end for

  delete doc;
  doc = NULL;

  delete this->file;
  file = NULL;

  return items;
} // vector<CraftingItem*>* XMLParser::parseCraftingFile(string _fileName)


vector<ConsumableItem*>* XMLParser::parseConsumableFile(string _fileName)
{
  rapidxml::xml_document<> *doc = new rapidxml::xml_document<>;;

  if(!this->openFile(_fileName, doc))
  {
    exit(401);
  } // end if

  vector<char> buffer((istreambuf_iterator<char>(*this->file)), istreambuf_iterator<char>());
  buffer.push_back('\0');

  doc->parse<0>(&buffer[0]);

  string nodeString;
  string attributeString;
  string valueString;
  ConsumableItem* item = NULL;
  vector<ConsumableItem*>* items = new vector<ConsumableItem*>;

  // Get the first node.
  for(rapidxml::xml_node<> *node = doc->first_node();node; node = node->next_sibling())
  {
    nodeString = node->name();
    if(nodeString == "consumable")
    {
      item = new ConsumableItem;
    } // end if
    for(rapidxml::xml_attribute<> *attr = node->first_attribute();attr; attr = attr->next_attribute())
    {
      attributeString = attr->name();
      valueString = attr->value();
      this->consumableToken(attributeString, valueString, item);
    } // end for

    items->push_back(item);

  } // end for

  delete doc;
  doc = NULL;

  delete this->file;
  file = NULL;

  return items;
} // vector<ConsumableItem*>* XMLParser::parseConsumableFile(string _filename)


vector<Skill*>* XMLParser::parseSkillFile(string _fileName)
{
  rapidxml::xml_document<> *doc = new rapidxml::xml_document<>;

  if(!this->openFile(_fileName, doc))
  {
    exit(401);
  } // end if

  vector<char> buffer((istreambuf_iterator<char>(*this->file)), istreambuf_iterator<char>());
  buffer.push_back('\0');

  doc->parse<0>(&buffer[0]);

  string nodeString;
  string attributeString;
  string valueString;

  Skill* skill = NULL;
  vector<Skill*>* skills = new vector<Skill*>;

  // Get the first node.
  for(rapidxml::xml_node<> *node = doc->first_node();node; node = node->next_sibling())
  {
    SkillEffect* skillEffect = NULL;
    vector<SkillEffect*>* skillEffects = new vector<SkillEffect*>;

    nodeString = node->name();
    if(nodeString == "skill")
    {
      skill = new Skill;
    } // end if
    for(rapidxml::xml_attribute<> *attr = node->first_attribute();attr; attr = attr->next_attribute())
    {
      attributeString = attr->name();
      valueString = attr->value();
      this->skillToken(attributeString, valueString, skill);
    } // end for

    for(rapidxml::xml_node<> *childNode = node->first_node(); childNode; childNode=childNode->next_sibling())
    {
      nodeString = childNode->name();
      if(nodeString == "skillEffect")
      {
        skillEffect = new SkillEffect;
      } // end if
      for(rapidxml::xml_attribute<> *attr = childNode->first_attribute();attr; attr = attr->next_attribute())
      {
        attributeString = attr->name();
        valueString = attr->value();
        this->skillEffectToken(attributeString, valueString, skillEffect);
      } // end for

      skillEffects->push_back(skillEffect);
    } // end for
    
    skill->setEffects(skillEffects);
    skills->push_back(skill);

  } // end for

  delete doc;
  doc = NULL;

  delete this->file;
  file = NULL;

  return skills;
} // vector<Skill*>* XMLParser::parseSkillFile(string _fileName)


vector<Mob*>* XMLParser::parseMobFile(string _fileName)
{
  rapidxml::xml_document<> *doc = new rapidxml::xml_document<>;;

  if(!this->openFile(_fileName, doc))
  {
    exit(401);
  } // end if

  vector<char> buffer((istreambuf_iterator<char>(*this->file)), istreambuf_iterator<char>());
  buffer.push_back('\0');

  doc->parse<0>(&buffer[0]);

  string nodeString;
  string attributeString;
  string valueString;

  Mob* mob = NULL;
  vector<Mob*>* mobs = new vector<Mob*>;

  // Get the first node.
  for(rapidxml::xml_node<> *node = doc->first_node();node; node = node->next_sibling())
  {
    nodeString = node->name();
    if(nodeString == "mob")
    {
      mob = new Mob;
    } // end if
    for(rapidxml::xml_attribute<> *attr = node->first_attribute();attr; attr = attr->next_attribute())
    {
      attributeString = attr->name();
      valueString = attr->value();
      this->mobToken(attributeString, valueString, mob);
    } // end for

    mobs->push_back(mob);

  } // end for

  delete doc;
  doc = NULL;

  delete this->file;
  file = NULL;

  return mobs;
} // vector<Mob*>* XMLParser::parseMobFile(string _fileName)


vector<Boss*>* XMLParser::parseBossFile(string _fileName)
{
  rapidxml::xml_document<> *doc = new rapidxml::xml_document<>;;

  if(!this->openFile(_fileName, doc))
  {
    exit(401);
  } // end if

  string nodeString;
  string attributeString;
  string valueString;

  vector<char> buffer((istreambuf_iterator<char>(*this->file)), istreambuf_iterator<char>());
  buffer.push_back('\0');

  doc->parse<0>(&buffer[0]);

  Boss* boss = NULL;
  vector<Boss*>* bosses = new vector<Boss*>;

  // Get the first node.
  for(rapidxml::xml_node<> *node = doc->first_node();node; node = node->next_sibling())
  {
    nodeString = node->name();
    if(nodeString == "boss")
    {
      boss = new Boss;
    } // end if
    for(rapidxml::xml_attribute<> *attr = node->first_attribute();attr; attr = attr->next_attribute())
    {
      attributeString = attr->name();
      valueString = attr->value();
      //this->mobToken(attributeString, valueString, boss);
      this->bossToken(attributeString, valueString, boss);
    } // end for

    bosses->push_back(boss);

  } // end for

  delete doc;
  doc = NULL;

  delete this->file;
  file = NULL;

  return bosses;
} // vector<Boss*>* XMLParser::parseBossFile(string _fileName)


vector<DropTable*>* XMLParser::parseDropTableFile(string _fileName)
{
  rapidxml::xml_document<> *doc = new rapidxml::xml_document<>;;

  if(!this->openFile(_fileName, doc))
  {
    exit(401);
  } // end if

  string nodeString;
  string attributeString;
  string valueString;

  vector<char> buffer((istreambuf_iterator<char>(*this->file)), istreambuf_iterator<char>());
  buffer.push_back('\0');

  doc->parse<0>(&buffer[0]);

  DropTable* dropTable = NULL;
  Drop* drop = NULL;
  vector<DropTable*>* dropTables = new vector<DropTable*>;

  // Get the first node.
  for(rapidxml::xml_node<> *node = doc->first_node();node; node = node->next_sibling())
  {
    nodeString = node->name();
    vector<Drop*>* drops = new vector<Drop*>;

    if(nodeString == "dropTable")
    {
      dropTable = new DropTable;
    } // end if
    for(rapidxml::xml_attribute<> *attr = node->first_attribute();attr; attr = attr->next_attribute())
    {
      attributeString = attr->name();
      valueString = attr->value();
      this->dropTableToken(attributeString, valueString, dropTable);
    } // end for

    for(rapidxml::xml_node<> *childNode = node->first_node(); childNode; childNode=childNode->next_sibling())
    {
      nodeString = childNode->name();
      if(nodeString == "drop")
      {
        drop = new Drop;
      } // end if
      for(rapidxml::xml_attribute<> *attr = childNode->first_attribute();attr; attr = attr->next_attribute())
      {
        attributeString = attr->name();
        valueString = attr->value();
        this->dropToken(attributeString, valueString, drop);
      } // end for

      drops->push_back(drop);

    } // end for(drops)

    dropTable->setDrops(drops);
    dropTables->push_back(dropTable);

  } // end for(droptables)

  delete doc;
  doc = NULL;

  delete this->file;
  file = NULL;

  return dropTables;

} // vector<DropTable*>* XMLParser::parseDropTableFile(string _fileName)


vector<Quest*>* XMLParser::parseQuestFile(string _fileName)
{
  rapidxml::xml_document<> *doc = new rapidxml::xml_document<>;;

  if(!this->openFile(_fileName, doc))
  {
    exit(401);
  } // end if

  string nodeString;
  string attributeString;
  string valueString;

  vector<char> buffer((istreambuf_iterator<char>(*this->file)), istreambuf_iterator<char>());
  buffer.push_back('\0');

  doc->parse<0>(&buffer[0]);

  Quest* quest = NULL;
  AbstractObjective* objective = NULL;
  RewardItem* reward = NULL;
  vector<Quest*>* quests = new vector<Quest*>;

  // Get the first node.
  for(rapidxml::xml_node<> *node = doc->first_node();node; node = node->next_sibling())
  {
    // Top level.
    nodeString = node->name();
    vector<AbstractObjective*>* objectives = new vector<AbstractObjective*>;
    vector<RewardItem*>*  rewards = new vector<RewardItem*>;

    if(nodeString == "quest")
    {
      quest = new Quest;
    } // end if
    
    // Get the values of the quest.
    for(rapidxml::xml_attribute<> *attr = node->first_attribute();attr; attr = attr->next_attribute())
    {
      attributeString = attr->name();
      valueString = attr->value();
      this->questToken(attributeString, valueString, quest);
    } // end for

    for(rapidxml::xml_node<> *childNode = node->first_node(); childNode; childNode=childNode->next_sibling())
    {
      nodeString = childNode->name();
      if(nodeString == "objective")
      {
        objective = NULL;
        string objType = childNode->first_attribute()->value();
        
        for(rapidxml::xml_attribute<> *attr = childNode->first_attribute();attr; attr = attr->next_attribute())
        {
          attributeString = attr->name();
          valueString = attr->value();
          
          if(objType == "kill")
          {
            if(!objective)
            {
              objective = new KillObjective;
              objective->setType(objType);
            } // end if
            this->killObjectiveToken(attributeString, valueString, (KillObjective*)objective);
          } // end if
          if(objType == "collect")
          {
            if(!objective)
            {
              objective = new CollectObjective;
              objective->setType(objType);
            } // end if
            this->collectObjectiveToken(attributeString, valueString, (CollectObjective*)objective);
          } // end if
          if(objType == "timed")
          {
            if(!objective)
            {
              objective = new TimedObjective;
              objective->setType(objType);
            } // end if

            this->timedObjectiveToken(attributeString, valueString, (TimedObjective*)objective);
          } // end if
        } // end for

        // Now push the objective onto the objectives.
        objectives->push_back(objective);

      } // end if
      if(nodeString == "reward")
      {
        reward = new RewardItem;
        for(rapidxml::xml_attribute<> *attr = childNode->first_attribute();attr; attr = attr->next_attribute())
        {
          attributeString = attr->name();
          valueString = attr->value();
          this->rewardToken(attributeString, valueString, reward);
        } // end for

        rewards->push_back(reward);
      } // end if
    } // end for(internal)

    quest->setRewards(rewards);
    quest->setObjectives(objectives);
    
    quests->push_back(quest);

  } // end for(rapidxml::xml_node<> *node = doc->first_node();node; node = node->next_sibling())
  
  delete doc;
  doc = NULL;

  delete this->file;
  file = NULL;

  return quests;

} // vector<Quest*>* XMLParser::parseQuestFile(string _fileName)


vector<NPC*>* XMLParser::parseNPCFile(string _fileName)
{
  rapidxml::xml_document<> *doc = new rapidxml::xml_document<>;;

  if(!this->openFile(_fileName, doc))
  {
    exit(401);
  } // end if

  string nodeString;
  string attributeString;
  string valueString;

  vector<char> buffer((istreambuf_iterator<char>(*this->file)), istreambuf_iterator<char>());
  buffer.push_back('\0');

  doc->parse<0>(&buffer[0]);

  NPC* npc = NULL;
  vector<NPC*>* npcs = new vector<NPC*>;

  // Get the first node.
  for(rapidxml::xml_node<> *node = doc->first_node();node; node = node->next_sibling())
  {
    // Top level.
    nodeString = node->name();

    if(nodeString == "npc")
    {
      npc = new NPC;
    } // end if
    else if(nodeString == "vendor")
    {
      npc = new VendorNPC;
    } // end else if
    
    // Get the values of the quest.
    for(rapidxml::xml_attribute<> *attr = node->first_attribute();attr; attr = attr->next_attribute())
    {
      attributeString = attr->name();
      valueString = attr->value();
      this->NPCToken(attributeString, valueString, npc);
    } // end for

    npcs->push_back(npc);

  } // end for

  delete doc;
  doc = NULL;

  delete this->file;
  file = NULL;

  return npcs;

} // vector<NPC*>* XMLParser::parseNPCFile(string _fileName)


vector<AbstractEffect*>* XMLParser::parseEffectFile(string _fileName)
{
  rapidxml::xml_document<> *doc = new rapidxml::xml_document<>;;

  if(!this->openFile(_fileName, doc))
  {
    exit(401);
  } // end if

  string nodeString;
  string attributeString;
  string valueString;

  vector<char> buffer((istreambuf_iterator<char>(*this->file)), istreambuf_iterator<char>());
  buffer.push_back('\0');

  doc->parse<0>(&buffer[0]);

  AbstractEffect* effect = NULL;
  vector<AbstractEffect*>* effects = new vector<AbstractEffect*>;
  
  // Get the first node.
  for(rapidxml::xml_node<> *node = doc->first_node();node; node = node->next_sibling())
  {
    // Top level.
    nodeString = node->name();
    string type = nodeString;

    // Check the nodeName and create the effect depending.
    if(nodeString == "heal")
    {
      effect = new HealEffect;
      effect->setType("heal");
    } // end else if
    else if(nodeString == "hot")
    {
      effect = new HealOverTimeEffect;
      effect->setType("hot");
    } // end else if
    else if(nodeString == "restore")
    {
      effect = new RestoreEffect;
      effect->setType("restore");
    } // end else if
    else if(nodeString == "dot")
    {
      effect = new DamageOverTimeEffect;
      effect->setType("dot");
    } // end else if
    else if(nodeString == "damageShield")
    {
      effect = new DamageShieldEffect;
      effect->setType("damageShield");
    } // end else if
    else if(nodeString == "dd")
    {
      effect = new DirectDamageEffect;
      effect->setType("dd");
    } // end else if
    else if(nodeString == "defenceIncrease")
    {
      effect = new DefenceIncreaseEffect;
      effect->setType("defenceIncrease");
    } // end else if
    else if(nodeString == "criticalIncrease")
    {
      effect = new CriticalIncreaseEffect;
      effect->setType("criticalIncrease");
    } // end else if
    else if(nodeString == "stun")
    {
      effect = new StunEffect;
      effect->setType("stun");
    } // end else if
    else if(nodeString == "invis")
    {
      effect = new InvisibleEffect;
      effect->setType("invis");
    } // end else if

    // Get the values of the effect.
    for(rapidxml::xml_attribute<> *attr = node->first_attribute();attr; attr = attr->next_attribute())
    {
      attributeString = attr->name();
      valueString = attr->value();
      this->effectToken(attributeString, valueString, effect, type);
    } // end for

    effects->push_back(effect);

  } // end for

  delete doc;
  doc = NULL;

  delete this->file;
  file = NULL;

  return effects;
} // vector<AbstractEffect*>* XMLParser::parseEffectFile(string _fileName)


vector<SkillSet*>* XMLParser::parseSkillSetFile(string _fileName)
{
  rapidxml::xml_document<> *doc = new rapidxml::xml_document<>;;

  if(!this->openFile(_fileName, doc))
  {
    exit(401);
  } // end if

  string nodeString;
  string attributeString;
  string valueString;

  vector<char> buffer((istreambuf_iterator<char>(*this->file)), istreambuf_iterator<char>());
  buffer.push_back('\0');

  doc->parse<0>(&buffer[0]);

  SkillSet* skillSet = NULL;
  vector<SkillSet*>* skillSets = new vector<SkillSet*>;
  
  // Get the first node.
  for(rapidxml::xml_node<> *node = doc->first_node();node; node = node->next_sibling())
  {
    // Top level.
    nodeString = node->name();
    Skill* skill = NULL;
    vector<Skill*>* skills = new vector<Skill*>;

    if(nodeString == "skillSet")
    {
      skillSet = new SkillSet;
    } // end if

    // Get the attributes of the first node.
    for(rapidxml::xml_attribute<> *attr = node->first_attribute();attr; attr = attr->next_attribute())
    {
      attributeString = attr->name();
      valueString = attr->value();
      this->skillSetToken(attributeString, valueString, skillSet);
    } // end for

    // Get the children nodes.
    for(rapidxml::xml_node<> *childNode = node->first_node(); childNode; childNode=childNode->next_sibling())
    {
      nodeString = childNode->name(); 

      if(nodeString == "skill")
      {
        rapidxml::xml_attribute<> *attr = childNode->first_attribute();
        string attrName = attr->name();
        if(attrName == "name")
        {
          valueString = attr->value();
          skill = SkillManager::getInstance()->createSkill(valueString, 1);
          skills->push_back(skill);
        } // end if
      } // end if

    } // end for

    skillSet->setSkills(skills);
    skillSets->push_back(skillSet);

  } // end for

  delete doc;
  doc = NULL;

  delete this->file;
  file = NULL;
  
  return skillSets;

} // vector<SkillSet*>* XMLParser::parseSkillSetFile(string _fileName)


map<string,string>* XMLParser::parseParticleFile(string _fileName)
{
  rapidxml::xml_document<> *doc = new rapidxml::xml_document<>;;

  if(!this->openFile(_fileName, doc))
  {
    exit(401);
  } // end if

  string nodeString;
  string attributeString;
  string valueString;

  vector<char> buffer((istreambuf_iterator<char>(*this->file)), istreambuf_iterator<char>());
  buffer.push_back('\0');

  doc->parse<0>(&buffer[0]);

  typedef map<string,string> ParticleMap;
  ParticleMap* particles = new ParticleMap;
  
  // Get the first node.
  for(rapidxml::xml_node<> *node = doc->first_node();node; node = node->next_sibling())
  {
    // Top level.
    nodeString = node->name();
    string name = "";
    string system = "";

    // Get the attributes of the node.
    for(rapidxml::xml_attribute<> *attr = node->first_attribute();attr; attr = attr->next_attribute())
    {
      attributeString = attr->name();
      valueString = attr->value();
      if(attributeString == "name")
      {
        name = valueString;
      } // end if
      else if(attributeString == "system")
      {
        system = valueString;
      } // end else if

    } // end for

    string str = string(valueString);
    boost::to_lower(str);
    particles->insert(ParticleMap::value_type(name, system)).second;

  } // end for

  delete doc;
  doc = NULL;

  delete this->file;
  file = NULL;

  return particles;

} // map<string,string>* XMLParser::parseParticleFile(string _file)


vector<Ammo*>* XMLParser::parseAmmoFile(string _fileName)
{
  rapidxml::xml_document<> *doc = new rapidxml::xml_document<>;;

  if(!this->openFile(_fileName, doc))
  {
    exit(401);
  } // end if

  vector<char> buffer((istreambuf_iterator<char>(*this->file)), istreambuf_iterator<char>());
  buffer.push_back('\0');

  doc->parse<0>(&buffer[0]);

  string nodeString;
  string attributeString;
  string valueString;
  Ammo* ammo = NULL;
  vector<Ammo*>* ammos = new vector<Ammo*>;

  // Get the first node.
  for(rapidxml::xml_node<> *node = doc->first_node();node; node = node->next_sibling())
  {
    nodeString = node->name();
    if(nodeString == "ammo")
    {
      ammo = new Ammo;
    } // end if
    for(rapidxml::xml_attribute<> *attr = node->first_attribute();attr; attr = attr->next_attribute())
    {
      attributeString = attr->name();
      valueString = attr->value();
      this->ammoToken(attributeString, valueString, ammo);
    } // end for

    ammos->push_back(ammo);

  } // end for

  delete doc;
  doc = NULL;

  delete this->file;
  file = NULL;

  return ammos;
} // vector<Ammo*>* XMLParser::parseAmmoFile(string _file)


vector<OgreAL::Sound*>* XMLParser::parseSoundFile(string _fileName)
{
  rapidxml::xml_document<> *doc = new rapidxml::xml_document<>;;

  if(!this->openFile(_fileName, doc))
  {
    exit(401);
  } // end if

  vector<char> buffer((istreambuf_iterator<char>(*this->file)), istreambuf_iterator<char>());
  buffer.push_back('\0');

  doc->parse<0>(&buffer[0]);

  string nodeString;
  string attributeString;
  string valueString;
  OgreAL::Sound* sound = NULL;
  vector<OgreAL::Sound*>* sounds = new vector<OgreAL::Sound*>;

  int numNodes = 0;
  // First loop through and determine how many nodes there are.
  for(rapidxml::xml_node<> *node = doc->first_node();node;node=node->next_sibling())
  {
    ++numNodes;
  } // end for

  LoadingScene* loadingScene = (LoadingScene*)GameManager::getInstance()->getCurrentScene();

  double progressBarRemainder = 1.0 - loadingScene->getLoadingBarProgress();
  
  // Work out a slice for each node.
  double slice = progressBarRemainder/numNodes;


  // Get the first node.
  for(rapidxml::xml_node<> *node = doc->first_node();node; node = node->next_sibling())
  {
    nodeString = node->name();
    string soundName = this->getAttrib(node, "name");
    string fileName  = this->getAttrib(node, "file");
    bool loop = this->getAttribBool(node, "loop");
    double gain = this->getAttribReal(node, "gain", 1.0f);
    loadingScene->addToLoadingBar(slice);

    sound = OgreAL::SoundManager::getSingleton().createSound(soundName, fileName, loop);
    sound->setGain(gain);
    sounds->push_back(sound);

  } // end for

  delete doc;
  doc = NULL;

  delete this->file;
  file = NULL;

  return sounds;

} // vector<OgreAL::Sound*>* XMLParser::parseSoundFile(string _fileName)


bool XMLParser::openFile(string _fileName, rapidxml::xml_document<> *_doc)
{
  this->file = new ifstream(_fileName);

  return this->file->is_open();
} // bool XMLParser::openFile(string _fileName)


void XMLParser::weaponToken(string _attribute, string _value, Weapon* _weapon)
{
  stringstream value;
  value << _value;
  if(_attribute == "name")
  {
    _weapon->setName(_value);
  } // end if
  else if(_attribute == "type")
  {
    _weapon->setWieldType(_value);
  } // end else if
  else if(_attribute == "from")
  {
    vector<string>* from = _weapon->getVectorFrom();
    from->push_back(_value);
  } // end else if
  else if(_attribute == "minDamage")
  {
    int minDamage;
    value >> minDamage;
    _weapon->setMinimumDamage(minDamage);
  } // end else if
  else if(_attribute == "maxDamage")
  {
    int maxDamage;
    value >> maxDamage;
    _weapon->setMaximumDamage(maxDamage);
  } // end else if
  else if(_attribute == "cooldown")
  {
    double cooldown;
    value >> cooldown;
    _weapon->setCooldown(cooldown);
  } // end else if
  else if(_attribute == "value")
  {
    int moneyValue;
    value >> moneyValue;
    _weapon->setValue(moneyValue);
  } // end else if
  else if(_attribute == "modelName")
  {
    _weapon->setModelName(_value);
  } // end if
  else if(_attribute == "imageName")
  {
    _weapon->setImageName(_value);
  } // end if
  else if(_attribute == "stackable")
  {
    bool stackable = false;
    if(_value == "true")
    {
      stackable = true;
    } // end if
    _weapon->setStackable(stackable);
  } // end else if
  else if(_attribute == "animationType")
  {
    _weapon->setAnimationType(_value);
  } // end else if
  else if(_attribute == "range")
  {
    int range = 0;
    value >> range;
    _weapon->setRange(range);
  } // end else if
  else if(_attribute == "hand")
  {
    _weapon->setHanded(_value);
  } // end else if
  else if(_attribute == "timing")
  {
    double timing = 0.6;
    value >> timing;
    _weapon->setTiming(timing);
  } // end else if
  else if(_attribute == "requiresAmmo")
  {
    bool requires = false;
    if(_value == "true")
    {
      requires = true;
    } // end if

    _weapon->setRequiresAmmo(requires);

  } // end else if
  else if(_attribute == "ammo")
  {
    _weapon->addAmmoName(_value);
  } // end else if
} // void XMLParser::weaponToken()


void XMLParser::armourToken(string _attribute, string _value, Armour* _armour)
{
  stringstream value;
  value << _value;
  if(_attribute == "name")
  {
    _armour->setName(_value);
  } // end if
  else if(_attribute == "type")
  {
    _armour->setType(_value);
  } // end if
  else if(_attribute == "from")
  {
    vector<string>* from = _armour->getVectorFrom();
    from->push_back(_value);
  } // end if
  else if(_attribute == "area")
  {
    _armour->setArea(_value);
  } // end if
  else if(_attribute == "defence")
  {
    int defence;
    value >> defence;
    _armour->setDefence(defence);
  } // end if
  else if(_attribute == "value")
  {
    int moneyValue;
    value >> moneyValue;
    _armour->setValue(moneyValue);
  } // end if
  else if(_attribute == "imageName")
  {
    _armour->setImageName(_value);
  } // end if
  else if(_attribute == "stackable")
  {
    bool stackable = false;
    if(_value == "true")
    {
      stackable = true;
    }
    _armour->setStackable(stackable);
  } // end if
} // void XMLParser::armourToken(string _attribute, string _value, Armour* _armour)


void XMLParser::itemToken(string _attribute, string _value, Item* _item)
{
  stringstream value;
  value << _value;

  if(_attribute == "name")
  {
    _item->setName(_value);
  } // end if
  else if(_attribute == "from")
  {
    _item->getVectorFrom()->push_back(_value);
  } // end if
  else if(_attribute == "value")
  {
    int moneyValue;
    value >> moneyValue;
    _item->setValue(moneyValue);
  } // end if
  else if(_attribute == "imageName")
  {
    _item->setImageName(_value);
  } // end if
  else if(_attribute == "stackable")
  {
    bool stackable = false;
    if(_value == "true")
    {
      stackable = true;
    } // end if
    _item->setStackable(stackable);
  } // end if

} // void XMLParser::itemToken(string _attribute, string _value, Item* _item)


void XMLParser::craftingToken(string _attribute, string _value, CraftingItem* _item)
{
  stringstream value;
  value << _value;

  if(_attribute == "name")
  {
    _item->setName(_value);
  } // end if
  else if(_attribute == "from")
  {
    _item->getVectorFrom()->push_back(_value);
  } // end if
  else if(_attribute == "value")
  {
    int moneyValue;
    value >> moneyValue;
    _item->setValue(moneyValue);
  } // end if
  else if(_attribute == "imageName")
  {
    _item->setImageName(_value);
  } // end if
  else if(_attribute == "stackable")
  {
    bool stackable = false;
    if(_value == "true")
    {
      stackable = true;
    } // end if
    _item->setStackable(stackable);
  } // end if
  else if(_attribute == "description")
  {
    _item->setFileDescription(_value);
  } // end else if
} // void XMLParser::craftingToken(string _attribute, string _value, CraftingItem* _item)


void XMLParser::consumableToken(string _attribute, string _value, ConsumableItem* _item)
{
  stringstream value;
  value << _value;

  if(_attribute == "name")
  {
    _item->setName(_value);
  } // end if
  else if(_attribute == "effect")
  {
    _item->setEffectName(_value);
  } // end if
  else if(_attribute == "effectLevel")
  {

  } // end if
  else if(_attribute == "from")
  {
    _item->getVectorFrom()->push_back(_value);
  } // end if
  else if(_attribute == "value")
  {
    int moneyValue;
    value >> moneyValue;
    _item->setValue(moneyValue);
  } // end if
  else if(_attribute == "imageName")
  {
    _item->setImageName(_value);
  } // end if
  else if(_attribute == "stackable")
  {
    bool stackable = false;
    if(_value == "true")
    {
      stackable = true;
    } // end if
    _item->setStackable(stackable);
  } // end if
  else if(_attribute == "hotSlotAble")
  {
    bool able = false;

    if(_value == "true")
    {
      able = true;
    } // end if

    _item->setHotSlotAble(able);

  } // end else if
  else if(_attribute == "combatAble")
  {
    bool able = true;

    if(_value == "false")
    {
      able = false;
    } // end if

    _item->setCombatAble(able);

  } // end else if
} // oid XMLParser::consumableToken(string _attribute, string _value, ConsumableItem* _item)


void XMLParser::skillToken(string _attribute, string _value, Skill* _skill)
{
  stringstream value;
  value << _value;

  if(_attribute == "name")
  {
    _skill->setName(_value);
  } // end else if
  else if(_attribute == "description")
  {
    _skill->setDescription(_value);
  } // end else if
  else if(_attribute == "cooldown")
  {
    int cooldown;
    value >> cooldown;
    _skill->setCooldown(cooldown);
  } // end else if
  else if(_attribute == "level")
  {
    int level = 0;
    value >> level;
    _skill->setLevel(level);
  } // end else if
  else if(_attribute == "stamina")
  {
    int stamina = 0;
    value >> stamina;
    _skill->setStaminaCost(stamina);
  } // end else if
  else if(_attribute == "reagent")
  {
    _skill->setRequiredReagentName(_value);
    _skill->setReagentRequired(true);
  } // end else if
  else if(_attribute == "requires")
  {
    _skill->setEffectRequired(true);
    _skill->setRequiredEffectName(_value);
  } // end else if
  else if(_attribute == "imageName")
  {
    _skill->setImageName(_value);
  } // end else if
  else if(_attribute == "target")
  {
    _skill->setTargetType(_value);
  } // end else if
  else if(_attribute == "animation")
  {
    _skill->setAnimationType(_value);
  } // end else if
} // void XMLParser::skillToken(string _attribute, string _value, Skill* _skill)


void XMLParser::mobToken(string _attribute, string _value, Mob* _mob)
{
  stringstream value;
  value << _value;

  if(_attribute == "name")
  {
    _mob->setName(_value);
  } // end else if
  else if(_attribute == "level")
  {
    int level;
    value >> level;
    _mob->setLevel(level);
  } // end else if
  else if(_attribute == "class")
  {
    _mob->setClassType(_value);
  } // end else if
  else if(_attribute == "strength")
  {
    int str;
    value >> str;
    _mob->setStrength(str);
  } // end else if
  else if(_attribute == "dexterity")
  {
    int dex;
    value >> dex;
    _mob->setDexterity(dex);
  } // end else if
  else if(_attribute == "agility")
  {
    int agi;
    value >> agi;
    _mob->setAgility(agi);
  } // end else if
  else if(_attribute == "constitution")
  {
    int con;
    value >> con;
    _mob->setConstitution(con);
  } // end else if
  else if(_attribute == "intelligence")
  {
    int intelli;
    value >> intelli;
    _mob->setIntelligence(intelli);
  } // end else if
  else if(_attribute == "wisdom")
  {
    int wis;
    value >> wis;
    _mob->setWisdom(wis);
  } // end else if
  else if(_attribute == "hp")
  {
    int hp;
    value >> hp;
    _mob->setMaxHP(hp);
  } // end else if
  else if(_attribute == "stamina")
  {
    int sta;
    value >> sta;
    _mob->setMaxStamina(sta);
  } // end else if
  else if(_attribute == "model")
  {
    _mob->setModelName(_value);
  } // else end if
  else if(_attribute == "aggressive")
  {
    bool aggressive = false;
    if(_value == "true")
    {
      aggressive = true;
    } // end if
    _mob->setAggressive(aggressive);
  } // else if(_attribute == "aggressive") 
  else if(_attribute == "scale")
  {
    double scale = 1.0;
    value >> scale;

    _mob->setScale(scale);
  } // end else if
  else if(_attribute == "minWeapon")
  {
    int weaponValue = 0;
    value >> weaponValue;
    _mob->setWeaponMin(weaponValue);
  } // end else if
  else if(_attribute == "maxWeapon")
  {
    int weaponValue = 0;
    value >> weaponValue;
    _mob->setWeaponMin(weaponValue);
  } // end else if
  else if(_attribute == "armour")
  {
    int armour = 0;
    value >> armour;
    _mob->setArmourDefence(armour);
  } // end else if

} // void XMLParser::mobToken(string _attribute, string _value, Mob* _mob)


void XMLParser::bossToken(string _attribute, string _value, Boss* _boss)
{
  stringstream value;
  value << _value;

  if(_attribute == "name")
  {
    _boss->setName(_value);
  } // end if
  else if(_attribute == "level")
  {
    int level;
    value >> level;
    _boss->setLevel(level);
  } // end else if
  else if(_attribute == "class")
  {
    _boss->setClassType(_value);
  } // end else if
  else if(_attribute == "class2")
  {
    _boss->setSecondClass(_value);
  } // end else if
  else if(_attribute == "strength")
  {
    int str;
    value >> str;
    _boss->setStrength(str);
  } // end else if
  else if(_attribute == "dexterity")
  {
    int dex;
    value >> dex;
    _boss->setDexterity(dex);
  } // end else if
  else if(_attribute == "agility")
  {
    int agi;
    value >> agi;
    _boss->setAgility(agi);
  } // end else if
  else if(_attribute == "constitution")
  {
    int con;
    value >> con;
    _boss->setConstitution(con);
  } // end else if
  else if(_attribute == "intelligence")
  {
    int intelli;
    value >> intelli;
    _boss->setIntelligence(intelli);
  } // end else if
  else if(_attribute == "wisdom")
  {
    int wis;
    value >> wis;
    _boss->setWisdom(wis);
  } // end else if
  else if(_attribute == "hp")
  {
    int hp;
    value >> hp;
    _boss->setMaxHP(hp);
  } // end else if
  else if(_attribute == "stamina")
  {
    int sta;
    value >> sta;
    _boss->setMaxStamina(sta);
  } // end else if

  else if(_attribute == "model")
  {
    _boss->setModelName(_value);
  } // end else if
  else if(_attribute == "aggressive")
  {
    bool aggressive = false;
    if(_value == "true")
    {
      aggressive = true;
    } // end if
    _boss->setAggressive(aggressive);
  } // else if(_attribute == "aggressive")
  else if(_attribute == "scale")
  {
    double scale = 1.0;
    value >> scale;

    _boss->setScale(scale);
  } // end else if

} // void bossToken(string _attribute, string _value, Boss* _boss)


void XMLParser::dropTableToken(string _attribute, string _value, DropTable* _table)
{
  stringstream value;
  value << _value;

  if(_attribute == "name")
  {
    _table->setName(_value);
  } // end else if
  else if(_attribute == "mob")
  {
    _table->addMob(_value);
  } // end else if
  else if(_attribute == "currencyMin")
  {
    int currencyMin = 0;
    value >> currencyMin;
    _table->setCurrencyMin(currencyMin);
  } // end else if
  else if(_attribute == "currencyMax")
  {
    int currencyMax = 0;
    value >> currencyMax;
    _table->setCurrencyMax(currencyMax);
  } // end else if

} // void XMLParser::dropTableToken(string _attribute, string _value, DropTable* _table)


void XMLParser::dropToken(string _attribute, string _value, Drop* _drop)
{
  stringstream value;
  value << _value;

  if(_attribute == "item")
  {
    _drop->setItem(ItemManager::getInstance()->getItem(_value));
  } // end if
  else if(_attribute == "amountMin")
  {
    int amount = 0;
    value >> amount;
    _drop->setAmountMin(amount);
  } // end else if
  else if(_attribute == "amountMax")
  {
    int amount = 0;
    value >> amount;
    _drop->setAmountMax(amount);
  } // end else if
  else if(_attribute == "chance")
  {
    int chance = 0;
    value >> chance;
    _drop->setChance(chance);
  } // end else if

} // void XMLParser::dropTableToken(string _attribute, string _value, Drop* _drop)


void XMLParser::questToken(string _attribute, string _value, Quest* _quest)
{
  stringstream value;
  value << _value;

  if(_attribute == "name")
  {
    _quest->setName(_value);
  } // end if
  else if(_attribute == "openingNPC")
  {
    _quest->setOpeningNPC(_value);
  } // end else if
  else if(_attribute == "closingNPC")
  {
    _quest->setClosingNPC(_value);
  } // end else if
  else if(_attribute == "openingDialogue")
  {
    _quest->setOpeningDialogue(_value);
  } // end else if
  else if(_attribute == "duringDialogue")
  {
    _quest->setDuringDialogue(_value);
  } // end else if
  else if(_attribute == "closingDialogue")
  {
    _quest->setClosingDialogue(_value);
  } // end else if
  else if(_attribute == "currency")
  {
    int currency = 0;
    value >> currency;
    _quest->setRewardCurrency(currency);
  } // end else if
  else if(_attribute == "requiresPrevious")
  {
    bool previous = false;
    if(_value == "true")
    {
       previous = true;
    } // end if

    _quest->setRequiresPrevious(previous);

  } // end else if
  else if(_attribute == "previousInChain")
  {
    _quest->setPreviousInChain(_value);
  } // end else if


} // void XMLParser::questToken(string _attribute, string _value, Quest* _quest)


void XMLParser::killObjectiveToken(string _attribute, string _value, KillObjective* _objective)
{
  stringstream value;
  value << _value;

  if(_attribute == "mob")
  {
    _objective->setMobName(_value);
  } // end if
  else if(_attribute == "count")
  {
    int count = 0;
    value >> count;
    _objective->setAmountNeeded(count);
  } // end else if

} // void XMLParser::objectiveToken(string _attribute, string _value, KillObjective* _objective)


void XMLParser::timedObjectiveToken(string _attribute, string _value, TimedObjective* _objective)
{
  stringstream value;
  value << _value;

  if(_attribute == "timer")
  {
    double time = 0;
    value >> time;
    _objective->setTimeInSeconds(time);
  } // end if
  else if(_attribute == "safePointX")
  {
    int pos = 0;
    value >> pos;
    _objective->setSafePointX(pos);
  } // end else if
  else if(_attribute == "safePointY")
  {
    int pos = 0;
    value >> pos;
    _objective->setSafePointY(pos);
  } // end else if
  else if(_attribute == "safePointZ")
  {
    int pos = 0;
    value >> pos;
    _objective->setSafePointZ(pos);
  } // end else if
  else if(_attribute == "radius")
  {
    double radius = 0;
    value >> radius;
    _objective->setRadius(radius);
  } // else end if

} // void XMLParser::timedObjectiveToken(string _attribute, string _value, TimedObjective* _objective)


void XMLParser::collectObjectiveToken(string _attribute, string _value, CollectObjective* _objective)
{
  stringstream value;
  value << _value;

  if(_attribute == "item")
  {
    _objective->setItemName(_value);
  } // end if
  else if(_attribute == "count")
  {
    int amount = 0;
    value >> amount;
    _objective->setAmountNeeded(amount);
  } // end else if

} // void XMLParser::collectObjectiveToken(string _attribute, string _value, CollectObjective* _objective)


void XMLParser::rewardToken(string _attribute, string _value, RewardItem* _item)
{
  stringstream value;
  value << _value;

  if(_attribute == "item")
  {
    _item->setItemName(_value);
  } // end else if
  else if(_attribute == "count")
  {
    int amount = 0;
    value >> amount;
    _item->setAmount(amount);
  } // end else if

} // void XMLParser::rewardToken(string _attribute, string _value, RewardItem* _item)


void XMLParser::NPCToken(string _attribute, string _value, NPC* _npc)
{
  stringstream value;
  value << _value;

  if(_attribute == "name")
  {
    _npc->setName(_value);
  } // end else if
  else if(_attribute == "quest")
  {
    _npc->setQuestName(_value);
  } // end else if
  else if(_attribute == "model")
  {
    _npc->setModelName(_value);
  } // end else if
  else if(_attribute == "scale")
  {
    double scale = 1.0;
    value >> scale;

    _npc->setScale(scale);
  } // end else if
} // void XMLParser::NPCToken(string _attribute, string _value, NPC* _npc)


void XMLParser::effectToken(string _attribute, string _value, AbstractEffect* _effect, string _type)
{
  stringstream value;
  value << _value;

  if(_attribute == "name")
  {
    _effect->setName(_value);
  } // end if
  else if(_attribute == "level")
  {
    int level = 0;
    value >> level;
    _effect->setLevel(level);
  } // end else if
  else if(_attribute == "healingAmount")
  {
    int healingAmount = 0;
    value >> healingAmount;
    if(_type == "heal")
    {
      static_cast<HealEffect*>(_effect)->setHealingAmount(healingAmount);
    } // end if
    else if(_type == "restore")
    {
      static_cast<RestoreEffect*>(_effect)->setHealingAmount(healingAmount);
    } // end else if
    else if(_type == "hot")
    {
      static_cast<HealOverTimeEffect*>(_effect)->setHealingAmount(healingAmount);
    } // end else if
        
  } // end else if
  else if(_attribute == "damageAmount")
  {
    int damageAmount = 0;
    value >> damageAmount;

    if(_type == "dd")
    {
      static_cast<DirectDamageEffect*>(_effect)->setDamageAmount(damageAmount);
    } // end if
    else if(_type == "dot")
    {
      static_cast<DamageOverTimeEffect*>(_effect)->setDamageAmount(damageAmount);
    } // end else if
    if(_type == "damageShield")
    {
      static_cast<DamageShieldEffect*>(_effect)->setDamageAmount(damageAmount);
    } // end if

  } // end else if
  else if(_attribute == "ignoresMitigation")
  {
    bool ignoresMitigation = (_value == "true");
    
    if(_type == "dot")
    {
      static_cast<DamageOverTimeEffect*>(_effect)->setIgnoresMitigation(ignoresMitigation);
    } // end if
    if(_type == "dd")
    {
      static_cast<DirectDamageEffect*>(_effect)->setIgnoresMitigation(ignoresMitigation);
    } // end if
    if(_type == "damageShield")
    {
      static_cast<DamageShieldEffect*>(_effect)->setIgnoresMitigation(ignoresMitigation);
    } // end if

  } // end else if
  else if(_attribute == "defenceAmount")
  {
    int defence = 0;
    value >> defence;
    if(_type == "defenceIncrease")
    {
      static_cast<DefenceIncreaseEffect*>(_effect)->setDefenceAmount(defence);
    } // end else if
  } // end else if
  else if(_attribute == "criticalAmount")
  {
    int critical = 0;
    value >> critical;
    if(_type == "criticalIncrease")
    {
      static_cast<CriticalIncreaseEffect*>(_effect)->setCriticalIncreaseAmount(critical);
    } // end else if
  } // end else if
  else if(_attribute == "combat")
  {
    bool combat = (_value == "true");
    if(_type == "invis")
    {
      static_cast<InvisibleEffect*>(_effect)->setCombatAble(combat);
    } // end if
  } // end else if
  else if(_attribute == "time")
  {
    double time;
    value >> time;

    if(_type == "hot")
    {
      static_cast<HealOverTimeEffect*>(_effect)->setTime(time);
    } // end if
    else if(_type == "dot")
    {
      static_cast<DamageOverTimeEffect*>(_effect)->setTime(time);
    } // end if
    else if(_type == "damageShield")
    {
      static_cast<DamageShieldEffect*>(_effect)->setTime(time);
    } // end else if
    else if(_type == "defenceIncrease")
    {
      static_cast<DefenceIncreaseEffect*>(_effect)->setTime(time);
    } // end else if
    else if(_type == "criticalIncrease")
    {
      static_cast<CriticalIncreaseEffect*>(_effect)->setTime(time);
    } // end else if
    else if(_type == "stun")
    {
      static_cast<StunEffect*>(_effect)->setTime(time);
    } // end else if
    else if(_type == "invis")
    {
      static_cast<InvisibleEffect*>(_effect)->setTime(time);
    } // end else if
  } // end else if(_attribute == "time")
  else if(_attribute == "image")
  {
    _effect->setImage(_value);
  } // end else if
  else if(_attribute == "callerParticle")
  {
    _effect->setCallerParticle(_value);
  } // end else if
  else if(_attribute == "targetParticle")
  {
    _effect->setTargetParticle(_value);
  } // end else if

} // void XMLParser::effectToken(string _attribute, string_value, AbstractEffect* _effect)


void XMLParser::skillSetToken(string _attribute, string _value, SkillSet* _skillSet)
{
  // Load up a string stream just in case.
  stringstream value;
  value << _value;

  if(_attribute == "name")
  {
    _skillSet->setName(_value);
  } // end if
  else if(_attribute == "type")
  {
    _skillSet->setType(_value);
  } // end else if
  else if(_attribute == "armourType")
  {
    _skillSet->addArmourType(_value);
  } // end else if
  else if(_attribute == "weaponType")
  {
    _skillSet->addWeaponType(_value);
  } // end else if
  else if(_attribute == "description")
  {
    _skillSet->setDescription(_value);
  } // end else if

} // void XMLParser::skillSetToken(string _attribute, string _value, SkillSet* _skillSet)


void XMLParser::skillEffectToken(string _attribute, string _value, SkillEffect* _skillEffect)
{
  // Load up a string stream just in case.
  stringstream value;
  value << _value;

  if(_attribute == "name")
  {
    _skillEffect->setEffect(EffectManager::getSingleton().createEffect(_value));
  } // end if
  else if(_attribute == "chance")
  {
    int chance = 0;
    value >> chance;
    _skillEffect->setChance(chance);
  } // end else if

} // void XMLParser::skillEffectToken(string _attribute, string _value, SkillEffect* _skillEffect)


void XMLParser::ammoToken(string _attribute, string _value, Ammo* _ammo)
{
  stringstream value;
  value << _value;

  if(_attribute == "name")
  {
    _ammo->setName(_value);
  } // end if
  else if(_attribute == "from")
  {
    _ammo->getVectorFrom()->push_back(_value);
  } // end if
  else if(_attribute == "value")
  {
    int moneyValue;
    value >> moneyValue;
    _ammo->setValue(moneyValue);
  } // end if
  else if(_attribute == "imageName")
  {
    _ammo->setImageName(_value);
  } // end if
  else if(_attribute == "stackable")
  {
    bool stackable = false;
    if(_value == "true")
    {
      stackable = true;
    } // end if
    _ammo->setStackable(stackable);
  } // end if
  else if(_attribute == "minDamage")
  {
    int damage = 0;
    value >> damage;
    _ammo->setMinDamage(damage);
  } // end else if
  else if(_attribute == "maxDamage")
  {
    int damage = 0;
    value >> damage;
    _ammo->setMaxDamage(damage);
  } // end else if
  else if(_attribute == "description")
  {
    _ammo->setDescription(_value);
  } // end else if
} // void XMLParser::ammoToken(string _attribute, string _value, Ammo* _ammo)


Ogre::String XMLParser::getAttrib(rapidxml::xml_node<>* XMLNode, const Ogre::String &attrib, const Ogre::String &defaultValue)
{
  if(XMLNode->first_attribute(attrib.c_str()))
  {
    return XMLNode->first_attribute(attrib.c_str())->value();
  } // end if
  else
  {
    return defaultValue;
  } // end else
} // Ogre::String XMLParser::getAttrib(rapidxml::xml_node<>* XMLNode, const Ogre::String &attrib, const Ogre::String &defaultValue)


Ogre::Real XMLParser::getAttribReal(rapidxml::xml_node<>* XMLNode, const Ogre::String &attrib, Ogre::Real defaultValue)
{
  if(XMLNode->first_attribute(attrib.c_str()))
  {
    return Ogre::StringConverter::parseReal(XMLNode->first_attribute(attrib.c_str())->value());
  } // end if
  else
  {
    return defaultValue;
  } // end else
} // Ogre::Real XMLParser::getAttribReal(rapidxml::xml_node<>* XMLNode, const Ogre::String &attrib, Ogre::Real defaultValue)


bool XMLParser::getAttribBool(rapidxml::xml_node<>* XMLNode, const Ogre::String &attrib, bool defaultValue)
{
  if(!XMLNode->first_attribute(attrib.c_str()))
  {
    return defaultValue;
  } // end if

  if(Ogre::String(XMLNode->first_attribute(attrib.c_str())->value()) == "true")
  {
    return true;
  } // end if

  return false;
} // bool XMLParser::getAttribBool(rapidxml::xml_node<>* XMLNode, const Ogre::String &attrib, bool defaultValue)


Ogre::Vector3 XMLParser::parseVector3(rapidxml::xml_node<>* XMLNode)
{
  return Ogre::Vector3(
    Ogre::StringConverter::parseReal(XMLNode->first_attribute("x")->value()),
    Ogre::StringConverter::parseReal(XMLNode->first_attribute("y")->value()),
    Ogre::StringConverter::parseReal(XMLNode->first_attribute("z")->value())
    );
} // Ogre::Vector3 XMLParser::parseVector3(rapidxml::xml_node<>* XMLNode)