#include "../../PrecompiledHeaders/stdafx.h"
#ifndef XML_PARSER_H
#define XML_PARSER_H

#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include <sstream>
#include "../../Headers/rapidxml.h"

using namespace std;

class Weapon;
class Armour;
class Item;
class CraftingItem;
class ConsumableItem;
class Skill;
class Mob;
class Boss;
class DropTable;
class Drop;
class Quest;
class AbstractObjective;
class KillObjective;
class CollectObjective;
class TimedObjective;
class RewardItem;
class NPC;
class AbstractEffect;
class SkillSet;
class SkillEffect;
class Ammo;
class OgreAL::Sound;

class XMLParser
{
private:
  XMLParser(){};
  static XMLParser* instance;
  ifstream* file;


public:
  static XMLParser* getInstance();

  // Parse file methods.
  vector<Weapon*>* parseWeaponFile(string _fileName);
  vector<Armour*>* parseArmourFile(string _fileName);
  vector<Item*>*   parseItemFile(string _fileName);
  vector<CraftingItem*>* parseCraftingFile(string _fileName);
  vector<ConsumableItem*>* parseConsumableFile(string _fileName);
  vector<Skill*>* parseSkillFile(string _fileName);
  vector<Mob*>* parseMobFile(string _fileName);
  vector<Boss*>* parseBossFile(string _fileName);
  vector<DropTable*>* parseDropTableFile(string _fileName);
  vector<Quest*>* parseQuestFile(string _fileName);
  vector<NPC*>* parseNPCFile(string _fileName);
  vector<AbstractEffect*>* parseEffectFile(string _fileName);
  vector<SkillSet*>* parseSkillSetFile(string _fileName);
  map<string,string>* parseParticleFile(string _fileName);
  vector<Ammo*>* parseAmmoFile(string _fileName);
  vector<OgreAL::Sound*>* parseSoundFile(string _fileName);

  // Token parse methods.
  void weaponToken(string _attribute, string _value, Weapon* _weapon);
  void armourToken(string _attribute, string _value, Armour* _armour);
  void itemToken(string _attribute, string _value, Item* _item);
  void craftingToken(string _attribute, string _value, CraftingItem* _item);
  void consumableToken(string _attribute, string _value, ConsumableItem* _item);
  void skillToken(string _attribute, string _value, Skill* _skill);
  void mobToken(string _attribute, string _value, Mob* _mob);
  void bossToken(string _attribute, string _value, Boss* _boss);
  void dropTableToken(string _attribute, string _value, DropTable* _table);
  void dropToken(string _attribute, string _value, Drop* _drop);
  void questToken(string _attribute, string _value, Quest* _quest);
  void killObjectiveToken(string _attribute, string _value, KillObjective* _objective);
  void timedObjectiveToken(string _attribute, string _value, TimedObjective* _objective);
  void collectObjectiveToken(string _attribute, string _value, CollectObjective* _objective);
  void rewardToken(string _attribute, string _value, RewardItem* _item);
  void NPCToken(string _attribute, string _value, NPC* _npc);
  void effectToken(string _attribute, string _value, AbstractEffect* _effect, string _type);
  void skillSetToken(string _attribute, string _value, SkillSet* _skillSet);
  void skillEffectToken(string _attribute, string _value, SkillEffect* _skillEffect);
  void ammoToken(string _attribute, string _value, Ammo* _ammo);

  bool openFile(string _fileName, rapidxml::xml_document<> *_doc);
  Ogre::String getAttrib(rapidxml::xml_node<>* XMLNode, const Ogre::String &parameter, const Ogre::String &defaultValue = "");
  Ogre::Real getAttribReal(rapidxml::xml_node<>* XMLNode, const Ogre::String &parameter, Ogre::Real defaultValue = 0);
  bool getAttribBool(rapidxml::xml_node<>* XMLNode, const Ogre::String &parameter, bool defaultValue = false);
  Ogre::Vector3 parseVector3(rapidxml::xml_node<>* XMLNode);

}; // class XMLParser

#endif