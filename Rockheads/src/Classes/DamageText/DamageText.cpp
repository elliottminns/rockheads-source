#include "stdafx.h"
#include "DamageText.h"
#include <OgreFontManager.h>
#include "../Managers/GameManager/GameManager.h"
 
using namespace Ogre;
 
DamageText::DamageText(int _index)
{
  string damageTextName = "DamageText-"+ Ogre::StringConverter::toString(_index);
  this->overlay = OverlayManager::getSingleton().create(damageTextName);
  this->container = (OverlayContainer*)OverlayManager::getSingleton().createOverlayElement("Panel", damageTextName+"-textContainer");
  this->overlay->add2D(this->container);

  this->textArea = OverlayManager::getSingleton().createOverlayElement("TextArea", damageTextName+"-textArea");
  this->textArea->setDimensions(0.8, 0.8);
  this->textArea->setMetricsMode(GMM_PIXELS);
  this->textArea->setPosition(0.1, 0.1);

  string fontName = "DamageFont";

  FontManager::getSingleton().load(fontName, ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME);
  this->font = (Font*)FontManager::getSingleton().getByName(fontName).getPointer();
  this->textArea->setParameter("font_name", fontName);
  this->textArea->setParameter("char_height", font->getParameter("size"));
  this->textArea->setParameter("horz_align", "left");
  this->textArea->setCaption("");

  this->container->addChild(textArea);
  this->visible = false;
  this->active = false;
  this->overlay->hide();

} // DamageText::DamageText()


DamageText::~DamageText()
{
  OverlayManager *overlayManager = OverlayManager::getSingletonPtr();
  this->container->removeChild(textArea->getName());
  this->overlay->remove2D(container);
  overlayManager->destroyOverlayElement(textArea);
  overlayManager->destroyOverlayElement(container);
  overlayManager->destroy(overlay);
} // DamageText::~DamageText()


void DamageText::createText(AbstractEntity* _entity, const Ogre::String& _text, const Ogre::ColourValue& _color)
{
  // Get the current scenes camera.
  this->camera = GameManager::getInstance()->getCurrentScene()->getCamera();

  this->colour = _color;
  this->textArea->setCaption(_text);
  this->textDim = getTextDimensions(_text);
  this->container->setDimensions(textDim.x, textDim.y);
  this->visible = true;
  this->active = true;
  this->displayCounter = 800;
  this->textArea->setColour(this->colour);

  this->object = _entity->getEntity();

  // Set the starting position of the text.
  // Derive the average point between the top-most corners of the object's bounding box
  const Ogre::AxisAlignedBox &AABB = object->getWorldBoundingBox(true);
  Ogre::Vector3 point = (AABB.getCorner(AxisAlignedBox::FAR_LEFT_TOP)
      + AABB.getCorner(AxisAlignedBox::FAR_RIGHT_TOP)
      + AABB.getCorner(AxisAlignedBox::NEAR_LEFT_TOP)
      + AABB.getCorner(AxisAlignedBox::NEAR_RIGHT_TOP)) / 4;
 
  // Is the camera facing that point? If not, hide the overlay and return.
  Ogre::Plane cameraPlane = Plane(Vector3(camera->getDerivedOrientation().zAxis()), camera->getDerivedPosition());
  if(cameraPlane.getSide(point) != Plane::NEGATIVE_SIDE)
  {
    overlay->hide();
    return;
  } // end if

  // Derive the 2D screen-space coordinates for that point
  point = camera->getProjectionMatrix() * (camera->getViewMatrix() * point);

  // Transform from coordinate space [-1, 1] to [0, 1]
  Real x = (point.x / 2) + 0.5f;
  Real y = 1 - ((point.y / 2) + 0.55f);

  this->textPosition = Ogre::Vector2(x,y);
  this->container->setPosition(this->textPosition.x, this->textPosition.y);

} // void DamageText::createText(AbstractEntity* _entity, const String& _text, const Ogre::ColourValue& _color)
 
void DamageText::setText(const String& _title)
{
  textArea->setCaption(_title);
  this->textDim = getTextDimensions(_title);
  this->container->setDimensions(textDim.x, textDim.y);
} // void DamageText::setText(const String& _title)
 
void DamageText::update(double _timeSinceLastFrame)
{
  if(!object->isInScene() || !this->visible)
  {
    overlay->hide();
    return;
  } // end if
 
  // Transform from coordinate space [-1, 1] to [0, 1]
  Real x = this->textPosition.x;
  Real y = this->textPosition.y -= 0.05*(_timeSinceLastFrame/1000);
 
  // Update the position (centering the text)
  container->setPosition(x - (textDim.x / 2), y);
  overlay->show();

  // Decrement the timer.
  this->displayCounter -= _timeSinceLastFrame;
  if(this->displayCounter <= 150)
  {
    Ogre::ColourValue previous = this->textArea->getColour();
    previous.a -= 0.09;
    this->textArea->setColour(previous);

    if(this->displayCounter <= 0)
    {
      this->visible = false;
      this->active = false;
      this->overlay->hide();
    } // end if
  } // end if
} // void DamageText::update()
 
Vector2 DamageText::getTextDimensions(String text)
{
  Real charHeight = StringConverter::parseReal(font->getParameter("size"));
 
  Vector2 result(0, 0);
 
  for(String::iterator i = text.begin(); i < text.end(); i++)
  {   
    if(*i == 0x0020)
    {
      result.x += font->getGlyphAspectRatio(0x0030);
    } // end if
    else
    {
      result.x += font->getGlyphAspectRatio(*i);
    } // end else
  } // end for
 
  result.x = (result.x * charHeight) / (float)camera->getViewport()->getActualWidth();
  result.y = charHeight / (float)camera->getViewport()->getActualHeight();
 
  return result;
} // Vector2 DamageText::getTextDimensions(String text)


void DamageText::setVisible(bool _visible)
{
  this->visible = _visible;
} // void DamageText::setVisible(bool _visible)


void DamageText::setActive(bool _active)
{
  this->active = _active;
} // void DamageText::setActive(bool _active)


bool DamageText::getVisible()
{
  return this->visible;
} // void DamageText::getVisible()


bool DamageText::getActive()
{
  return this->active;
} // void DamageText::getActive()