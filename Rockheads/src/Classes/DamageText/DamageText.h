#include "../../PrecompiledHeaders/stdafx.h"
#ifndef DAMAGE_TEXT_H
#define DAMAGE_TEXT_H

#include "../GameEntities/AbstractEntity/AbstractEntity.h"

class DamageText
{
private:
  const Ogre::MovableObject* object;
  const Ogre::Camera* camera;
  Ogre::Overlay* overlay;
  Ogre::OverlayElement* textArea;
  Ogre::OverlayContainer* container;
  Ogre::Vector2 textDim;
  Ogre::Vector2 textPosition;
  Ogre::Font* font;
  Ogre::ColourValue colour;
 
  Ogre::Vector2 getTextDimensions(Ogre::String text);

  int displayCounter;
  bool visible;
  bool active;

public:
  DamageText(int _index);
 
  ~DamageText();

  void createText(AbstractEntity* _entity, const Ogre::String& _text,  const Ogre::ColourValue& color = Ogre::ColourValue::White);
 
  void setText(const Ogre::String& title);
  void setVisible(bool _visible);
  void setActive(bool _active);

  bool getVisible();
  bool getActive();
 
  void update(double _timeSinceLastFrame);
}; 

#endif