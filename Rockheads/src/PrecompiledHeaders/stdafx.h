#ifndef STDAFX_H
#define STDAFX_H

#include <OgreCamera.h>
#include <OgreEntity.h>
#include <OgreLogManager.h>
#include <OgreOverlay.h>
#include <OgreOverlayElement.h>
#include <OgreOverlayManager.h>
#include <OgreRoot.h>
#include <OgreViewport.h>
#include <OgreSceneManager.h>
#include <OgreRenderWindow.h>
#include <OgreConfigFile.h>

#include <OISEvents.h>
#include <OISInputManager.h>
#include <OISKeyboard.h>
#include <OISMouse.h>
#include <OgreSubEntity.h>
#include <OgreMaterialManager.h>

#include <OgreALListener.h>
#include <OgreALSoundManager.h>

#include <windows.h>

#include <SdkTrays.h>

#include <iostream>
#include <string>
#include <CEGUI.h>
#include <CEGUI/RendererModules/Ogre/CEGUIOgreRenderer.h>

#include "../Classes/GameScenes/AbstractScene.h"
#include "../Classes/OgreFramework/OgreFramework.h"
#include "../Classes/Managers/GameManager/GameManager.h"

#include "../Classes/Managers/ItemManager/ItemManager.h"
#include "../Classes/GameObjects/InventorySlot/InventorySlot.h"
#include "../Classes/GameObjects/Items/Armour/Armour.h"
#include "../Classes/GameObjects/Items/Weapon/Weapon.h"
#include "../Classes/Managers/CollisionManager/CollisionManager.h"
#include "../Classes/Managers/AudioManager/AudioManager.h"

#include <sstream>

#define _USE_MATH_DEFINES
#include <math.h>
#include <cmath>

#include "boost\algorithm\string.hpp"

#endif